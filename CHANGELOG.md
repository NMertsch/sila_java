# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

### Changed

### Fixed

### Removed

## [0.8.0] - 2022-10-12

### Added
- `BinaryUploadParameterWhitelistInterceptor`
- `BinaryDatabaseInjector`: Inject `BinaryDatabase` instance into gRPC Context
- `BinaryDatabase.current`: Retrieve database from gRPC Context
- `BinaryTransferTest`
- `dnsjava`: Library to replace previous home-made DNS implementation.

### Changed
- `MetadataInterceptor`: Validate Binary affected Parameter Identifier and call handler delegation
- `ListNetworkInterfaces`: Migrated under `sila_java.library.core.utils`
- `BinaryTransferErrorHandler`: Migrated under `sila_java.library.core.sila.binary_transfer`
- `ByteArrayProcessedInputStream`: Migrated under `sila_java.examples.test_server.utils`
- `sila_java.library.core.communication.*`: Migrated under `sila_java.library.communication.*`
- Utils
  - `blockUntilShutdown`: Allow to block current thread while underlying gRPC Server is running
  - `blockUntilStop`: Deprecated over Utils.blockUntilShutdown and Server.awaitTermination
- SiLAServer 
  - `withPersistedTLS`: Renamed to `withPersistentTLS`
  - `withoutDiscovery`: Disable network discovery
  - `withBinaryDatabaseInjector`: Add BinaryDatabase instance in gRPC Context

### Fixed
- `BinaryUpload`: raise proper BinaryTransferError
- `H2BinaryDatabase`: close method can be called twice without raising an exception

### Removed
- `sila_java.library.core.discovery.networking.dns.*`: Replaced by `dnsjava` library

## [0.7.0] - 2022-07-09

### Added
- Server CLI arguments
  - `-d` / `--discovery`: Enable or disable network discovery.
- SiLAServer
  - `getBinaryDatabase`:
- SiLAServer.Builder
  - `ConfigWrapperProvider`: Interface to provide a custom config wrapper
  - `BinaryDatabaseProvider`: Interface to provide a custom binary database
  - `getCustomServerConfigProvider`
  - `getCustomBinaryDatabaseProvider`
  - `withDiscovery`: Enable or disable network discovery
  - `withNetworkInterface`: Specify a network interface to provide network discovery on
  - `withHost`: Specify a network to provide network discovery o,
  - `withName`: Override server name
  - `withUUID`: Override server UUID
  - `withDefaultServerConfiguration`: Provide a default server configuration
  - `withBinaryTransferSupport`: Enable or disable binary transfer support
  - `withCustomBinaryDatabaseProvider`: Provide a custom binary database
  - `withCustomConfigWrapperProvider`: Provide a custom config wrapper
  - `withPersistentConfig`: Enable or disable persistent config
  - `withPersistentConfigFile`: File to persist the config
  - `getNewServerConfigurationWrapper`: Get a new server configuration wrapper instance
- `H2BinaryDatabase` test

### Changed
- `ServerConfiguration` API (breaking)
- `SiLAServerRegistration` API (breaking): support interface and host based network discovery
  - `SiLAServerRegistration` Constructor replaced by newInstanceFromInterface & newInstanceFromHost
- `EncryptionUtils`: Add malformed private key check
- `SiLAServer.Builder`: 
  - `withoutConfig` & `withConfig` replaced by `newBuilder` & `withPersistentConfig`
  - `withBinaryTransferSupport` now takes a boolean and does not return a database

### Fixed

- `SiLA Service` -> `Get Feature Definition` raise correct DefinedExecutionError on unimplemented features
- `ServerInformation` & `ServerConfiguration`: Reject values which would violate SiLAService constraints
- Self Signed Certificate generation
- `UnobservableCommand` & `UploadService` stream Memory leak
- `H2BinaryDatabase` binary size (used to be utf-8 length instead of octet length)

### Removed
- `BinaryDatabase.reserveBytes`

## [0.6.0] - 2022-08-08

### Added

- Server CLI arguments
    - `-u` / `--unsafeCommunication`: allow unsafe (plain-text) communications
    - `-k` / `--privateKey`: file path to private key
    - `-cr` / `--certificate`: file path to certificate
    - `-crp` / `--certificatePassword`: certificate password
- Javadoc for most of the code base
- New metadata API
  - `ServerMetadataContainer`
  - `MetadataInterceptor`
  - `MetadataExtractingInterceptor`
  - `MetadataUtils`
  - `FullyQualifiedIdentifierUtils`
  - `ServerMetadataContainer`
- Add `SiLAServer.Builer` API:
    - `withPersistedTLS`
    - `addInterceptor`
    - `withMetadataExtractingInterceptor`
    - `withPersistedTLS`
- Add `ServerManager` API:
    - `allowUnsecureConnection`: setter & getter
    - `rootCertificate `: setter & getter
- Add `EncryptionUtils` API:
    - `readPrivateKey`
    - `readCertificate` (from File/String)
    - `writeCertificateToFile`
    - `writePrivateKeyToFile`
    - `writePEMObjToFile`
    - `writeCertificateToString`
- Add metadata server and test 

### Changed

- Update most dependencies with vulnerabilities [Lift SonaType Report](https://sbom.lift.sonatype.com/report/T1-a0368c8f29fdaa555824-8712395c006910-1659841342-40509c7f189044f48a428942c1557946)
- Replace deprecated AdoptOpenJdk by Adoptium (Eclispe Temurin™)
- Replace UniteLabs reference by SiLA
- Unsecure (plain-text) communication are deprecated and not enabled by default
- `SiLAServer` now automatically load or create certificate and private key 
- `ServerLoading.attemptConnectionWithServer` now takes a certificate and boolean option to accept unsecure connection
- `ServerManager` does not blindly allow non-standard untrusted certificates
- `ChannelFactory`:
  - Add `getTLSEncryptedChannel` (with and without certificate)
  - Remove `withClientInterceptor`
- `ChannelBuilder` :
  - Add `withTLSEncryption` (with and without certificate)
  - Add `withoutEncryption`
  - Remove `builderWithoutEncryption`
  - Remove `builderWithEncryption`
- Fix `SelfSignedCertificate` to generate SiLA standard compliant self signed certificate
- `SiLAServerRegistration` supports certificate and send certificate in the mdns answers
- `SiLADiscovery` supports certificate provided in the mdns answers
- `ServerListener.serverAdded` optionally provided a certificate

### Fixed

- Fix maximum mdns response size with custom forked jmdns library
- Fix not working unsecure communication (`SiLAServer`, `ServerLoading`)
- Fix transitive dependencies issues (Dependency Hell) when using the library

### Removed

- Removed `FullQualifierUtils` replaced by `FullyQualifiedIdentifierUtils`
- Removed `SSLContextFactory`
- Removed `ChannelUtils`
  
## [0.5.0] - 2022-03-22

## [Template]

### Added

- What has been Added

### Changed

- What has been changed

### Fixed

- What has been fixed

### Removed

- What has been removed

[unreleased]: https://gitlab.com/SiLA2/sila_java/-/compare/v0.8.0...master?from_project_id=4205706
[0.8.0]: https://gitlab.com/SiLA2/sila_java/-/compare/v0.7.0...v0.8.0?from_project_id=4205706
[0.7.0]: https://gitlab.com/SiLA2/sila_java/-/compare/v0.6.0...v0.7.0?from_project_id=4205706
[0.6.0]: https://gitlab.com/SiLA2/sila_java/-/compare/v0.5.0...v0.6.0?from_project_id=4205706
[0.5.0]: https://gitlab.com/SiLA2/sila_java/-/compare/v0.1.0...v0.5.0?from_project_id=4205706
