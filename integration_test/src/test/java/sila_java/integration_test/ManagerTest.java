package sila_java.integration_test;

import junit.framework.Assert;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import sila_java.integration_test.utils.ServerInfo;
import sila_java.library.manager.ServerAdditionException;
import sila_java.library.manager.ServerFinder;
import sila_java.library.manager.ServerManager;
import sila_java.library.manager.models.Server;
import sila_java.library.server_base.config.ServerConfiguration;
import sila_java.library.server_base.identification.ServerInformation;
import sila_java.library.server_base.utils.ServerProcessBuilder;
import sila_java.examples.test_server.TestServer;
import sila_java.integration_test.utils.TestServerHelper;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static sila_java.integration_test.utils.ScanUtils.scanAndWaitServerState;
import static sila_java.integration_test.utils.ScanUtils.waitServerState;

/**
 * Class responsible for doing integration test with the Manager and with SiLA servers
 */
@Slf4j
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ManagerTest {
    private static final int TIMEOUT = 60; // [s]
    private static final String HOST = "127.0.0.1";
    private static final String NETWORK_INTERFACE = "local";

    private static final String HELLO_SILA_SERVER_JAR = "../examples/hello_sila/target/hello_sila-exec.jar";
    private static final ServerInfo SERVER_1 = new ServerInfo(Paths.get("./tmp_server1.cfg"), 50058);
    private static final ServerInfo SERVER_2 = new ServerInfo(Paths.get("./tmp_server2.cfg"), 50059);
    private static final ServerInfo SERVER_3 = new ServerInfo(Paths.get("./tmp_server3.cfg"), 50060);

    private static final List<Process> PROCESSES = new ArrayList<>();
    private static final List<ServerInfo> SERVER_INFORMATION = Arrays.asList(
            SERVER_1,
            SERVER_2,
            SERVER_3
    );

    @AfterAll
    static void cleanup() {
        ServerManager.getInstance().close();
    }

    @BeforeEach
    void checkManager() {
        // No server online
        Assert.assertTrue(ServerManager.getInstance().getServers().isEmpty());
    }

    @AfterEach
    void cleanManager() {
        ServerManager.getInstance().clear();

        if (!PROCESSES.isEmpty()) {
            PROCESSES.forEach(Process::destroyForcibly);
            PROCESSES.clear();
        }

        SERVER_INFORMATION.forEach(
                ServerInfo::clear
        );
    }

    @Test
    void testManagerDiscovery() throws IOException, InterruptedException, TimeoutException, ExecutionException {
        // Start server 1 in parallel
        log.info("Starting first Hello SiLA Server");
        final CompletableFuture<Process> server1Future =
                CompletableFuture.supplyAsync(()->startHelloSiLAServerWithDiscovery(SERVER_1));

        // Two Servers online
        log.info("Starting second Hello SiLA Server and discover it");
        {
            final Process server2 = startHelloSiLAServerWithDiscovery(SERVER_2);
            sendStopMessageToServer(server2);
        }
        // One server online, one offline
        final UUID server2UUID = SERVER_2.getUUID(TIMEOUT);
        waitServerState(server2UUID, Server.Status.OFFLINE, TIMEOUT);

        final ServerFinder server2Finder = ServerFinder.filterBy(ServerFinder.Filter.uuid(server2UUID));
        final Server managerServer2 = server2Finder.findOne().orElseThrow(AssertionError::new);
        log.info("Second Hello SiLA Server discovered and offline detected");

        // make Sure Server 1 is up and detected online
        final Process server1 = server1Future.get();
        Assert.assertEquals(
                1,
                ServerFinder.filterBy(ServerFinder.Filter.status(Server.Status.ONLINE)).find().size()
        );

        // Offline server up again
        {
            final Process server2 = startHelloSiLAServerWithDiscovery(SERVER_2);
            waitServerState(server2UUID, Server.Status.ONLINE, TIMEOUT);
            // Check if the UUID is still the same and "old server" is online
            server2Finder.findOne().ifPresent(
                    server -> Assert.assertEquals(
                            managerServer2.getConfiguration().getUuid(),
                            server.getConfiguration().getUuid()
                    )
            );

            log.info("Second Hello SiLA Server up again with same UUID");
            sendStopMessageToServer(server2);
        }
        waitServerState(managerServer2.getConfiguration().getUuid(), Server.Status.OFFLINE, TIMEOUT);
        Assert.assertEquals(Server.Status.OFFLINE, managerServer2.getStatus());

        // Two servers online, one offline (same port)
        try (final TestServer server3 = TestServerHelper.build(
                SERVER_2.getPort(),
                SERVER_3.getConfigFile().getPath(),
                NETWORK_INTERFACE
        )) {
            final UUID server3UUID = SERVER_3.getUUID(TIMEOUT);
            scanAndWaitServerState(server3UUID, Server.Status.ONLINE, TIMEOUT);

            // Test server online, not equal and stop old server
            Assertions.assertNotEquals(server2UUID, server3UUID);
            sendStopMessageToServer(server1);
            waitServerState(SERVER_1.getUUID(TIMEOUT), Server.Status.OFFLINE, TIMEOUT);
        }
    }

    @SneakyThrows
    private Process startHelloSiLAServerWithDiscovery(ServerInfo serverInfo) {
        final Process serverProcess = startHelloSiLAServer(serverInfo, NETWORK_INTERFACE);

        // Scan and wait until online
        final UUID serverUUID = serverInfo.getUUID(TIMEOUT);
        scanAndWaitServerState(serverUUID, Server.Status.ONLINE, TIMEOUT);

        return serverProcess;
    }

    @Test
    void testManagerInitialisation() throws IOException, InterruptedException, TimeoutException, ExecutionException {
        log.info("Setting up 2 Servers");
        // Setup server 1
        final Process server1 = startHelloSiLAServer(SERVER_1);
        final Server serverModel1 = createTestServer(SERVER_1, "Test server 1");

        // Setup server 2
        // Start and stop the server just so it creates its configuration file
        {
            final Process server2 = startHelloSiLAServer(SERVER_2);
            sendStopMessageToServer(server2);
        }
        final Server serverModel2 = createTestServer(SERVER_2, "Test server 2");
        log.info("Servers Setup, Initialising");

        //Initialize manager
        final UUID server1UUID = serverModel1.getConfiguration().getUuid();
        final UUID server2UUID = serverModel2.getConfiguration().getUuid();

        final Map<UUID, Server> servers = new HashMap<>();
        servers.put(server1UUID, serverModel1);
        servers.put(server2UUID, serverModel2);
        ServerManager.getInstance().initialize(servers);

        log.info("Servers initialised, checking server states");
        waitServerState(server1UUID, Server.Status.ONLINE, TIMEOUT);
        final Server managerServer2 = ServerFinder
                .filterBy(ServerFinder.Filter.uuid(server2UUID))
                .findOne()
                .orElseThrow(AssertionError::new);
        Assert.assertEquals(Server.Status.OFFLINE, managerServer2.getStatus());

        log.info("Server states confirmed, starting server 2 and confirming online state");
        // Start server 2
        final Process server2 = startHelloSiLAServer(SERVER_2);
        waitServerState(server2UUID, Server.Status.ONLINE, TIMEOUT);

        log.info("Stoping servers and confirming states");
        sendStopMessageToServer(server1);
        sendStopMessageToServer(server2);
        waitServerState(server1UUID, Server.Status.OFFLINE, TIMEOUT);
        waitServerState(server2UUID, Server.Status.OFFLINE, TIMEOUT);
    }

    private static Server createTestServer(ServerInfo serverInfo, String serverName) throws IOException {
        final Server server = new Server();
        server.setNegotiationType(Server.NegotiationType.TLS);
        server.setStatus(Server.Status.OFFLINE);
        server.setHost(HOST);
        server.setPort(serverInfo.getPort());
        server.setJoined(new Date());
        server.setConfiguration(new ServerConfiguration(serverName, serverInfo.getUUID(TIMEOUT)));
        server.setInformation(new ServerInformation("Test", "Test", "https://test.com", "0.1"));

        return server;
    }

    @Test
    void testServerOnDifferentPorts() throws IOException, InterruptedException, TimeoutException, ExecutionException, ServerAdditionException {
        // Start and stop Server 1
        final Process server1 = startHelloSiLAServerWithDiscovery(SERVER_1);
        final UUID uuid = SERVER_1.getUUID(TIMEOUT);
        waitServerState(uuid, Server.Status.ONLINE, TIMEOUT);
        sendStopMessageToServer(server1);
        waitServerState(uuid, Server.Status.OFFLINE, TIMEOUT);

        // Restart Server 1 on different port and check if uuid found
        final Process server1OnDifferentPort = ServerProcessBuilder.build(
                HELLO_SILA_SERVER_JAR,
                SERVER_1.getPort() + 2,
                SERVER_1.getConfigFile().toPath(),
                "local"
        );
        PROCESSES.add(server1OnDifferentPort);

        scanAndWaitServerState(uuid, Server.Status.ONLINE, TIMEOUT);
        sendStopMessageToServer(server1OnDifferentPort);
        waitServerState(uuid, Server.Status.OFFLINE, TIMEOUT);
    }

    private Process startHelloSiLAServer(ServerInfo serverInfo) {
        return startHelloSiLAServer(serverInfo, null);
    }

    @SneakyThrows
    private Process startHelloSiLAServer(ServerInfo serverInfo, String networkInterface) {
        final Process serverProcess = ServerProcessBuilder.build(
                HELLO_SILA_SERVER_JAR,
                serverInfo.getPort(),
                serverInfo.getConfigFile().toPath(),
                networkInterface
        );
        PROCESSES.add(serverProcess);
        return serverProcess;
    }

    private static void sendStopMessageToServer(@NonNull final Process serverProcess) throws IOException, InterruptedException {
        try (final BufferedWriter writer = new BufferedWriter(
                new OutputStreamWriter(serverProcess.getOutputStream()))
        ) {
            writer.write("stop");
            writer.newLine();
            writer.flush();
        }

        Assertions.assertTrue(serverProcess.waitFor(TIMEOUT, TimeUnit.SECONDS));
    }
}
