package sila_java.integration_test;

import io.grpc.ServerServiceDefinition;
import junit.framework.Assert;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import sila_java.library.manager.ServerAdditionException;
import sila_java.library.manager.ServerFinder;
import sila_java.library.manager.ServerListener;
import sila_java.library.manager.ServerManager;
import sila_java.library.manager.models.Server;
import sila_java.library.server_base.SiLAServer;
import sila_java.library.server_base.identification.ServerInformation;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;
import java.util.concurrent.*;

import static sila_java.library.core.utils.FileUtils.getResourceContent;

/**
 * Class responsible for doing integration test with the Manager Server events
 */
@Slf4j
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ManagerServerEventsTest {
    private static final String SERVER_HOST = "127.0.0.1";
    private static final int SERVER_PORT_1 = 44444;
    private static final int SERVER_PORT_2 = 55555;
    private static final int WAIT_TIMEOUT_SEC = 20;
    private static final Path SERVER_CFG_PATH = Paths.get("./test.cfg");
    private static final String SERVER_FEATURE_ID = "Test";
    private static final String SERVER_FEATURE_NAME = "TestFeature.sila.xml";

    static final class FutureServerEvents implements ServerListener {
        private CompletableFuture<Event> onAdd = new CompletableFuture<>();
        private CompletableFuture<Event> onRemove = new CompletableFuture<>();
        private CompletableFuture<Event> onChange = new CompletableFuture<>();
        private CompletableFuture<Fail> onFail = new CompletableFuture<>();

        @AllArgsConstructor
        static final class Event {
            UUID uuid;
            Server server;
        }

        @AllArgsConstructor
        static final class Fail {
            String host;
            int port;
            String reason;
        }

        void reset() {
            onAdd = new CompletableFuture<>();
            onRemove = new CompletableFuture<>();
            onChange = new CompletableFuture<>();
            onFail = new CompletableFuture<>();
        }

        @Override
        public void onServerAdded(UUID uuid, Server server) {
            onAdd.complete(new Event(uuid, server));
        }

        @Override
        public void onServerRemoved(UUID uuid, Server server) {
            onRemove.complete(new Event(uuid, server));
        }

        @Override
        public void onServerChange(UUID uuid, Server server) {
            onChange.complete(new Event(uuid, server));
        }

        @Override
        public void onServerAdditionFail(String host, int port, String reason) {
            onFail.complete(new Fail(host, port, reason));
        }

        Event getOnAddOrThrow() throws InterruptedException, ExecutionException, TimeoutException {
            return onAdd.get(WAIT_TIMEOUT_SEC, TimeUnit.SECONDS);
        }

        Event getOnRemoveOrThrow() throws InterruptedException, ExecutionException, TimeoutException {
            return onRemove.get(WAIT_TIMEOUT_SEC, TimeUnit.SECONDS);
        }

        Event getOnChangeOrThrow() throws InterruptedException, ExecutionException, TimeoutException {
            return onChange.get(WAIT_TIMEOUT_SEC, TimeUnit.SECONDS);
        }

        Fail getOnFailOrThrow() throws InterruptedException, ExecutionException, TimeoutException {
            return onFail.get(WAIT_TIMEOUT_SEC, TimeUnit.SECONDS);
        }
    }

    @AfterAll
    void endCleanup() {
        SERVER_CFG_PATH.toFile().deleteOnExit();
    }

    @AfterEach
    void resetManager() {
        ServerManager.getInstance().close();
    }

    @Test
    void serverAdditionFail() throws ExecutionException, InterruptedException, TimeoutException {
        final FutureServerEvents serverListener = new FutureServerEvents();
        ServerManager.getInstance().addServerListener(serverListener);

        try {
            ServerManager.getInstance().addServer(SERVER_HOST, SERVER_PORT_1);
        } catch (ServerAdditionException ignored) { }

        final FutureServerEvents.Fail failedServerAdd = serverListener.getOnFailOrThrow();

        Assert.assertEquals(SERVER_HOST, failedServerAdd.host);
        Assert.assertEquals(SERVER_PORT_1, failedServerAdd.port);
    }

    @Test
    void serverAddChangeDelete()
            throws ExecutionException, InterruptedException, IOException, ServerAdditionException, TimeoutException {
        final FutureServerEvents serverListener = new FutureServerEvents();
        ServerManager.getInstance().addServerListener(serverListener);

        final SiLAServer.Builder serverBuilder1 = SiLAServer.Builder.withConfig(
                SERVER_CFG_PATH,
                new ServerInformation("TestServer", "Test server", "https://gitlab.com/SiLA2/sila_java", "0.1")
        ).withPort(SERVER_PORT_1);

        // server added -> online, changed offline
        try (final SiLAServer ignored = serverBuilder1.start()) {
            ServerManager.getInstance().addServer(SERVER_HOST, SERVER_PORT_1);
            assertHostPortStatusMatchServer(SERVER_PORT_1, serverListener.getOnAddOrThrow(), Server.Status.ONLINE);
            serverListener.reset();
        }

        final FutureServerEvents.Event offlineEvent = serverListener.getOnChangeOrThrow();
        assertHostPortStatusMatchServer(SERVER_PORT_1, offlineEvent, Server.Status.OFFLINE);
        serverListener.reset();

        // server changed -> online
        try (final SiLAServer ignored = serverBuilder1.start()) {
            final FutureServerEvents.Event onlineEvent = serverListener.getOnChangeOrThrow();
            assertHostPortStatusMatchServer(SERVER_PORT_1, onlineEvent, Server.Status.ONLINE);
            assertSameServer(onlineEvent, offlineEvent);
            serverListener.reset();
        }

        // server changed -> offline
        assertHostPortStatusMatchServer(SERVER_PORT_1, serverListener.getOnChangeOrThrow(), Server.Status.OFFLINE);
        serverListener.reset();

        // server changed (different port and feature) -> online
        testServerWithDifferentPortAndFeature(serverListener, offlineEvent);

        // remove server listening
        ServerManager.getInstance().removeServer(offlineEvent.uuid);
        final FutureServerEvents.Event onRemove = serverListener.getOnRemoveOrThrow();
        Assert.assertEquals(SERVER_HOST, onRemove.server.getHost());
        Assert.assertEquals(SERVER_PORT_2, (int)onRemove.server.getPort());
        Assert.assertFalse(ServerFinder.filterBy(ServerFinder.Filter.uuid(onRemove.uuid)).findOne().isPresent());
    }

    private static void testServerWithDifferentPortAndFeature(
            @NonNull final FutureServerEvents serverListener,
            @NonNull FutureServerEvents.Event otherEvent
    ) throws IOException, ServerAdditionException, InterruptedException, ExecutionException, TimeoutException {
        final SiLAServer.Builder serverBuilder = SiLAServer.Builder.withConfig(
                SERVER_CFG_PATH,
                new ServerInformation("TestServer", "Test server", "https://gitlab.com/SiLA2/sila_java", "0.1")
        ).addFeature(
                getResourceContent(SERVER_FEATURE_NAME), () -> ServerServiceDefinition.builder("someService").build()
        ).withPort(SERVER_PORT_2);

        try (SiLAServer ignored = serverBuilder.start()) {
            ServerManager.getInstance().addServer(SERVER_HOST, SERVER_PORT_2);

            // server changed to online with different port
            final FutureServerEvents.Event serverChangeEvent = serverListener.getOnChangeOrThrow();
            Assert.assertFalse(serverListener.onAdd.isDone());
            assertHostPortStatusMatchServer(SERVER_PORT_2, serverChangeEvent, Server.Status.ONLINE);

            // one feature with the corresponding feature ID was detected
            Assert.assertEquals(1, serverChangeEvent.server.getFeatures().size());
            Assert.assertEquals(SERVER_FEATURE_ID, serverChangeEvent.server.getFeatures().get(0).getIdentifier());

            // check that same server as in other event was found
            assertSameServer(serverChangeEvent, otherEvent);
            serverListener.reset();
        }
    }

    private static void assertSameServer(
            @NonNull FutureServerEvents.Event event,
            @NonNull FutureServerEvents.Event otherEvent
    ) {
        Assert.assertEquals(event.uuid, otherEvent.uuid);
        Assert.assertEquals(event.server.getConfiguration().getName(), otherEvent.server.getConfiguration().getName());
        Assert.assertEquals(event.server.getInformation().getType(), otherEvent.server.getInformation().getType());
    }

    private static void assertHostPortStatusMatchServer(int port, FutureServerEvents.Event event, Server.Status status) {
        Assert.assertEquals(SERVER_HOST, event.server.getHost());
        Assert.assertEquals(port, (int) event.server.getPort());
        Assert.assertEquals(status, event.server.getStatus());
    }
}
