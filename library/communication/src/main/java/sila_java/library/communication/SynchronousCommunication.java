package sila_java.library.communication;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import sila_java.library.communication.socket.CommunicationSocket;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.*;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.*;
import java.util.function.Supplier;


/**
 * Serial Communication Class
 *
 * Keeps connection alive and synchronises send & receive calls.
 */
@Slf4j
public class SynchronousCommunication implements AutoCloseable {
    private final static int INPUT_INTERVAL = 100; // [ms] Interval that input stream is handled
    // @implNote Use StringBuffer as its threadsafe!
    private final StringBuffer inputBuffer = new StringBuffer();
    // Thread pool for async read calls
    private final ExecutorService executor = Executors.newCachedThreadPool();

    // Stream Handling
    private OutputStream out = null;
    private InputStream in = null;
    private Thread inputThread = null;

    // Connection Handling
    private boolean isUp = false;

    private final CommunicationSocket communicationSocket;
    private final String sendDelimiter;
    private final String receiveDelimiter;
    private final WelcomeFlusherFunction startupFunction;
    private final CheckResultInterface errorChecker;

    /**
     * Check result interface
     */
    public interface CheckResultInterface {
        /**
         * Check the Result for Error and throw and Error if detected.
         *
         * @param result Raw Result including line-breaks etc.
         */
        void checkResult(final String result) throws IOException;
    }

    /**
     * Private Constructor
     */
    private SynchronousCommunication(
            @NonNull CommunicationSocket communicationSocket,
            @NonNull String sendDelimiter,
            @NonNull String receiveDelimiter,
            @Nullable WelcomeFlusherFunction startupFunction,
            @Nullable CheckResultInterface errorChecker
    ) {
        this.communicationSocket = communicationSocket;
        this.sendDelimiter = sendDelimiter;
        this.receiveDelimiter = receiveDelimiter;
        this.startupFunction = startupFunction;
        this.errorChecker = errorChecker;
    }

    /**
     * Builder
     */
    public static class Builder {
        private final CommunicationSocket communicationSocket;
        private String sendDelimiter = "\r\n";
        private String receiveDelimiter = "\r\n";
        private WelcomeFlusherFunction startupFunction = null;
        private CheckResultInterface errorChecker = null;

        /**
         * Builder
         * @param communicationSocket Communication Socket that will be handled by this class
         */
        public Builder(final @NonNull CommunicationSocket communicationSocket) {
            this.communicationSocket = communicationSocket;
        }

        /**
         * @param sendDelimiter Delimiter used to mark end of sent messages
         */
        public Builder withSendDelimiter(final @NonNull String sendDelimiter) {
            this.sendDelimiter = sendDelimiter;
            return this;
        }

        /**
         * @param receiveDelimiter Delimiter used to mark end of received messages
         */
        public Builder withReceiveDelimiter(final @NonNull String receiveDelimiter) {
            this.receiveDelimiter = receiveDelimiter;
            return this;
        }

        /**
         * Some connections send startup messages that need to be flushed, so this will assign a runnable
         * to the startupFunction.
         *
         * @param startupFunction Start function that is triggered on every reconnection
         */
        public Builder withStartupMessageFlusher(final @NonNull WelcomeFlusherFunction startupFunction) {
            this.startupFunction = startupFunction;
            return this;
        }

        /**
         * Errors caught on the message layer, be sure to catch the whole error message to
         * empty the input buffers.
         *
         * Recommendation: Only use this if errors lead to different receive delimiters than in
         * normal operation, otherwise its safer to check for errors after reading has executed.
         *
         * @param errorCheckerFunction Function that throws IOException or derivative on detected error.
         */
        public Builder withErrorChecker(final CheckResultInterface errorCheckerFunction) {
            this.errorChecker = errorCheckerFunction;
            return this;
        }

        /**
         * Build {@link SynchronousCommunication}
         * @return a new {@link SynchronousCommunication} instance
         */
        public SynchronousCommunication build() {
            return new SynchronousCommunication(
                    this.communicationSocket,
                    this.sendDelimiter,
                    this.receiveDelimiter,
                    this.startupFunction,
                    this.errorChecker
            );
        }
    }

    /**
     * Heartbeat to re-establish connection
     */
    private class HeartBeatAgent implements Runnable {
        private final Thread heartBeatThread;
        private final int samplingTime;

        private final Supplier<Boolean> connectionTester;

        /**
         * Constructor
         * @param samplingTime the sampling time
         * @param connectionTester the connection tester
         */
        HeartBeatAgent(int samplingTime, @Nonnull Supplier<Boolean> connectionTester) {
            this.heartBeatThread = new Thread(this, this.getClass().getName() + "_Thread");
            this.samplingTime = samplingTime;
            this.connectionTester = connectionTester;
        }

        /**
         * Start heart beat
         */
        void start() {
            this.heartBeatThread.start();
        }

        /**
         * Stop heart beat
         */
        void stop() {
            this.heartBeatThread.interrupt();
        }

        /**
         * Check communication connection state
         * @inheritDoc
         */
        @Override
        public void run() {
            // otherwise open() will get executed even thou we already decided to interrupt!
            while (true) {
                try {
                    if (!isUp()) {
                        try {
                            open();
                        } catch (IOException e) {
                            log.info("Serial Comm IO not possible: " + e.getMessage());
                            closeStreams();
                        }
                    } else {
                        if (!connectionTester.get()) {
                            log.info("Connection got lost");
                            closeStreams();
                        }
                    }
                    Thread.sleep(this.samplingTime);
                } catch (InterruptedException | IOException e) {
                    log.info("Driver heart beat interrupted.");
                    try {
                        closeStreams();
                    } catch (IOException e1) {
                        log.error("Error trying to closeStreams communication during Interrupt: {}", e1.getMessage());
                    }
                    Thread.currentThread().interrupt();
                    break;
                }

            }
        }
    }

    private HeartBeatAgent heartBeatAgent = null;

    /**
     * Start Heartbeat that probes the connection and reestablishes it
     *
     * @param samplingTime Minimum time interval to check if connection is up
     * @param connectionTester Boolean check that indicates if connection nis healthy
     */
    public void startHeartbeat(int samplingTime, @Nonnull Supplier<Boolean> connectionTester) {
        log.info("Starting HeartbeatAgent...");

        if (heartBeatAgent != null) {
            heartBeatAgent.stop();
            heartBeatAgent = null;
        }

        heartBeatAgent = new HeartBeatAgent(samplingTime, connectionTester);
        heartBeatAgent.start();
    }

    /**
     * {@inheritDoc}
     *
     * @implNote Stops heartbeat by means of an interruption signal. Upon Interrupted Exception, the heartbeat
     * will then clean this object - No need to closeStreams everywhere else
     */
    @Override
    public void close() {
        log.info("Shutting down synchronousCommunication");
        if (heartBeatAgent != null) {
            log.info("Stopping Heartbeat.");
            this.heartBeatAgent.stop();
        }
        try {
            this.closeStreams();
        } catch (IOException e) {
            log.warn("Wasn't able to close Synchronous Communication IO Streams: {}", e.getMessage());
        }
        if (!this.executor.isShutdown()) {
            this.executor.shutdownNow();
        }
    }

    /**
     * Open the Serial Port
     *
     * @implNote Currently using fixed Settings, change API when necessary
     */
    public synchronized void open() throws IOException {
        // If the serial port is up, simply return
        if (isUp) return;

        log.info("Open/Re-open connection");
        communicationSocket.open();

        // Continuous output flushing (only ends with interrupting)
        out = new DataOutputStream(communicationSocket.getOutputStream());
        in = new DataInputStream(communicationSocket.getInputStream());

        // Continuous appending to input buffer, cleared from synchronized functions
        inputThread = new Thread(() -> {
            try {
                while (true) {
                    if (in.available() > 0) {
                        byte[] newData = new byte[in.available()];
                        final int numRead = in.read(newData, 0, newData.length);

                        if (newData.length != numRead) {
                            throw new IOException("Buffer error in stream reading");
                        }

                        final String character = new String(newData, 0, newData.length);
                        this.inputBuffer.append(character);
                    } else {
                        Thread.sleep(INPUT_INTERVAL);
                    }
                }
            } catch (IOException | InterruptedException e) {
                log.info(e.getMessage());
            }
        });
        inputThread.start();

        // Consume startup message
        if (this.startupFunction != null) {
            this.startupFunction.apply(this);
        }

        isUp = true;
    }

    /**
     * Close the Serial Port
     */
    private synchronized void closeStreams() throws IOException {
        log.warn("Closing connection");

        if (out != null) {
            out.close();
        }

        if (inputThread != null) {
            try {
                inputThread.interrupt();
                inputThread.join();
                inputThread = null;
            } catch (InterruptedException e) {
                throw new IOException(e.getMessage());
            }
        }

        if (in != null) {
            in.close();
        }

        communicationSocket.close();

        isUp = false;
    }

    /**
     * Is communication up
     * @return true if the communication is up
     */
    public boolean isUp() {
        return this.isUp;
    }

    /**
     * Send and Receive Messages when a single line response is expected
     *
     * @param msg Message to serialse and send
     * @param timeout timeout Timeout in [ms]
     * @return Single Line, otherwise Throws Exception
     */
    public synchronized String sendReceiveSingleLine(String msg, long timeout) throws IOException {
        final List<String> response = sendReceive(msg, timeout);
        if (response.size() != 1) {
            throw new IllegalStateException("Only one response allowed in sendReceiveSingleLine");
        } else {
            return response.get(0);
        }
    }

    /**
     * Send and Receive Messages over Serial Port synchronously
     *
     * @param msg Message to serialise and send
     * @param timeout Timeout in [ms]
     *
     * @return The answer from the serial port split in line breaks
     */
    public synchronized List<String> sendReceive(String msg, long timeout) throws IOException {
        // Always clean input buffer before sending
        this.inputBuffer.setLength(0);

        final String finalMsg = msg + this.sendDelimiter;
        this.out.write(finalMsg.getBytes());
        this.out.flush();
        return read(timeout);
    }

    /**
     * Read with timeout
     * @param timeout the timeout
     * @return read result
     * @throws IOException
     */
    public synchronized List<String> read(long timeout) throws IOException {
        return this.read(timeout, this.receiveDelimiter);
    }

    /**
     * Directly read from the serial port in case of asynchronous inputs
     *
     * @param timeout Timeout in [ms]
     *
     * @return The answer from the serial port split in line breaks
     */
    public synchronized List<String> read(final long timeout, @NonNull final String receiveDelimiter) throws IOException {
        // parallel read task with timeout
        final Callable<String> readTask = () -> {
            while (true) {
                final String message = this.inputBuffer.toString();

                if (this.errorChecker != null) errorChecker.checkResult(message);

                if (message.contains(receiveDelimiter)) {
                    log.debug("Message: {}", message);

                    // Clear inputBuffer until receiveDelimiter
                    final int mark = this.inputBuffer.indexOf(receiveDelimiter);
                    this.inputBuffer.delete(0, mark + receiveDelimiter.length());

                    return message;
                }

                Thread.sleep(INPUT_INTERVAL);
            }
        };
        final Future<String> future = this.executor.submit(readTask);

        final String result;
        try {
            result = future.get(timeout, TimeUnit.MILLISECONDS);
        } catch (TimeoutException | InterruptedException ex) {
            this.inputBuffer.setLength(0);
            throw new IOException("Reading from serial buffer failed: " + ex.getClass().getName());
        } catch (ExecutionException e) {
            this.inputBuffer.setLength(0);
            throw new IOException("Executing serial command failed: " + e.getMessage());
        } finally {
            future.cancel(true);
        }

        return new LinkedList<>(Arrays.asList(result.split("\\R+")));
    }
}

