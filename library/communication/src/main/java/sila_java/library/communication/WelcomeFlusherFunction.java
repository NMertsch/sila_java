package sila_java.library.communication;

import java.io.IOException;

/**
 * Welcome flusher function
 * @param <E>
 */
@FunctionalInterface
public interface WelcomeFlusherFunction<E extends IOException> {
    /**
     * Callback on startup
     * @param communication communication
     * @throws E exception
     */
    void apply(SynchronousCommunication communication) throws E;
}
