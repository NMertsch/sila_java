package sila_java.library.communication.socket;

import com.fazecast.jSerialComm.SerialPort;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Implementation for a Serial Communication Socket (e.g. RS232)
 */
@Slf4j
public class SerialCommunication extends CommunicationSocket {
    public final static String DEFAULT_SERIAL_COM_NAME = "USB-to-Serial Port";

    private final String serialComName;
    private SerialPort serialPort;

    /**
     * Serial Communication Constructor
     */
    public SerialCommunication() { this.serialComName = DEFAULT_SERIAL_COM_NAME; }

    /**
     * Serial Communication Constructor
     * @param serialComName Name that is looked for when retrieving the socket port
     */
    public SerialCommunication(@Nonnull String serialComName) { this.serialComName = serialComName; }

    /**
     * @inheritDoc
     */
    @Override
    synchronized void openSocket() throws IOException {
        // Try to find Serial Adapter (only one supported currently)
        for (SerialPort port : SerialPort.getCommPorts()) {
            if (port.getDescriptivePortName()
                    .contains(serialComName)) {
                log.info("Found serial adapter on " +
                        port.getSystemPortName());
                serialPort = port;
                break;
            }
        }

        if (serialPort == null) {
            throw new IOException("Can not find serial adapter!");
        }

        serialPort.openPort();
        if (!serialPort.isOpen()) {
            throw new IOException("Port " +
                    serialPort.getDescriptivePortName() +
                    " cannot be opened. Perhaps permissions " +
                    "(this user has no access to port)?");
        }
        serialPort.setComPortTimeouts(SerialPort.TIMEOUT_READ_SEMI_BLOCKING,
                5000, 0);

        serialPort.setComPortParameters(9600, 8,
                SerialPort.ONE_STOP_BIT, SerialPort.NO_PARITY);
        serialPort.setFlowControl(SerialPort.FLOW_CONTROL_DISABLED);
    }

    /**
     * @inheritDoc
     */
    @Override
    public synchronized void closeSocket() {
        if (serialPort != null) {
            serialPort.closePort();
            serialPort = null;
        }
    }

    /**
     * @inheritDoc
     */
    @Override
    public OutputStream getOutputStream() { return serialPort.getOutputStream(); }

    /**
     * @inheritDoc
     */
    @Override
    public InputStream getInputStream() { return serialPort.getInputStream(); }
}
