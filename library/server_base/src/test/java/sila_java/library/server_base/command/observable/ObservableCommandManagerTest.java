package sila_java.library.server_base.command.observable;

import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import sila2.org.silastandard.SiLAFramework;

import java.time.Duration;
import java.util.UUID;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ObservableCommandManagerTest {

    @Test
    void managerCreation() {
        final ObservableCommandTaskRunner runner = new ObservableCommandTaskRunner(0, 1);
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new ObservableCommandManager<Integer, Integer>(runner, command -> 0, Duration.ofSeconds(0)));
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new ObservableCommandManager<Integer, Integer>(runner, command -> 0, Duration.ofSeconds(-1)));
        ObservableCommandManager<Integer, Integer> manager = new ObservableCommandManager<>(runner, command -> 0, null);
        manager.close();
        manager = new ObservableCommandManager<>(runner, command -> 0, Duration.ofNanos(1));
        manager.close();
    }

    @Test
    void manualCommandRemove() {
        final ObservableCommandTaskRunner runner = new ObservableCommandTaskRunner(0, 1);
        final ObservableCommandManager<Integer, Integer> manager =
                new ObservableCommandManager<>(runner, command -> {
                    while (true) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            // Silently catching this exception for the purpose of the test
                        }
                    }
                }, Duration.ofSeconds(10));
        final ObservableCommandWrapper<Integer, Integer> command =
                manager.addCommand(42, new StreamObserver<SiLAFramework.CommandConfirmation>() {
                    public void onNext(SiLAFramework.CommandConfirmation confirmation) { }
                    public void onError(Throwable throwable) { }
                    public void onCompleted() { }
                });
        final ObservableCommandWrapper<Integer, Integer> command1 = manager.get(command.getExecutionId());
        Assertions.assertNotNull(command1);
        Assertions.assertEquals(command.getExecutionId(), command1.getExecutionId());
        manager.remove(command.getExecutionId());
        Assertions.assertThrows(StatusRuntimeException.class, () -> manager.get(command.getExecutionId()));
        Assertions.assertEquals(
                SiLAFramework.ExecutionInfo.CommandStatus.finishedWithError,
                command.getExecutionInfo().getState()
        );
        Assertions.assertTrue(command.getFuture().isCancelled());
        Assertions.assertThrows(CancellationException.class, () -> command.getFuture().get());
        manager.close();
    }

    @Test
    void listeners() throws ExecutionException, InterruptedException {
        final int nbIntermediateResponse = 10;
        final ObservableCommandTaskRunner runner = new ObservableCommandTaskRunner(0, 1);
        final ObservableCommandManager<Integer, Integer> manager =
                new ObservableCommandManager<>(runner, command -> {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }
                    for (int i = 0; i < nbIntermediateResponse; ++i) {
                        command.notifyIntermediateResponse(i);
                        try {
                            Thread.sleep(10);
                        } catch (InterruptedException e) {
                            Thread.currentThread().interrupt();
                        }
                    }
                    return command.getParameter() * 2;
                });
        final ObservableCommandWrapper<Integer, Integer> command =
                manager.addCommand(21, new StreamObserver<SiLAFramework.CommandConfirmation>() {
                    public void onNext(SiLAFramework.CommandConfirmation confirmation) {
                        String commandId = confirmation.getCommandExecutionUUID().getValue();
                        UUID uuid = UUID.fromString(commandId);
                    }
                    public void onError(Throwable throwable) {
                        throw new RuntimeException(throwable);
                    }
                    public void onCompleted() { }
                });
        command.addStateObserver(new StreamObserver<SiLAFramework.ExecutionInfo>() {
            SiLAFramework.ExecutionInfo.CommandStatus prevState = command.getExecutionInfo().getState();
            public void onNext(SiLAFramework.ExecutionInfo executionInfo) {
                SiLAFramework.ExecutionInfo.CommandStatus s = executionInfo.getCommandStatus();
                if (prevState != s) {
                    if (prevState == SiLAFramework.ExecutionInfo.CommandStatus.waiting)
                        Assertions.assertEquals(SiLAFramework.ExecutionInfo.CommandStatus.running, s);
                    if (prevState == SiLAFramework.ExecutionInfo.CommandStatus.running)
                        Assertions.assertEquals(SiLAFramework.ExecutionInfo.CommandStatus.finishedSuccessfully, s);
                }
                prevState = s;
            }
            public void onError(Throwable throwable) {
                throw new RuntimeException(throwable);
            }
            public void onCompleted() {
                Assertions.assertEquals(prevState, command.getExecutionInfo().getState());
            }
        });
         command.addIntermediateResponseObserver(new StreamObserver<Integer>() {
            private int i = 0;
            public void onNext(Integer executionInfo) {
                if (i < nbIntermediateResponse) {
                    Assertions.assertEquals(i, executionInfo.intValue());
                } else if (i == nbIntermediateResponse) {
                    Assertions.assertEquals(42, executionInfo.intValue());
                }
                Assertions.assertTrue(i <= nbIntermediateResponse);
                ++i;
            }
            public void onError(Throwable throwable) {
                throw new RuntimeException(throwable);
            }
            public void onCompleted() {
                Assertions.assertEquals(nbIntermediateResponse, i);
            }
        });

        Integer result = command.getFuture().get();
        Assertions.assertEquals(
                SiLAFramework.ExecutionInfo.CommandStatus.finishedSuccessfully,
                command.getExecutionInfo().getState()
        );
        Assertions.assertTrue(command.getFuture().isDone());
        Assertions.assertFalse(command.getFuture().isCancelled());
        Assertions.assertEquals(42, result.intValue());
        manager.close();
    }

}
