package sila_java.library.server_base.binary_transfer;

import io.grpc.Channel;
import io.grpc.InsecureServerCredentials;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Server;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import io.grpc.netty.NettyServerBuilder;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import sila2.org.silastandard.SiLABinaryTransfer;
import sila_java.library.core.sila.binary_transfer.BinaryDownloadHelper;
import sila_java.library.core.sila.binary_transfer.BinaryInfo;
import sila_java.library.core.sila.binary_transfer.BinaryTransferErrorHandler;
import sila_java.library.core.sila.binary_transfer.BinaryUploadHelper;
import sila_java.library.server_base.binary_transfer.database.BinaryDatabaseException;
import sila_java.library.server_base.binary_transfer.database.impl.H2BinaryDatabase;
import sila_java.library.server_base.binary_transfer.download.DownloadService;
import sila_java.library.server_base.binary_transfer.upload.UploadService;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.SQLException;
import java.util.Optional;
import java.util.UUID;

public class BinaryTransferTest {
    private Server server;
    private BinaryUploadHelper uploader;
    private BinaryDownloadHelper downloader;
    private final String parameterIdentifier
            = "org.silastandard/examples/UnknownFeature/v1/Command/UnknownCommand/Parameter/UnknownParameter";
    private H2BinaryDatabase db;

    @BeforeEach
    public void setUp() throws SQLException, IOException, InterruptedException {
        String HOSTNAME = "127.0.0.1";
        int PORT = 50052;

        db = new H2BinaryDatabase(UUID.randomUUID());
        server = NettyServerBuilder.forPort(PORT, InsecureServerCredentials.create()).addService(new UploadService(db))
                .addService(new DownloadService(db)).build();
        server.start();

        Thread.sleep(100);

        Channel channel = ManagedChannelBuilder.forAddress(HOSTNAME, PORT).usePlaintext().build();
        uploader = new BinaryUploadHelper(channel);
        downloader = new BinaryDownloadHelper(channel);
    }

    @AfterEach
    public void tearDown() {
        server.shutdown();
    }

    @Test
    void testUploadFromByteArray() throws BinaryDatabaseException {
        final byte[] bytesToUpload = Utils.generateRandomLargeBinary();
        BinaryInfo uploadInfo = uploader.upload(bytesToUpload, parameterIdentifier);
        Assertions.assertEquals(bytesToUpload.length, uploadInfo.getLength(), "Upload info reports wrong size");
        Assertions.assertEquals(
                bytesToUpload.length,
                db.getBinaryInfo(uploadInfo.getId()).getLength(),
                "Database reports wrong size"
        );
    }

    @Test
    void testUploadFromFile() throws BinaryDatabaseException, IOException {
        final byte[] bytesToUpload = Utils.generateRandomLargeBinary();
        Path tempUploadFile = Files.createTempFile("sila-download-test-", ".bin");
        tempUploadFile.toFile().deleteOnExit();
        Files.write(tempUploadFile, bytesToUpload);

        BinaryInfo uploadInfo = uploader.upload(
                Files.newInputStream(tempUploadFile),
                bytesToUpload.length,
                parameterIdentifier
        );
        Assertions.assertEquals(bytesToUpload.length, uploadInfo.getLength(), "Upload info reports wrong size");
        Assertions.assertEquals(
                bytesToUpload.length,
                db.getBinaryInfo(uploadInfo.getId()).getLength(),
                "Database reports wrong size"
        );
    }

    @Test
    void testDownloadToByteArray() {
        final byte[] bytesToUpload = Utils.generateRandomLargeBinary();
        BinaryInfo uploadInfo = uploader.upload(bytesToUpload, parameterIdentifier);

        Assertions.assertArrayEquals(bytesToUpload, downloader.download(uploadInfo.getId()));
    }

    @Test
    void testDownloadToFile() throws IOException {
        final byte[] bytesToUpload = Utils.generateRandomLargeBinary();
        BinaryInfo uploadInfo = uploader.upload(bytesToUpload, parameterIdentifier);

        Path tempDownloadFile = Files.createTempFile("sila-download-test-", ".bin");
        tempDownloadFile.toFile().deleteOnExit();

        downloader.downloadToFile(uploadInfo.getId(), tempDownloadFile);
        Assertions.assertArrayEquals(bytesToUpload, Files.readAllBytes(tempDownloadFile));
    }

    @Test
    void testUploaderCanDeleteBinary() {
        final byte[] bytesToUpload = Utils.generateRandomLargeBinary();
        BinaryInfo uploadInfo = uploader.upload(bytesToUpload, parameterIdentifier);
        UUID uploadId = uploadInfo.getId();

        Assertions.assertEquals(bytesToUpload.length, downloader.getBinaryInfo(uploadId).getLength());
        uploader.deleteBinary(uploadId);
        assertThrowsBinaryTransferError(() -> downloader.getBinaryInfo(uploadId));
    }

    @Test
    void testDownloaderCanDeleteBinary() {
        final byte[] bytesToUpload = Utils.generateRandomLargeBinary();
        BinaryInfo uploadInfo = uploader.upload(bytesToUpload, parameterIdentifier);
        UUID uploadId = uploadInfo.getId();

        Assertions.assertEquals(bytesToUpload.length, downloader.getBinaryInfo(uploadId).getLength());
        downloader.deleteBinary(uploadId);
        assertThrowsBinaryTransferError(() -> downloader.getBinaryInfo(uploadId));
    }

    private void assertThrowsBinaryTransferError(Executable call) {
        StatusRuntimeException ex = Assertions.assertThrows(
                StatusRuntimeException.class,
                call,
                "Didn't throw StatusRuntimeException"
        );
        Assertions.assertEquals(Status.Code.ABORTED, ex.getStatus().getCode(), "Call failed with wrong status");
        Optional<SiLABinaryTransfer.BinaryTransferError> maybeErr
                = BinaryTransferErrorHandler.retrieveBinaryTransferError(ex);
        Assertions.assertTrue(maybeErr.isPresent(), "No BinaryTransferError");
        Assertions.assertEquals(
                SiLABinaryTransfer.BinaryTransferError.ErrorType.INVALID_BINARY_TRANSFER_UUID,
                maybeErr.get().getErrorType(),
                "Error has wrong type"
        );
        Assertions.assertTrue(maybeErr.get().getMessage().length() > 0, "Error has no error message");
    }
}
