package sila_java.library.server_base.standard_features;

import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.core.lockcontroller.v1.LockControllerOuterClass;
import sila_java.library.core.sila.errors.SiLAErrors;
import sila_java.library.core.sila.types.SiLAInteger;
import sila_java.library.core.sila.types.SiLAString;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class LockControllerTest {

    private LockController cut;

    @BeforeEach
    void setUp() {
        cut = new LockController();
    }

    @Test
    void testLockServerWithoutLockId() throws Exception {

        TestStreamObserver<LockControllerOuterClass.LockServer_Responses> observer = executeLockServer(null);

        assertEquals(1, observer.getErrors().size());
        assertEquals(0, observer.getResponses().size());
    }

    @Test
    void testLockServer() throws Exception {

        TestStreamObserver<LockControllerOuterClass.LockServer_Responses> observer = executeLockServer("42");

        assertEquals(0, observer.getErrors().size());
        assertEquals(0, observer.getResponses().size());

        // already locked
        observer = executeLockServer("42");

        List<Throwable> errors = observer.getErrors();
        assertEquals(1, errors.size());

        Optional<SiLAFramework.SiLAError> siLAError = SiLAErrors.retrieveSiLAError((StatusRuntimeException) errors.get(0));
        assertTrue(siLAError.isPresent());

        assertEquals("ServerAlreadyLocked", siLAError.get().getDefinedExecutionError().getErrorIdentifier());
        assertEquals(0, observer.getResponses().size());

    }

    @Test
    void testUnlockUnlockedServer() throws Exception {

        TestStreamObserver<LockControllerOuterClass.UnlockServer_Responses> observer = executeUnlockServer("42");
        List<Throwable> errors = observer.getErrors();
        assertEquals(1, errors.size());
        assertEquals(0, observer.getResponses().size());

        Optional<SiLAFramework.SiLAError> siLAError = SiLAErrors.retrieveSiLAError((StatusRuntimeException) errors.get(0));
        assertTrue(siLAError.isPresent());

        assertEquals("ServerNotLocked", siLAError.get().getDefinedExecutionError().getErrorIdentifier());
    }

    @Test
    void testUnlockServerWithWrongId() throws Exception {
        // lock
        {
            TestStreamObserver<LockControllerOuterClass.LockServer_Responses> observer = executeLockServer("42");
            assertEquals(0, observer.getErrors().size());
            assertEquals(0, observer.getResponses().size());
        }

        // locked
        {
            TestStreamObserver<LockControllerOuterClass.Subscribe_IsLocked_Responses> observer = executeIsLocked();

            List<Throwable> errors = observer.getErrors();
            assertEquals(0, errors.size());
            assertEquals(1, observer.getResponses().size());

            LockControllerOuterClass.Subscribe_IsLocked_Responses lockedResponses = observer.getResponses().get(0);
            assertTrue(lockedResponses.getIsLocked().getValue());
        }

        // try unlock
        {
            TestStreamObserver<LockControllerOuterClass.UnlockServer_Responses> observer = executeUnlockServer("24");
            List<Throwable> errors = observer.getErrors();
            assertEquals(1, errors.size());
            assertEquals(0, observer.getResponses().size());

            Optional<SiLAFramework.SiLAError> siLAError = SiLAErrors.retrieveSiLAError((StatusRuntimeException) errors.get(0));
            assertTrue(siLAError.isPresent());

            assertEquals("InvalidLockIdentifier", siLAError.get().getDefinedExecutionError().getErrorIdentifier());
        }

        // still locked
        {
            TestStreamObserver<LockControllerOuterClass.Subscribe_IsLocked_Responses> observer = executeIsLocked();

            List<Throwable> errors = observer.getErrors();
            assertEquals(0, errors.size());
            assertEquals(1, observer.getResponses().size());

            LockControllerOuterClass.Subscribe_IsLocked_Responses lockedResponses = observer.getResponses().get(0);
            assertTrue(lockedResponses.getIsLocked().getValue());
        }
    }

    private TestStreamObserver<LockControllerOuterClass.UnlockServer_Responses> executeUnlockServer(String lockIdentifier) throws InterruptedException {
        LockControllerOuterClass.UnlockServer_Parameters.Builder serverParametersBuilder = LockControllerOuterClass.UnlockServer_Parameters.newBuilder();

        if (StringUtils.isNotEmpty(lockIdentifier)) {
            serverParametersBuilder.setLockIdentifier(SiLAString.from(lockIdentifier));
        }

        CountDownLatch latch = new CountDownLatch(1);
        TestStreamObserver<LockControllerOuterClass.UnlockServer_Responses> observer = new TestStreamObserver<>(latch);

        cut.unlockServer(serverParametersBuilder.build(), observer);

        assertTrue(latch.await(1, TimeUnit.SECONDS));
        return observer;
    }

    @Test
    void testLockServerWithTimeout() throws Exception {
        // lock server for 3s
        {
            TestStreamObserver<LockControllerOuterClass.LockServer_Responses> observer = executeLockServer("42", 3);
            assertEquals(0, observer.getErrors().size());
            assertEquals(0, observer.getResponses().size());
        }

        {
            TestStreamObserver<LockControllerOuterClass.Subscribe_IsLocked_Responses> observer = executeIsLocked();

            List<Throwable> errors = observer.getErrors();
            assertEquals(0, errors.size());
            assertEquals(1, observer.getResponses().size());

            LockControllerOuterClass.Subscribe_IsLocked_Responses lockedResponses = observer.getResponses().get(0);
            assertTrue(lockedResponses.getIsLocked().getValue());
        }

        Thread.sleep(5000);

        {
            TestStreamObserver<LockControllerOuterClass.Subscribe_IsLocked_Responses> observer = executeIsLocked();

            List<Throwable> errors = observer.getErrors();
            assertEquals(0, errors.size());
            assertEquals(1, observer.getResponses().size());

            LockControllerOuterClass.Subscribe_IsLocked_Responses lockedResponses = observer.getResponses().get(0);
            assertFalse(lockedResponses.getIsLocked().getValue());
        }

    }

    @Test
    void testIsServerLocked() throws Exception {

        {
            TestStreamObserver<LockControllerOuterClass.Subscribe_IsLocked_Responses> observer = executeIsLocked();
            List<Throwable> errors = observer.getErrors();
            assertEquals(0, errors.size());
            assertEquals(1, observer.getResponses().size());

            LockControllerOuterClass.Subscribe_IsLocked_Responses lockedResponses = observer.getResponses().get(0);
            assertFalse(lockedResponses.getIsLocked().getValue());
        }

        {
            TestStreamObserver<LockControllerOuterClass.LockServer_Responses> observer = executeLockServer("42");
            assertEquals(0, observer.getErrors().size());
            assertEquals(0, observer.getResponses().size());
        }

        {
            TestStreamObserver<LockControllerOuterClass.Subscribe_IsLocked_Responses> observer = executeIsLocked();

            List<Throwable> errors = observer.getErrors();
            assertEquals(0, errors.size());
            assertEquals(1, observer.getResponses().size());

            LockControllerOuterClass.Subscribe_IsLocked_Responses lockedResponses = observer.getResponses().get(0);
            assertTrue(lockedResponses.getIsLocked().getValue());
        }

        {
            TestStreamObserver<LockControllerOuterClass.UnlockServer_Responses> observer = executeUnlockServer("42");
            assertEquals(0, observer.getErrors().size());
        }

        {
            TestStreamObserver<LockControllerOuterClass.Subscribe_IsLocked_Responses> observer = executeIsLocked();
            List<Throwable> errors = observer.getErrors();
            assertEquals(0, errors.size());
            assertEquals(1, observer.getResponses().size());

            LockControllerOuterClass.Subscribe_IsLocked_Responses lockedResponses = observer.getResponses().get(0);
            assertFalse(lockedResponses.getIsLocked().getValue());
        }
    }

    private TestStreamObserver<LockControllerOuterClass.LockServer_Responses> executeLockServer(String lockIdentifier) throws InterruptedException {
        return executeLockServer(lockIdentifier, -1);
    }

    private TestStreamObserver<LockControllerOuterClass.LockServer_Responses> executeLockServer(String lockIdentifier, long timeout) throws InterruptedException {
        LockControllerOuterClass.LockServer_Parameters.Builder serverParametersBuilder = LockControllerOuterClass.LockServer_Parameters.newBuilder();
        if (StringUtils.isNotEmpty(lockIdentifier)) {
            serverParametersBuilder.setLockIdentifier(SiLAString.from(lockIdentifier));
        }
        if (timeout >= 0) {
            serverParametersBuilder.setTimeout(SiLAInteger.from(timeout));
        }

        CountDownLatch latch = new CountDownLatch(1);
        TestStreamObserver<LockControllerOuterClass.LockServer_Responses> observer = new TestStreamObserver<>(latch);
        // lock
        cut.lockServer(serverParametersBuilder.build(), observer);

        assertTrue(latch.await(1, TimeUnit.SECONDS));
        return observer;
    }

    private TestStreamObserver<LockControllerOuterClass.Subscribe_IsLocked_Responses> executeIsLocked() throws InterruptedException {
        LockControllerOuterClass.Subscribe_IsLocked_Parameters serverParameters = LockControllerOuterClass.Subscribe_IsLocked_Parameters.getDefaultInstance();

        CountDownLatch latch = new CountDownLatch(1);
        TestStreamObserver<LockControllerOuterClass.Subscribe_IsLocked_Responses> observer = new TestStreamObserver<>(latch);

        cut.subscribeIsLocked(serverParameters, observer);

        assertTrue(latch.await(1, TimeUnit.SECONDS));
        return observer;
    }


    @Getter
    static class TestStreamObserver<T> implements StreamObserver<T> {

        final List<Throwable> errors = new ArrayList<>();
        final List<T> responses = new ArrayList<>();
        final CountDownLatch latch;

        public TestStreamObserver(CountDownLatch latch) {
            this.latch = latch;
        }

        @Override
        public void onNext(T lockServer_responses) {
            responses.add(lockServer_responses);
        }

        @Override
        public void onError(Throwable throwable) {
            errors.add(throwable);
        }

        @Override
        public void onCompleted() {
            latch.countDown();
        }
    }

}
