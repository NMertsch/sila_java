package sila_java.library.server_base.identification;

import lombok.Getter;
import lombok.NonNull;

@Getter
public class ServerInformation {
    private final String type;
    private final String description;
    private final String vendorURL;
    private final String version;

    public ServerInformation (@NonNull final String type, @NonNull final String description, @NonNull final String vendorURL, @NonNull final String version) {
        if (!type.matches("[A-Z][a-zA-Z\\d]*")) {
            throw new IllegalArgumentException("SiLA Server Type must start with a capital letter and can only contain letters and digits (e.g. 'ExampleServerType123')");
        }

        if (!vendorURL.matches("https?://.+")) {
            throw new IllegalArgumentException("SiLA Server Vendor URL must start with 'https://' or 'http://'");
        }

        if (!version.matches("(0|[1-9]\\d*)\\.(0|[1-9]\\d*)(\\.(0|[1-9]\\d*))?(_\\w+)?")) {
            throw new IllegalArgumentException("SiLA Server Version does not follow the required pattern. Examples: '2.1', '0.1.3', '1.2.3_preview'");
        }

        this.type = type;
        this.description = description;
        this.vendorURL = vendorURL;
        this.version = version;
    }
}
