package sila_java.library.server_base.binary_transfer.database;

import io.grpc.Context;
import lombok.NonNull;
import sila_java.library.core.sila.binary_transfer.BinaryInfo;
import sila_java.library.server_base.binary_transfer.Binary;

import java.io.InputStream;
import java.time.Duration;
import java.util.UUID;

/**
 * Interface of a binary database
 * @implNote The implementer must take care of removing expired entries
 */
public interface BinaryDatabase extends AutoCloseable {
    /**
     * Get a binary by its identifier
     * @param binaryId The binary identifier
     * @return The binary with the specified identifier
     * @throws BinaryDatabaseException when the binary does not exist or cannot be retrieved
     */
    Binary getBinary(UUID binaryId) throws BinaryDatabaseException;

    /**
     * Get the information of a binary by its identifier
     * @param binaryId the binary identifier
     * @return The information of the binary with the specified identifier
     * @throws BinaryDatabaseException when the binary does not exist or cannot be retrieved
     */
    BinaryInfo getBinaryInfo(UUID binaryId) throws BinaryDatabaseException;

    /**
     * Add a binary into the database
     * @param binaryId The binary identifier
     * @param bytes The data to save in the database
     * @return The expiration duration of the binary
     * @throws BinaryDatabaseException when the binary cannot be added into database
     */
    Duration addBinary(@NonNull UUID binaryId, byte[] bytes) throws BinaryDatabaseException;

    /**
     * Add a binary into the database
     * @param binaryId The binary identifier
     * @param stream The data stream to save in the database
     * @return The expiration duration of the binary
     * @throws BinaryDatabaseException when the binary cannot be added into database
     */
    Duration addBinary(UUID binaryId, InputStream stream) throws BinaryDatabaseException;

    /**
     * Extend the expiration date of a binary
     * @param binaryId The binary identifier
     * @return The extended expiration duration
     * @throws BinaryDatabaseException when the binary expiration cannot be extended
     */
    Duration extendBinaryExpiration(UUID binaryId) throws BinaryDatabaseException;

    /**
     * Remove all binaries from the database
     * @throws BinaryDatabaseException when the binaries cannot be removed from the database
     */
    void removeAllBinaries() throws BinaryDatabaseException;

    /**
     * Remove a binary from the database
     * @param binaryId The binary identifier to remove
     * @throws BinaryDatabaseException when the binary cannot be removed from the database
     */
    void removeBinary(UUID binaryId) throws BinaryDatabaseException;

    /**
     * Get a reference to the current {@link BinaryDatabase} from the {@link Context#current()}
     *
     * @return The current {@link BinaryDatabase}
     */
    static BinaryDatabase current() {
        return BinaryDatabaseInjector.BINARY_DATABASE_KEY.get(Context.current());
    }
}
