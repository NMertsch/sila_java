package sila_java.library.server_base.command.observable;

import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila_java.library.core.sila.errors.SiLAErrors;
import sila_java.library.core.sila.types.SiLAReal;

import javax.annotation.Nullable;
import java.time.Duration;
import java.util.Iterator;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.*;
import java.util.function.Consumer;

/**
 * Wrapper for all observable commands
 * @param <ParamType>
 * @param <ResultType>
 */
@Slf4j
public class ObservableCommandWrapper<ParamType, ResultType> implements AutoCloseable {
    @Getter private final UUID executionId = UUID.randomUUID();
    @Getter private final ParamType parameter;
    @Getter private final Set<StreamObserver> stateObservers = ConcurrentHashMap.newKeySet();
    @Getter private final Set<StreamObserver> intermediateResponseObservers = ConcurrentHashMap.newKeySet();

    @Getter(AccessLevel.PACKAGE)
    private final ExecutionInfo executionInfo = new ExecutionInfo();
    private final Duration lifeTimeOfExecution;

    @Getter(AccessLevel.PACKAGE)
    private final RunnableCommandTask<ParamType, ResultType> task;
    @Getter(AccessLevel.PACKAGE)
    private final Future<ResultType> future;
    private final ScheduledExecutorService scheduler;
    private final Consumer<UUID> expirationConsumer;

    static final class ExecutionInfo {
        @Getter(AccessLevel.PACKAGE)
        private SiLAFramework.ExecutionInfo.CommandStatus state = SiLAFramework.ExecutionInfo.CommandStatus.waiting;
        private double progressionPercent = 0d;
        private Duration estimatedRemainingTime = null;
    }

    /**
     * Constructor
     * @param parameter The command parameter
     * @param task The task
     * @param expirationConsumer Consumer of UUID once the command is expired (if lifetimeOfExecution is not null
     * @param scheduler The scheduler
     * @param runner The runner
     */
    ObservableCommandWrapper(
            @NonNull final ParamType parameter,
            @NonNull final RunnableCommandTask<ParamType, ResultType> task,
            @NonNull final ObservableCommandTaskRunner runner,
            @NonNull final Consumer<UUID> expirationConsumer,
            @NonNull final ScheduledExecutorService scheduler
    ) {
        this(parameter, task, runner, expirationConsumer, scheduler, null);
    }

    /**
     * Constructor
     * @param param The command parameter
     * @param task The task
     * @param expirationConsumer Consumer of UUID once the command is expired (if lifetimeOfExecution is not null
     * @param scheduler The scheduler
     * @param runner The runner
     * @param lifeTimeOfExecution The life time of execution of the command
     *                            If null the command will never expire unless removed manually
     */
    ObservableCommandWrapper(
            @NonNull final ParamType param,
            @NonNull final RunnableCommandTask<ParamType, ResultType> task,
            @NonNull final ObservableCommandTaskRunner runner,
            @NonNull final Consumer<UUID> expirationConsumer,
            @NonNull final ScheduledExecutorService scheduler,
            @Nullable final Duration lifeTimeOfExecution
    ) {
        this.parameter = param;
        this.task = task;
        this.scheduler = scheduler;
        this.future = runner.enqueueTask(this::call);
        this.lifeTimeOfExecution = lifeTimeOfExecution;
        this.expirationConsumer = expirationConsumer;
    }

    /**
     * Retrieving Initial Command Confirmation
     * @return Command Confirmation
     */
    public SiLAFramework.CommandConfirmation getCommandConfirmation() {
        final SiLAFramework.CommandConfirmation.Builder builder = SiLAFramework.CommandConfirmation
                .newBuilder()
                .setCommandExecutionUUID(SiLAFramework.CommandExecutionUUID
                        .newBuilder()
                        .setValue(this.executionId.toString())
                        .build()
                );
        if (this.lifeTimeOfExecution != null) {
            builder.setLifetimeOfExecution(SiLAFramework.Duration
                    .newBuilder()
                    .setSeconds(this.lifeTimeOfExecution.getSeconds())
                    .setNanos(this.lifeTimeOfExecution.getNano())
                    .build()
            );
        }
        return builder.build();
    }

    /**
     * Wrapps the command task with appropriate state notifications
     * @return Result if successful
     */
    public ResultType call() throws StatusRuntimeException {
        setStateAndNotify(SiLAFramework.ExecutionInfo.CommandStatus.running);
        try {
            final ResultType result = task.run(this);
            if (result != null) {
                setStateAndNotify(SiLAFramework.ExecutionInfo.CommandStatus.finishedSuccessfully);
                return result;
            }
        } catch (final StatusRuntimeException e) {
            setStateAndNotify(SiLAFramework.ExecutionInfo.CommandStatus.finishedWithError);
            throw e;
        } catch (final Throwable e) {
            log.warn("Error occurred during observable command {" + executionId + "} task execution", e);
            setStateAndNotify(SiLAFramework.ExecutionInfo.CommandStatus.finishedWithError);
            throw SiLAErrors.generateGenericExecutionError(e);
        }
        setStateAndNotify(SiLAFramework.ExecutionInfo.CommandStatus.finishedWithError);
        throw SiLAErrors.generateDefinedExecutionError(
                "NoResult",
                "No result was returned after end of command execution"
        );
    }

    /**
     * Add a command state observer
     * @param streamObserver The observer
     */
    public void addStateObserver(@NonNull final StreamObserver<SiLAFramework.ExecutionInfo> streamObserver) {
        streamObserver.onNext(getSiLAExecutionInfo());
        this.stateObservers.add(streamObserver);
    }

    /**
     * Add an intermediate response observer
     * @param streamObserver The observer
     */
    public <IntermediateResultType> void addIntermediateResponseObserver(
            @NonNull final StreamObserver<IntermediateResultType> streamObserver) {
        intermediateResponseObservers.add(streamObserver);
    }

    /**
     * Send command result to an observer or an error if command execution was not successful
     * @param streamObserver the observer
     */
    public void sendResult(@NonNull final StreamObserver<ResultType> streamObserver) {
        if (future.isDone()) {
            try {
                streamObserver.onNext(future.get());
            } catch (final InterruptedException | ExecutionException | StatusRuntimeException e) {
                streamObserver.onError(e);
            }
        } else {
            streamObserver.onError(SiLAErrors.generateFrameworkError(
                    SiLAFramework.FrameworkError.ErrorType.COMMAND_EXECUTION_NOT_FINISHED,
                    "Command Execution is not finished. The result is not yet available.")
            );
        }
        streamObserver.onCompleted();
    }

    /**
     * Notify intermediate response observers an intermediate response
     * @param intermediateResponse The intermediate response to notify
     */
    public <IntermediateResultType>  void notifyIntermediateResponse(
            @NonNull final IntermediateResultType intermediateResponse) {
        ObservableCommandWrapper.notifyIntermediateObservers(this.intermediateResponseObservers, intermediateResponse);

    }

    private static <IntermediateResultType> void notifyIntermediateObservers(Set<StreamObserver> intermediateResponseObservers, IntermediateResultType intermediateResponse) {
        final Iterator<StreamObserver> it = intermediateResponseObservers.iterator();
        while (it.hasNext()) {
            final StreamObserver<IntermediateResultType> element = it.next();
            try {
                element.onNext(intermediateResponse);
            } catch (final Exception e) {
                element.onError(e);
                it.remove();
            }
        }
    }

    /**
     * Dispose all resources owned by the wrapper
     */
    @Override
    public void close() {
        this.future.cancel(true);
        this.task.close();
        if (this.executionInfo.state != SiLAFramework.ExecutionInfo.CommandStatus.finishedSuccessfully) {
            this.setStateAndNotify(SiLAFramework.ExecutionInfo.CommandStatus.finishedWithError);
        }
        ObservableCommandWrapper.closeObservers(this.stateObservers);
        this.stateObservers.clear();
        ObservableCommandWrapper.closeObservers(this.intermediateResponseObservers);
        this.intermediateResponseObservers.clear();

    }

    /**
     * Return whether or not the command is done
     * @return true if the command is done, otherwise false
     */
    public boolean isDone() {
        return (this.executionInfo.state == SiLAFramework.ExecutionInfo.CommandStatus.finishedWithError ||
                this.executionInfo.state == SiLAFramework.ExecutionInfo.CommandStatus.finishedSuccessfully);
    }

    /**
     * Set the execution information and notify observers
     * @param progressionPercent The progression percentage
     * @param estimatedRemainingTime The estimated remaining time
     */
    public void setExecutionInfoAndNotify(
            final double progressionPercent,
            @Nullable final Duration estimatedRemainingTime
    ) {
        this.executionInfo.progressionPercent = progressionPercent;
        this.executionInfo.estimatedRemainingTime = estimatedRemainingTime;
        this.notifyState();
    }

    /**
     * Set the state and notify observers of the change
     * @param state The state to set
     */
    private void setStateAndNotify(@NonNull final SiLAFramework.ExecutionInfo.CommandStatus state) {
        this.executionInfo.state = state;

        if (isDone() && this.lifeTimeOfExecution != null) {
            this.scheduler.schedule
                    (() ->
                                    ObservableCommandWrapper.this.expirationConsumer.accept(
                                            ObservableCommandWrapper.this.executionId
                                    ),
                            this.lifeTimeOfExecution.toMillis(),
                            TimeUnit.MILLISECONDS
                    );
        }

        this.notifyState();
    }



    /**
     * Notify state observers with the current state
     */
    private void notifyState() {
        ObservableCommandWrapper.notifyObservers(this.stateObservers, getSiLAExecutionInfo());
    }

    /**
     * Utility Function to Notify all observers with a value
     * @param observers The observers
     * @param valueToNotify The value to notify
     * @param <NotifyType> The type of the value to notify
     */
    private static <NotifyType> void notifyObservers(
            @NonNull final Set<StreamObserver> observers,
            @NonNull final NotifyType valueToNotify
    ) {
        final Iterator<StreamObserver> it = observers.iterator();
        while (it.hasNext()) {
            final StreamObserver<NotifyType> element = it.next();
            try {
                element.onNext(valueToNotify);
            } catch (final Exception e) {
                element.onError(e);
                it.remove();
            }
        }
    }

    /**
     * Utility Function to close all observers of a set
     * @param observers the observers
     * @param <NotifyType> The type observers are observing
     */
    private static <NotifyType> void closeObservers(@NonNull final Set<StreamObserver> observers) {
        final Iterator<StreamObserver> it = observers.iterator();
        while (it.hasNext()) {
            final StreamObserver<NotifyType> element = it.next();
            try {
                element.onCompleted();
            } catch (final Exception e) {
                it.remove();
            }
        }
    }

    /**
     * Return the current execution information
     * @return the current execution information
     */
    private SiLAFramework.ExecutionInfo getSiLAExecutionInfo() {
        final Duration remainingTime = this.executionInfo.estimatedRemainingTime;
        final SiLAFramework.ExecutionInfo.Builder builder = SiLAFramework.ExecutionInfo
                .newBuilder()
                .setCommandStatus(this.executionInfo.state)
                .setProgressInfo(SiLAReal.from(this.executionInfo.progressionPercent));
        if (this.lifeTimeOfExecution != null) {
            builder.setUpdatedLifetimeOfExecution(SiLAFramework.Duration
                    .newBuilder()
                    .setSeconds(this.lifeTimeOfExecution.getSeconds())
                    .setNanos(this.lifeTimeOfExecution.getNano())
                    .build()
            );
        }
        if (remainingTime != null) {
            builder.setEstimatedRemainingTime(SiLAFramework.Duration
                    .newBuilder()
                    .setSeconds(remainingTime.getSeconds())
                    .setNanos(remainingTime.getNano())
                    .build()
            );
        }
        return builder.build();
    }
}
