package sila_java.library.server_base.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.annotation.Nullable;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Utility class to build a server process for testing purposes
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class ServerProcessBuilder {
    private static final File NULL_FILE = new File((System.getProperty("os.name").startsWith("Windows")) ?
            "NUL" :
            "/dev/null"
    );
    /**
     * Build a new server process
     * @param serverJarPath the path to the server jar
     * @param port the port to listen to
     * @param configPath the server configuration path
     * @param netInterface the network interface
     * @return a new sever process
     * @throws IOException
     */
    public static Process build(
            @NonNull final String serverJarPath,
            final int port,
            @NonNull final Path configPath,
            @Nullable final String netInterface
    ) throws IOException {
        final List<String> args = new ArrayList<String>() {{
            add("java");
            add("-jar");
            add(serverJarPath);
            Collections.addAll(this, ArgumentHelperBuilder.toArgs(port, configPath.toString(), netInterface));
        }};
        return new ProcessBuilder(args)
                .redirectError(ProcessBuilder.Redirect.INHERIT)
                .redirectOutput(NULL_FILE) // todo fixme On Windows, a call to log.info block the execution of the program unless we redirect the process output
                .start();
    }
}
