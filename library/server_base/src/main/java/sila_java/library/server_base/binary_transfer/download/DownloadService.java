package sila_java.library.server_base.binary_transfer.download;

import com.google.protobuf.ByteString;
import io.grpc.stub.StreamObserver;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.BinaryDownloadGrpc;
import sila2.org.silastandard.SiLABinaryTransfer;
import sila_java.library.core.sila.types.SiLADuration;
import sila_java.library.core.sila.binary_transfer.BinaryInfo;
import sila_java.library.server_base.binary_transfer.Binary;
import sila_java.library.server_base.binary_transfer.database.BinaryDatabase;
import sila_java.library.core.sila.binary_transfer.BinaryTransferErrorHandler;

import java.time.Duration;
import java.time.OffsetDateTime;
import java.util.UUID;

/**
 * Service responsible for downloading binaries
 */
@Slf4j
@AllArgsConstructor
public class DownloadService extends BinaryDownloadGrpc.BinaryDownloadImplBase {
    private final BinaryDatabase binaryDatabase;

    /**
     * Retrieve the binary information linked to a Binary Transfer UUID
     *
     * @param request Request containing a Binary Transfer UUID
     * @param responseObserver Response observer
     */
    @Override
    public void getBinaryInfo(
            final SiLABinaryTransfer.GetBinaryInfoRequest request,
            final StreamObserver<SiLABinaryTransfer.GetBinaryInfoResponse> responseObserver
    ) {
        final BinaryInfo binaryInfo;
        try {
            binaryInfo = this.binaryDatabase.getBinaryInfo(UUID.fromString(request.getBinaryTransferUUID()));
        } catch (Exception e) {
            responseObserver.onError(BinaryTransferErrorHandler.generateBinaryTransferError(SiLABinaryTransfer.BinaryTransferError.ErrorType.INVALID_BINARY_TRANSFER_UUID, e.getMessage()));
            return;
        }
        responseObserver.onNext(
                SiLABinaryTransfer.GetBinaryInfoResponse.newBuilder()
                        .setBinarySize(binaryInfo.getLength())
                        .setLifetimeOfBinary(
                                SiLADuration.from(Duration.between(OffsetDateTime.now(), binaryInfo.getExpiration()))
                        )
                        .build()
        );
        responseObserver.onCompleted();
    }

    /**
     * Retrieve binary chunk requests sent through the stream
     *
     * @param responseObserver The stream response observer
     *
     * @return the request stream observer
     */
    @Override
    public StreamObserver<SiLABinaryTransfer.GetChunkRequest> getChunk(
            final StreamObserver<SiLABinaryTransfer.GetChunkResponse> responseObserver
    ) {
        return new StreamObserver<SiLABinaryTransfer.GetChunkRequest>() {
            @Override
            public void onNext(final SiLABinaryTransfer.GetChunkRequest request) {
                getChunk(request, responseObserver);
            }

            @Override
            public void onError(final Throwable t) {
                log.warn("Download chunk stream exception: {}", t.getMessage(), t);
            }

            @Override
            public void onCompleted() {
                responseObserver.onCompleted();
            }
        };
    }

    /**
     * Get a binary transfer chunk by Binary Transfer UUID at the specified offset and with the specified length
     *
     * @param request The chunk request
     * @param responseObserver The response observer
     *
     * If successful return {@link SiLABinaryTransfer.GetChunkResponse}
     * If the binary transfer does not exist return {@link SiLABinaryTransfer.BinaryTransferError.ErrorType#INVALID_BINARY_TRANSFER_UUID error}
     * If the binary transfer download fails return {@link SiLABinaryTransfer.BinaryTransferError.ErrorType#BINARY_DOWNLOAD_FAILED error}
     */
    private void getChunk(
            final SiLABinaryTransfer.GetChunkRequest request,
            final StreamObserver<SiLABinaryTransfer.GetChunkResponse> responseObserver
    ) {
        final UUID id;
        try {
            id = UUID.fromString(request.getBinaryTransferUUID());
        } catch (Exception e) {
            responseObserver.onError(BinaryTransferErrorHandler.generateBinaryTransferError(SiLABinaryTransfer.BinaryTransferError.ErrorType.INVALID_BINARY_TRANSFER_UUID, e.getMessage()));
            return;
        }
        try {
            final Duration extendedExpiration = binaryDatabase.extendBinaryExpiration(id);
            final Binary binary = binaryDatabase.getBinary(id);
            responseObserver.onNext(
                    SiLABinaryTransfer.GetChunkResponse.newBuilder()
                            .setPayload(
                                    ByteString.readFrom(
                                            binary.getData().getBinaryStream(
                                                    request.getOffset() + 1L, // First byte is at index 1
                                                    request.getLength()
                                            )
                                    )
                            )
                            .setOffset(request.getOffset())
                            .setBinaryTransferUUID(request.getBinaryTransferUUID())
                            .setLifetimeOfBinary(SiLADuration.from(extendedExpiration))
                            .build()
            );
        } catch (Exception e) {
            responseObserver.onError(BinaryTransferErrorHandler.generateBinaryTransferError(SiLABinaryTransfer.BinaryTransferError.ErrorType.BINARY_DOWNLOAD_FAILED, e.getMessage()));
        }
    }

    /**
     * Delete a Binary Transfer
     *
     * @param request The Binary Transfer UUID to remove
     * @param responseObserver The response observer
     *
     * If successful return {@link SiLABinaryTransfer.DeleteBinaryResponse}
     * If error return {@link SiLABinaryTransfer.BinaryTransferError.ErrorType#INVALID_BINARY_TRANSFER_UUID error}
     */
    @Override
    public void deleteBinary(
            final SiLABinaryTransfer.DeleteBinaryRequest request,
            final StreamObserver<SiLABinaryTransfer.DeleteBinaryResponse> responseObserver
    ) {
        try {
            this.binaryDatabase.removeBinary(UUID.fromString(request.getBinaryTransferUUID()));
        } catch (Exception e) {
            responseObserver.onError(BinaryTransferErrorHandler.generateBinaryTransferError(SiLABinaryTransfer.BinaryTransferError.ErrorType.INVALID_BINARY_TRANSFER_UUID, e.getMessage()));
            return;
        }
        responseObserver.onNext(SiLABinaryTransfer.DeleteBinaryResponse.newBuilder().build());
        responseObserver.onCompleted();
    }
}