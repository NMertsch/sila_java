package sila_java.library.server_base.binary_transfer.upload;

import io.grpc.stub.StreamObserver;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.BinaryUploadGrpc;
import sila2.org.silastandard.SiLABinaryTransfer;
import sila_java.library.core.sila.types.SiLADuration;
import sila_java.library.server_base.binary_transfer.database.BinaryDatabase;
import sila_java.library.server_base.binary_transfer.database.BinaryDatabaseException;
import sila_java.library.core.sila.binary_transfer.BinaryTransferErrorHandler;

import java.io.IOException;
import java.io.InputStream;
import java.time.Duration;
import java.util.UUID;

/**
 * Service responsible for uploading binaries
 */
@Slf4j
@AllArgsConstructor
public class UploadService extends BinaryUploadGrpc.BinaryUploadImplBase {

    /**
     * The maximum lifetime of a binary upload
     */
    private static final Duration MAX_UPLOAD_DURATION = Duration.ofMinutes(10);
    private final UploadManager uploadManager = new UploadManager();
    private final BinaryDatabase binaryDatabase;

    /**
     * Create a binary upload.
     *
     * Add an upload entry in {@link UploadService#uploadManager}
     *
     * @implNote todo support metadata
     *
     * @param request the request
     * @param responseObserver the response observer
     */
    @Override
    public void createBinary(
            final SiLABinaryTransfer.CreateBinaryRequest request,
            final StreamObserver<SiLABinaryTransfer.CreateBinaryResponse> responseObserver
    ) {
        final UUID blobId = UUID.randomUUID();
        final Duration expiration = MAX_UPLOAD_DURATION;

        uploadManager.addUpload(blobId, request.getBinarySize(), request.getChunkCount(), expiration);
        responseObserver.onNext(
                SiLABinaryTransfer.CreateBinaryResponse
                        .newBuilder()
                        .setLifetimeOfBinary(SiLADuration.from(expiration))
                        .setBinaryTransferUUID(blobId.toString())
                        .build()
        );
        responseObserver.onCompleted();
    }

    /**
     * Upload chunk stream
     *
     * @param responseObserver the stream response observer
     * @return the stream request observer
     */
    @Override
    public StreamObserver<SiLABinaryTransfer.UploadChunkRequest> uploadChunk(
            final StreamObserver<SiLABinaryTransfer.UploadChunkResponse> responseObserver
    ) {
        return new StreamObserver<SiLABinaryTransfer.UploadChunkRequest>() {
            @Override
            public void onNext(final SiLABinaryTransfer.UploadChunkRequest request) {
                uploadChunk(request, responseObserver);
            }

            @Override
            public void onError(final Throwable t) {
                log.warn("Upload chunk stream exception: {}", t.getMessage(), t);
            }

            @Override
            public void onCompleted() {
                responseObserver.onCompleted();
            }
        };
    }

    /**
     * Upload a chunk
     *
     * @param request the chunk upload request
     * @param responseObserver the stream response observer
     *
     * If successful return {@link SiLABinaryTransfer.UploadChunkResponse}
     * If binary does not exist or cannot be removed return {@link SiLABinaryTransfer.BinaryTransferError.ErrorType#INVALID_BINARY_TRANSFER_UUID}
     */
    private void uploadChunk(
            final SiLABinaryTransfer.UploadChunkRequest request,
            final StreamObserver<SiLABinaryTransfer.UploadChunkResponse> responseObserver
    ) {
        Duration expiration = MAX_UPLOAD_DURATION;
        final UUID blobId = UUID.fromString(request.getBinaryTransferUUID());

        try {
            final UploadCompletion uploadCompletion = this.uploadManager.updateUpload(
                    blobId,
                    request.getChunkIndex(),
                    request.getPayload().toByteArray(),
                    expiration
            );
            if (uploadCompletion.isComplete()) {
                log.info("Upload {} from client complete", blobId);
                // Close the upload, remove it and add to database
                this.uploadManager.removeUpload(blobId);
                try (InputStream chunkStream = uploadCompletion.chunksToStream()) {
                    expiration = this.binaryDatabase.addBinary(blobId, chunkStream);
                }
                uploadCompletion.close();
                uploadChunkResponse(request, responseObserver, expiration);
                responseObserver.onCompleted();
                return;
            }
        } catch (final IOException | BinaryDatabaseException e) {
            log.error("The following error occurred when attempting to save received chunk: {}", e.getMessage(), e);
            responseObserver.onError(BinaryTransferErrorHandler.generateBinaryTransferError(SiLABinaryTransfer.BinaryTransferError.ErrorType.BINARY_UPLOAD_FAILED, e.getMessage()));
            return;
        }
        uploadChunkResponse(request, responseObserver, expiration);
    }

    /**
     * Upload chunk response
     *
     * @param request The request
     * @param responseObserver The response stream observer
     * @param newExpiration The new expiration
     */
    private void uploadChunkResponse(SiLABinaryTransfer.UploadChunkRequest request, final StreamObserver<SiLABinaryTransfer.UploadChunkResponse> responseObserver, Duration newExpiration) {
        responseObserver.onNext(
                SiLABinaryTransfer.UploadChunkResponse
                        .newBuilder()
                        .setBinaryTransferUUID(request.getBinaryTransferUUID())
                        .setChunkIndex(request.getChunkIndex())
                        .setLifetimeOfBinary(SiLADuration.from(newExpiration))
                        .build()
        );
    }

    /**
     *  Delete binary
     *
     * @param request the request
     * @param responseObserver the response observer
     *
     * If successful return {@link SiLABinaryTransfer.DeleteBinaryResponse}
     * If binary does not exist or cannot be removed return {@link SiLABinaryTransfer.BinaryTransferError.ErrorType#INVALID_BINARY_TRANSFER_UUID}
     */
    @Override
    public void deleteBinary(
            final SiLABinaryTransfer.DeleteBinaryRequest request,
            final StreamObserver<SiLABinaryTransfer.DeleteBinaryResponse> responseObserver
    ) {
        UUID binaryToDelete = UUID.fromString(request.getBinaryTransferUUID());

        // check for invalid UUID
        boolean fullUploadExists;
        try {
            binaryDatabase.getBinaryInfo(binaryToDelete);
            fullUploadExists = true;
        } catch (BinaryDatabaseException ex) {
            fullUploadExists = false;
        }
        boolean partialUploadExists = uploadManager.getUpload(binaryToDelete) != null;
        if (!fullUploadExists && !partialUploadExists) {
            responseObserver.onError(BinaryTransferErrorHandler.generateBinaryTransferError(
                    SiLABinaryTransfer.BinaryTransferError.ErrorType.INVALID_BINARY_TRANSFER_UUID,
                    String.format("No binary found with UUID %s", binaryToDelete)
            ));
            return;
        }

        // try deletion
        if (fullUploadExists) {
            try {
                binaryDatabase.removeBinary(binaryToDelete);
            } catch (BinaryDatabaseException ex) {
                responseObserver.onError(BinaryTransferErrorHandler.generateBinaryTransferError(
                        SiLABinaryTransfer.BinaryTransferError.ErrorType.BINARY_UPLOAD_FAILED,
                        String.format("Failed to delete binary with UUID %s", binaryToDelete)));
                return;
            }
        }
        if (partialUploadExists && uploadManager.removeUpload(binaryToDelete) == null) {
            responseObserver.onError(BinaryTransferErrorHandler.generateBinaryTransferError(
                    SiLABinaryTransfer.BinaryTransferError.ErrorType.BINARY_UPLOAD_FAILED,
                    String.format("Failed to delete binary with UUID %s", binaryToDelete)));
            return;
        }

        // success
        responseObserver.onNext(SiLABinaryTransfer.DeleteBinaryResponse.newBuilder().build());
        responseObserver.onCompleted();
    }
}