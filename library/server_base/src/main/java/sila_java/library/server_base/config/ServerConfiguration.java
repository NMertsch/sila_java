package sila_java.library.server_base.config;

import lombok.Getter;
import lombok.NonNull;

import javax.annotation.concurrent.Immutable;
import java.util.UUID;

/**
 * Server configuration modal
 */
@Immutable
@Getter
public final class ServerConfiguration {
    private final String name;
    private final UUID uuid;

    /**
     * Create a server configuration from a given server name and UUID
     *
     * @param serverName The server name
     * @param uuid The server UUID
     */
    public ServerConfiguration(@NonNull final String serverName, UUID uuid) {
        if (serverName.length() > 255) {
            throw new IllegalArgumentException("SiLA Server Name must be shorter than 265 characters");
        }

        this.name = serverName;
        this.uuid = uuid;
    }

    /**
     * Create a server configuration from a given server name, using a random UUID
     *
     * @param serverName The server name
     */
    public ServerConfiguration(@NonNull final String serverName) {
        this(serverName, UUID.randomUUID());
    }
}
