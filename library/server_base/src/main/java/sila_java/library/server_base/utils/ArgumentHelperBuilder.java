package sila_java.library.server_base.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.annotation.Nullable;
import java.util.ArrayList;

/**
 * Utility class to programmatically build a {@link ArgumentHelper}
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class ArgumentHelperBuilder {

    /**
     * Build a {@link ArgumentHelper} from provided arguments
     * @param port the server port
     * @param serverName the server name
     * @param configPath the server configuration path
     * @param netInterface the server network interface
     * @return a new {@link ArgumentHelper} instance
     */
    public static ArgumentHelper build(
            final int port,
            final @NonNull String serverName,
            final @Nullable String configPath,
            final @Nullable String netInterface
    ) {
        return new ArgumentHelper(toArgs(port, configPath, netInterface), serverName);
    }

    /**
     * Converts provided parameters into CLI arguments
     * @param port the server port
     * @param configPath the server configuration path
     * @param netInterface the network interface
     * @return CLI arguments
     */
    public static String[] toArgs(
            final int port,
            final @Nullable String configPath,
            final @Nullable String netInterface
    ) {
        final ArrayList<String> args = new ArrayList<>();
        args.add("-p");
        args.add(Integer.toString(port));
        if (netInterface != null && !netInterface.isEmpty()) {
            args.add("-n");
            args.add(netInterface);
        }
        if (configPath != null) {
            args.add("-c");
            args.add(configPath);
        }
        return args.toArray(new String[0]);
    }
}
