package sila_java.library.server_base;

import io.grpc.Context;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Set;

/**
 * Constants class
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class Constants {
    /**
     * Constant context metadata identifier key
     */
    public static final Context.Key<Set<FullyQualifiedMetadataContextKey<byte[]>>> METADATA_IDENTIFIERS_CTX_KEY = Context.key("metadataFQIContextKeys");
}
