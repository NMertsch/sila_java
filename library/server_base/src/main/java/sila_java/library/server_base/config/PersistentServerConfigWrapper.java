package sila_java.library.server_base.config;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import lombok.val;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.UUID;

/**
 * Persistent server configuration container to load & save configuration from a file
 */
@Slf4j
public class PersistentServerConfigWrapper implements IServerConfigWrapper {
    private final Gson gson;
    private final File dbFile;
    private ServerConfiguration configuration;

    /**
     * If present and valid, the config specified will be used
     * Otherwise it will be created and filled with default generated config
     * @param configFile The path to the config file
     * @param defaultName The default name to use in case the config does not exist or is invalid
     * @throws IOException if unable to create and or save the configuration
     */
    public PersistentServerConfigWrapper(
            @NonNull final Path configFile,
            @NonNull final String defaultName
    ) throws IOException {
        this.dbFile = setupConfigFile(configFile);
        this.gson = new GsonBuilder().setPrettyPrinting().create();
        try {
            retrieveConfiguration();
        } catch (final RuntimeException e) {
            log.info("No existing Server Configuration found.");
            this.setConfig(new ServerConfiguration(defaultName));
        }
        if (this.configuration == null) {
            log.error("Unable to use and or save config file at " + configFile);
            throw new IOException("Cannot use and or save config in file " + configFile);
        }
    }

    /**
     * Create or update configuration with the one provided
     * @param configFile The path to the config file
     * @param serverConfiguration The server configuration to create and or save
     * @throws IOException if unable to create and or save the configuration
     */
    public PersistentServerConfigWrapper(
            @NonNull final Path configFile,
            @NonNull final ServerConfiguration serverConfiguration
    ) throws IOException {
        this.dbFile = setupConfigFile(configFile);
        this.gson = new GsonBuilder().setPrettyPrinting().create();
        try {
            retrieveConfiguration();
        } catch (final RuntimeException e) {
            log.info("No existing Server Configuration found.");
            this.setConfig(serverConfiguration);
        }
        if (this.configuration == null) {
            log.error("Unable to use and or save config file at " + configFile);
            throw new IOException("Cannot use and or save config in file " + configFile);
        }
    }

    /**
     * If present and valid, the config specified will be used
     * Otherwise it will be created and filled with default generated config
     * @param configFile The path to the config file
     * @param defaultName The default name to use in case the config does not exist or is invalid
     * @param serverUUID The server UUID to set
     * @throws IOException if unable to create and or save the configuration
     */
    public PersistentServerConfigWrapper(
            @NonNull final Path configFile,
            @NonNull final String defaultName,
            @NonNull final UUID serverUUID
    ) throws IOException {
        this.dbFile = setupConfigFile(configFile);
        this.gson = new GsonBuilder().setPrettyPrinting().create();
        try {
            retrieveConfiguration();
            this.setConfig(new ServerConfiguration(this.configuration.getName(), serverUUID));
        } catch (final RuntimeException e) {
            log.info("No existing Server Configuration found.");
            this.setConfig(new ServerConfiguration(defaultName, serverUUID));
        }
        if (this.configuration == null) {
            log.error("Unable to use and or save config file at " + configFile);
            throw new IOException("Cannot use and or save config in file " + configFile);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ServerConfiguration getCacheConfig() {
        if (configuration == null) {
            throw new IllegalStateException("Server Configuration not yet loaded, use retrieveConfiguration!");
        }
        return configuration;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setConfig(@NonNull final ServerConfiguration serverConfiguration) throws IOException {
        this.configuration = serverConfiguration;
        log.info("Setting serverId: {} and serverName: {}", this.configuration.getUuid(), this.configuration.getName());
        this.writeConfiguration();
    }

    /**
     * Write the cache configuration into the DB location
     */
    private void writeConfiguration() throws IOException {
        try (val writer = new FileWriter(dbFile)) {
            gson.toJson(configuration, writer);
            writer.flush();
        }
    }

    /**
     * Read a config from DB location
     *
     * @implNote Retrieve the configuration from the database into the cache
     */
    private void retrieveConfiguration() {
        try (final FileReader reader = new FileReader(dbFile)) {
            final ServerConfiguration serverConfiguration = gson.fromJson(reader, ServerConfiguration.class);

            if (serverConfiguration.getName() == null || serverConfiguration.getUuid() == null) {
                throw new JsonParseException("At least one of the config parameters has no value.");
            }
            this.configuration = serverConfiguration;
        } catch (IOException e) {
            throw new RuntimeException("Internal Logic error, persistence file not created.");
        }
    }

    /**
     * Prepare the configuration file with an empty configuration if the file is not present
     * @param configFile path to the configuration file
     * @return The configuration file
     * @throws IOException If unable to retrieve the configuration or write to the file
     */
    @SuppressWarnings("UnusedReturnValue")
    private File setupConfigFile(@NonNull final Path configFile) throws IOException {
        final File dbFile = configFile.toAbsolutePath().toFile();
        if (!dbFile.exists()) {
            dbFile.getParentFile().mkdirs();
            if(!dbFile.createNewFile()){
                throw new IOException("Failed to create config file: " + dbFile);
            }
            Files.write(dbFile.toPath(), "{}".getBytes());
        }
        return (dbFile);
    }
}