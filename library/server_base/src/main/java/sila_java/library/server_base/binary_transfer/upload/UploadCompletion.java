package sila_java.library.server_base.binary_transfer.upload;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;

import java.io.*;
import java.util.*;

/**
 * Upload completion class.
 * Tracks the completion of a binary upload
 */
@Slf4j
@Getter(value = AccessLevel.PACKAGE)
class UploadCompletion implements AutoCloseable {
    /**
     * File extension to append to binary chunk file name
     */
    private static final String CHUNK_FILE_EXT = ".chunk";
    private final SortedMap<Integer, File> chunks = new TreeMap<>(Comparator.naturalOrder());
    private final long totalLength;
    private final int totalChunks;

    /**
     * Constructor
     *
     * @param totalLength the total expected length to be uploaded
     * @param nbChunk the number of expected chunk to be uploaded
     */
    UploadCompletion(final long totalLength, final int nbChunk) {
        this.totalLength = totalLength;
        this.totalChunks = nbChunk;
    }

    /**
     * Add a chunk to the completion tracker and save it on the device
     *
     * @param chunkIndex The chunk index to add
     * @param data The chunk data to add and save
     *
     * @throws IOException If unable to save the chunk on the disk
     */
    @SuppressWarnings("ResultOfMethodCallIgnored") // ignore result of File.delete
    void addChunk(final int chunkIndex, final byte[] data) throws IOException {
        if (this.isComplete()) {
            throw new IllegalStateException("Download is complete");
        }
        if (data.length == 0) {
            throw new IllegalArgumentException("Chunk is empty");
        }
        if (chunkIndex < 0 || chunkIndex >= this.totalChunks) {
            throw new IllegalArgumentException("Invalid chunk index `" + chunkIndex + "`");
        }
        final File previousEntry = this.chunks.put(chunkIndex, saveChunkToFile(data));
        if (previousEntry != null) {
            previousEntry.delete();
        }
    }

    /**
     * Tells if the upload is complete
     * @return true if the download is complete, false otherwise
     */
    boolean isComplete() {
        return this.chunks.size() == this.totalChunks && this.totalLength == getTotalChunksLength();
    }

    /**
     * Get the total cumulated chunks length
     *
     * @return the cumulative length of the chunk
     */
    private long getTotalChunksLength() {
        return this.chunks.values().stream().mapToLong(File::length).sum();
    }

    /**
     * Create an input stream of all the chunks in ascending chunk index order
     * @return Input stream of all the chunks in ascending chunk index order
     * @throws IOException if a chunk is missing or not readable
     *
     * @implNote uses {@link SequenceInputStream} internally
     */
    InputStream chunksToStream() throws IOException {
        final List<InputStream> streams = new ArrayList<>(this.chunks.size());
        try {
            for (final Map.Entry<Integer, File> entry : this.chunks.entrySet()) {
                streams.add(entry.getKey(), new FileInputStream(entry.getValue()));
            }
        } catch (FileNotFoundException e) {
            for (final InputStream stream : streams) {
                try {
                    stream.close();
                } catch (IOException e1) {
                    log.debug("Exception occurred while closing inputstream: {}", e1.getMessage(), e1);
                }
            }
            throw new IOException(e);
        }
        return new SequenceInputStream(Collections.enumeration(streams));
    }

    /**
     * Save the chunk data on the device
     * @param data The data to save
     * @return The file containing the saved data
     * @throws IOException If enable to create or save the file
     */
    @SuppressWarnings("ResultOfMethodCallIgnored") // ignore result of File.delete
    private static File saveChunkToFile(final byte[] data) throws IOException {
        final File tempFile = File.createTempFile(UUID.randomUUID().toString(), CHUNK_FILE_EXT);
        tempFile.deleteOnExit();
        try {
            FileUtils.writeByteArrayToFile(tempFile, data);
        } catch (IOException e) {
            tempFile.delete();
            throw e;
        }
        return tempFile;
    }

    /**
     * Remove chunks file stored on the device and {@link UploadCompletion#chunks} )}
     */
    @SuppressWarnings("ResultOfMethodCallIgnored") // ignore result of File.delete
    @Override
    public void close() {
        this.chunks.values().forEach(File::delete);
        this.chunks.clear();
    }
}