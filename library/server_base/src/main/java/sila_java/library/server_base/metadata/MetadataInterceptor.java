package sila_java.library.server_base.metadata;

import io.grpc.ClientCall;
import io.grpc.Context;
import io.grpc.Contexts;
import io.grpc.ForwardingServerCallListener;
import io.grpc.Metadata;
import io.grpc.ServerCall;
import io.grpc.ServerCallHandler;
import io.grpc.ServerInterceptor;
import io.grpc.StatusRuntimeException;
import lombok.NonNull;
import sila2.org.silastandard.SiLABinaryTransfer;
import sila_java.library.core.sila.binary_transfer.BinaryTransferErrorHandler;
import sila_java.library.core.sila.errors.SiLAErrorException;
import sila_java.library.core.sila.errors.SiLAErrors;
import sila_java.library.core.sila.utils.FullyQualifiedIdentifierUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public abstract class MetadataInterceptor implements ServerInterceptor {
    private final List<Pattern> affectedCallTargetPatterns;
    private final List<String> affectedCallIdentifiers;

    /**
     * Handle received SiLA Client Metadata
     *
     * @param call incoming call
     * @param metadata received SiLA Client Metadata
     * @param <ReqT> request type
     * @param <RespT> response type
     *
     * @return context for the next call (usually {@link Context#current})
     *
     * @implNote The returned {@link Context} is ignored when handling metadata during SiLA Binary Upload
     */
    public abstract <ReqT, RespT> Context intercept(
            final ServerCall<ReqT, RespT> call, ServerMetadataContainer metadata);

    public MetadataInterceptor(@NonNull final List<String> affectedCalls) {
        this.affectedCallIdentifiers = new ArrayList<>(affectedCalls);
        this.affectedCallTargetPatterns = getAffectedCallTargetPatterns(affectedCalls);
    }

    public MetadataInterceptor(@NonNull final String affectedCall) {
        this(Collections.singletonList(affectedCall));
    }

    /**
     * The actual interception logic: Calls {@link MetadataInterceptor#intercept(ServerCall, ServerMetadataContainer)}
     * and forwards the returned context to the next call handler. On error, the appropriate SiLA Error is issued.
     *
     * @param call object to receive response messages
     * @param headers which can contain extra call metadata from {@link ClientCall#start}, e.g. authentication
     *         credentials.
     * @param next next processor in the interceptor chain
     * @param <ReqT> request type
     * @param <RespT> response type
     *
     * @return next call listener
     */
    @Override
    public final <ReqT, RespT> ServerCall.Listener<ReqT> interceptCall(
            final ServerCall<ReqT, RespT> call, final Metadata headers, final ServerCallHandler<ReqT, RespT> next) {
        final String callTarget = call.getMethodDescriptor().getFullMethodName();

        // if binary upload: check if metadata affects target parameter of the upload
        if (callTarget.equals("sila2.org.silastandard.BinaryUpload/CreateBinary")) {
            return new ForwardingServerCallListener<ReqT>() {
                /**
                 * Listener which does nothing with the received call, so it won't fail if the call was closed
                 */
                private final ServerCall.Listener<ReqT> NOOP_LISTENER = new ServerCall.Listener<ReqT>() {};
                private ServerCall.Listener<ReqT> delegate = next.startCall(call, headers);

                /**
                 * {@inheritDoc}
                 */
                @Override
                protected ServerCall.Listener<ReqT> delegate() {
                    return delegate;
                }

                /**
                 * {@inheritDoc}
                 */
                @Override
                public void onMessage(final ReqT message) {
                    SiLABinaryTransfer.CreateBinaryRequest request = (SiLABinaryTransfer.CreateBinaryRequest) message;
                    if (affectedCallIdentifiers.stream()
                            .anyMatch(identifier -> request.getParameterIdentifier().startsWith(identifier))) {
                        try {
                            intercept(call, getMetadata(headers));  // ignore the returned context during binary upload
                        } catch (Exception e) {
                            if (e instanceof StatusRuntimeException) {
                                call.close(((StatusRuntimeException) e).getStatus(), headers);
                            } else {
                                call.close(BinaryTransferErrorHandler.generateBinaryTransferError(SiLABinaryTransfer.BinaryTransferError.ErrorType.BINARY_UPLOAD_FAILED,
                                                                                                  e.getMessage()
                                ).getStatus(), headers);
                            }
                            delegate = NOOP_LISTENER;
                            return;
                        }
                    }
                    super.onMessage(message);
                }
            };
        }

        // if call is not affected: forward to next call handler
        if (this.affectedCallTargetPatterns.stream().noneMatch(pattern -> pattern.matcher(callTarget).matches())) {
            return Contexts.interceptCall(Context.current(), call, headers, next);
        }

        // apply interceptor logic
        try {
            final Context newContext = intercept(call, getMetadata(headers));
            return Contexts.interceptCall(newContext, call, headers, next);
        } catch (final Throwable e) {
            call.close(new SiLAErrorException(SiLAErrors.throwableToSiLAError(e)).getStatus(), headers);
            return new ServerCall.Listener<ReqT>() {};  // don't hand closed call to the next handler
        }
    }

    /**
     * If another interceptor already attached metadata, use it. Else, parse metadata from headers.
     */
    private ServerMetadataContainer getMetadata(Metadata headers) {
        if (ServerMetadataContainer.current() == null) {
            return ServerMetadataContainer.fromHeaders(headers);
        }
        return ServerMetadataContainer.current();
    }

    /**
     * Convert fully qualified feature, command, and property identifiers to regex patterns for matching incoming gRPC
     * call method names
     *
     * @return Regex patterns for matching gRPC call method names
     */
    private static List<Pattern> getAffectedCallTargetPatterns(@NonNull final List<String> affectedCalls) {
        return affectedCalls.stream().map(MetadataInterceptor::getAffectedCallTargetPattern)
                .collect(Collectors.toList());
    }

    /**
     * Convert fully qualified feature, command, and property identifiers to regex pattern for matching incoming gRPC
     * call method names
     *
     * @return Regex patterns for matching gRPC call method names
     */
    private static Pattern getAffectedCallTargetPattern(String affectedCall) {
        final Matcher featureIdentifierMatcher
                = FullyQualifiedIdentifierUtils.FullyQualifiedFeatureIdentifierPattern.matcher(affectedCall);
        final Matcher commandIdentifierMatcher
                = FullyQualifiedIdentifierUtils.FullyQualifiedCommandIdentifierPattern.matcher(affectedCall);
        final Matcher propertyIdentifierMatcher
                = FullyQualifiedIdentifierUtils.FullyQualifiedPropertyIdentifierPattern.matcher(affectedCall);

        if (!(featureIdentifierMatcher.matches() || commandIdentifierMatcher.matches() ||
                propertyIdentifierMatcher.matches())) {
            throw new IllegalArgumentException(String.format(
                    "Given argument is no fully qualified feature, command, or property identifier: '%s'",
                    affectedCall
            ));
        }

        final String[] affectedCallElements = affectedCall.split("/");
        final String originator = affectedCallElements[0];
        final String category = affectedCallElements[1];
        final String featureIdentifier = affectedCallElements[2];
        final String majorVersion = affectedCallElements[3];

        String callPrefix = String.join(
                "\\.",
                "sila2",
                originator,
                category,
                featureIdentifier.toLowerCase(),
                majorVersion,
                featureIdentifier
        );

        if (commandIdentifierMatcher.matches()) {
            // command identifier, e.g.
            // org.silastandard/core/SiLAService/v1/Command/GetFeatureDefinition
            callPrefix += "/" + affectedCall.split("/")[5];
        } else if (propertyIdentifierMatcher.matches()) {
            // property identifier, e.g.
            // org.silastandard/core/SiLAService/v1/Property/ImplementedFeatures
            callPrefix += "/(Get_|Subscribe_)" + affectedCall.split("/")[5];
        } else {
            // feature identifier, e.g. org.silastandard/core/SiLAService/v1
            callPrefix += "/(Get_|Subscribe_)?" + "[A-Z][a-zA-Z0-9]*";
        }
        return Pattern.compile(callPrefix);
    }
}
