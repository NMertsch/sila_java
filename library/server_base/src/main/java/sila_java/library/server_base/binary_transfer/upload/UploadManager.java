package sila_java.library.server_base.binary_transfer.upload;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import net.jodah.expiringmap.ExpirationPolicy;
import net.jodah.expiringmap.ExpiringMap;

import java.io.IOException;
import java.time.Duration;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Upload manager
 */
@Slf4j
class UploadManager {
    /**
     * Map holding upload completion that automatically remove expired entries
     */
    private final ExpiringMap<UUID, UploadCompletion> uploads = ExpiringMap
            .builder()
            .variableExpiration()
            .asyncExpirationListener(UploadManager::onEntryExpiration)
            .build();

    /**
     * Remove the upload completion associated with the binary identifier
     * @param binaryId The binary identifier
     * @return The removed uplaod completion
     */
    UploadCompletion removeUpload(@NonNull final UUID binaryId) {
        return this.uploads.remove(binaryId);
    }

    /**
     * Get upload completion associated with the binary identifier
     * @param binaryId The binary identifier
     * @return the upload completion with the specified identifier
     */
    UploadCompletion getUpload(@NonNull final UUID binaryId) {
        return (this.uploads.get(binaryId));
    }

    /**
     * Update a binary upload
     * @param binaryId The binary identifier
     * @param chunkIndex The chunk index
     * @param data The binary data
     * @param expiration The upload expiration
     */
    UploadCompletion updateUpload(
            @NonNull final UUID binaryId,
            final int chunkIndex,
            @NonNull final byte[] data,
            @NonNull final Duration expiration
    ) throws IOException {
        final UploadCompletion upload = this.getUpload(binaryId);
        this.uploads.setExpiration(expiration.getSeconds(), TimeUnit.SECONDS);
        upload.addChunk(chunkIndex, data);
        return upload;
    }

    /**
     * Add a binary upload
     * @param binaryId The binary identifier
     * @param totalLength The total binary length in bytes
     * @param nbChunks The number of chunks
     * @param expiration The upload expiration
     */
    void addUpload(
            @NonNull final UUID binaryId,
            final long totalLength,
            final int nbChunks,
            @NonNull final Duration expiration
    ) {
        if (!this.uploads.containsKey(binaryId)) {
            this.uploads.put(binaryId,
                    new UploadCompletion(totalLength, nbChunks),
                    ExpirationPolicy.CREATED,
                    expiration.getSeconds(),
                    TimeUnit.SECONDS);
        } else {
            throw new IllegalStateException("Upload is already present in the upload manager");
        }
    }

    /**
     * Callback function called when an entry is added in {@link UploadManager#uploads } that close the upload completion
     * @param uploadId The upload id
     * @param uploadCompletion The upload completion
     */
    private static void onEntryExpiration(final UUID uploadId, final UploadCompletion uploadCompletion) {
        log.info("Client upload with id {} expired", uploadId);
        uploadCompletion.close();
    }
}
