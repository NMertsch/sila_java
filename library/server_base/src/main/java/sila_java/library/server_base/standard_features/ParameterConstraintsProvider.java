package sila_java.library.server_base.standard_features;

import io.grpc.BindableService;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.core.commands.parameterconstraintsprovider.v1.ParameterConstraintsProviderGrpc;
import sila2.org.silastandard.core.commands.parameterconstraintsprovider.v1.ParameterConstraintsProviderOuterClass.Subscribe_ParametersConstraints_Parameters;
import sila2.org.silastandard.core.commands.parameterconstraintsprovider.v1.ParameterConstraintsProviderOuterClass.Subscribe_ParametersConstraints_Responses;
import sila2.org.silastandard.core.commands.parameterconstraintsprovider.v1.ParameterConstraintsProviderOuterClass.Subscribe_ParametersConstraints_Responses.ParametersConstraints_Struct;
import sila_java.library.core.models.Constraints;
import sila_java.library.core.sila.errors.SiLAErrors;
import sila_java.library.sila_base.EmptyClass;

import javax.xml.bind.*;
import javax.xml.namespace.QName;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import static sila_java.library.core.utils.FileUtils.getFileContent;

/**
 * Parameter constraints provider core feature implementation
 */
@Slf4j
public class ParameterConstraintsProvider implements FeatureImplementation, AutoCloseable {
    private final Map<String, Constraints> constraints = new ConcurrentHashMap<>();
    private final List<StreamObserver<Subscribe_ParametersConstraints_Responses>> constraintUpdateObservers = new CopyOnWriteArrayList<>();

    /**
     * @inheritDoc
     */
    @Override
    @SneakyThrows(IOException.class)
    public String getFeatureDescription() {
        return getFileContent(EmptyClass.class.getResourceAsStream(
                "/sila_base/feature_definitions/org/silastandard/core/commands/ParameterConstraintsProvider.sila.xml"
        ));
    }

    /**
     * @inheritDoc
     */
    @Override
    public BindableService getService() {
        return new ParameterConstraintsProvider.ServiceImpl();
    }

    /**
     * complete observers stream and clear {@link ParameterConstraintsProvider#constraintUpdateObservers}
     *
     * @inheritDoc
     */
    @Override
    synchronized public void close() {
        this.constraintUpdateObservers.forEach(observer -> {
            try {
                observer.onCompleted();
            } catch (final StatusRuntimeException e) {
                log.warn(e.getMessage(), e);
            }
        });
        this.constraintUpdateObservers.clear();
    }

    /**
     * Parameter constraints provider service implementation
     */
    private class ServiceImpl extends ParameterConstraintsProviderGrpc.ParameterConstraintsProviderImplBase {

        /**
         * A new observer and notify him of the current constraints
         * @param request the request
         * @param responseObserver the response observer
         */
        @Override
        public void subscribeParametersConstraints(
                final Subscribe_ParametersConstraints_Parameters request,
                final StreamObserver<Subscribe_ParametersConstraints_Responses> responseObserver
        ) {
            try {
                if (notifyObserver(responseObserver, getConstraintsResponse())) {
                    return;
                }
            } catch (JAXBException e) {
                throw SiLAErrors.generateGenericExecutionError(e);
            }
            constraintUpdateObservers.add(responseObserver);
        }
    }

    /**
     * Get parameters constraints
     *
     * @return an unmodifiable map of constraints
     */
    public Map<String, Constraints> getParametersConstraints() {
        return Collections.unmodifiableMap(constraints);
    }

    /**
     * Insert a constraint to a parameter
     * @param parameterId the parameter identifier
     * @param constraints the constraints
     */
    public void putConstraint(
            @NonNull final String parameterId,
            @NonNull final Constraints constraints
    ) {
        // todo check if parameterId is fully qualified
        if (this.constraints.containsKey(parameterId)) {
            log.debug("Parameter {} already had a constraint, overwriting it", parameterId);
        }
        this.constraints.put(parameterId, constraints);
        this.notifyObservers();
    }

    /**
     * Remove a parameter constraint
     * @param parameterId the parameter identifier
     */
    public void removeConstraint(@NonNull final String parameterId) {
        this.constraints.remove(parameterId);
        this.notifyObservers();
    }

    /**
     * Notify an Observer
     * @param observer An observer
     * @param response The response to notify
     * @return True if an error occurred, else false
     */
    private boolean notifyObserver(
            @NonNull final StreamObserver<Subscribe_ParametersConstraints_Responses> observer,
            @NonNull final Subscribe_ParametersConstraints_Responses response
    ) {
        try {
            observer.onNext(response);
        } catch (final Throwable e) {
            observer.onError(e);
            log.debug("Lost connection with constraints property observer", e);
            return (true);
        }
        return (false);
    }

    /**
     * Notify all observers with the current constraints
     */
    private void notifyObservers() {
        final Subscribe_ParametersConstraints_Responses response;
        try {
            response = getConstraintsResponse();
        } catch (final JAXBException e) {
            throw SiLAErrors.generateGenericExecutionError(e);
        }

        this.constraintUpdateObservers.removeIf(next -> notifyObserver(next, response));
    }

    /**
     * Build {@link Subscribe_ParametersConstraints_Responses} from {@link ParameterConstraintsProvider#constraints}
     * @return {@link Subscribe_ParametersConstraints_Responses}
     * @throws JAXBException if {@link ParameterConstraintsProvider#constraints} cannot be converted into an XML string
     */
    private Subscribe_ParametersConstraints_Responses getConstraintsResponse() throws JAXBException {
        final Subscribe_ParametersConstraints_Responses.Builder builder = Subscribe_ParametersConstraints_Responses.newBuilder();
        for (final Map.Entry<String, Constraints> entry : this.constraints.entrySet()) {
            builder.addParametersConstraints(
                    ParametersConstraints_Struct
                            .newBuilder()
                            .setCommandParameterIdentifier(SiLAFramework.String.newBuilder().setValue(entry.getKey()).build())
                            .setConstraints(SiLAFramework.String.newBuilder().setValue(constraintToXml(entry.getValue())).build())
                            .build()
            );
        }
        return builder.build();
    }


    /**
     * Utility to create XML constraints
     *
     * @param constraints The constraints model
     * @return an XML string representation of the constraints model
     * @throws JAXBException if the model cannot be converted into an XML string
     */
    private static String constraintToXml(@NonNull final Constraints constraints) throws JAXBException {
        final Marshaller jaxbMarshaller = JAXBContext.newInstance(Constraints.class).createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        final StringWriter stringWriter = new StringWriter();
        final QName qName = new QName("", Constraints.class.getSimpleName());
        final JAXBElement<Constraints> root = new JAXBElement<>(qName, Constraints.class, constraints);
        jaxbMarshaller.marshal(root, stringWriter);
        return stringWriter.toString();
    }
}
