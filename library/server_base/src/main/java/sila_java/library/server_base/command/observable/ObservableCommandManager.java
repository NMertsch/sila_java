package sila_java.library.server_base.command.observable;

import io.grpc.StatusRuntimeException;
import io.grpc.stub.ServerCallStreamObserver;
import io.grpc.stub.StreamObserver;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila_java.library.core.sila.errors.SiLAErrors;

import javax.annotation.Nullable;
import java.time.Duration;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.*;

/**
 * Observable command manager
 * @param <ParamType> The command parameter type
 * @param <ResultType> The command result type
 */
@Slf4j
public class ObservableCommandManager<ParamType, ResultType> implements AutoCloseable {
    private final Map<UUID, ObservableCommandWrapper<ParamType, ResultType>> commands = new ConcurrentHashMap<>();
    private final ScheduledExecutorService scheduledExecutor = Executors.newSingleThreadScheduledExecutor();
    private final RunnableCommandTask<ParamType, ResultType> task;
    private final ObservableCommandTaskRunner runner;
    private final Duration lifeTimeOfCommandExecution;

    /**
     * Constructor
     * @param taskRunner The runner
     * @param task The task that each commands will run
     *
     * Commands will never expire unless removed manually
     */
    public ObservableCommandManager(
            @NonNull final ObservableCommandTaskRunner taskRunner,
            @NonNull final RunnableCommandTask<ParamType, ResultType> task
    ) {
        this(taskRunner, task, null);
    }

    /**
     * Constructor
     * @param taskRunner The runner
     * @param task The task that each commands will run
     * @param lifeTimeOfCommandExecution The duration after which a command can be removed from the manager
     *                                   If null the commands will never expire unless removed manually
     */
    public ObservableCommandManager(
            @NonNull final ObservableCommandTaskRunner taskRunner,
            @NonNull final RunnableCommandTask<ParamType, ResultType> task,
            @Nullable final Duration lifeTimeOfCommandExecution
    ) {
        if (lifeTimeOfCommandExecution != null &&
                (lifeTimeOfCommandExecution.isNegative() || lifeTimeOfCommandExecution.isZero())) {
            throw new IllegalArgumentException("LifeTimeOfCommandExecution duration must be greater than 0");
        }
        this.task = task;
        this.runner = taskRunner;
        this.lifeTimeOfCommandExecution = lifeTimeOfCommandExecution;
    }

    /**
     * Add a command to the manager
     * @param param The command parameter
     * @param observer The command confirmation stream observer of the command
     * @return A wrapped observable command
     */
    public ObservableCommandWrapper<ParamType, ResultType> addCommand(
            @NonNull final ParamType param,
            @NonNull final StreamObserver<SiLAFramework.CommandConfirmation> observer
    ) throws StatusRuntimeException {
        try {
            final ObservableCommandWrapper<ParamType, ResultType> command = new ObservableCommandWrapper<>(
                    param,
                    this.task,
                    this.runner,
                    this::remove,
                    this.scheduledExecutor,
                    this.lifeTimeOfCommandExecution
            );
            this.notifyNewConcurrentCommand(command);
            this.commands.put(command.getExecutionId(), command);

            if (observer instanceof ServerCallStreamObserver) {
                // Note: This is an experimental API by gRPC and you can't know if cancelHandler is being overwritten
                ((ServerCallStreamObserver<ResultType>) observer).setOnCancelHandler(() -> this.remove(command.getExecutionId()));
            } else {
                log.warn("Current stream observer implementation does not allow to check reception of command UUID");
            }

            observer.onNext(command.getCommandConfirmation());
            observer.onCompleted();

            return command;
        } catch (final RejectedExecutionException e) {
            throw SiLAErrors.generateGenericExecutionError(e);
        }
    }

    /**
     * Retrieve a command from the manager
     * @param executionId The command identifier
     * @return The command
     * @throws StatusRuntimeException if the command does not exist
     */
    public ObservableCommandWrapper<ParamType, ResultType> get(
            @NonNull final SiLAFramework.CommandExecutionUUID executionId
    ) throws StatusRuntimeException {
        final UUID uuid;
        try {
            uuid = UUID.fromString(executionId.getValue());
        } catch (final IllegalArgumentException e) {
            throw SiLAErrors.generateGenericExecutionError(e);
        }
        return this.get(uuid);
    }

    /**
     * Retrieve a command from the manager
     * @param executionId The command identifier
     * @return The command
     * @throws StatusRuntimeException if the command does not exist
     */
    public ObservableCommandWrapper<ParamType, ResultType> get(@NonNull final UUID executionId) throws StatusRuntimeException {
        final ObservableCommandWrapper<ParamType, ResultType> command = this.commands.get(executionId);

        if (command == null)
            throw SiLAErrors.generateFrameworkError(
                    SiLAFramework.FrameworkError.ErrorType.INVALID_COMMAND_EXECUTION_UUID,
                    "The Command Execution UUID is not valid. There is no command executed with the UUID."
            );
        return command;
    }

    /**
     * Close all resources owned by the manager
     * After this call the manager won't be usable anymore
     */
    @Override
    public void close() {
        this.commands.values().forEach(ObservableCommandWrapper::close);
        this.commands.clear();
        this.runner.close();
        this.scheduledExecutor.shutdownNow();
    }

    /**
     * Remove a command from the manager
     */
    public void remove(@NonNull final UUID executionId) {
        final ObservableCommandWrapper<ParamType, ResultType> command = this.commands.get(executionId);
        if (command != null) {
            command.close();
            this.commands.remove(executionId);
        }
    }

    /**
     * Notify commands that a new command has been added into the manager
     * @param command The command added into the manager
     */
    private void notifyNewConcurrentCommand(@NonNull final ObservableCommandWrapper<ParamType, ResultType> command) {
        this.commands.values().forEach(c -> c.getTask().onNewCommand(command));
    }
}
