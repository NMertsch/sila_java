package sila_java.library.server_base.utils;

import com.google.common.util.concurrent.MoreExecutors;
import com.google.common.util.concurrent.SettableFuture;
import io.grpc.*;
import io.grpc.internal.SerializingExecutor;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import sila_java.library.core.sila.errors.SiLAErrors;

import java.util.concurrent.ExecutionException;
import javax.annotation.Nullable;

/**
 * Transmit Throwable as {@link StatusRuntimeException}
 *
 * Almost identical to TransmitStatusRuntimeExceptionInterceptor
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class TransmitThrowableInterceptor implements ServerInterceptor {

    public static ServerInterceptor instance() {
        return new TransmitThrowableInterceptor();
    }

    public <ReqT, RespT> ServerCall.Listener<ReqT> interceptCall(
            ServerCall<ReqT, RespT> call, Metadata headers,
            ServerCallHandler<ReqT, RespT> next
    ) {
        final ServerCall<ReqT, RespT> serverCall = new TransmitThrowableInterceptor.SerializingServerCall(call);
        ServerCall.Listener<ReqT> listener = next.startCall(serverCall, headers);
        return new ForwardingServerCallListener.SimpleForwardingServerCallListener<ReqT>(listener) {
            public void onMessage(ReqT message) {
                try {
                    super.onMessage(message);
                } catch (Throwable e) {
                    this.closeWithException(e);
                }
            }

            public void onHalfClose() {
                try {
                    super.onHalfClose();
                } catch (Throwable e) {
                    this.closeWithException(e);
                }
            }

            public void onCancel() {
                try {
                    super.onCancel();
                } catch (Throwable e) {
                    this.closeWithException(e);
                }
            }

            public void onComplete() {
                try {
                    super.onComplete();
                } catch (Throwable e) {
                    this.closeWithException(e);
                }
            }

            public void onReady() {
                try {
                    super.onReady();
                } catch (Throwable e) {
                    this.closeWithException(e);
                }
            }

            private void closeWithException(Throwable t) {
                if (!(t instanceof StatusRuntimeException)) {
                    t = SiLAErrors.generateGenericExecutionError(t);
                }
                Metadata metadata = ((StatusRuntimeException) t).getTrailers();
                if (metadata == null) {
                    metadata = new Metadata();
                }
                serverCall.close(((StatusRuntimeException) t).getStatus(), metadata);
            }
        };
    }

    private static class SerializingServerCall<ReqT, RespT> extends ForwardingServerCall.SimpleForwardingServerCall<ReqT, RespT> {
        private final SerializingExecutor serializingExecutor = new SerializingExecutor(MoreExecutors.directExecutor());
        private boolean closeCalled = false;

        SerializingServerCall(ServerCall<ReqT, RespT> delegate) {
            super(delegate);
        }

        public void sendMessage(final RespT message) {
            this.serializingExecutor.execute(() -> SerializingServerCall.super.sendMessage(message));
        }

        public void request(final int numMessages) {
            this.serializingExecutor.execute(() -> SerializingServerCall.super.request(numMessages));
        }

        public void sendHeaders(final Metadata headers) {
            this.serializingExecutor.execute(() -> SerializingServerCall.super.sendHeaders(headers));
        }

        public void close(final Status status, final Metadata trailers) {
            this.serializingExecutor.execute(() -> {
                if (!SerializingServerCall.this.closeCalled) {
                    SerializingServerCall.this.closeCalled = true;
                    SerializingServerCall.super.close(status, trailers);
                }
            });
        }

        public boolean isReady() {
            final SettableFuture<Boolean> future = SettableFuture.create();
            this.serializingExecutor.execute(() -> future.set(SerializingServerCall.super.isReady()));

            try {
                return future.get();
            } catch (InterruptedException | ExecutionException e) {
                throw new RuntimeException("Encountered error during serialized access", e);
            }
        }

        public boolean isCancelled() {
            final SettableFuture<Boolean> future = SettableFuture.create();
            this.serializingExecutor.execute(() -> future.set(SerializingServerCall.super.isCancelled()));

            try {
                return future.get();
            } catch (InterruptedException | ExecutionException e) {
                throw new RuntimeException("Encountered error during serialized access", e);
            }
        }

        public void setMessageCompression(final boolean enabled) {
            this.serializingExecutor.execute(() -> SerializingServerCall.super.setMessageCompression(enabled));
        }

        public void setCompression(final String compressor) {
            this.serializingExecutor.execute(() -> SerializingServerCall.super.setCompression(compressor));
        }

        public Attributes getAttributes() {
            final SettableFuture<Attributes> future = SettableFuture.create();
            this.serializingExecutor.execute(() -> future.set(SerializingServerCall.super.getAttributes()));

            try {
                return future.get();
            } catch (InterruptedException | ExecutionException e) {
                throw new RuntimeException("Encountered error during serialized access", e);
            }
        }

        @Nullable
        public String getAuthority() {
            final SettableFuture<String> future = SettableFuture.create();
            this.serializingExecutor.execute(() -> future.set(SerializingServerCall.super.getAuthority()));

            try {
                return future.get();
            } catch (InterruptedException | ExecutionException e) {
                throw new RuntimeException("Encountered error during serialized access", e);
            }
        }
    }
}
