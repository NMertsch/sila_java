package sila_java.library.manager.server_management;

import com.google.protobuf.Descriptors;
import io.grpc.ConnectivityState;
import io.grpc.ManagedChannel;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import sila_java.library.core.sila.mapping.feature.MalformedSiLAFeature;
import sila_java.library.core.sila.mapping.grpc.ProtoMapper;
import sila_java.library.manager.models.Server;

import java.security.KeyException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Represent a SiLA compliant connection with a server that exposes features callable through a channel
 */
@Slf4j
public class Connection implements AutoCloseable {
    private final UUID serverId;
    private final Map<String, Descriptors.ServiceDescriptor> featureMap = new HashMap<>();
    @Getter
    private final Map<String, Descriptors.FileDescriptor> fileDescriptorMap = new HashMap<>();
    @Getter
    private final ManagedChannel managedChannel;
    private final AutoCloseable closeConnection;

    /**
     * Connection Listener interface
     */
    public interface ConnectionListener {
        /**
         * Callback when the connection is updated
         * @param id the server identifier
         * @param managedChannel the channel
         */
        void connectionChanged(UUID id, ManagedChannel managedChannel);
    }

    /**
     * Create a new SiLA Connection
     * @param server Server Description
     * @param managedChannel gRPC Channel to Server
     * @throws MalformedSiLAFeature if one of the features is invalid
     */
    public Connection(
            @NonNull final Server server,
            @NonNull final ManagedChannel managedChannel
    ) throws MalformedSiLAFeature {
        this.serverId = server.getConfiguration().getUuid();
        this.managedChannel = managedChannel;
        this.closeConnection = () -> {
            try {
                managedChannel.shutdownNow().awaitTermination(5, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                log.warn(e.getMessage());
            }
        };

        loadFeaturesFromServer(server);
    }

    /**
     * Convert and validate the server features into {@link Descriptors.FileDescriptor} and save them in {@link Connection#fileDescriptorMap} and {@link Connection#featureMap}
     * @param server the server
     * @throws MalformedSiLAFeature if a feature is malformed
     */
    private void loadFeaturesFromServer(Server server) throws MalformedSiLAFeature {
        // Load gRPC Mappings
        for (val feature : server.getFeatures()) {
            final String featureIdentifier = getFeatureId(feature.getIdentifier());
            final Descriptors.FileDescriptor protoFile = ProtoMapper
                    .usingFeature(feature)
                    .generateProto();
            this.fileDescriptorMap.put(featureIdentifier, protoFile);
            this.featureMap.put(featureIdentifier, protoFile.getServices().get(0));
        }
    }

    /**
     * Constructor
     * @param server the server
     * @param closeable the cleanup method to call when closing the connection
     * @throws MalformedSiLAFeature
     */
    public Connection(
            @NonNull final Server server,
            @NonNull final AutoCloseable closeable
    ) throws MalformedSiLAFeature {
        this.serverId = server.getConfiguration().getUuid();
        this.managedChannel = null;
        this.closeConnection = closeable;
        loadFeaturesFromServer(server);
    }

    /**
     * @param connectionListener Listeners to changes in the connection
     */
    public void attachAndTriggerListener(@NonNull final ConnectionListener connectionListener) {
        // Subscribe to channel changes
        this.managedChannel.notifyWhenStateChanged(managedChannel.getState(false), new Runnable() {
            @Override
            public void run() {
                final ConnectivityState state = managedChannel.getState(false);
                log.debug("state of server with id `{}` is `{}`", serverId, state.name());
                connectionListener.connectionChanged(serverId, managedChannel);
                // Re-subscribe to the one-off callback with this Runnable anonymous class instance
                managedChannel.notifyWhenStateChanged(state, this);
            }
        });

        // We trigger the change once at subscription to receive the correct initial connection state
        connectionListener.connectionChanged(serverId, this.managedChannel);
    }

    /**
     * Get a feature service
     * @param featureId The feature identifier
     * @return The matching feature service
     * @throws KeyException when no identifier does not match any feature
     */
    public Descriptors.ServiceDescriptor getFeatureService(@NonNull final String featureId) throws KeyException {
        if (!featureMap.containsKey(featureId)) {
            throw new KeyException("Cannot find feature: " + featureId);
        } else {
            return featureMap.get(featureId);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void close() {
        try {
            this.closeConnection.close();
        } catch (Exception e) {
            log.warn("Exception occured while closing connection of server {}", serverId, e);
        }
    }

    /**
     * This is a temporary hack to retrieve the Feature Identifier correctly, even if it's fully
     * qualified (delimited by '/')
     *
     * TODO: Find a cleaner way to do this
     *
     * @param featureIdentifier The full Feature Identifier
     * @return the subset of the feature identifier useful for gRPC Calls
     */
    private static String getFeatureId(@NonNull final String featureIdentifier) {
        final int lastIndexOf = featureIdentifier.lastIndexOf('/');
        if (lastIndexOf > 0 && lastIndexOf != featureIdentifier.length() - 1) {  // If fully qualified feature, convert to simple feature id
            return (featureIdentifier.substring(lastIndexOf + 1));
        }
        return (featureIdentifier);
    }
}
