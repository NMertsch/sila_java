package sila_java.library.manager.grpc;

import com.google.protobuf.Descriptors;
import com.google.protobuf.DynamicMessage;
import com.google.protobuf.ExtensionRegistryLite;
import io.grpc.MethodDescriptor;

import java.io.IOException;
import java.io.InputStream;

/** A Marshaller for dynamic messages. */
public class DynamicMessageMarshaller implements MethodDescriptor.Marshaller<Object> {
    private final Descriptors.Descriptor messageDescriptor;

    /**
     * Constructor
     * @param messageDescriptor the message descriptor
     */
    public DynamicMessageMarshaller(Descriptors.Descriptor messageDescriptor) {
        this.messageDescriptor = messageDescriptor;
    }

    /**
     * @inheritDoc
     */
    @Override
    public InputStream stream(Object o) {
        if (!(o instanceof DynamicMessage))
            throw new IllegalArgumentException("Expected " + DynamicMessage.class.getName() + " but got " + o.getClass().getName());
        return (this.stream((DynamicMessage) o));
    }

    /**
     * @inheritDoc
     */
    @Override
    public DynamicMessage parse(InputStream inputStream) {
        try {
            return DynamicMessage.newBuilder(messageDescriptor)
                    .mergeFrom(inputStream, ExtensionRegistryLite.getEmptyRegistry())
                    .build();
        } catch (IOException e) {
            throw new RuntimeException("Unable to merge from the supplied input stream", e);
        }
    }

    /**
     * Convert a {@link DynamicMessage} into a {@link InputStream}
     * @param abstractMessage the input stream
     * @return a {@link InputStream}
     */
    private InputStream stream(DynamicMessage abstractMessage) {
        return abstractMessage.toByteString().newInput();
    }
}
