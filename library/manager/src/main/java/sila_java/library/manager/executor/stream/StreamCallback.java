package sila_java.library.manager.executor.stream;

/**
 * Stream callback
 */
@FunctionalInterface
public interface StreamCallback {
    /**
     * Called when a new message is received
     * @param message The message
     * @return False to cancel the stream, true to continue reading
     */
    boolean onNext(String message);
}