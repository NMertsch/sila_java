package sila_java.library.manager.executor;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.protobuf.*;
import com.google.protobuf.util.JsonFormat;
import io.grpc.*;
import io.grpc.stub.ClientCalls;
import io.grpc.stub.StreamObserver;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.*;
import sila_java.library.cloudier.client.CloudierClient;
import sila_java.library.cloudier.client.CloudierClientEndpoint;
import sila_java.library.cloudier.client.CloudierClientObserver;
import sila_java.library.core.models.Feature;
import sila_java.library.core.sila.errors.ExceptionGeneration;
import sila_java.library.core.sila.errors.SiLAErrorException;
import sila_java.library.core.sila.errors.SiLAErrors;
import sila_java.library.core.sila.mapping.grpc.GrpcNameMapper;
import sila_java.library.core.sila.mapping.grpc.ProtoMapper;
import sila_java.library.core.sila.types.SiLAString;
import sila_java.library.manager.ServerManager;
import sila_java.library.manager.executor.stream.StaticStreamObserver;
import sila_java.library.manager.executor.stream.StreamCallback;
import sila_java.library.manager.grpc.Constants;
import sila_java.library.manager.grpc.DynamicMessageMarshaller;
import sila_java.library.manager.grpc.FullyQualifiedMetadataContextKey;
import sila_java.library.manager.models.Server;
import sila_java.library.manager.models.*;

import javax.annotation.Nullable;
import java.io.IOException;
import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * Executor to handle all possible SiLA tasks
 */
@Slf4j
@AllArgsConstructor
public class ServerCallExecutor implements AutoCloseable {
    private final ExecutableServerCall call;
    private final CallListener callListener;
    private final List<CompletableFuture<List<String>>> internalFutures = new CopyOnWriteArrayList<>();
    private final List<ClientCall<Object, Object>> internalCalls = new CopyOnWriteArrayList<>();

    /**
     * Constructor
     * @param call the {@link ExecutableServerCall}
     */
    public ServerCallExecutor(@NonNull final ExecutableServerCall call) {
        this(call, new CallListener() {});
    }

    /**
     * Cancel and clean each {@link ServerCallExecutor#internalCalls internal tasks} and {@link ServerCallExecutor#internalCalls futures}
     * @inheritDoc
     */
    @Override
    public void close() {
        internalFutures.forEach(f -> {
            if (!f.isDone()) {
                f.cancel(true);
            }
        });
        internalCalls.forEach(c -> c.cancel("Interrupted", null));
    }

    /**
     * Execute a server initiated connectivity task
     * @return the gRPC encoded response String
     */
    String executeCloudier() {
        final CloudierClient cloudierClient = ServerManager.getInstance().getCloudierClient();
        final CloudierClientObserver cloudierClientObserver;
        if (cloudierClient != null) {
            cloudierClientObserver = cloudierClient.getEndpointService().getResponseObservers().get(this.call.getBaseCall().getServerId().toString());
        } else {
            cloudierClientObserver = null;
        }
        final Server server = ServerManager.getInstance().getServers().get(this.call.getBaseCall().getServerId());
        final Feature feature = server.getFeatures()
                .stream()
                .filter((f) -> f.getIdentifier().equals(this.call.getBaseCall().getFeatureId()))
                .findAny()
                .orElse(null);
        final String fullyQualifiedFeature = (feature == null) ? ("") :
                (feature.getOriginator() + "/" + feature.getCategory() + "/" + feature.getIdentifier() + "/v" + (int)Float.parseFloat(feature.getFeatureVersion()));
        final String fullyQualifiedCallIdentifier;
        final String callIdPrefix;
        switch (this.call.getBaseCall().getType()) {
            case OBSERVABLE_PROPERTY_READ:
            case OBSERVABLE_PROPERTY:
                fullyQualifiedCallIdentifier = fullyQualifiedFeature + "/Property/" + this.call.getBaseCall().getCallId();
                callIdPrefix = "Subscribe_";
                break;
            case UNOBSERVABLE_PROPERTY:
                fullyQualifiedCallIdentifier = fullyQualifiedFeature + "/Property/" + this.call.getBaseCall().getCallId();
                callIdPrefix = "Get_";
                break;
            case OBSERVABLE_COMMAND:
                fullyQualifiedCallIdentifier = fullyQualifiedFeature + "/Command/" + this.call.getBaseCall().getCallId();
                callIdPrefix = "";
                break;
            case UNOBSERVABLE_COMMAND:
                fullyQualifiedCallIdentifier = fullyQualifiedFeature + "/Command/" + this.call.getBaseCall().getCallId();
                callIdPrefix = "";
                break;
            case GET_FCP_AFFECTED_BY_METADATA:
                fullyQualifiedCallIdentifier = fullyQualifiedFeature + "/Metadata/" + this.call.getBaseCall().getCallId();
                callIdPrefix = GrpcNameMapper.getMetadataRPC("");
                break;
            default:
                fullyQualifiedCallIdentifier = "";
                callIdPrefix = "";
        }
        if (cloudierClientObserver == null) {
            throw new RuntimeException("Server " + this.call.getBaseCall().getServerId() + " is not connected for server initiated connection");
        }
        if (this.call.getBaseCall().getType() != SiLACall.Type.UPLOAD_BINARY && this.call.getBaseCall().getType() != SiLACall.Type.DOWNLOAD_BINARY &&
                (fullyQualifiedFeature.isEmpty() || fullyQualifiedCallIdentifier.isEmpty())
        ) {
            throw new RuntimeException("Invalid fully qualified call identifier");
        }
        log.debug("[Cloudier] FQ call id " + fullyQualifiedCallIdentifier);
        final String callId = callIdPrefix + this.call.getBaseCall().getCallId();
        switch (this.call.getBaseCall().getType()) {
            case UNOBSERVABLE_COMMAND:
                return executeCallWithProgression(() -> {
                    final Descriptors.MethodDescriptor method = getMethodDescriptor(callId);
                    final DynamicMessage requestParameter = ServerCallExecutor.getRequestMessage(method, this.call.getBaseCall().getParameters());
                    final SiLACloudConnector.CommandParameter.Builder commandParameterBuilder = SiLACloudConnector.CommandParameter.newBuilder()
                            .setParameters(requestParameter.toByteString());
                    commandParameterBuilder.addAllMetadata(getCloudMetadataSet(this.getMetadataMap()));
                    CompletableFuture<ByteString> byteStringCompletableFuture = cloudierClientObserver.runUnobservableCommand(
                            SiLACloudConnector.UnobservableCommandExecution.newBuilder()
                                    .setCommandParameter(
                                            commandParameterBuilder.build()
                                    )
                                    .setFullyQualifiedCommandId(fullyQualifiedCallIdentifier)
                                    .build()
                    );
                    try {
                        final ByteString bytes = callFuture(byteStringCompletableFuture, ignored -> {});
                        final DynamicMessage requestResponse = ServerCallExecutor.getRequestResponse(method, bytes);
                        return ProtoMapper.serializeToJson(requestResponse);
                    } catch (InvalidProtocolBufferException e) {
                        throw new RuntimeException(e);
                    }
                });
            case OBSERVABLE_COMMAND:
                return executeCallWithProgression(() -> {
                    final Descriptors.MethodDescriptor method = getMethodDescriptor(callId);
                    final DynamicMessage requestParameter = ServerCallExecutor.getRequestMessage(method, this.call.getBaseCall().getParameters());
                    final SiLACloudConnector.CommandParameter.Builder commandParameterBuilder = SiLACloudConnector.CommandParameter.newBuilder()
                            .setParameters(requestParameter.toByteString());
                    commandParameterBuilder.addAllMetadata(getCloudMetadataSet(this.getMetadataMap()));
                    Optional<Descriptors.MethodDescriptor> intermediateResponse = Optional.empty();
                    try {
                        intermediateResponse = Optional.of(getMethodDescriptor(this.call.getBaseCall().getCallId() + "_Intermediate"));
                    } catch (RuntimeException e) {
                        log.info("Call {} does not have intermediate response", this.call.getBaseCall().getCallId());
                    }
                    final Optional<Descriptors.MethodDescriptor> finalIntermediateResponse = intermediateResponse;
                    CompletableFuture<ByteString> byteStringCompletableFuture = cloudierClientObserver.runObservableCommand(
                            commandParameterBuilder.build(),
                            fullyQualifiedCallIdentifier,
                            intermediateResponse.isPresent(),
                            new CloudierClientEndpoint.CallListener() {
                                @Override
                                public void onCommandInit(SiLACloudConnector.ObservableCommandConfirmation observableCommandConfirmation) {
                                    ServerCallExecutor.this.callListener.onObservableCommandInit(ServerCallExecutor.this.call.getBaseCall(), observableCommandConfirmation.getCommandConfirmation());
                                }

                                @Override
                                public void onCommandExecutionInfo(SiLACloudConnector.ObservableCommandExecutionInfo observableCommandExecutionInfo) {
                                    ServerCallExecutor.this.callListener.onObservableCommandExecutionInfo(ServerCallExecutor.this.call.getBaseCall(), observableCommandExecutionInfo.getExecutionInfo());
                                }

                                @Override
                                public void onIntermediateResponse(SiLACloudConnector.ObservableCommandIntermediateResponse observableCommandExecutionInfo) {
                                    finalIntermediateResponse.ifPresent((intermediateResponse) -> {
                                        final DynamicMessage requestResponse = ServerCallExecutor.getRequestResponse(intermediateResponse, observableCommandExecutionInfo.getResponse());
                                        ServerCallExecutor.this.callListener.onObservableIntermediateResponse(ServerCallExecutor.this.call.getBaseCall(), requestResponse);
                                    });
                                }

                                @Override
                                public void onError(SiLAFramework.SiLAError siLAError) {

                                }
                            }
                    );
                    try {
                        final ByteString bytes = callFuture(byteStringCompletableFuture, ignored -> {});
                        final Descriptors.MethodDescriptor methodResult = getMethodDescriptor(callId + "_Result");
                        final DynamicMessage requestResponse = ServerCallExecutor.getRequestResponse(methodResult, bytes);
                        return ProtoMapper.serializeToJson(requestResponse);
                    } catch (InvalidProtocolBufferException e) {
                        throw new RuntimeException(e);
                    }
                });
            case UNOBSERVABLE_PROPERTY:
                return executeCallWithProgression(() -> {
                    final Descriptors.MethodDescriptor method = getMethodDescriptor(callId);
                    final SiLACloudConnector.UnobservablePropertyRead.Builder propertyBuilder = SiLACloudConnector.UnobservablePropertyRead
                            .newBuilder()
                            .setFullyQualifiedPropertyId(fullyQualifiedCallIdentifier);
                    propertyBuilder.addAllMetadata(getCloudMetadataSet(this.getMetadataMap()));
                    CompletableFuture<ByteString> byteStringCompletableFuture = cloudierClientObserver.readUnobservableProperty(
                            propertyBuilder.build()
                    );
                    try {
                        final ByteString bytes = callFuture(byteStringCompletableFuture, ignored -> {});
                        final DynamicMessage requestResponse = ServerCallExecutor.getRequestResponse(method, bytes);
                        return ProtoMapper.serializeToJson(requestResponse);
                    } catch (InvalidProtocolBufferException e) {
                        throw new RuntimeException(e);
                    }
                });
            case OBSERVABLE_PROPERTY_READ:
            case OBSERVABLE_PROPERTY:
                return executeCallWithProgression(() -> {
                    final Descriptors.MethodDescriptor method = getMethodDescriptor(callId);
                    final SiLACloudConnector.ObservablePropertySubscription.Builder propertySubscription = SiLACloudConnector.ObservablePropertySubscription
                            .newBuilder()
                            .setFullyQualifiedPropertyId(fullyQualifiedCallIdentifier);
                    propertySubscription.addAllMetadata(getCloudMetadataSet(this.getMetadataMap()));
                    CompletableFuture<ByteString> byteStringCompletableFuture = cloudierClientObserver.readObservableProperty(
                            propertySubscription.build(), (response) -> {
                                final DynamicMessage requestResponse = ServerCallExecutor.getRequestResponse(method, response);
                                try {
                                    // todo cancel subscription
                                    this.callListener.onObservablePropertyUpdate(this.call.getBaseCall(), ProtoMapper.serializeToJson(requestResponse));
                                } catch (InvalidProtocolBufferException e) {
                                    // todo error
                                    e.printStackTrace();
                                }
                                // stop reading if not observable property subscription
                                return this.call.getBaseCall().getType().equals(SiLACall.Type.OBSERVABLE_PROPERTY);
                            }
                    );
                    try {
                        final ByteString bytes = callFuture(byteStringCompletableFuture, ignored -> {});
                        final DynamicMessage requestResponse = ServerCallExecutor.getRequestResponse(method, bytes);
                        return ProtoMapper.serializeToJson(requestResponse);
                    } catch (InvalidProtocolBufferException e) {
                        throw new RuntimeException(e);
                    }
                });
            case GET_FCP_AFFECTED_BY_METADATA:
                return executeCallWithProgression(() -> {
                    final Descriptors.MethodDescriptor method = getMethodDescriptor(callId);
                    CompletableFuture<SiLACloudConnector.GetFCPAffectedByMetadataResponse> byteStringCompletableFuture = cloudierClientObserver.getFCPAffectedByMetadata(
                            fullyQualifiedCallIdentifier
                    );
                    try {
                        final SiLACloudConnector.GetFCPAffectedByMetadataResponse getFCPAffectedByMetadataResponse = callFuture(byteStringCompletableFuture, ignored -> {});
                        final List<SiLAFramework.String> metadataList = getFCPAffectedByMetadataResponse.getAffectedCallsList().stream().map(SiLAString::from).collect(Collectors.toList());
                        final Descriptors.Descriptor outputType = method.getOutputType();
                        if (outputType == null) {
                            throw new RuntimeException("Service proto does not have get FCP Affected by metadata method");
                        }
                        // todo create constant for AffectedCalls
                        final Descriptors.FieldDescriptor affectedCallsField = outputType.findFieldByName("AffectedCalls");
                        if (affectedCallsField == null) {
                            throw new RuntimeException("FCP Affected by metadata method output type is invalid");
                        }
                        final DynamicMessage.Builder builder = DynamicMessage.newBuilder(outputType);
                        builder.setField(affectedCallsField, metadataList);
                        return ProtoMapper.serializeToJson(builder.build());
                    } catch (InvalidProtocolBufferException e) {
                        throw new RuntimeException(e);
                    }
                });
            case UPLOAD_BINARY:
                // todo check if server supports large binary upload
                return executeCallWithProgression(() -> {
                    final BinaryUploader binaryUploader = this.call.getBinaryUploader().orElseThrow(
                            () -> new RuntimeException("Binary uploader needs to be supplied to upload a large binary")
                    );
                    try {
                        final SiLABinaryTransfer.CreateBinaryResponse createBinaryResponse = cloudierClientObserver.createBinaryUploadRequest(
                                binaryUploader.getRequest(),
                                getCloudMetadataSet(this.getMetadataMap())
                        ).get();
                        for (int i = 0; i < binaryUploader.getChunkCount(); i++) {
                            final CompletableFuture<SiLABinaryTransfer.UploadChunkResponse> uploadBinaryChunkResponse = cloudierClientObserver.uploadBinaryChunkRequest(
                                    binaryUploader.getNextChunkUploadRequest(createBinaryResponse.getBinaryTransferUUID())
                            );
                            uploadBinaryChunkResponse.get();
                        }
                        return ProtoMapper.serializeToJson(createBinaryResponse);
                    } catch (InterruptedException | ExecutionException | IOException e) {
                        throw new RuntimeException(e);
                    }
                });
            case DOWNLOAD_BINARY:
                // todo check if server supports large binary upload
                return executeCallWithProgression(() -> {
                    final BinaryDownloader binaryDownloader = this.call.getBinaryDownloader().orElseThrow(
                            () -> new RuntimeException("Binary downloader needs to be supplied to download a large binary")
                    );
                    try {
                        final SiLABinaryTransfer.GetBinaryInfoResponse binaryInfoResponse = cloudierClientObserver.getBinaryInfoResponseRequest(binaryDownloader.getBinaryInfoRequest()).get();
                        final int chunkCount = BinaryDownloader.getChunkCount(binaryInfoResponse.getBinarySize());
                        for (int i = 0; i < chunkCount; i++) {
                            final SiLABinaryTransfer.GetChunkResponse getChunkResponse = cloudierClientObserver.getBinaryChunkRequest(
                                    binaryDownloader.getNextChunkDownloadRequest(binaryInfoResponse)
                            ).get();
                            binaryDownloader.writeChunk(getChunkResponse);
                        }
                        return ProtoMapper.serializeToJson(binaryInfoResponse);
                    } catch (InterruptedException | ExecutionException | IOException e) {
                        throw new RuntimeException(e);
                    }
                });
        }
        // todo throw
        throw new RuntimeException("Unknown call type " + this.call.getBaseCall().getType());
    }

    /**
     * Converts a map of fully qualified identifier and SiLA Basic values into a Set of {@link SiLACloudConnector.Metadata}
     * @param metadataMap the map of fully qualified identifier and SiLA Basic values
     * @return a Set of {@link SiLACloudConnector.Metadata}
     */
    private static Set<SiLACloudConnector.Metadata> getCloudMetadataSet(Map<String, DynamicMessage> metadataMap) {
        return metadataMap.entrySet().stream().map((entry) -> (
                SiLACloudConnector.Metadata.newBuilder()
                        .setValue(entry.getValue().toByteString())
                        .setFullyQualifiedMetadataId(entry.getKey())
                        .build()
        )).collect(Collectors.toSet());
    }

    /**
     * Execute a client initiated connectivity task
     *
     * @return the gRPC encoded response String
     */
    String execute() {
        Server server = ServerManager.getInstance().getServers().get(this.call.getBaseCall().getServerId());
        if (server == null) {
            return "";
            // todo return error
        }
        if (server.getConnectionType() == Server.ConnectionType.SERVER_INITIATED) {
            final CloudierClient cloudierClient = ServerManager.getInstance().getCloudierClient();
            CloudierClientObserver cloudierClientObserver = null;
            if (cloudierClient != null) {
                cloudierClientObserver = cloudierClient.getEndpointService().getResponseObservers().get(this.call.getBaseCall().getServerId().toString());
            }
            if (cloudierClientObserver != null) {
                log.debug("Executing cloudier call");
                return this.executeCloudier();
            } else {
                // todo error
                return "";
            }
        } else {
            String result = "";
            switch (this.call.getBaseCall().getType()) {
                case UNOBSERVABLE_COMMAND:
                    result = executeCallWithProgression(this::executeUnobservableCommand);
                    break;
                case OBSERVABLE_COMMAND:
                    result = executeCallWithProgression(this::executeObservableCommand);
                    break;
                case UNOBSERVABLE_PROPERTY:
                    result = executeCallWithProgression(this::getUnobservableProperty);
                    break;
                case OBSERVABLE_PROPERTY_READ:
                case OBSERVABLE_PROPERTY:
                    result = executeCallWithProgression(this::getObservableProperty);
                    break;
                case GET_FCP_AFFECTED_BY_METADATA:
                    result = executeCallWithProgression(this::getFPCAffectedByMetadata);
                    break;
                case UPLOAD_BINARY:
                    result = executeCallWithProgression(this::uploadBinary);
                    break;
                case DOWNLOAD_BINARY:
                    result = executeCallWithProgression(this::downloadBinary);
                    break;
                default:
                    // todo throw
            }
            return result;
        }
    }

    /**
     * Task executor interface
     */
    private interface CallExecutor {
        /**
         * Execute a task
         * @return the gRPC encoded response String
         */
        String execute();
    }

    /**
     * Execute a task with progression updates
     *
     * @param callExecutor the task executor
     * @return the gRPC encoded response String
     */
    private String executeCallWithProgression(@NonNull final CallExecutor callExecutor) {
        log.debug("Call {} started", this.call.getBaseCall().getCallId());
        final CallStarted callStarted = new CallStarted(OffsetDateTime.now(), this.call.getTimeout(), this.call.getBaseCall());
        this.callListener.onStart(callStarted);
        try {
            final String result = callExecutor.execute();
            log.debug("Call {} ended successfully", callStarted.getSiLACall().getCallId());
            final CallCompleted callCompleted = new CallCompleted(
                    callStarted.getStartDate(), OffsetDateTime.now(), result, this.call.getBaseCall()
            );
            this.callListener.onComplete(callCompleted);
            return result;
        } catch (Throwable e) {
            throw extractMeaningfulError((errorMessage) -> {
                final CallErrored callErrored = new CallErrored(
                        callStarted.getStartDate(),
                        OffsetDateTime.now(),
                        errorMessage,
                        this.call.getBaseCall()
                );
                log.debug("Call {} ended with error", callStarted.getSiLACall().getCallId());
                this.callListener.onError(callErrored);
            }, e);
        }
    }

    /**
     * Execute an Unobservable Command
     * @return the gRPC encoded response String
     */
    private String executeUnobservableCommand() {
        return (this.executeCall(this.call.getBaseCall().getCallId(), this.call.getBaseCall().getParameters()));
    }

    /**
     * Execute an Observable Command
     * @return the gRPC encoded response String
     */
    private String executeObservableCommand() {
        final SiLAFramework.CommandConfirmation.Builder command = SiLAFramework.CommandConfirmation.newBuilder();

        String commandId;
        try {
            JsonFormat.parser().merge(this.executeCall(this.call.getBaseCall().getCallId(), this.call.getBaseCall().getParameters()), command);
            commandId = ProtoMapper.serializeToJson(command.getCommandExecutionUUID());
            this.callListener.onObservableCommandInit(this.call.getBaseCall(), command.build());
        } catch (InvalidProtocolBufferException e) {
            throw new RuntimeException("Received a malformed message");
        }
        final AtomicInteger commandStatus = new AtomicInteger(SiLAFramework.ExecutionInfo.CommandStatus.waiting_VALUE);
        this.callListener.onObservableCommandExecutionInfo(
                this.call.getBaseCall(),
                SiLAFramework.ExecutionInfo
                        .newBuilder()
                        .setCommandStatus(SiLAFramework.ExecutionInfo.CommandStatus.waiting)
                        .build()
        );
        final CompletableFuture<List<String>> stateCommandFuture = this.executeStream(
                GrpcNameMapper.getStateCommand(this.call.getBaseCall().getCallId()),
                commandId,
                message -> {
                    try {
                        final SiLAFramework.ExecutionInfo.Builder stateBuilder = SiLAFramework.ExecutionInfo.newBuilder();
                        JsonFormat.parser().merge(message, stateBuilder);
                        log.debug("Received status for call " + this.call.getBaseCall().getCallId());
                        log.debug(stateBuilder.toString());

                        this.callListener.onObservableCommandExecutionInfo(this.call.getBaseCall(), stateBuilder.build());
                        commandStatus.set(stateBuilder.getCommandStatus().getNumber());

                        return (
                                commandStatus.get() == SiLAFramework.ExecutionInfo.CommandStatus.running_VALUE ||
                                        commandStatus.get() == SiLAFramework.ExecutionInfo.CommandStatus.waiting_VALUE
                        );

                    } catch (final InvalidProtocolBufferException e) {
                        log.warn("Received a malformed message: ", e);
                        return (false);
                    }
                },
                false
        );

        final String intermediateCommandName = GrpcNameMapper.getIntermediateCommand(this.call.getBaseCall().getCallId());
        Optional<Descriptors.MethodDescriptor> optionalIntermediateResponse = Optional.empty();
        try {
            optionalIntermediateResponse = Optional.of(getMethodDescriptor(intermediateCommandName));
        } catch (RuntimeException e) {
            log.debug("Call {} does not have intermediate response", this.call.getBaseCall().getCallId());
        }
        optionalIntermediateResponse.ifPresent(intermediateResponseMethod -> {
            final CompletableFuture<List<String>> intermediateResponseFuture = this.executeStream(
                    intermediateCommandName,
                    commandId,
                    message -> {
                        try {
                            final DynamicMessage.Builder responseType = DynamicMessage.newBuilder(intermediateResponseMethod.getOutputType());
                            JsonFormat.parser().merge(message, responseType);
                            log.debug("Received intermediate response for call " + intermediateCommandName);
                            log.debug(responseType.toString());

                            this.callListener.onObservableIntermediateResponse(this.call.getBaseCall(), responseType.build());
                            return (true);
                        } catch (final InvalidProtocolBufferException e) {
                            log.warn("Received a malformed message: ", e);
                            return (false);
                        }
                    },
                    false
            );
        });

        callFuture(stateCommandFuture, null);
        this.callListener.onObservableCommandExecutionInfo(
                this.call.getBaseCall(),
                SiLAFramework.ExecutionInfo
                        .newBuilder()
                        .setCommandStatus(SiLAFramework.ExecutionInfo.CommandStatus.forNumber(commandStatus.get()))
                        .build()
        );
        if (commandStatus.get() == SiLAFramework.ExecutionInfo.CommandStatus.finishedSuccessfully_VALUE) {
            return (this.executeCall(GrpcNameMapper.getResult(this.call.getBaseCall().getCallId()), commandId));
        } else {
            // An exception should be raised with the cause of the error
            this.executeCall(GrpcNameMapper.getResult(this.call.getBaseCall().getCallId()), commandId);

            // If no exception is raised or is not a valid SiLA Error, raise one
            throw new RuntimeException("Command finished with an error without further information!");
        }
    }

    /**
     * Get an Unobservable Property
     * @return the gRPC encoded response String
     */
    private String getUnobservableProperty() {
        return (this.executeCall(GrpcNameMapper.getUnobservableProperty(this.call.getBaseCall().getCallId()), this.call.getBaseCall().getParameters()));
    }

    /**
     * Get Feature/Property/Command affected by metadata
     *
     * @return the gRPC encoded response String
     */
    private String getFPCAffectedByMetadata() {
        return (this.executeCall(GrpcNameMapper.getMetadataRPC(this.call.getBaseCall().getCallId()), this.call.getBaseCall().getParameters()));
    }

    /**
     * Upload a binary
     *
     * @return the gRPC encoded response String
     */
    @SneakyThrows
    private String uploadBinary() {
        // todo check if server supports large binary upload
        final BinaryUploader binaryUploader = this.call.getBinaryUploader().orElseThrow(
                () -> new RuntimeException("Binary uploader needs to be supplied to upload a large binary")
        );
        final ManagedChannel managedChannel = this.call.getConnection().getManagedChannel();
        final Context contextWithMetadata = createContextWithMetadata(Context.current());
        Context initialContext = null;
        try {
            initialContext = contextWithMetadata.attach();
            final BinaryUploadGrpc.BinaryUploadBlockingStub binaryUploadBlockingStub = BinaryUploadGrpc.newBlockingStub(managedChannel);
            final BinaryUploadGrpc.BinaryUploadStub binaryUploadStub = BinaryUploadGrpc.newStub(managedChannel);
            final SiLABinaryTransfer.CreateBinaryResponse createBinaryResponse = binaryUploadBlockingStub.createBinary(binaryUploader.getRequest());
            final BinaryUploaderStream binaryUploaderStream = new BinaryUploaderStream(binaryUploader, createBinaryResponse.getBinaryTransferUUID());
            final StreamObserver<SiLABinaryTransfer.UploadChunkRequest> uploadChunkRequestStreamObserver = binaryUploadStub.uploadChunk(binaryUploaderStream);
            binaryUploaderStream.startUpload(uploadChunkRequestStreamObserver);
            binaryUploaderStream.getVoidCompletableFuture().join();
            // todo catch error like in executeCall?
            return ProtoMapper.serializeToJson(createBinaryResponse);
        } finally {
            if (initialContext != null) {
                contextWithMetadata.detach(initialContext);
            }
        }
    }

    /**
     * Download a binary
     *
     * @return the gRPC encoded response String
     */
    @SneakyThrows
    private String downloadBinary() {
        // todo check if server supports large binary upload
        final BinaryDownloader binaryDownloader = this.call.getBinaryDownloader().orElseThrow(
                () -> new RuntimeException("Binary downloader needs to be supplied to download a large binary")
        );
        final ManagedChannel managedChannel = this.call.getConnection().getManagedChannel();
        final Context contextWithMetadata = createContextWithMetadata(Context.current());
        Context initialContext = null;
        try {
            initialContext = contextWithMetadata.attach();
            final BinaryDownloadGrpc.BinaryDownloadBlockingStub binaryDownloadBlockingStub = BinaryDownloadGrpc.newBlockingStub(managedChannel);
            final BinaryDownloadGrpc.BinaryDownloadStub binaryDownloadStub = BinaryDownloadGrpc.newStub(managedChannel);
            final SiLABinaryTransfer.GetBinaryInfoResponse binaryInfo = binaryDownloadBlockingStub.getBinaryInfo(binaryDownloader.getBinaryInfoRequest());
            final BinaryDownloaderStream binaryDownloaderStream = new BinaryDownloaderStream(binaryDownloader, binaryInfo);
            final StreamObserver<SiLABinaryTransfer.GetChunkRequest> getChunkRequestStreamObserver = binaryDownloadStub.getChunk(binaryDownloaderStream);
            binaryDownloaderStream.startDownload(getChunkRequestStreamObserver);
            binaryDownloaderStream.getVoidCompletableFuture().join();
            // todo catch error like in executeCall?
            return ProtoMapper.serializeToJson(binaryInfo);
        } finally {
            if (initialContext != null) {
                contextWithMetadata.detach(initialContext);
            }
        }
    }

    /**
     * Get an observable property
     *
     * @return the gRPC encoded response String
     */
    private String getObservableProperty() {
        CompletableFuture<List<String>> future = this.executeStream(
                GrpcNameMapper.getObservableProperty(this.call.getBaseCall().getCallId()),
                this.call.getBaseCall().getParameters(),
                message -> {
                    this.callListener.onObservablePropertyUpdate(this.call.getBaseCall(), message);
                    // stop reading if not observable property subscription
                    return this.call.getBaseCall().getType().equals(SiLACall.Type.OBSERVABLE_PROPERTY);
                },
                true
        );
        List<String> results = callFuture(future, null);
        if (results.isEmpty()) {
            throw new RuntimeException("No result");
        } else {
            return (results.get(results.size() - 1));
        }
    }

    /**
     * Private utility to call a future and employ the proper error handling
     */
    private <T> T callFuture(
            @NonNull final Future<T> future,
            @Nullable final Consumer<String> errorConsumer
    ) {
        try {
            return future.get(this.call.getTimeout().get(ChronoUnit.SECONDS), TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            if (!future.isDone()) {
                future.cancel(true);
            }
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        } catch (Throwable e) {
            throw extractMeaningfulError(errorConsumer, e);
        }
    }

    /**
     * Extract meaningful error from a {@link Throwable}
     * @param errorConsumer the meaningful error message callback
     * @param e the Error
     *
     * @implNote if the provided {@link Throwable} is not a {@link SiLAErrorException} the {@link Throwable} will be wrapped in a {@link RuntimeException}
     *
     * @return the relevant {@link Throwable}
     */
    private RuntimeException extractMeaningfulError(Consumer<String> errorConsumer, Throwable e) {
        // Completable future has the cause wrapped
        Throwable relevantThrowable = e;
        if (relevantThrowable.getCause() != null) {
            relevantThrowable = relevantThrowable.getCause();
        }

        String errorMessage;

        if (relevantThrowable instanceof SiLAErrorException) {
            try {
                errorMessage = JsonFormat.printer().includingDefaultValueFields().print(((SiLAErrorException) relevantThrowable).getSiLAError());
            } catch (InvalidProtocolBufferException ex) {
                errorMessage = ((SiLAErrorException) relevantThrowable).getSiLAError().toString();
            }
        } else {
            errorMessage = ExceptionGeneration.generateMessage(relevantThrowable, this.call.getTimeout());
        }

        if (errorConsumer != null) {
            errorConsumer.accept(errorMessage);
        }

        if (relevantThrowable instanceof SiLAErrorException) {
            return (SiLAErrorException)relevantThrowable;
        }

        return new RuntimeException(relevantThrowable);
    }

    /**
     * Simple Unary gRPC Call with given Service Id and parameters
     * @param callId Service Name provided in the proto definition
     * @param params Protobuf Parameters in JSON Format
     * @return Protobuf Response in JSON Format
     */
    private String executeCall(@NonNull final String callId, @NonNull final String params) {
        try {
            final Descriptors.MethodDescriptor method = getMethodDescriptor(callId);
            final DynamicMessage request = ServerCallExecutor.getRequestMessage(method, params);
            final MethodDescriptor<Object, Object> methodDescriptor = getMethodDescriptor(method);
            final Context contextWithMetadata = createContextWithMetadata(Context.current());
            final Context initialContext = contextWithMetadata.attach();
            try {
                final ClientCall<Object, Object> clientCall = this.call.getConnection().getManagedChannel().newCall(
                        methodDescriptor,
                        CallOptions.DEFAULT.withDeadlineAfter(this.call.getTimeout().getSeconds(), TimeUnit.SECONDS)
                );
                final DynamicMessage unaryCall = (DynamicMessage) ClientCalls.blockingUnaryCall(
                        clientCall,
                        request
                );
                return ProtoMapper.serializeToJson(unaryCall);
            } finally {
                contextWithMetadata.detach(initialContext);
            }
        } catch (Throwable e) {
            if (e instanceof StatusRuntimeException) {
                SiLAErrors.retrieveSiLAError((StatusRuntimeException) e).ifPresent((siLAError) -> {
                    throw new SiLAErrorException(siLAError, ((StatusRuntimeException) e).getStatus());
                });
            }
            throw new RuntimeException(ExceptionGeneration.generateMessage(e, this.call.getTimeout()));
        }
    }

    /**
     * Create a new context with metadata based on the provided context
     * @param originalContext the original context
     * @return the new context with metadata
     */
    private Context createContextWithMetadata(final Context originalContext) {
        Context contextWithMetadata = originalContext;
        final Map<String, DynamicMessage> metadataMap = getMetadataMap();
        final Set<FullyQualifiedMetadataContextKey<DynamicMessage>> contextKeySet = new HashSet<>();
        for (Map.Entry<String, DynamicMessage> entry : metadataMap.entrySet()) {
            final Context.Key<DynamicMessage> contextKey = Context.key(entry.getKey());
            contextKeySet.add(new FullyQualifiedMetadataContextKey<>(entry.getKey(), contextKey));
            contextWithMetadata = contextWithMetadata.withValue(contextKey, entry.getValue());
        }
        contextWithMetadata = contextWithMetadata.withValue(Constants.METADATA_IDENTIFIERS_CTX_KEY, contextKeySet);
        return contextWithMetadata;
    }

    /**
     * Get metadata from the task
     * @return the metadata linked to the task
     */
    private Map<String, DynamicMessage> getMetadataMap() {
        if (!this.call.getBaseCall().getMetadatas().isEmpty() && !this.call.getBaseCall().getMetadatas().equals("{}")) {
            try {
                final JsonParser parser = new JsonParser();
                final JsonElement rootNode = parser.parse(this.call.getBaseCall().getMetadatas());
                final Map<String, DynamicMessage> metadataMap = new HashMap<>();
                if (rootNode.isJsonObject()) {
                    for (final Map.Entry<String, JsonElement> entry : rootNode.getAsJsonObject().entrySet()) {
                        final String[] qualifiers = entry.getKey().split("/");
                        if (qualifiers.length < 6) {
                            log.warn("Invalid metadata identifier {}", entry.getKey());
                            continue;
                        }
                        final String featureIdentifier = qualifiers[2];
                        final String metadataIdentifier = qualifiers[5];
                        final Descriptors.FileDescriptor fileDescriptor = this.call.getConnection().getFileDescriptorMap().get(featureIdentifier);
                        final Descriptors.Descriptor messageTypeByName = fileDescriptor.findMessageTypeByName(GrpcNameMapper.getMetadata(metadataIdentifier));
                        final DynamicMessage.Builder messageBuilder = DynamicMessage.newBuilder(messageTypeByName);
                        JsonFormat.parser().merge(entry.getValue().toString(), messageBuilder);
                        metadataMap.put(entry.getKey(), messageBuilder.build());
                    }
                }
                return metadataMap;
            } catch (InvalidProtocolBufferException e) {
                log.warn("Malformed metadata value", e);
            } catch (NullPointerException e) {
                log.warn("Unknown metadata received", e);
            }
        }
        return Collections.emptyMap();
    }

    /**
     * Calling Server Side Streaming
     * @param callId Service Name provided in the proto definition
     * @param params Protobuf Parameters in JSON Format
     * @param callback Callback to consume Streaming messages
     * @return Future to get a list of JSON Responses
     */
    private CompletableFuture<List<String>> executeStream(
            @NonNull final String callId,
            @NonNull final String params,
            @Nullable final StreamCallback callback,
            final boolean storeResult
    ) {
        final Descriptors.MethodDescriptor method = getMethodDescriptor(callId);

        final DynamicMessage request = ServerCallExecutor.getRequestMessage(method, params);
        final MethodDescriptor<Object, Object> methodDescriptor = getMethodDescriptor(method);
        final ClientCall<Object, Object> clientCall = this.call.getConnection().getManagedChannel().newCall(
                methodDescriptor,
                CallOptions.DEFAULT.withDeadlineAfter(this.call.getTimeout().getSeconds(), TimeUnit.SECONDS)
        );
        this.internalCalls.add(clientCall);
        final StaticStreamObserver propertyObserver = new StaticStreamObserver(clientCall, callback, storeResult);
        final CompletableFuture<List<String>> future = propertyObserver.getFuture();
        this.internalFutures.add(future);
        ClientCalls.asyncServerStreamingCall(clientCall, request, propertyObserver);
        return future;
    }

    /**
     * Find a method by command or property identifier
     * @param callId the command or property identifier
     * @return the {@link MethodDescriptor}
     */
    private Descriptors.MethodDescriptor getMethodDescriptor(String callId) {
        if (!this.call.getFeature().isPresent()) {
            throw new RuntimeException("Call requires a feature but Optional is empty");
        }
        final Descriptors.MethodDescriptor method = this.call.getFeature().get().findMethodByName(callId);
        if (method == null) {
            throw new RuntimeException("Server " + this.call.getBaseCall().getServerId() + " doesn't expose call to " + callId);
        }
        return method;
    }

    /**
     * Private Helpers to retrieve dynamic protobuf constructs
     * @param method the {@link Descriptors.MethodDescriptor}
     * @return {@link MethodDescriptor}
     */
    private static MethodDescriptor<Object, Object> getMethodDescriptor(
            @NonNull final Descriptors.MethodDescriptor method
    ) {
        return (MethodDescriptor
                .newBuilder()
                .setType(MethodDescriptor.MethodType.UNARY)
                .setFullMethodName(getFullMethodName(method))
                .setRequestMarshaller(new DynamicMessageMarshaller(method.getInputType()))
                .setResponseMarshaller(new DynamicMessageMarshaller(method.getOutputType()))
                .build()
        );
    }

    /**
     * Extract the method full name from {@link Descriptors.MethodDescriptor}
     * @param method the {@link Descriptors.MethodDescriptor}
     * @return the method full name
     */
    private static String getFullMethodName(@NonNull final Descriptors.MethodDescriptor method) {
        return (MethodDescriptor.generateFullMethodName(
                method.getService().getFullName(),
                method.getName())
        );
    }

    /**
     * Converts a gRPC encoded request string into a Dynamic message
     * @param method the {@link Descriptors.MethodDescriptor}
     * @param params the gRPC encoded string
     * @return the method full name
     */
    private static DynamicMessage getRequestMessage(
            @NonNull final Descriptors.MethodDescriptor method,
            @NonNull final String params
    ) {
        final DynamicMessage.Builder parBuilder = DynamicMessage.newBuilder(method.getInputType());

        try {
            JsonFormat.parser().merge(params, parBuilder);
        } catch (InvalidProtocolBufferException e) {
            throw new IllegalArgumentException(e.getMessage());
        }

        return (parBuilder.build());
    }

    /**
     * Converts a gRPC encoded response byte string into a Dynamic message
     * @param method the {@link Descriptors.MethodDescriptor}
     * @param response the gRPC encoded byte string
     * @return the method full name
     */
    private static DynamicMessage getRequestResponse(
            @NonNull final Descriptors.MethodDescriptor method,
            @NonNull final ByteString response
    ) {
        final DynamicMessage.Builder parBuilder = DynamicMessage.newBuilder(method.getOutputType());

        try {
            parBuilder.mergeFrom(response);
        } catch (InvalidProtocolBufferException e) {
            throw new IllegalArgumentException(e.getMessage());
        }

        return (parBuilder.build());
    }
}
