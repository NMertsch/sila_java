package sila_java.library.manager;

import com.google.protobuf.util.JsonFormat;
import io.grpc.*;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.bouncycastle.asn1.x500.RDN;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.core.silaservice.v1.SiLAServiceGrpc;
import sila2.org.silastandard.core.silaservice.v1.SiLAServiceOuterClass;
import sila_java.library.cloudier.client.CloudierClient;
import sila_java.library.cloudier.client.CloudierClientEndpoint;
import sila_java.library.cloudier.client.CloudierClientObserver;
import sila_java.library.core.discovery.SiLADiscovery;
import sila_java.library.core.encryption.SelfSignedCertificate;
import sila_java.library.core.sila.mapping.feature.MalformedSiLAFeature;
import sila_java.library.core.sila.types.SiLAString;
import sila_java.library.core.utils.Utils;
import sila_java.library.manager.executor.CallListener;
import sila_java.library.manager.executor.DefaultClientCallMetadataInjector;
import sila_java.library.manager.executor.ServerCallManager;
import sila_java.library.manager.models.CallCompleted;
import sila_java.library.manager.models.Server;
import sila_java.library.manager.models.SiLACall;
import sila_java.library.manager.server_management.Connection;
import sila_java.library.manager.server_management.ServerConnectionException;
import sila_java.library.manager.server_management.ServerLoading;
import sila_java.library.server_base.config.ServerConfiguration;
import sila_java.library.server_base.identification.ServerInformation;

import java.io.IOException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

import static sila_java.library.core.encryption.EncryptionUtils.readCertificate;
import static sila_java.library.core.encryption.EncryptionUtils.writeCertificateToString;
import static sila_java.library.core.sila.mapping.feature.FeatureGenerator.generateFeature;
import static sila_java.library.manager.server_management.ServerLoading.*;

/**
 * Singleton Manager to manage SiLA Servers
 *
 * This Manager will keep a cache of all servers added either through discovery or via invoking
 * the add function by other means (e.g. manually). It will also keep track of their Status with a
 * Heartbeat.
 */
@Slf4j
public class ServerManager implements AutoCloseable {
    private static final int MAX_SERVICE_TIMEOUT = 3; // [s]
    private static ServerManager instance;
    @Getter
    private final ServerCallManager serverCallManager = new ServerCallManager();
    @Getter
    private final SiLADiscovery discovery = new SiLADiscovery();
    private final Map<UUID, Connection> connections = new ConcurrentHashMap<>();
    private final Map<UUID, Server> servers = new ConcurrentHashMap<>();
    private final List<ServerListener> serverListenerList = new CopyOnWriteArrayList<>();
    @Getter
    private CloudierClient cloudierClient;
    private ClientInterceptor clientInterceptor = new DefaultClientCallMetadataInjector();

    @Getter @Setter
    private boolean allowUnsecureConnection = false;

    @Getter @Setter
    private X509Certificate rootCertificate = null; // todo refactor to avoid race condition happening between instantiation and set

    /**
     * Create a SiLAManager instance if not already present and returns it.
     * @throws RuntimeException if unable to build SslContext during first call
     * @return The single manager instance
     */
    public static ServerManager getInstance() {
        synchronized (ServerManager.class) {
            if (instance == null) {
                instance = new ServerManager();
            }
        }
        return instance;
    }

    /**
     * Constructor
     */
    private ServerManager() {
        try {
            this.cloudierClient = new CloudierClient(new CloudierClientEndpoint.CloudServerListener() {
                @Override
                public void onAdd(UUID serverUUID, CloudierClientObserver cloudierClientObserver) {
                    CompletableFuture.runAsync(() -> {
                        // todo try and catch to avoid silent exception
                        final Server server = new Server();
                        server.setJoined(new Date());
                        //server.setHost(null);
                        //server.setPort(null);
                        // todo check how to handle this, should be null?
                        server.setHost("localhost");
                        server.setPort(50051);
                        server.setConnectionType(Server.ConnectionType.SERVER_INITIATED);
                        server.setStatus(Server.Status.ONLINE);
                        try {
                            CompletableFuture<SiLAServiceOuterClass.Get_ServerUUID_Responses> serverUUIDResponse = cloudierClientObserver.getOptionalServerUUID();
                            CompletableFuture<SiLAServiceOuterClass.Get_ServerName_Responses> serverName = cloudierClientObserver.getServerName();
                            CompletableFuture<SiLAServiceOuterClass.Get_ServerType_Responses> serverType = cloudierClientObserver.getServerType();
                            CompletableFuture<SiLAServiceOuterClass.Get_ServerDescription_Responses> serverDescription = cloudierClientObserver.getServerDescription();
                            CompletableFuture<SiLAServiceOuterClass.Get_ServerVendorURL_Responses> serverVendorURL = cloudierClientObserver.getServerVendorUrl();
                            CompletableFuture<SiLAServiceOuterClass.Get_ServerVersion_Responses> serverVersion = cloudierClientObserver.getServerVersion();
                            CompletableFuture<SiLAServiceOuterClass.Get_ImplementedFeatures_Responses> features = cloudierClientObserver.getServerImplementedFeatures();
                            CompletableFuture.allOf(
                                    serverUUIDResponse, serverName, serverType, serverDescription, serverVendorURL, serverVersion, features
                            ).get(60, TimeUnit.SECONDS);
                            final UUID uuid = UUID.fromString(serverUUIDResponse.get().getServerUUID().getValue());
                            server.setConfiguration(
                                    new ServerConfiguration(
                                            serverName.get().getServerName().getValue(),
                                            uuid
                                    )
                            );
                            server.setNegotiationType(Server.NegotiationType.TLS);
                            server.setInformation(new ServerInformation(
                                    serverType.get().getServerType().getValue(),
                                    serverDescription.get().getServerDescription().getValue(),
                                    serverVendorURL.get().getServerVendorURL().getValue(),
                                    serverVersion.get().getServerVersion().getValue()
                            ));
                            final List<CompletableFuture<SiLAServiceOuterClass.GetFeatureDefinition_Responses>> featuresDefinition = features.get().getImplementedFeaturesList()
                                    .stream()
                                    .map(SiLAFramework.String::getValue)
                                    .map(cloudierClientObserver::getServerImplementedFeatures)
                                    .collect(Collectors.toList());
                            CompletableFuture.allOf(featuresDefinition.toArray(new CompletableFuture[0])).get(60, TimeUnit.SECONDS);
                            for (CompletableFuture<SiLAServiceOuterClass.GetFeatureDefinition_Responses> f : featuresDefinition) {
                                final SiLAServiceOuterClass.GetFeatureDefinition_Responses responses = f.get();
                                final String rawFeatureDefinition = Utils.cleanupXMLString(responses.getFeatureDefinition().getValue());
                                server.getFeatures().add(generateFeature(rawFeatureDefinition));
                            }
                        } catch (InterruptedException | ExecutionException | TimeoutException | IOException e) {
                            log.warn("Error while adding server", e);
                            // todo raise exception
                        }
                        final UUID uuid = server.getConfiguration().getUuid();
                        final Server serverInManager = servers.get(uuid);
                        final boolean isNewServer = (serverInManager == null);
                        if (!isNewServer && (serverInManager.getStatus() == Server.Status.ONLINE)) {
                            log.warn("Server with id `{}` is already present in the manager and online! ", uuid);
                            // todo raise exception
                        }
                        try {
                            connections.put(uuid, new Connection(server, () -> {
                                log.info("Ended connection with server {}", serverUUID.toString());
                                cloudierClientObserver.close();
                            }));
                        } catch (MalformedSiLAFeature e) {
                            // todo raise exception
                            e.printStackTrace();
                        }
                        if (isNewServer) {
                            servers.put(uuid, server);
                            serverListenerList.forEach(listener -> listener.onServerAdded(uuid, server));
                        } else {
                            // todo we should merge serverInManager & the new one together instead replacing?
                            // to prevent setServerStatus from setting it offline
                            serverInManager.setConnectionType(Server.ConnectionType.SERVER_INITIATED);
                            serverListenerList.forEach(listener -> listener.onServerChange(uuid, server));
                            servers.put(uuid, server);
                            serverListenerList.forEach(listener -> listener.onServerChange(uuid, server));
                        }
                    });
                }

                @Override
                public void onEnd(Optional<UUID> optionalServerUUID, Optional<Throwable> t) {
                    final String serverId = optionalServerUUID.map(UUID::toString).orElse("Unknown UUID");
                    if (t.isPresent()) {
                        log.error("Server initiated connection with server {} ended with an error", serverId, t.get());
                    } else {
                        log.warn("Server initiated connection with server {} has been closed", serverId);
                    }
                    optionalServerUUID.ifPresent(uuid -> setServerStatus(uuid, Server.Status.OFFLINE));
                }
            });
        } catch (IOException | CertificateEncodingException | SelfSignedCertificate.CertificateGenerationException e) {
            this.cloudierClient = null;
            log.error("Failed to start cloudier client", e);
        }
        discovery.addListener((instanceId, host, port, certificate) -> {
            if (!this.allowUnsecureConnection && this.rootCertificate == null && !certificate.isPresent()) {
                log.warn("Unsafe connections to servers are not allowed! id: {}", instanceId);
                return;
            }
            final String certificateStr;
            if (this.rootCertificate != null || certificate.isPresent()) {
                final X509Certificate x509Certificate = (this.rootCertificate != null) ? this.rootCertificate : certificate.get();
                final String cnStr;
                try {
                    X500Name x500name = new JcaX509CertificateHolder(x509Certificate).getSubject();
                    RDN cn = x500name.getRDNs(BCStyle.CN)[0];
                    cnStr = cn.getFirst().getValue().toString();
                } catch (Exception e) {
                    log.debug("Unable to identify certificate common name", e);
                    return;
                }
                if (this.rootCertificate == null && !cnStr.equals("SiLA2")) {
                    log.debug("Untrusted server certificate first common name (CN) must be \"SiLA2\"");
                    return;
                }
                try {
                    certificateStr = writeCertificateToString(x509Certificate);
                } catch (IOException e) {
                    log.debug(
                            "Server with instance id: '{}' has an invalid certificate authority: '{}'",
                            instanceId,
                            e.getMessage()
                    );
                    return;
                }
            } else {
                certificateStr = null;
            }

            try {
                final Server server = this.servers.get(instanceId);
                if (server != null && (server.getStatus() == Server.Status.ONLINE)) {
                    log.debug("Discovery found server with uuid: " + instanceId);
                    return;
                }
                if ((certificateStr != null)) {
                    ServerManager.getInstance().addServer(host, port, certificateStr);
                } else {
                    ServerManager.getInstance().addServer(host, port);
                }
            } catch (final ServerAdditionException e) {
                log.warn(
                        "Failed to add server instance: '{}'. Reason: '{}'",
                        instanceId,
                        e.getMessage()
                );
            }
        });
        this.serverCallManager.addListener(new CallListener() {
            @Override
            public void onComplete(CallCompleted callCompleted) {
                final SiLACall siLACall = callCompleted.getSiLACall();
                final boolean isSetServerNameCall =
                        siLACall.getType().equals(SiLACall.Type.UNOBSERVABLE_COMMAND) &&
                        siLACall.getFeatureId().equals("SiLAService") &&
                        siLACall.getCallId().equals("SetServerName");
                if (!isSetServerNameCall) {
                    return;
                }
                final UUID serverId = siLACall.getServerId();
                final String serverName;
                try {
                    final SiLAServiceOuterClass.SetServerName_Parameters.Builder builder = SiLAServiceOuterClass.SetServerName_Parameters.newBuilder();
                    JsonFormat.parser().merge(siLACall.getParameters(), builder);
                    serverName = builder.build().getServerName().getValue();
                } catch (Exception e) {
                    log.debug("Failed to parse & build set server name parameters: [{}]", siLACall.getParameters());
                    return;
                }
                final Server server = servers.get(serverId);
                if (server == null) {
                    log.info("Cannot update server [{}] name since it is not present in the manager", serverId.toString());
                    return;
                }
                log.info("Server [{}] changed it's name to {}", serverId.toString(), serverName);

                server.setConfiguration(new ServerConfiguration(serverName, server.getConfiguration().getUuid()));
                serverListenerList.forEach(listener -> listener.onServerChange(serverId, server));
            }
        });
    }

    /**
     * Initialise SiLA Manager with pre-defined SiLA Servers, e.g. from a persistence layer
     * It will also set the servers status in the map to offline
     * @param silaServers Map containing all predefined SiLAServers
     */
    public void initialize(@NonNull final Map<UUID, Server> silaServers) {
        if (!this.connections.isEmpty() || !this.servers.isEmpty()) {
            throw new IllegalStateException("SiLA Manager can only be initialised in an empty state");
        }
        log.info("Initializing SiLA Manager with " + silaServers.size() + " servers");

        silaServers.forEach((uuid, server) -> {
            server.setStatus(Server.Status.OFFLINE);
            final ManagedChannel managedChannel =
                    server.getNegotiationType().equals(Server.NegotiationType.TLS) ?
                            this.getServerCallManager().newChannelBuilderWithEncryption(server.getHost(), server.getPort()).build() :
                            this.getServerCallManager().newChannelBuilderWithoutEncryption(server.getHost(), server.getPort()).build();
            final Connection connection;
            try {
                connection = new Connection(server, managedChannel);
            } catch (final MalformedSiLAFeature e) {
                managedChannel.shutdownNow();
                return;
            }
            this.servers.put(uuid, server);
            this.connections.put(uuid, connection);
            connection.attachAndTriggerListener(this::updateServerState);
        });
        log.info("Initialization complete");
    }

    /**
     * Clear any servers from manager
     */
    public void clear() {
        this.servers.keySet().forEach(this::removeServer);
        this.discovery.clearCache();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void close() {
        this.discovery.close();
        this.connections.values().forEach(Connection::close);
        this.connections.clear();
        if (this.cloudierClient != null) {
            this.cloudierClient.close();
        }
        this.servers.clear();
        this.serverListenerList.clear();
        instance = null;
    }

    /**
     * Removing Server manually
     *
     * @param id Server Id used for referencing to the Server
     */
    public synchronized void removeServer(@NonNull final UUID id) {
        final Connection connection = connections.get(id);
        final Server server = servers.get(id);

        if (connection == null || server == null) {
            log.warn("Server with id: " + id + " already removed from cache.");
            return;
        }

        // Inform listeners of server removal
        serverListenerList.forEach(listener -> listener.onServerRemoved(id, server));

        // First close connection to stop channel updates
        connections.remove(id);
        connection.close();
        servers.remove(id);

        log.info("[removeServer] removed serverId={}", id);
    }

    /**
     * Add Server to the manager
     *
     * @param host Host on which SiLA Server is exposed
     * @param port Port on which SiLA Server is exposed
     * @throws ServerAdditionException if unable to add the server
     */
    @Deprecated
    public synchronized void addServer(@NonNull final String host, final int port) throws ServerAdditionException {
        // Create Server
        final Server server = new Server();
        server.setJoined(new Date());
        server.setHost(host);
        server.setPort(port);
        server.setConnectionType(Server.ConnectionType.CLIENT_INITIATED);
        server.setStatus(Server.Status.ONLINE);

        try {
            addServer(server);
        } catch (final ServerAdditionException e) {
            serverListenerList.forEach(listener -> listener.onServerAdditionFail(host, port, e.getMessage()));
            throw e;
        }
    }

    /**
     * Add a server into the manager
     *
     * @param host the server host
     * @param port the server port
     * @param certificate the server certificate
     * @throws ServerAdditionException if the server could not be added
     */
    @SneakyThrows
    public synchronized void addServer(@NonNull final String host, final int port, String certificate) throws ServerAdditionException {
        // Create Server
        final Server server = new Server();
        server.setJoined(new Date());
        server.setHost(host);
        server.setPort(port);
        server.setConnectionType(Server.ConnectionType.CLIENT_INITIATED);
        server.setStatus(Server.Status.ONLINE);
        server.setCertificateAuthority(certificate);

        try {
            addServer(server);
        } catch (final ServerAdditionException e) {
            serverListenerList.forEach(listener -> listener.onServerAdditionFail(host, port, e.getMessage()));
            throw e;
        }
    }

    /**
     * Add Server
     *
     * @param server the server to add
     * @throws ServerAdditionException if unable to add the server
     */
    private void addServer(@NonNull final Server server) throws ServerAdditionException {
        if (!this.allowUnsecureConnection && (server.getCertificateAuthority() == null || server.getCertificateAuthority().isEmpty())) {
            throw new RuntimeException("A server certificate must be provided to add a server.");
        }
        // Establish Connection
        final ManagedChannel managedChannel;
        // Load SiLA Server
        try {
            managedChannel = ServerLoading.attemptConnectionWithServer(
                    server,
                    Optional.ofNullable(this.clientInterceptor),
                    this.allowUnsecureConnection,
                    (server.getCertificateAuthority() != null) ? readCertificate(server.getCertificateAuthority()) : null
            );
        } catch (final IOException | ServerConnectionException e) {
            throw new ServerAdditionException(server, e.getMessage());
        }
        try {
            loadServer(server, managedChannel);
        } catch (final ServerLoadingException e) {
            managedChannel.shutdownNow();
            throw new ServerAdditionException(server, e.getMessage());
        }

        log.info(
                "[addServer] Resolved SiLA Server serverName={} on {}:{}",
                server.getConfiguration().getName(),
                server.getHost(),
                server.getPort()
        );

        // Protect against re-adding when Online
        final UUID uuid = server.getConfiguration().getUuid();
        final Server serverInManager = servers.get(uuid);
        final boolean isNewServer = (serverInManager == null);
        if (!isNewServer && (serverInManager.getStatus() == Server.Status.ONLINE)) {
            log.warn("Server with id `{}` is already present in the manager and online! ", uuid);
            managedChannel.shutdownNow();
            throw new ServerAdditionException(
                    server,
                    "Server with id [" + uuid + "] is already in the manager and online"
            );
        }

        // Add Server and notify when successful resolving
        final Connection connection;
        try {
            connection = new Connection(server, managedChannel);
        } catch (final MalformedSiLAFeature e) {
            managedChannel.shutdownNow();
            throw new ServerAdditionException(server, e.getMessage());
        }

        servers.put(uuid, server);
        final Connection previousConnection = connections.put(uuid, connection);
        if (previousConnection != null) {
            previousConnection.close();
        }
        if (isNewServer) {
            serverListenerList.forEach(listener -> listener.onServerAdded(uuid, server));
        } else {
            serverListenerList.forEach(listener -> listener.onServerChange(uuid, server));
        }
        connection.attachAndTriggerListener(this::updateServerState);
    }

    /**
     * Private Utility to check and update the state of the server
     * Attempt to execute a call to retrieve the UUID of the server.
     * If the call succeed and the UUID match, the status of server will be set to online, offline otherwise.
     *
     * @param serverId The server unique identifier
     * @param channel The server channel
     */
    private void updateServerState(@NonNull final UUID serverId, @NonNull final ManagedChannel channel) {
        final Server server = this.servers.get(serverId);
        if (server == null) {
            log.warn("Unable to update server state because server {} does not exist", serverId);
            return;
        }
        if (server.getConnectionType().equals(Server.ConnectionType.SERVER_INITIATED)) {
            log.warn("No need to manually check server {} state in server initiated mode", serverId);
            return;
        }
        final ConnectivityState state = channel.getState(false);
        if (state == ConnectivityState.IDLE) {
            CompletableFuture.runAsync(() -> {
                Server.Status status = Server.Status.OFFLINE;
                try {
                    final UUID uuid = getServerId(SiLAServiceGrpc.newBlockingStub(channel));
                    if (server.getConfiguration().getUuid().equals(uuid)) {
                        status = Server.Status.ONLINE;
                    } else {
                        log.debug(
                                "GetServerId returned by server UUID: {} differ from the one in manager {}",
                                uuid,
                                server.getConfiguration().getUuid()
                        );
                    }
                } catch (final Exception e) {
                    log.debug("Failed UUID Retrieval {}", e.getMessage());
                }
                if (server.getConnectionType().equals(Server.ConnectionType.SERVER_INITIATED)) {
                    log.warn("No need to manually check server {} state in server initiated mode", serverId);
                    return;
                }
                this.setServerStatus(serverId, status);
            });
        }
    }

    /**
     * Set the Status of a Server
     * @param id Server Id
     * @param status Server Status
     */
    private void setServerStatus(@NonNull final UUID id, @NonNull final Server.Status status) {
        final Connection siLAConnection = this.connections.get(id);

        if (siLAConnection == null) {
            log.warn("Server with id " + id + " doesn't exist.");
            return;
        }

        final Server server = this.servers.get(id);

        // Only Notify Change, if Change happened
        if (!server.getStatus().equals(status)) {
            server.setStatus(status);
            log.info("[changeServerStatus] change {} status to {}", id, status);
            // also update listeners
            serverListenerList.forEach(listener -> listener.onServerChange(id, server));
        }
    }

    /**
     * Set the Status of a Server
     *
     * @implNote serverNameResponse has no response at the moment, as such
     * we proceed assuming the setServerName was successful and catching RcpException at application level
     * alternatively, wrap this function call with try {} catch(RpcException e) {}
     *
     * @param id Server Id
     * @param newServerName String Server Name
     */
    public void setServerName(@NonNull final UUID id, @NonNull final String newServerName) {
        final Connection connection = this.connections.get(id);

        if (connection == null) {
            log.warn("Server with id " + id + " doesn't exit.");
            return;
        }

        final Server server = this.servers.get(id);
        if (!server.getConfiguration().getName().equals(newServerName)) {
            log.info("[setServerName] server name changed to {}", newServerName);
            @NonNull final ManagedChannel managedChannel = connection.getManagedChannel();
            @NonNull final SiLAServiceGrpc.SiLAServiceBlockingStub siLAServiceBlockingStub = SiLAServiceGrpc
                    .newBlockingStub(managedChannel);

            siLAServiceBlockingStub
                    .withDeadlineAfter(MAX_SERVICE_TIMEOUT, TimeUnit.SECONDS)
                    .setServerName(SiLAServiceOuterClass.SetServerName_Parameters.newBuilder().setServerName(
                            SiLAString.from(newServerName)).build()
                    );
            // check for updated name on server
            final String serverName = getServerName(siLAServiceBlockingStub);
            if (serverName.equals(newServerName)) {
                log.info("[setServerName] server name changed to {}", newServerName);
                // update server prior to updating through the listener
                server.setConfiguration(new ServerConfiguration(newServerName, server.getConfiguration().getUuid()));
                serverListenerList.forEach(listener -> listener.onServerChange(id, server));
            } else {
                throw new IllegalStateException("Server Name was not updated on SiLA Server.");
            }
        }
    }

    /**
     * Get the map of servers
     *
     * @implNote Synchronised call because servers might be in removal state
     *
     * @return A read only map of servers
     */
    public Map<UUID, Server> getServers() {
        return Collections.unmodifiableMap(this.servers);
    }

    /**
     * Get server connections
     * @return the server connections
     */
    public synchronized Map<UUID, Connection> getConnections() {
        return Collections.unmodifiableMap(this.connections);
    }

    /**
     * Add additional listener to retrieve SiLA Server information in-process
     */
    public void addServerListener(@NonNull final ServerListener siLAServerListener) {
        serverListenerList.add(siLAServerListener);
    }

    /**
     * Remove server listener
     * @param siLAServerListener the server listener
     */
    public void removeServerListener(@NonNull final ServerListener siLAServerListener) {
        serverListenerList.remove(siLAServerListener);
    }

    /**
     * Get client interceptor
     * @return the client interceptor
     */
    public ClientInterceptor getClientInterceptor() {
        return clientInterceptor;
    }

    /**
     * Set client interceptor
     * @param clientInterceptor the client interceptor
     */
    public void setClientInterceptor(ClientInterceptor clientInterceptor) {
        this.clientInterceptor = clientInterceptor;
    }
}
