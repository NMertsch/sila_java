package sila_java.library.manager.server_management;

import lombok.NonNull;
import sila_java.library.manager.models.Server;

/**
 * Class representing a failed connection attempt to a server
 */
public class ServerConnectionException extends Exception {

    /**
     * Constructor
     * @param server The Server with which the connection failed
     */
    ServerConnectionException(@NonNull final Server server) {
        super("Unable to connect to remote server: " + server.getHost() + ":" + server.getPort());
    }

    /**
     * Constructor
     * @param server The Server with which the connection failed
     * @param reason The reason
     */
    ServerConnectionException(@NonNull final Server server, @NonNull final String reason) {
        super("Unable to connect to remote server: " + server.getHost() + ":" + server.getPort() + " because: " + reason);
    }
}