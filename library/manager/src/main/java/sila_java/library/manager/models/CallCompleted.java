package sila_java.library.manager.models;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.OffsetDateTime;

/**
 * Task completed model
 */
@Getter
@AllArgsConstructor
public class CallCompleted {
    private final OffsetDateTime startDate;
    private final OffsetDateTime endDate;
    private final String result;
    private final SiLACall siLACall;
}
