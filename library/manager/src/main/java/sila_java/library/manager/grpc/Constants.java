package sila_java.library.manager.grpc;

import com.google.protobuf.DynamicMessage;
import io.grpc.Context;
import java.util.Set;

/**
 * Constants class
 */
public class Constants {
    /**
     * Constant context metadata identifier key
     */
    public static final Context.Key<Set<FullyQualifiedMetadataContextKey<DynamicMessage>>>
            METADATA_IDENTIFIERS_CTX_KEY = Context.key("metadataFQIContextKeys");
}
