package sila_java.library.manager.models;

import lombok.Getter;
import lombok.NonNull;
import sila_java.library.manager.ServerManager;

import java.util.UUID;

/**
 * Represents a generic SiLA Call
 */
@Getter
public class SiLACall {
    // todo add fully qualified call id
    private final UUID identifier;
    private final UUID serverId;
    private final String featureId;
    private final String callId;
    private final Type type;
    private final String parameters;
    private final String metadatas;

    public enum Type {
        UNOBSERVABLE_COMMAND,
        OBSERVABLE_COMMAND,
        UNOBSERVABLE_PROPERTY,
        OBSERVABLE_PROPERTY,
        OBSERVABLE_PROPERTY_READ,
        GET_FCP_AFFECTED_BY_METADATA,
        UPLOAD_BINARY,
        DOWNLOAD_BINARY,
        /*GET_BINARY_INFO,
        GET_CHUNK,
        BINARY_UPLOAD_REQUEST,
        UPLOAD_BINARY_CHUNK,
        DELETE_UPLOADED_BINARY,
        DELETE_DOWNLOADED_BINARY
         */
    }

    /**
     * Task builder
     */
    public static class Builder {
        private final UUID serverId;
        private final String featureId;
        private final String callId;
        private final Type type;

        private UUID identifier = UUID.randomUUID();
        private String parameters = "{}";
        private String metadatas = "{}";

        /**
         * Constructor
         * @param serverId the server UUID
         * @param featureId the feature identifier
         * @param callId the task identifier
         * @param type the task type
         */
        public Builder(
                @NonNull final UUID serverId,
                @NonNull final String featureId,
                @NonNull final String callId,
                @NonNull final Type type
        ) {
            this.serverId = serverId;
            this.featureId = featureId;
            this.callId = callId;
            this.type = type;
        }

        /**
         * Set task parameters
         * @param parameters the parameters
         * @return the builder instance
         */
        public Builder withParameters(@NonNull final String parameters) {
            this.parameters = parameters;
            return this;
        }

        /**
         * Set task identifier
         * @param identifier the task identifier
         * @return the builder instance
         */
        public Builder withIdentifier(@NonNull final UUID identifier) {
            this.identifier = identifier;
            return this;
        }

        /**
         * Set task metadata
         * @param metadatas the metadata
         * @return the builder instance
         */
        public Builder withMetadata(@NonNull final String metadatas) {
            this.metadatas = metadatas;
            return this;
        }

        /**
         * Build a new {@link SiLACall} instance
         * @return a new {@link SiLACall} instance
         */
        public SiLACall build() {
            return new SiLACall(identifier, serverId, featureId, callId, type, parameters, metadatas);
        }
    }

    /**
     * Call Constructor
     * @param serverId As added to the {@link ServerManager}
     * @param featureId As defined in the Feature Definition
     * @param callId Either a Command or Property Identifier
     * @param type Different Call Types: Unobservable command, Observable command, unobservable & Observable Properties
     * @param parameters Parameters in Protobuf JSON Format as seen on the protobuf documentation
     *
     * @see <a href="https://developers.google.com/protocol-buffers/docs/proto3#json">Protobuf Spec</a>
     */
    public SiLACall(
            @NonNull final UUID serverId,
            @NonNull final String featureId,
            @NonNull final String callId,
            @NonNull final Type type,
            @NonNull final String parameters
    ) {
        this.identifier = UUID.randomUUID();
        this.serverId = serverId;
        this.featureId = featureId;
        this.callId = callId;
        this.type = type;
        this.parameters = parameters;
        this.metadatas = "{}";
    }

    /**
     * Call Constructor
     * @param serverId As added to the {@link ServerManager}
     * @param featureId As defined in the Feature Definition
     * @param callId Either a Command or Property Identifier
     * @param type Different Call Types: Unobservable command, Observable command, unobservable & Observable Properties
     *
     * @see <a href="https://developers.google.com/protocol-buffers/docs/proto3#json">Protobuf Spec</a>
     */
    public SiLACall(
            @NonNull final UUID serverId,
            @NonNull final String featureId,
            @NonNull final String callId,
            @NonNull final Type type
    ) {
        this(serverId, featureId, callId, type, "{}");
    }

    /**
     * Copy Constructor
     * @param siLACall the task
     */
    public SiLACall(@NonNull final SiLACall siLACall) {
        this.identifier = siLACall.identifier;
        this.serverId = siLACall.serverId;
        this.featureId = siLACall.featureId;
        this.callId = siLACall.callId;
        this.type = siLACall.type;
        this.parameters = siLACall.parameters;
        this.metadatas = siLACall.metadatas;
    }

    /**
     * Call Constructor
     * @param identifier the task identifier
     * @param serverId As added to the {@link ServerManager}
     * @param featureId As defined in the Feature Definition
     * @param callId Either a Command or Property Identifier
     * @param type Different Call Types: Unobservable command, Observable command, unobservable & Observable Properties
     * @param parameters Parameters in Protobuf JSON Format as seen on the protobuf documentation
     * @param metadatas the task metadata
     *
     * @see <a href="https://developers.google.com/protocol-buffers/docs/proto3#json">Protobuf Spec</a>
     */
    public SiLACall(
            @NonNull final UUID identifier,
            @NonNull final UUID serverId,
            @NonNull final String featureId,
            @NonNull final String callId,
            @NonNull final Type type,
            @NonNull final String parameters,
            @NonNull final String metadatas
    ) {
        this.identifier = identifier;
        this.serverId = serverId;
        this.featureId = featureId;
        this.callId = callId;
        this.type = type;
        this.parameters = parameters;
        this.metadatas = metadatas;
    }
}
