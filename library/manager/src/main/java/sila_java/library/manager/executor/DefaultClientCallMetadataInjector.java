package sila_java.library.manager.executor;

import io.grpc.*;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import sila_java.library.manager.grpc.Constants;

import java.util.Locale;

import static io.grpc.Metadata.BINARY_BYTE_MARSHALLER;

/**
 * {@link ClientInterceptor} implementation that injects SiLA Metadata in the gRPC context
 */
@Slf4j
@NoArgsConstructor
public class DefaultClientCallMetadataInjector implements ClientInterceptor {

    /**
     * Intercept a call and inject metadata in its context
     *
     * @inheritDoc
     *
     * @param methodDescriptor the remote method to be called.
     * @param callOptions the runtime options to be applied to this call.
     * @param channel the channel which is being intercepted.
     * @return a new {@link ForwardingClientCall.SimpleForwardingClientCall} with SiLA Metadata context
     * @param <ReqT> the request type
     * @param <RespT> the response type
     */
    @Override
    public <ReqT, RespT> ClientCall<ReqT, RespT> interceptCall(
            final MethodDescriptor<ReqT, RespT> methodDescriptor,
            final CallOptions callOptions, Channel channel
    ) {
        return new ForwardingClientCall.SimpleForwardingClientCall<ReqT, RespT>(
                channel.newCall(methodDescriptor, callOptions)
        ) {
            /**
             * On call start, injects SiLA Metadata into the gRPC context
             *
             * @inheritDoc
             *
             * @param responseListener receives response messages
             * @param headers which can contain extra call metadata, e.g. authentication credentials.
             */
            @Override
            public void start(final Listener<RespT> responseListener, final Metadata headers) {
                if (Constants.METADATA_IDENTIFIERS_CTX_KEY.get() != null) {
                    Constants.METADATA_IDENTIFIERS_CTX_KEY.get().forEach(contextKey -> {
                        headers.put(
                                Metadata.Key.of(String.format("sila-%s-bin", contextKey.getFullyQualifiedIdentifier()
                                        .replace("/", "-")
                                        .toLowerCase(Locale.ROOT)), BINARY_BYTE_MARSHALLER),
                                contextKey.getContextKey().get().toByteArray()
                        );
                    });
                }
                super.start(responseListener, headers);
            }
        };
    }
}
