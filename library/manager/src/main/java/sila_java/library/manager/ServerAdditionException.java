package sila_java.library.manager;

import lombok.NonNull;
import sila_java.library.manager.models.Server;

/**
 * Class representing a failed attempt to add a server
 */
public class ServerAdditionException extends Exception {
    /**
     * Constructor
     * @param server The Server that was not added
     * @param reason The reason describing why the server was not added
     */
    public ServerAdditionException(@NonNull final Server server, @NonNull final String reason) {
        super("Addition of server [" + server.getHost() + ":" + server.getPort() + "] failed. Reason: " + reason);
    }
}