package sila_java.library.manager.executor;

import com.google.protobuf.DynamicMessage;
import sila2.org.silastandard.SiLAFramework;
import sila_java.library.manager.models.CallCompleted;
import sila_java.library.manager.models.CallErrored;
import sila_java.library.manager.models.CallStarted;
import sila_java.library.manager.models.SiLACall;

/**
 * Call listener interface for SiLA tasks
 */
public interface CallListener {
    /**
     * Callback when a task starts
     * @param callInProgress The task in progress
     */
    default void onStart(CallStarted callInProgress) {}

    /**
     * Callback when a task is completed
     * @param callCompleted the task completed model
     */
    default void onComplete(CallCompleted callCompleted) {}

    /**
     * Callback when a task is cancelled because of an error
     * @param callErrored the task errored model
     */
    default void onError(CallErrored callErrored) {}

    /**
     * Callback when an Observable Property receives a Property update
     * @param baseCall The base task model
     * @param value The gRPC value encoded as a String
     */
    default void onObservablePropertyUpdate(SiLACall baseCall, String value) {}

    /**
     * Callback when an Observable Command is initialized
     * @param baseCall the base task model
     * @param command the command confirmation model
     */
    default void onObservableCommandInit(SiLACall baseCall, SiLAFramework.CommandConfirmation command) {}

    /**
     * Callback when an Observable Command receives an Execution Info
     * @param baseCall the base task model
     * @param executionInfo the command execution information model
     */
    default void onObservableCommandExecutionInfo(SiLACall baseCall, SiLAFramework.ExecutionInfo executionInfo) {}

    /**
     * Callback when an Observable Command receives an Intermediate Response
     *
     * @param baseCall the base task model
     * @param response the intermediate response
     */
    default void onObservableIntermediateResponse(SiLACall baseCall, DynamicMessage response) {}
}