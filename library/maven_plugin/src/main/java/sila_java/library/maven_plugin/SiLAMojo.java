package sila_java.library.maven_plugin;

import com.google.protobuf.Descriptors;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.BuildPluginManager;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import sila_java.library.core.sila.mapping.feature.MalformedSiLAFeature;
import sila_java.library.core.sila.mapping.grpc.ProtoMapper;
import sila_java.library.core.utils.FileUtils;
import sila_java.library.sila_base.EmptyClass;

import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import static org.twdata.maven.mojoexecutor.MojoExecutor.*;
import static sila_java.library.core.sila.mapping.grpc.DynamicProtoBuilder.generateProtoFile;

/**
 * @implNote Basically search for Feature Definitions and create both proto files and the descendants :-)
 *
 * @implNote Only supports generated protos in ${basedir}/src/main/proto
 */
@Mojo( name = "run", defaultPhase = LifecyclePhase.PROCESS_SOURCES)
public class SiLAMojo extends AbstractMojo {
    /**
     * The protobuf version for the protoc compiler
     */
    @Parameter
    private String protobufVersion;

    /**
     * The grpc version to build the stups for
     */
    @Parameter
    private String grpcVersion;

    /**
     * Input Features that conform with SiLA
     */
    @Parameter
    private File[] features;

    /**
     * Additional folders to generate proto files into
     */
    @Parameter
    private File[] protoGenerationFolders;

    /**
     * Maven project
     */
    @Parameter( defaultValue = "${project}", readonly = true )
    private MavenProject mavenProject;

    /**
     * Maven session
     */
    @Parameter( defaultValue = "${session}", readonly = true )
    private MavenSession mavenSession;

    /**
     * Components are necessary to provide context to other plugins
     */
    @Component
    private BuildPluginManager pluginManager;

    /**
     * @inheritDoc
     */
    @Override
    public void execute() throws MojoExecutionException {
        // Input Validation
        if (protobufVersion == null) {
            throw new MojoExecutionException("Protobuf version has to be specified!");
        }

        if (grpcVersion == null) {
            throw new MojoExecutionException("gRPC Version has to be specified!");
        }

        if (features == null) {
            throw new MojoExecutionException("Features to parse have to be specified!");
        }

        if (features.length == 0) {
            throw new MojoExecutionException("At least one feature has to be specified to parse "
                    + "(what else should the plugin be for?)");
        }

        final boolean generateAdditionalProtos = (protoGenerationFolders != null);
        if (generateAdditionalProtos && (protoGenerationFolders.length == 0)) {
            throw new MojoExecutionException("If additional proto folders are specified, at least one has to be defined!");
        }

        // Show user configuration
        getLog().info( "Executing Custom SiLA Maven Plugin" );
        getLog().info("Found protobuf version: " + protobufVersion);
        getLog().info("Found gRPC version: " + grpcVersion);

        // Copy the proto into the target folder
        final String frameworkProtoName = "SiLAFramework.proto";

        final URL frameworkLocation = EmptyClass.class.getResource("/sila_base/protobuf/" + frameworkProtoName);
        if (frameworkLocation == null) {
            throw new MojoExecutionException("Can not find " + frameworkProtoName);
        }

        final String targetClassesLocation = mavenProject.getBuild().getDirectory() +
                File.separator + "generated-sources" + File.separator + "sila";
        getLog().info("Generating classes into: " + targetClassesLocation);

        final String frameworkProtoLocation = targetClassesLocation + File.separator +
                frameworkProtoName;
        try {
            Files.createDirectories(Paths.get(targetClassesLocation));

            if (generateAdditionalProtos) {
                for (File folder : protoGenerationFolders) {
                    Files.createDirectories(folder.toPath());
                }
            }
        } catch (IOException e) {
            throw new MojoExecutionException("Could not create directories");
        }

        try {
            Files.copy(
                    frameworkLocation.openStream(),
                    Paths.get(frameworkProtoLocation),
                    StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new MojoExecutionException("Could not copy " + frameworkProtoName);
        }

        // Create the proto files from feature definitions
        for (File file : features) {
            try {
                final ProtoMapper protoMapper = ProtoMapper.usingFeature(
                        FileUtils.getFileContent(file.getAbsolutePath())
                );

                final Descriptors.FileDescriptor protoFile = protoMapper.generateProto();
                final String featureName = protoFile.getName();
                getLog().info("Generating Classes for Feature: " + featureName);

                final String protoFileContent = generateProtoFile(protoFile);

                String targetFilePath = targetClassesLocation + File.separator
                        + featureName + ".proto";
                createFile(targetFilePath, protoFileContent);

                if (generateAdditionalProtos) {
                    for (File folder : protoGenerationFolders) {
                        targetFilePath = folder.getPath() + File.separator +
                                featureName + ".proto";
                        createFile(targetFilePath, protoFileContent);
                    }
                }
            } catch (IOException | MalformedSiLAFeature e) {
                throw new MojoExecutionException(e.getClass().getName() + " : Could not parse " + file.toString() + "\n" +
                        e.getMessage());
            }
        }

        // Execute protoc-jar-maven-plugin
        final String protocMavenPluginVersion = "3.5.1.1";

        executeMojo(
                plugin(
                        groupId("com.github.os72"),
                        artifactId("protoc-jar-maven-plugin"),
                        version(protocMavenPluginVersion)
                ),
                goal("run"),
                configuration(
                        element(
                                name("protocArtifact"),
                                "com.google.protobuf:protoc:" + protobufVersion
                        ),
                        element(
                                name("addProtoSources"),
                                "all"
                        ),
                        element(
                               name("includeDirectories"),
                                element(name("include"),
                                        targetClassesLocation)
                        ),
                        element(name("inputDirectories"),
                                element(name("include"),
                                        targetClassesLocation)),
                        element(name("outputTargets"),
                                element(name("outputTarget"),
                                        element(name("type"),
                                                "java")
                                ),
                                element(name("outputTarget"),
                                        element(name("type"),
                                                "grpc-java"),
                                        element(name("pluginArtifact"),
                                                "io.grpc:protoc-gen-grpc-java:" + grpcVersion)
                                )
                        )
                ),
                executionEnvironment(
                        mavenProject,
                        mavenSession,
                        pluginManager
                )
        );
    }

    /**
     * Create a file a the specified absolute path and write content inside
     * @param absoluteFilePath the absolute path
     * @param content the content
     * @throws IOException if cannot create or write the content to the file
     */
    private void createFile(String absoluteFilePath, String content) throws IOException {
        try (BufferedWriter writer = new BufferedWriter(
                new FileWriter(  absoluteFilePath ))) {
            writer.write(content);
        }
        getLog().info("Generated " + absoluteFilePath);
    }
}
