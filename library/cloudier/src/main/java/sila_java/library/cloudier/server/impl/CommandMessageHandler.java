package sila_java.library.cloudier.server.impl;

import io.grpc.stub.StreamObserver;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLACloudConnector;
import sila2.org.silastandard.SiLAFramework;
import sila_java.library.core.sila.errors.SiLAErrors;

@Slf4j
@AllArgsConstructor
public class CommandMessageHandler {
    private final StreamObserver<SiLACloudConnector.SiLAServerMessage> response;

    public void sendSiLAError(String requestUuid, SiLAFramework.SiLAError error) {
        response.onNext(SiLACloudConnector.SiLAServerMessage
                .newBuilder()
                .setRequestUUID(requestUuid)
                .setCommandError(
                        error
                )
                .build()
        );
    }

    public void sendThrowableError(String requestUuid, Throwable error) {
        log.info("Error occurred with request {}, {}", requestUuid, error);
        sendSiLAError(requestUuid, SiLAErrors.throwableToSiLAError(error));
    }

    public void sendUnknownCommandError(String requestUuid, String fqi) {
        log.warn("Client with request {} attempted to call an unknown command {}", requestUuid, fqi);
        sendSiLAError(requestUuid, SiLAFramework.SiLAError
                .newBuilder()
                .setUndefinedExecutionError(
                        SiLAFramework.UndefinedExecutionError
                                .newBuilder()
                                .setMessage("Server does not expose command with id " + fqi)
                                .build()
                )
                .build()
        );
    }
}
