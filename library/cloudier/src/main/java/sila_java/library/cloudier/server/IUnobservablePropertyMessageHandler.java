package sila_java.library.cloudier.server;

import sila2.org.silastandard.SiLACloudConnector;

public interface IUnobservablePropertyMessageHandler {
    void onUnobservableProperty(CloudierRequest<SiLACloudConnector.UnobservablePropertyRead> request);
}
