package sila_java.library.cloudier.server.impl;

import com.google.protobuf.GeneratedMessageV3;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLACloudConnector;
import sila_java.library.cloudier.server.CallMessageMap;
import sila_java.library.cloudier.server.CloudCallForwarder;
import sila_java.library.cloudier.server.CloudierRequest;
import sila_java.library.cloudier.server.IUnobservablePropertyMessageHandler;

import java.util.Optional;

@Slf4j
public class UnobservablePropertyMessageHandler implements IUnobservablePropertyMessageHandler {
    private final StreamObserver<SiLACloudConnector.SiLAServerMessage> response;
    private final CallMessageMap callMessageMap;
    private final PropertyMessageHandler propertyMessageHandler;

    public UnobservablePropertyMessageHandler(StreamObserver<SiLACloudConnector.SiLAServerMessage> response, CallMessageMap callMessageMap) {
        this.response = response;
        this.callMessageMap = callMessageMap;
        this.propertyMessageHandler = new PropertyMessageHandler(response);
    }

    @Override
    public void onUnobservableProperty(CloudierRequest<SiLACloudConnector.UnobservablePropertyRead> request) {
        final String fqi = request.getRequest().getFullyQualifiedPropertyId();
        final Optional<CloudCallForwarder<GeneratedMessageV3, GeneratedMessageV3>> callHandler
                = this.callMessageMap.getCallHandler(fqi, SiLACloudConnector.SiLAClientMessage.MessageCase.UNOBSERVABLEPROPERTYREAD);
        if (callHandler.isPresent()) {
            log.info("Forwarding call for " + fqi);
            callHandler.get().forward(request.getRequestUUID(), request.getRequest().getMetadataList(), request.getRequest().toByteString(), (message) -> {
                response.onNext(SiLACloudConnector.SiLAServerMessage
                        .newBuilder()
                        .setRequestUUID(request.getRequestUUID())
                        .setUnobservablePropertyValue(
                                SiLACloudConnector.UnobservablePropertyValue
                                        .newBuilder()
                                        .setValue(message.toByteString())
                                        .build()
                        )
                        .build()
                );
            }, (throwable -> propertyMessageHandler.sendThrowableError(request.getRequestUUID(), throwable)));
        } else {
            propertyMessageHandler.sendUnknownPropertyError(request.getRequestUUID(), fqi);
        }
    }
}
