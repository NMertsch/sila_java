package sila_java.library.cloudier.server.impl;

import com.google.protobuf.GeneratedMessageV3;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLACloudConnector;
import sila2.org.silastandard.SiLAFramework;
import sila_java.library.cloudier.server.CallMessageMap;
import sila_java.library.cloudier.server.CloudCallForwarder;
import sila_java.library.cloudier.server.CloudierRequest;
import sila_java.library.cloudier.server.IObservableCommandMessageHandler;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Slf4j
public class ObservableCommandMessageHandler implements IObservableCommandMessageHandler {
    private final StreamObserver<SiLACloudConnector.SiLAServerMessage> response;
    private final CallMessageMap callMessageMap;
    private final CommandMessageHandler commandMessageHandler;
    // todo remove entry from map once life time expires
    // todo make thread safe
    private final Map<String, String> uuidToFqiMap = new HashMap<>();

    public ObservableCommandMessageHandler(StreamObserver<SiLACloudConnector.SiLAServerMessage> response, CallMessageMap callMessageMap) {
        this.response = response;
        this.callMessageMap = callMessageMap;
        this.commandMessageHandler = new CommandMessageHandler(response);
    }

    @Override
    public void onCommandInit(CloudierRequest<SiLACloudConnector.ObservableCommandInitiation> request) {
        final String fqi = request.getRequest().getFullyQualifiedCommandId();
        final Optional<CloudCallForwarder<GeneratedMessageV3, SiLAFramework.CommandConfirmation>> callHandler =
                this.callMessageMap.getCallHandler(
                        fqi, SiLACloudConnector.SiLAClientMessage.MessageCase.OBSERVABLECOMMANDINITIATION
        );
        if (callHandler.isPresent()) {
            log.info("Forwarding call for " + fqi);
            uuidToFqiMap.put(request.getRequestUUID(), fqi);
            callHandler.get().forward(
                    request.getRequestUUID(),
                    request.getRequest().getCommandParameter().getMetadataList(),
                    request.getRequest().getCommandParameter().getParameters(),
                    (message) -> {
                        response.onNext(SiLACloudConnector.SiLAServerMessage
                                .newBuilder()
                                .setRequestUUID(request.getRequestUUID())
                                .setObservableCommandConfirmation(
                                        SiLACloudConnector.ObservableCommandConfirmation
                                                .newBuilder()
                                                .setCommandConfirmation(message)
                                                .build()
                                )
                                .build()
                        );
                    }, (throwable -> commandMessageHandler.sendThrowableError(request.getRequestUUID(), throwable)));
        } else {
            commandMessageHandler.sendUnknownCommandError(request.getRequestUUID(), fqi);
        }
    }

    @Override
    public void onCommandExecInfo(CloudierRequest<SiLACloudConnector.ObservableCommandExecutionInfoSubscription> request) {
        final String fqi = this.uuidToFqiMap.get(request.getRequestUUID());
        final Optional<CloudCallForwarder<GeneratedMessageV3, SiLAFramework.ExecutionInfo>> callHandler =
                this.callMessageMap.getCallHandler(fqi, SiLACloudConnector.SiLAClientMessage.MessageCase.OBSERVABLECOMMANDEXECUTIONINFOSUBSCRIPTION);
        if (callHandler.isPresent()) {
            log.info("Forwarding call for " + fqi);
            callHandler.get().forward(request.getRequestUUID(), request.getRequest().getCommandExecutionUUID().toByteString(), (message) -> {
                response.onNext(SiLACloudConnector.SiLAServerMessage
                        .newBuilder()
                        .setRequestUUID(request.getRequestUUID())
                        .setObservableCommandExecutionInfo(SiLACloudConnector.ObservableCommandExecutionInfo.newBuilder()
                                .setCommandExecutionUUID(request.getRequest().getCommandExecutionUUID())
                                .setExecutionInfo(message)
                                .build()
                        )
                        .build()
                );
            }, (throwable -> commandMessageHandler.sendThrowableError(request.getRequestUUID(), throwable)));
        } else {
            commandMessageHandler.sendUnknownCommandError(request.getRequestUUID(), fqi);
        }
    }

    @Override
    public void onIntermediate(CloudierRequest<SiLACloudConnector.ObservableCommandIntermediateResponseSubscription> request) {
        final String fqi = this.uuidToFqiMap.get(request.getRequestUUID());
        final Optional<CloudCallForwarder<GeneratedMessageV3, GeneratedMessageV3>> callHandler =
                this.callMessageMap.getCallHandler(fqi, SiLACloudConnector.SiLAClientMessage.MessageCase.OBSERVABLECOMMANDINTERMEDIATERESPONSESUBSCRIPTION);
        if (callHandler.isPresent()) {
            log.info("Forwarding call for " + fqi);
            callHandler.get().forward(request.getRequestUUID(), request.getRequest().getCommandExecutionUUID().toByteString(), (message) -> {
                response.onNext(SiLACloudConnector.SiLAServerMessage
                        .newBuilder()
                        .setRequestUUID(request.getRequestUUID())
                        .setObservableCommandIntermediateResponse(SiLACloudConnector.ObservableCommandIntermediateResponse.newBuilder()
                                .setCommandExecutionUUID(request.getRequest().getCommandExecutionUUID())
                                .setResponse(message.toByteString())
                                .build()
                        )
                        .build()
                );
            }, (throwable -> commandMessageHandler.sendThrowableError(request.getRequestUUID(), throwable)));
        } else {
            commandMessageHandler.sendUnknownCommandError(request.getRequestUUID(), fqi);
        }
    }

    @Override
    public void onResult(CloudierRequest<SiLACloudConnector.ObservableCommandGetResponse> request) {
        final String fqi = this.uuidToFqiMap.get(request.getRequestUUID());
        final Optional<CloudCallForwarder<GeneratedMessageV3, GeneratedMessageV3>> callHandler =
                this.callMessageMap.getCallHandler(fqi, SiLACloudConnector.SiLAClientMessage.MessageCase.OBSERVABLECOMMANDGETRESPONSE);
        if (callHandler.isPresent()) {
            log.info("Forwarding call for " + fqi);
            callHandler.get().forward(request.getRequestUUID(), request.getRequest().getCommandExecutionUUID().toByteString(), (message) -> {
                response.onNext(SiLACloudConnector.SiLAServerMessage
                        .newBuilder()
                        .setRequestUUID(request.getRequestUUID())
                        .setObservableCommandResponse(SiLACloudConnector.ObservableCommandResponse.newBuilder()
                                .setCommandExecutionUUID(request.getRequest().getCommandExecutionUUID())
                                .setResponse(message.toByteString())
                                .build()
                        )
                        .build()
                );
            }, (throwable -> commandMessageHandler.sendThrowableError(request.getRequestUUID(), throwable)));
        } else {
            commandMessageHandler.sendUnknownCommandError(request.getRequestUUID(), fqi);
        }
    }

    @Override
    public void onCancelIntermediate(CloudierRequest<SiLACloudConnector.CancelObservableCommandIntermediateResponseSubscription> request) {
        final String fqi = this.uuidToFqiMap.get(request.getRequestUUID());
        final Optional<CloudCallForwarder<GeneratedMessageV3, GeneratedMessageV3>> callHandler =
                this.callMessageMap.getCallHandler(fqi, SiLACloudConnector.SiLAClientMessage.MessageCase.OBSERVABLECOMMANDINTERMEDIATERESPONSESUBSCRIPTION);
        if (callHandler.isPresent()) {
            callHandler.get().cancelRequest(request.getRequestUUID());
            log.info("Subscription to observable command intermediate response request {} cancelled", request.getRequestUUID());
        }
    }

    @Override
    public void onCancelExecInfo(CloudierRequest<SiLACloudConnector.CancelObservableCommandExecutionInfoSubscription> request) {
        final String fqi = this.uuidToFqiMap.get(request.getRequestUUID());
        final Optional<CloudCallForwarder<GeneratedMessageV3, GeneratedMessageV3>> callHandler =
                this.callMessageMap.getCallHandler(fqi, SiLACloudConnector.SiLAClientMessage.MessageCase.OBSERVABLECOMMANDEXECUTIONINFOSUBSCRIPTION);
        if (callHandler.isPresent()) {
            callHandler.get().cancelRequest(request.getRequestUUID());
            log.info("Subscription to observable command execution info request {} cancelled", request.getRequestUUID());
        }
    }
}
