package sila_java.library.cloudier.server;

import com.google.protobuf.ByteString;
import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.Parser;
import io.grpc.*;
import io.grpc.stub.StreamObserver;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLACloudConnector;
import sila_java.library.core.sila.utils.MetadataUtils;
import sila_java.library.server_base.Constants;
import sila_java.library.server_base.FullyQualifiedMetadataContextKey;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

@Slf4j
@Getter
public class CloudCallForwarder<ParameterType extends GeneratedMessageV3, ResponseType extends GeneratedMessageV3> {
    private final Parser<ParameterType> parser;
    private final CloudierServerEndpoint.CloudCallHandler<ParameterType, ResponseType> cloudCallHandler;
    private final CloudierServerEndpoint.AsyncCloudCallHandler<ParameterType, ResponseType> asyncCloudCallHandler;
    // todo map thread safe
    private final Map<String, Observer> runningRequests = new HashMap<>();

    public CloudCallForwarder(Parser<ParameterType> parser, CloudierServerEndpoint.CloudCallHandler<ParameterType, ResponseType> cloudCallHandler) {
        this.parser = parser;
        this.cloudCallHandler = cloudCallHandler;
        this.asyncCloudCallHandler = null;
    }

    public CloudCallForwarder(Parser<ParameterType> parser, CloudierServerEndpoint.AsyncCloudCallHandler<ParameterType, ResponseType> asyncCloudCallHandler) {
        this.parser = parser;
        this.asyncCloudCallHandler = asyncCloudCallHandler;
        this.cloudCallHandler = null;
    }

    public void cancelRequest(final String requestUuid) {
        Observer observer = this.runningRequests.get(requestUuid);
        if (observer != null) {
            log.info("Cancelled request {}", requestUuid);
            observer.onError(new StatusRuntimeException(Status.CANCELLED));
        }
        // making sure request has been removed
        if (this.runningRequests.containsKey(requestUuid)) {
            log.warn("Running request still running after calling onError");
            this.runningRequests.remove(requestUuid);
        }
    }

    public CompletableFuture<ResponseType> forward(
            String requestUuid,
            ByteString request, Consumer<ResponseType> onNextCallback,
            Consumer<Throwable> onErrorCallback
    ) {
        return forward(requestUuid, Collections.emptyList(), request, onNextCallback, onErrorCallback);
    }

    public CompletableFuture<ResponseType> forward(
            String requestUuid,
            List<SiLACloudConnector.Metadata> metadataSet,
            ByteString request, Consumer<ResponseType> onNextCallback,
            Consumer<Throwable> onErrorCallback
    ) {
        final CompletableFuture<ResponseType> completableFuture = new CompletableFuture<>();
        try {
            if (this.runningRequests.containsKey(requestUuid)) {
                throw new RuntimeException("Request with uuid " + requestUuid + " already exist");
            }
            final Observer<ResponseType> responseTypeObserver = new Observer<>(
                    completableFuture, this.runningRequests, requestUuid, onNextCallback, onErrorCallback
            );
            this.runningRequests.put(requestUuid, responseTypeObserver);
            final ParameterType parameter = this.parser.parseFrom(request);
            final Set<FullyQualifiedMetadataContextKey<byte[]>> keySet = new HashSet<>();
            Context contextWithMetadata = Context.current();
            // todo use cancelable context to be then cancel when calling cancelRequest
            for (SiLACloudConnector.Metadata metadata : metadataSet) {
                final String keyIdentifier = MetadataUtils.fullyQualifiedMetadataIdentifierToGrpcMetadataKey(metadata.getFullyQualifiedMetadataId());
                final FullyQualifiedMetadataContextKey<byte[]> contextKey = new FullyQualifiedMetadataContextKey<>(metadata.getFullyQualifiedMetadataId(), Context.key(keyIdentifier));
                keySet.add(contextKey);
                contextWithMetadata = contextWithMetadata.withValue(contextKey.getContextKey(), metadata.getValue().toByteArray());
            }
            contextWithMetadata = contextWithMetadata.withValue(Constants.METADATA_IDENTIFIERS_CTX_KEY, keySet);
            final Context initialContext = contextWithMetadata.attach();
            try {
                if (this.cloudCallHandler != null) {
                    this.cloudCallHandler.handle(parameter, responseTypeObserver);
                } else {
                    final StreamObserver<ParameterType> parameterObserver = this.asyncCloudCallHandler.handle(responseTypeObserver);
                    parameterObserver.onNext(parameter);
                    // todo we should complete the call when the response stream either call onNext/Completed/Error
                    parameterObserver.onCompleted();
                }
            } finally {
                // todo check race condition, we might need to wait for call to end before detaching
                contextWithMetadata.detach(initialContext);
            }
        } catch (Throwable e) {
            this.cancelRequest(requestUuid);
            if (!completableFuture.isDone()) {
                completableFuture.obtrudeException(e);
                onErrorCallback.accept(e);
            }
        }
        return completableFuture;
    }
    public static class Observer<Type> implements StreamObserver<Type> {
        private final CompletableFuture<Type> completableFuture;
        private final AtomicReference<Type> lastResponse;
        private final Map<String, Observer> runningRequests;
        private final String requestUuid;
        private final Consumer<Type> onNextCallback;
        private final Consumer<Throwable> onErrorCallback;

        Observer(
                @NonNull final CompletableFuture<Type> completableFuture,
                @NonNull final Map<String, Observer> runningRequests,
                final String requestUuid,
                @NonNull final Consumer<Type> onNextCallback,
                @NonNull final Consumer<Throwable> onErrorCallback
        ) {
            this.completableFuture = completableFuture;
            this.lastResponse = new AtomicReference<>(null);
            this.runningRequests = runningRequests;
            this.requestUuid = requestUuid;
            this.onNextCallback = onNextCallback;
            this.onErrorCallback = onErrorCallback;
        }

        @Override
        public void onNext(Type responseType) {
            if (!runningRequests.containsKey(requestUuid)) {
                throw new StatusRuntimeException(Status.CANCELLED);
            }
            if (!completableFuture.isDone()) {
                lastResponse.set(responseType);
                onNextCallback.accept(responseType);
            }
        }

        @Override
        public void onError(Throwable throwable) {
            this.runningRequests.remove(requestUuid);
            if (!completableFuture.isDone()) {
                onErrorCallback.accept(throwable);
                completableFuture.obtrudeException(throwable);
            }
        }

        @Override
        public void onCompleted() {
            this.runningRequests.remove(requestUuid);
            if (!completableFuture.isDone()) {
                completableFuture.complete(lastResponse.get());
            }
        }
    }
}