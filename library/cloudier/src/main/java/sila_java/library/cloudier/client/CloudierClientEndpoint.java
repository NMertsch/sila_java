package sila_java.library.cloudier.client;

import io.grpc.stub.StreamObserver;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.CloudClientEndpointGrpc;
import sila2.org.silastandard.SiLACloudConnector;
import sila2.org.silastandard.SiLAFramework;
import sila_java.library.cloudier.server.SynchronizedStreamObserver;

import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
public class CloudierClientEndpoint extends CloudClientEndpointGrpc.CloudClientEndpointImplBase {
    @Getter
    private final Map<String, CloudierClientObserver> responseObservers = new ConcurrentHashMap<>();
    private final CloudServerListener cloudServerListener;

    @FunctionalInterface
    public interface MessageListener {
        void onMessage(SiLACloudConnector.SiLAServerMessage silaServerMessage);
    }

    public interface CloudServerListener {
        void onAdd(UUID serverUUID, CloudierClientObserver cloudierClientObserver);
        void onEnd(Optional<UUID> serverUUID, Optional<Throwable> t);
    }

    public interface CallListener {
        void onCommandInit(SiLACloudConnector.ObservableCommandConfirmation observableCommandConfirmation);
        void onCommandExecutionInfo(SiLACloudConnector.ObservableCommandExecutionInfo observableCommandExecutionInfo);
        void onIntermediateResponse(SiLACloudConnector.ObservableCommandIntermediateResponse intermediateResponse);
        void onError(SiLAFramework.SiLAError siLAError);
    }

    public CloudierClientEndpoint(final CloudServerListener cloudServerListener) {
        this.cloudServerListener = cloudServerListener;
    }

    @Override
    public StreamObserver<SiLACloudConnector.SiLAServerMessage> connectSiLAServer(
            StreamObserver<SiLACloudConnector.SiLAClientMessage> responseObserver
    ) {
        log.info("new server connected");
        final CloudierClientObserver cloudierClientObserver = new CloudierClientObserver(
                new SynchronizedStreamObserver<>(responseObserver), this.cloudServerListener
        );
        cloudierClientObserver.getOptionalServerUUID().whenComplete((serverUUID, ex) -> {
            if (ex != null) {
                log.warn("Failed to add server because server did not provide a valid uuid", ex);
                // todo response onError instead?
                responseObserver.onCompleted();
                return ;
            }
            log.info("server connected with uuid " + serverUUID.getServerUUID());
            final UUID serverUUIDValue = UUID.fromString(serverUUID.getServerUUID().getValue());
            cloudierClientObserver.setServerUUID(serverUUIDValue);
            this.responseObservers.put(serverUUIDValue.toString(), cloudierClientObserver);
            this.cloudServerListener.onAdd(serverUUIDValue, cloudierClientObserver);
        });
        return cloudierClientObserver;
    }
}
