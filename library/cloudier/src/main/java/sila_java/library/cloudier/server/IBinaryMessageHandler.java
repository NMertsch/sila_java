package sila_java.library.cloudier.server;

import sila2.org.silastandard.SiLABinaryTransfer;
import sila2.org.silastandard.SiLACloudConnector;

public interface IBinaryMessageHandler {
    void onCreateBinaryUpload(CloudierRequest<SiLACloudConnector.CreateBinaryUploadRequest> request);
    void onUploadChunk(CloudierRequest<SiLABinaryTransfer.UploadChunkRequest> request);
    void onBinaryInfo(CloudierRequest<SiLABinaryTransfer.GetBinaryInfoRequest> request);
    void onGetChunk(CloudierRequest<SiLABinaryTransfer.GetChunkRequest> request);
    void onDeleteBinaryDownload(CloudierRequest<SiLABinaryTransfer.DeleteBinaryRequest> request);
    void onDeleteBinaryUpload(CloudierRequest<SiLABinaryTransfer.DeleteBinaryRequest> request);
}
