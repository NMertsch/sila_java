package sila_java.library.cloudier.server;

import com.google.protobuf.GeneratedMessageV3;
import lombok.NonNull;
import sila2.org.silastandard.BinaryDownloadGrpc;
import sila2.org.silastandard.BinaryUploadGrpc;
import sila2.org.silastandard.SiLABinaryTransfer;
import sila2.org.silastandard.SiLACloudConnector;
import sila2.org.silastandard.core.connectionconfigurationservice.v1.ConnectionConfigurationServiceOuterClass;
import sila2.org.silastandard.core.silaservice.v1.SiLAServiceOuterClass;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class CallMessageMap {
    public final String SILA_BINARY_TRANSFER_FQI = "SiLABinaryTransfer";
    // todo make thread safe
    private final Map<String, MessageCaseHandler> callForwarderMap;

    public CallMessageMap(
            final @NonNull CloudierSiLAService cloudierSiLAService,
            final @NonNull CloudierConnectionConfigurationService cloudierConnectionConfigurationService,
            final @NonNull Map<String, MessageCaseHandler> callForwarderMap
    ) {
        this(cloudierSiLAService, cloudierConnectionConfigurationService, callForwarderMap, null, null);
    }

    public CallMessageMap(
            final @NonNull CloudierSiLAService cloudierSiLAService,
            final @NonNull CloudierConnectionConfigurationService cloudierConnectionConfigurationService,
            final @NonNull Map<String, MessageCaseHandler> callForwarderMap,
            @Nullable final BinaryUploadGrpc.BinaryUploadImplBase uploadImplBase,
            @Nullable final BinaryDownloadGrpc.BinaryDownloadImplBase downloadImplBase
    ) {
        this.callForwarderMap = new HashMap<>();
        this.callForwarderMap.putAll(callForwarderMap);
        if (uploadImplBase != null && downloadImplBase != null) {
            this.callForwarderMap.putAll(new HashMap<String, MessageCaseHandler>(){{
                put(SILA_BINARY_TRANSFER_FQI, new MessageCaseHandler()
                        .with(
                                SiLACloudConnector.SiLAClientMessage.MessageCase.CREATEBINARYUPLOADREQUEST,
                                SiLABinaryTransfer.CreateBinaryRequest.parser(), uploadImplBase::createBinary
                        ).with(
                                SiLACloudConnector.SiLAClientMessage.MessageCase.DELETEUPLOADEDBINARYREQUEST,
                                SiLABinaryTransfer.DeleteBinaryRequest.parser(), uploadImplBase::deleteBinary
                        ).withAsync(
                                SiLACloudConnector.SiLAClientMessage.MessageCase.UPLOADCHUNKREQUEST,
                                SiLABinaryTransfer.UploadChunkRequest.parser(), uploadImplBase::uploadChunk
                        ).with(
                                SiLACloudConnector.SiLAClientMessage.MessageCase.DELETEDOWNLOADEDBINARYREQUEST,
                                SiLABinaryTransfer.DeleteBinaryRequest.parser(), downloadImplBase::deleteBinary
                        ).with(
                                SiLACloudConnector.SiLAClientMessage.MessageCase.GETBINARYINFOREQUEST,
                                SiLABinaryTransfer.GetBinaryInfoRequest.parser(), downloadImplBase::getBinaryInfo
                        ).withAsync(
                                SiLACloudConnector.SiLAClientMessage.MessageCase.GETCHUNKREQUEST,
                                SiLABinaryTransfer.GetChunkRequest.parser(), downloadImplBase::getChunk
                        )
                );
            }});
        }
        this.callForwarderMap.putAll(new HashMap<String, MessageCaseHandler>(){{
            // SiLA Service
            put("org.silastandard/core/SiLAService/v1/Property/ServerUUID", new MessageCaseHandler().withUnobservableProperty(
                    SiLAServiceOuterClass.Get_ServerUUID_Parameters.parser(), cloudierSiLAService::getServerUUID
            ));
            put("org.silastandard/core/SiLAService/v1/Property/ServerName", new MessageCaseHandler().withUnobservableProperty(
                    SiLAServiceOuterClass.Get_ServerName_Parameters.parser(), cloudierSiLAService::getServerName
            ));
            put("org.silastandard/core/SiLAService/v1/Property/ServerDescription", new MessageCaseHandler().withUnobservableProperty(
                    SiLAServiceOuterClass.Get_ServerDescription_Parameters.parser(), cloudierSiLAService::getServerDescription
            ));
            put("org.silastandard/core/SiLAService/v1/Property/ServerVendorURL", new MessageCaseHandler().withUnobservableProperty(
                    SiLAServiceOuterClass.Get_ServerVendorURL_Parameters.parser(), cloudierSiLAService::getServerVendorURL
            ));
            put("org.silastandard/core/SiLAService/v1/Property/ServerType", new MessageCaseHandler().withUnobservableProperty(
                    SiLAServiceOuterClass.Get_ServerType_Parameters.parser(), cloudierSiLAService::getServerType
            ));
            put("org.silastandard/core/SiLAService/v1/Property/ServerVersion", new MessageCaseHandler().withUnobservableProperty(
                    SiLAServiceOuterClass.Get_ServerVersion_Parameters.parser(), cloudierSiLAService::getServerVersion
            ));
            put("org.silastandard/core/SiLAService/v1/Property/ImplementedFeatures", new MessageCaseHandler().withUnobservableProperty(
                    SiLAServiceOuterClass.Get_ImplementedFeatures_Parameters.parser(), cloudierSiLAService::getImplementedFeatures
            ));
            put("org.silastandard/core/SiLAService/v1/Command/GetFeatureDefinition", new MessageCaseHandler().withUnobservableCommand(
                    SiLAServiceOuterClass.GetFeatureDefinition_Parameters.parser(), cloudierSiLAService::getFeatureDefinition
            ));
            put("org.silastandard/core/SiLAService/v1/Command/SetServerName", new MessageCaseHandler().withUnobservableCommand(
                    SiLAServiceOuterClass.SetServerName_Parameters.parser(), cloudierSiLAService::setServerName
            ));
            // Connection Configuration Service
            put("org.silastandard/core/ConnectionConfigurationService/v1/Command/EnableServerInitiatedConnectionMode", new MessageCaseHandler().withUnobservableCommand(
                    ConnectionConfigurationServiceOuterClass.EnableServerInitiatedConnectionMode_Parameters.parser(), cloudierConnectionConfigurationService::enableServerInitiatedConnectionMode
            ));
            put("org.silastandard/core/ConnectionConfigurationService/v1/Command/DisableServerInitiatedConnectionMode", new MessageCaseHandler().withUnobservableCommand(
                    ConnectionConfigurationServiceOuterClass.DisableServerInitiatedConnectionMode_Parameters.parser(), cloudierConnectionConfigurationService::disableServerInitiatedConnectionMode
            ));
            put("org.silastandard/core/ConnectionConfigurationService/v1/Command/ConnectSiLAClient", new MessageCaseHandler().withUnobservableCommand(
                    ConnectionConfigurationServiceOuterClass.ConnectSiLAClient_Parameters.parser(), cloudierConnectionConfigurationService::connectSiLAClient
            ));
            put("org.silastandard/core/ConnectionConfigurationService/v1/Command/DisconnectSiLAClient", new MessageCaseHandler().withUnobservableCommand(
                    ConnectionConfigurationServiceOuterClass.DisconnectSiLAClient_Parameters.parser(), cloudierConnectionConfigurationService::disconnectSiLAClient
            ));
            put("org.silastandard/core/ConnectionConfigurationService/v1/Property/ServerInitiatedConnectionModeStatus", new MessageCaseHandler().withObservableProperty(
                    ConnectionConfigurationServiceOuterClass.Subscribe_ServerInitiatedConnectionModeStatus_Parameters.parser(), cloudierConnectionConfigurationService::subscribeServerInitiatedConnectionModeStatus
            ));
            put("org.silastandard/core/ConnectionConfigurationService/v1/Property/ConfiguredSiLAClients", new MessageCaseHandler().withObservableProperty(
                    ConnectionConfigurationServiceOuterClass.Subscribe_ConfiguredSiLAClients_Parameters.parser(), cloudierConnectionConfigurationService::subscribeConfiguredSiLAClients
            ));
        }});
    }

    public Optional<MessageCaseHandler> getMessageHandler(String fullyQualifiedCallId) {
        return Optional.ofNullable(callForwarderMap.get(fullyQualifiedCallId));
    }

    public <RequestType extends GeneratedMessageV3, ResponseType extends GeneratedMessageV3> Optional<CloudCallForwarder<RequestType, ResponseType>> getBinaryCallHandler(
            SiLACloudConnector.SiLAClientMessage.MessageCase messageCase
    ) {
        MessageCaseHandler messageCaseHandler = callForwarderMap.get(SILA_BINARY_TRANSFER_FQI);
        if (messageCaseHandler != null) {
            return messageCaseHandler.get(messageCase);
        }
        return Optional.empty();
    }

    public <RequestType extends GeneratedMessageV3, ResponseType extends GeneratedMessageV3> Optional<CloudCallForwarder<RequestType, ResponseType>> getCallHandler(
            String fullyQualifiedCallId, SiLACloudConnector.SiLAClientMessage.MessageCase messageCase
    ) {
        MessageCaseHandler messageCaseHandler = callForwarderMap.get(fullyQualifiedCallId);
        if (messageCaseHandler != null) {
            return messageCaseHandler.get(messageCase);
        }
        return Optional.empty();
    }
}
