package sila_java.library.cloudier.server.impl;

import com.google.protobuf.ByteString;
import com.google.protobuf.Descriptors;
import com.google.protobuf.GeneratedMessageV3;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLACloudConnector;
import sila2.org.silastandard.SiLAFramework;
import sila_java.library.cloudier.server.CallMessageMap;
import sila_java.library.cloudier.server.CloudCallForwarder;
import sila_java.library.cloudier.server.CloudierRequest;
import sila_java.library.cloudier.server.IMetadataMessageHandler;

import java.util.*;

@Slf4j
public class MetadataMessageHandler implements IMetadataMessageHandler {
    private final StreamObserver<SiLACloudConnector.SiLAServerMessage> response;
    private final CallMessageMap callMessageMap;
    private final CommandMessageHandler commandMessageHandler;

    public MetadataMessageHandler(StreamObserver<SiLACloudConnector.SiLAServerMessage> response, CallMessageMap callMessageMap) {
        this.response = response;
        this.callMessageMap = callMessageMap;
        this.commandMessageHandler = new CommandMessageHandler(response);
    }

    @Override
    public void onGetAffectedByMetadata(CloudierRequest<SiLACloudConnector.GetFCPAffectedByMetadataRequest> request) {
        final String fqi = request.getRequest().getFullyQualifiedMetadataId();
        final Optional<CloudCallForwarder<GeneratedMessageV3, GeneratedMessageV3>> callHandler = this.callMessageMap.getCallHandler(fqi, SiLACloudConnector.SiLAClientMessage.MessageCase.METADATAREQUEST);
        if (callHandler.isPresent()) {
            log.info("Forwarding call for " + fqi);
            callHandler.get().forward(request.getRequestUUID(), ByteString.EMPTY, (message) -> {
                final List<String> fcpAffectedList = new ArrayList<>();
                final Optional<Descriptors.FieldDescriptor> optionalFieldDescriptor = message
                        .getAllFields()
                        .keySet()
                        .stream()
                        .filter(f -> f.isRepeated() && f.getName().equals("AffectedCalls"))
                        .findAny();
                optionalFieldDescriptor.ifPresent(fieldDescriptor -> {
                    final int count = message.getRepeatedFieldCount(fieldDescriptor);
                    for (int i = 0; i < count; i++) {
                        final Object fcpAffected = message.getRepeatedField(fieldDescriptor, i);
                        if (fcpAffected instanceof SiLAFramework.String) {
                            fcpAffectedList.add(((SiLAFramework.String) fcpAffected).getValue());
                        } else {
                            log.warn("Invalid FCP Affected By Metadata value {}", fcpAffected);
                        }
                    }
                });

                response.onNext(SiLACloudConnector.SiLAServerMessage
                        .newBuilder()
                        .setRequestUUID(request.getRequestUUID())
                        .setGetFCPAffectedByMetadataResponse(
                                SiLACloudConnector.GetFCPAffectedByMetadataResponse
                                        .newBuilder()
                                        .addAllAffectedCalls(fcpAffectedList)
                                        .build()
                        )
                        .build()
                );
            }, (throwable -> commandMessageHandler.sendThrowableError(request.getRequestUUID(), throwable)));
        } else {
            sendUnknownMetadataError(request.getRequestUUID(), fqi);
        }
    }

    public void sendUnknownMetadataError(String requestUuid, String fqi) {
        log.warn("Client with request {} attempted to call an unknown affected by metadata {}", requestUuid, fqi);
        commandMessageHandler.sendSiLAError(requestUuid, SiLAFramework.SiLAError
                .newBuilder()
                .setUndefinedExecutionError(
                        SiLAFramework.UndefinedExecutionError
                                .newBuilder()
                                .setMessage("Server does not expose call to get affected by metadata with id " + fqi)
                                .build()
                )
                .build()
        );
    }
}
