package sila_java.library.cloudier.server.impl;

import com.google.protobuf.GeneratedMessageV3;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLACloudConnector;
import sila_java.library.cloudier.server.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;


@Slf4j
public class ObservablePropertyMessageHandler implements IObservablePropertyMessageHandler {
    private final StreamObserver<SiLACloudConnector.SiLAServerMessage> response;
    private final CallMessageMap callMessageMap;
    private final PropertyMessageHandler propertyMessageHandler;
    // todo make thread safe
    private final Map<String, String> uuidToFqiMap = new HashMap<>();

    public ObservablePropertyMessageHandler(StreamObserver<SiLACloudConnector.SiLAServerMessage> response, CallMessageMap callMessageMap) {
        this.response = response;
        this.callMessageMap = callMessageMap;
        this.propertyMessageHandler = new PropertyMessageHandler(response);
    }

    @Override
    public void onObservableProperty(CloudierRequest<SiLACloudConnector.ObservablePropertySubscription> request) {
        final String fqi = request.getRequest().getFullyQualifiedPropertyId();
        final Optional<CloudCallForwarder<GeneratedMessageV3, GeneratedMessageV3>> callHandler =
                this.callMessageMap.getCallHandler(fqi, SiLACloudConnector.SiLAClientMessage.MessageCase.OBSERVABLEPROPERTYSUBSCRIPTION);
        if (callHandler.isPresent()) {
            log.info("Forwarding call for " + fqi);
            uuidToFqiMap.put(request.getRequestUUID(), fqi);
            callHandler.get().forward(request.getRequestUUID(), request.getRequest().getMetadataList(), request.getRequest().toByteString(), (message) -> {
                response.onNext(SiLACloudConnector.SiLAServerMessage
                        .newBuilder()
                        .setRequestUUID(request.getRequestUUID())
                        .setObservablePropertyValue(
                                SiLACloudConnector.ObservablePropertyValue
                                        .newBuilder()
                                        .setValue(message.toByteString())
                                        .build()
                        )
                        .build()
                );
            }, (throwable -> propertyMessageHandler.sendThrowableError(request.getRequestUUID(), throwable)));
        } else {
            propertyMessageHandler.sendUnknownPropertyError(request.getRequestUUID(), fqi);
        }
    }

    @Override
    public void onCancelObservableProperty(CloudierRequest<SiLACloudConnector.CancelObservablePropertySubscription> request) {
        final String fqi = this.uuidToFqiMap.get(request.getRequestUUID());
        final Optional<CloudCallForwarder<GeneratedMessageV3, GeneratedMessageV3>> callHandler =
                this.callMessageMap.getCallHandler(fqi, SiLACloudConnector.SiLAClientMessage.MessageCase.OBSERVABLEPROPERTYSUBSCRIPTION);
        if (callHandler.isPresent()) {
            callHandler.get().cancelRequest(request.getRequestUUID());
            log.info("Subscription to observable property request {} cancelled", request.getRequestUUID());
        }
        this.uuidToFqiMap.remove(request.getRequestUUID());
    }
}
