package sila_java.library.cloudier.server;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class CloudierRequest<T> {
    private final String requestUUID;
    private final T request;
}
