package sila_java.library.cloudier.server;

import com.google.common.collect.ImmutableMap;
import io.grpc.ManagedChannel;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.CloudClientEndpointGrpc;
import sila_java.library.core.encryption.EncryptionUtils;
import sila_java.library.core.encryption.SelfSignedCertificate;
import sila_java.library.core.sila.clients.ChannelFactory;
import sila_java.library.sila_base.EmptyClass;

import java.io.IOException;
import java.util.Collections;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static sila_java.library.core.utils.FileUtils.getFileContent;
import static sila_java.library.core.utils.Utils.blockUntilShutdown;

@Slf4j
public class CloudierServer implements CloudierConnectionConfigurationService.ConnectionConfigurationSwitch {
    public static final String serverUUID = "498edc02-f1b8-41c7-a337-39569ec4b2b3";
    public static final String serverDescription = "Test";
    public static final String serverVersion = "0.0";
    public static final String serverName = "sila-test";
    public static final String serverType = "cloud-test";
    public static final String serverURL = "com.timothy.diguiet";
    private final CloudierSiLAService cloudierSiLAService;
    private final CloudierConnectionConfigurationService cloudierConnectionConfigurationService;
    private ManagedChannel channel;
    private Server server;
    private CloudClientEndpointGrpc.CloudClientEndpointStub clientEndpoint;
    private CloudierServerEndpoint cloudServerEndpointService;

    public static void main(String[] args) throws IOException {
        final CloudierServer cloudierServer = new CloudierServer();
        blockUntilShutdown();
    }

    public CloudierServer() throws IOException {
        this.cloudierSiLAService = new CloudierSiLAService(
                serverName,
                serverType,
                serverUUID,
                serverVersion,
                serverDescription,
                serverURL,
                ImmutableMap.of(
                        "SiLAService", getFileContent(
                                Objects.requireNonNull(
                                        EmptyClass.class.getResourceAsStream("/sila_base/feature_definitions/org/silastandard/core/SiLAService.sila.xml")
                                )
                        ),
                        "ConnectionConfigurationService", getFileContent(
                                Objects.requireNonNull(
                                        EmptyClass.class.getResourceAsStream("/sila_base/feature_definitions/org/silastandard/core/ConnectionConfigurationService.sila.xml")
                                )
                        )
                )
        );
        this.cloudierConnectionConfigurationService = new CloudierConnectionConfigurationService(
                false, this
        );
        //this.startClientInitiatedConnection();
        this.startServerInitiatedConnection();
        //this.onSwitch(false);
    }

    @Override
    public void onSwitch(boolean serverInitiatedConnectionEnabled) {
        log.info("server initiated turned " + (serverInitiatedConnectionEnabled ? "on" : "off"));
        if (serverInitiatedConnectionEnabled) {
            if (this.server != null) {
                try {
                    this.server.shutdown().awaitTermination(5, TimeUnit.SECONDS);
                } catch (InterruptedException e) {
                    log.warn("Error while stopping server ", e);
                } finally {
                    this.server = null;
                }
            }
            startServerInitiatedConnection();
        } else {
            this.cloudServerEndpointService = null;
            this.clientEndpoint = null;
            if (this.channel != null) {
                try {
                    this.channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
                } catch (InterruptedException e) {
                    log.warn("Error while closing channel ", e);
                } finally {
                    this.channel = null;
                }
            }
            startClientInitiatedConnection();
        }
    }

    private void startClientInitiatedConnection() {
        try {
            final SelfSignedCertificate selfSignedCertificate = SelfSignedCertificate
                    .newBuilder()
                    .withServerUUID(UUID.fromString(serverUUID))
                    .withServerIP("127.0.0.1")
                    .build();
            this.server = ServerBuilder
                    .forPort(50052)
                    .useTransportSecurity(
                            EncryptionUtils.certificateToStream(selfSignedCertificate.getCertificate()),
                            EncryptionUtils.keyToStream(selfSignedCertificate.getPrivateKey())
                    )
                    .addService(this.cloudierSiLAService)
                    .addService(this.cloudierConnectionConfigurationService)
                    .build()
                    .start();
        } catch (Exception e) {
            log.error("Failed to start server ", e);
        }
    }

    private void startServerInitiatedConnection() {
        this.channel = ChannelFactory.getTLSEncryptedChannel("localhost", 50051);
        this.clientEndpoint = CloudClientEndpointGrpc.newStub(this.channel);
        this.cloudServerEndpointService = new CloudierServerEndpoint(
                this.cloudierSiLAService,
                this.cloudierConnectionConfigurationService,
                this.clientEndpoint,
                Collections.emptyMap()
        );
    }
}
