package sila_java.library.cloudier.server;

import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.Parser;
import io.grpc.stub.StreamObserver;
import sila2.org.silastandard.SiLACloudConnector;

import java.util.EnumMap;
import java.util.Optional;

public class MessageCaseHandler {
    private final EnumMap<SiLACloudConnector.SiLAClientMessage.MessageCase, CloudCallForwarder> messageHandlerMap =
            new EnumMap<>(SiLACloudConnector.SiLAClientMessage.MessageCase.class);

    public <RequestType extends GeneratedMessageV3, ResponseType extends GeneratedMessageV3> Optional<CloudCallForwarder<RequestType, ResponseType>> get(SiLACloudConnector.SiLAClientMessage.MessageCase messageCase) {
        return Optional.ofNullable(messageHandlerMap.get(messageCase));
    }

    public <ParameterType extends GeneratedMessageV3, ResponseType extends GeneratedMessageV3> MessageCaseHandler with(
            SiLACloudConnector.SiLAClientMessage.MessageCase type, Parser<ParameterType> parser, CloudierServerEndpoint.CloudCallHandler<ParameterType, ResponseType> handler
    ) {
        messageHandlerMap.put(type, new CloudCallForwarder<>(
                parser,
                handler
        ));
        return this;
    }

    public <ParameterType extends GeneratedMessageV3, ResponseType extends GeneratedMessageV3> MessageCaseHandler withAsync(
            SiLACloudConnector.SiLAClientMessage.MessageCase type, Parser<ParameterType> parser, CloudierServerEndpoint.AsyncCloudCallHandler<ParameterType, ResponseType> handler
    ) {
        messageHandlerMap.put(type, new CloudCallForwarder<>(
                parser,
                handler
        ));
        return this;
    }

    public <ParameterType extends GeneratedMessageV3, ResponseType extends GeneratedMessageV3> MessageCaseHandler withUnobservableProperty(
            Parser<ParameterType> parser, CloudierServerEndpoint.CloudCallHandler<ParameterType, ResponseType> handler
    ) {
        return with(SiLACloudConnector.SiLAClientMessage.MessageCase.UNOBSERVABLEPROPERTYREAD, parser, handler);
    }

    public <ParameterType extends GeneratedMessageV3, ResponseType extends GeneratedMessageV3> MessageCaseHandler withObservableProperty(
            Parser<ParameterType> parser, CloudierServerEndpoint.CloudCallHandler<ParameterType, ResponseType> handler
    ) {
        return with(SiLACloudConnector.SiLAClientMessage.MessageCase.OBSERVABLEPROPERTYSUBSCRIPTION, parser, handler);
    }

    public <ParameterType extends GeneratedMessageV3, ResponseType extends GeneratedMessageV3> MessageCaseHandler withUnobservableCommand(
            Parser<ParameterType> parser, CloudierServerEndpoint.CloudCallHandler<ParameterType, ResponseType> handler
    ) {
        return with(SiLACloudConnector.SiLAClientMessage.MessageCase.UNOBSERVABLECOMMANDEXECUTION, parser, handler);
    }

    public <ParameterType extends GeneratedMessageV3, ResponseType extends GeneratedMessageV3> MessageCaseHandler withObservableCommand(
            Parser<ParameterType> parser, CloudierServerEndpoint.CloudCallHandler<ParameterType, ResponseType> handler
    ) {
        return with(SiLACloudConnector.SiLAClientMessage.MessageCase.OBSERVABLECOMMANDINITIATION, parser, handler);
    }

    public <ParameterType extends GeneratedMessageV3, ResponseType extends GeneratedMessageV3> MessageCaseHandler withExecInfo(
            Parser<ParameterType> parser, CloudierServerEndpoint.CloudCallHandler<ParameterType, ResponseType> handler
    ) {
        return with(SiLACloudConnector.SiLAClientMessage.MessageCase.OBSERVABLECOMMANDEXECUTIONINFOSUBSCRIPTION, parser, handler);
    }

    public <ParameterType extends GeneratedMessageV3, ResponseType extends GeneratedMessageV3> MessageCaseHandler withIntermediate(
            Parser<ParameterType> parser, CloudierServerEndpoint.CloudCallHandler<ParameterType, ResponseType> handler
    ) {
        return with(SiLACloudConnector.SiLAClientMessage.MessageCase.OBSERVABLECOMMANDINTERMEDIATERESPONSESUBSCRIPTION, parser, handler);
    }

    public <ParameterType extends GeneratedMessageV3, ResponseType extends GeneratedMessageV3> MessageCaseHandler withMetadata(
            Parser<ParameterType> parser, CloudierServerEndpoint.CloudCallHandler<ParameterType, ResponseType> handler
    ) {
        return with(SiLACloudConnector.SiLAClientMessage.MessageCase.METADATAREQUEST, parser, handler);
    }

    public <ParameterType extends GeneratedMessageV3, ResponseType extends GeneratedMessageV3> MessageCaseHandler withResult(
            Parser<ParameterType> parser, CloudierServerEndpoint.CloudCallHandler<ParameterType, ResponseType> handler
    ) {
        return with(SiLACloudConnector.SiLAClientMessage.MessageCase.OBSERVABLECOMMANDGETRESPONSE, parser, handler);
    }
}
