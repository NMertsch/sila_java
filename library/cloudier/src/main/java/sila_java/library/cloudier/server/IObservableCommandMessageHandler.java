package sila_java.library.cloudier.server;

import sila2.org.silastandard.SiLACloudConnector;

public interface IObservableCommandMessageHandler {
    void onCommandInit(CloudierRequest<SiLACloudConnector.ObservableCommandInitiation> request);
    void onCommandExecInfo(CloudierRequest<SiLACloudConnector.ObservableCommandExecutionInfoSubscription> request);
    void onIntermediate(CloudierRequest<SiLACloudConnector.ObservableCommandIntermediateResponseSubscription> request);
    void onResult(CloudierRequest<SiLACloudConnector.ObservableCommandGetResponse> request);
    void onCancelIntermediate(CloudierRequest<SiLACloudConnector.CancelObservableCommandIntermediateResponseSubscription> request);
    void onCancelExecInfo(CloudierRequest<SiLACloudConnector.CancelObservableCommandExecutionInfoSubscription> request);
}
