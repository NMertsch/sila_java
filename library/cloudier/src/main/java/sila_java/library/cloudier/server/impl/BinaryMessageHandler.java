package sila_java.library.cloudier.server.impl;

import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.*;
import sila_java.library.cloudier.server.CallMessageMap;
import sila_java.library.cloudier.server.CloudCallForwarder;
import sila_java.library.cloudier.server.CloudierRequest;
import sila_java.library.cloudier.server.IBinaryMessageHandler;
import sila_java.library.core.sila.errors.SiLAErrors;
import sila_java.library.core.sila.binary_transfer.BinaryTransferErrorHandler;

import java.util.Optional;


@Slf4j
public class BinaryMessageHandler implements IBinaryMessageHandler {
    private final StreamObserver<SiLACloudConnector.SiLAServerMessage> response;
    private final CallMessageMap callMessageMap;

    public BinaryMessageHandler(
            StreamObserver<SiLACloudConnector.SiLAServerMessage> response,
            CallMessageMap callMessageMap
    ) {
        this.response = response;
        this.callMessageMap = callMessageMap;
    }

    @Override
    public void onCreateBinaryUpload(CloudierRequest<SiLACloudConnector.CreateBinaryUploadRequest> request) {
        final Optional<CloudCallForwarder<SiLACloudConnector.CreateBinaryUploadRequest, SiLABinaryTransfer.CreateBinaryResponse>> binaryCallHandler =
                this.callMessageMap.getBinaryCallHandler(SiLACloudConnector.SiLAClientMessage.MessageCase.CREATEBINARYUPLOADREQUEST);
        if (binaryCallHandler.isPresent()) {
            log.info("Forwarding call for binary create upload");
            binaryCallHandler.get().forward(
                    request.getRequestUUID(),
                    request.getRequest().getMetadataList(),
                    request.getRequest().getCreateBinaryRequest().toByteString(),
                    (message) -> {
                        response.onNext(SiLACloudConnector.SiLAServerMessage
                                .newBuilder()
                                .setRequestUUID(request.getRequestUUID())
                                .setCreateBinaryResponse(message)
                                .build()
                        );
                    }, (throwable -> sendThrowableError(request.getRequestUUID(), throwable)));
        } else {
            sendUnknownBinaryTransferCallError(request.getRequestUUID(), "Create binary upload request");
        }
    }

    @Override
    public void onUploadChunk(CloudierRequest<SiLABinaryTransfer.UploadChunkRequest> request) {
        final Optional<CloudCallForwarder<SiLABinaryTransfer.UploadChunkRequest, SiLABinaryTransfer.UploadChunkResponse>> binaryCallHandler =
                this.callMessageMap.getBinaryCallHandler(SiLACloudConnector.SiLAClientMessage.MessageCase.UPLOADCHUNKREQUEST);
        if (binaryCallHandler.isPresent()) {
            log.info("Forwarding call for binary chunk upload");
            binaryCallHandler.get().forward(
                    request.getRequestUUID(),
                    request.getRequest().toByteString(),
                    (message) -> {
                        response.onNext(SiLACloudConnector.SiLAServerMessage
                                .newBuilder()
                                .setRequestUUID(request.getRequestUUID())
                                .setUploadChunkResponse(message)
                                .build()
                        );
                    }, (throwable -> sendThrowableError(request.getRequestUUID(), throwable)));
        } else {
            sendUnknownBinaryTransferCallError(request.getRequestUUID(), "Upload chunk request");
        }
    }

    @Override
    public void onBinaryInfo(CloudierRequest<SiLABinaryTransfer.GetBinaryInfoRequest> request) {
        final Optional<CloudCallForwarder<SiLABinaryTransfer.GetBinaryInfoRequest, SiLABinaryTransfer.GetBinaryInfoResponse>> binaryCallHandler =
                this.callMessageMap.getBinaryCallHandler(SiLACloudConnector.SiLAClientMessage.MessageCase.GETBINARYINFOREQUEST);
        if (binaryCallHandler.isPresent()) {
            log.info("Forwarding call for binary info");
            binaryCallHandler.get().forward(
                    request.getRequestUUID(),
                    request.getRequest().toByteString(),
                    (message) -> {
                        response.onNext(SiLACloudConnector.SiLAServerMessage
                                .newBuilder()
                                .setRequestUUID(request.getRequestUUID())
                                .setGetBinaryResponse(message)
                                .build()
                        );
                    }, (throwable -> sendThrowableError(request.getRequestUUID(), throwable)));
        } else {
            sendUnknownBinaryTransferCallError(request.getRequestUUID(), "Get binary info request");
        }
    }

    @Override
    public void onGetChunk(CloudierRequest<SiLABinaryTransfer.GetChunkRequest> request) {
        final Optional<CloudCallForwarder<SiLABinaryTransfer.GetChunkRequest, SiLABinaryTransfer.GetChunkResponse>> binaryCallHandler =
                this.callMessageMap.getBinaryCallHandler(SiLACloudConnector.SiLAClientMessage.MessageCase.GETCHUNKREQUEST);
        if (binaryCallHandler.isPresent()) {
            log.info("Forwarding call for binary get chunk");
            binaryCallHandler.get().forward(
                    request.getRequestUUID(),
                    request.getRequest().toByteString(),
                    (message) -> {
                        response.onNext(SiLACloudConnector.SiLAServerMessage
                                .newBuilder()
                                .setRequestUUID(request.getRequestUUID())
                                .setGetChunkResponse(message)
                                .build()
                        );
                    }, (throwable -> sendThrowableError(request.getRequestUUID(), throwable)));
        } else {
            sendUnknownBinaryTransferCallError(request.getRequestUUID(), "Get chunk request");
        }
    }

    @Override
    public void onDeleteBinaryDownload(CloudierRequest<SiLABinaryTransfer.DeleteBinaryRequest> request) {
        final Optional<CloudCallForwarder<SiLABinaryTransfer.DeleteBinaryRequest, SiLABinaryTransfer.DeleteBinaryResponse>> binaryCallHandler =
                this.callMessageMap.getBinaryCallHandler(SiLACloudConnector.SiLAClientMessage.MessageCase.DELETEDOWNLOADEDBINARYREQUEST);
        if (binaryCallHandler.isPresent()) {
            log.info("Forwarding call for binary delete download");
            binaryCallHandler.get().forward(
                    request.getRequestUUID(),
                    request.getRequest().toByteString(),
                    (message) -> {
                        response.onNext(SiLACloudConnector.SiLAServerMessage
                                .newBuilder()
                                .setRequestUUID(request.getRequestUUID())
                                .setDeleteBinaryResponse(message)
                                .build()
                        );
                    }, (throwable -> sendThrowableError(request.getRequestUUID(), throwable)));
        } else {
            sendUnknownBinaryTransferCallError(request.getRequestUUID(), "Delete downloaded binary request");
        }
    }

    @Override
    public void onDeleteBinaryUpload(CloudierRequest<SiLABinaryTransfer.DeleteBinaryRequest> request) {
        final Optional<CloudCallForwarder<SiLABinaryTransfer.DeleteBinaryRequest, SiLABinaryTransfer.DeleteBinaryResponse>> binaryCallHandler =
                this.callMessageMap.getBinaryCallHandler(SiLACloudConnector.SiLAClientMessage.MessageCase.DELETEUPLOADEDBINARYREQUEST);
        if (binaryCallHandler.isPresent()) {
            log.info("Forwarding call for binary delete ulpoad");
            binaryCallHandler.get().forward(
                    request.getRequestUUID(),
                    request.getRequest().toByteString(),
                    (message) -> {
                        response.onNext(SiLACloudConnector.SiLAServerMessage
                                .newBuilder()
                                .setRequestUUID(request.getRequestUUID())
                                .setDeleteBinaryResponse(message)
                                .build()
                        );
                    }, (throwable -> sendThrowableError(request.getRequestUUID(), throwable)));
        } else {
            sendUnknownBinaryTransferCallError(request.getRequestUUID(), "Delete uploaded binary request");
        }
    }

    public void sendSiLABinaryError(String requestUuid, SiLABinaryTransfer.BinaryTransferError error) {
        response.onNext(SiLACloudConnector.SiLAServerMessage
                .newBuilder()
                .setRequestUUID(requestUuid)
                .setBinaryTransferError(
                        error
                )
                .build()
        );
    }

    public void sendThrowableError(String requestUuid, Throwable error) {
        log.info("Error occurred with request {}, {}", requestUuid, error);
        SiLABinaryTransfer.BinaryTransferError binaryTransferError = null;
        if (error instanceof StatusRuntimeException) {
            Optional<SiLABinaryTransfer.BinaryTransferError> optional = BinaryTransferErrorHandler.retrieveBinaryTransferError((StatusRuntimeException) error);
            if (optional.isPresent()) {
                binaryTransferError = optional.get();
            }
        }
        if (binaryTransferError == null) {
            SiLAFramework.SiLAError siLAError = SiLAErrors.throwableToSiLAError(error);
            final String errorMessage;
            if (siLAError.hasValidationError()) {
                errorMessage = siLAError.getValidationError().getMessage();
            } else if (siLAError.hasFrameworkError()) {
                errorMessage = siLAError.getFrameworkError().getMessage();
            } else if (siLAError.hasDefinedExecutionError()) {
                errorMessage = siLAError.getDefinedExecutionError().getMessage();
            } else if (siLAError.hasUndefinedExecutionError()) {
                errorMessage = siLAError.getUndefinedExecutionError().getMessage();
            } else {
                errorMessage = "Unknown error: " + error.getMessage();
            }
            binaryTransferError = SiLABinaryTransfer.BinaryTransferError
                    .newBuilder()
                    .setErrorType(SiLABinaryTransfer.BinaryTransferError.ErrorType.UNRECOGNIZED)
                    .setMessage(errorMessage)
                    .build();
        }

        sendSiLABinaryError(requestUuid, binaryTransferError);
    }

    public void sendUnknownBinaryTransferCallError(String requestUuid, String name) {
        log.warn("Client with request {} attempted to call unknown binary transfer {}", requestUuid, name);
        sendSiLABinaryError(requestUuid, SiLABinaryTransfer.BinaryTransferError
                .newBuilder()
                .setMessage("Server does not expose binary transfer call to " + name)
                .setErrorType(SiLABinaryTransfer.BinaryTransferError.ErrorType.UNRECOGNIZED)
                .build()
        );
    }

}
