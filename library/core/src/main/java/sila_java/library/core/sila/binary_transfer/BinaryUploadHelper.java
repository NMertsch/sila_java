package sila_java.library.core.sila.binary_transfer;

import com.google.protobuf.ByteString;
import com.google.protobuf.Message;
import io.grpc.Channel;
import io.grpc.stub.StreamObserver;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.BinaryUploadGrpc;
import sila2.org.silastandard.SiLABinaryTransfer;
import sila_java.library.core.sila.utils.MetadataUtils;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.Duration;
import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.IntStream;

/**
 * Client-side utility class for SiLA Binary Upload
 */
@Slf4j
public class BinaryUploadHelper {
    private static final int MAX_CHUNK_LENGTH = (int) Math.pow(1024, 2);
    private final BinaryUploadGrpc.BinaryUploadBlockingStub uploadBlockingStub;
    /**
     * This stub is used for the CreateBinary RPC because it may require metadata
     */
    private final BinaryUploadGrpc.BinaryUploadBlockingStub createBinaryBlockingStub;
    private final BinaryUploadGrpc.BinaryUploadStub uploadStreamStub;

    /**
     * Create a {@link BinaryUploadHelper} using a provided gRPC channel
     *
     * @param channel The gRPC channel
     */
    public BinaryUploadHelper(Channel channel) {
        this.uploadBlockingStub = BinaryUploadGrpc.newBlockingStub(channel);
        this.createBinaryBlockingStub = uploadBlockingStub;
        this.uploadStreamStub = BinaryUploadGrpc.newStub(channel);
    }

    private BinaryUploadHelper(
            BinaryUploadGrpc.BinaryUploadBlockingStub uploadStub,
            BinaryUploadGrpc.BinaryUploadBlockingStub createBinaryStub, BinaryUploadGrpc.BinaryUploadStub streamStub) {
        this.uploadBlockingStub = uploadStub;
        this.createBinaryBlockingStub = createBinaryStub;
        this.uploadStreamStub = streamStub;
    }

    /**
     * Create a {@link BinaryUploadHelper} connected to the same {@link Channel} which sends SiLA Client Metadata when
     * requesting a new upload
     *
     * @param metadataMessages The SiLA Client Metadata messages to send when requesting a new upload
     *
     * @return The new {@link BinaryUploadHelper}
     */
    public BinaryUploadHelper withMetadata(List<Message> metadataMessages) {
        return new BinaryUploadHelper(
                uploadBlockingStub,
                createBinaryBlockingStub.withInterceptors(MetadataUtils.newMetadataInjector(
                        metadataMessages)),
                uploadStreamStub
        );
    }

    /**
     * Create a {@link BinaryUploadHelper} connected to the same {@link Channel} which sends SiLA Client Metadata when
     * requesting a new upload
     *
     * @param metadataMessages The SiLA Client Metadata messages to send when requesting a new upload
     *
     * @return The new {@link BinaryUploadHelper}
     */
    public BinaryUploadHelper withMetadata(Message... metadataMessages) {
        return withMetadata(Arrays.asList(metadataMessages));
    }

    /**
     * Upload a binary
     *
     * @param byteStream Stream with the bytes to upload
     * @param length Number of bytes to upload
     * @param parameterIdentifier Fully qualified identifier of the command parameter for which the uploaded binary is intended
     * @return Information about the uploaded binary (UUID, size, lifetime)
     * @throws IOException In case of issues during reading the {@code byteStream}
     *
     * @implNote If the stream contains more that {@code length} bytes they will be ignored
     */
    public BinaryInfo upload(
            @NonNull final InputStream byteStream, final long length, @NonNull final String parameterIdentifier)
            throws IOException {
        final int numChunks = (int) Math.ceil(length / (double) MAX_CHUNK_LENGTH);

        log.debug("Calling CreateBinary for {} bytes ({} chunks)", length, numChunks);
        final SiLABinaryTransfer.CreateBinaryResponse binary = createBinaryBlockingStub.createBinary(
                SiLABinaryTransfer.CreateBinaryRequest
                        .newBuilder()
                        .setBinarySize(length)
                        .setChunkCount(numChunks)
                        .setParameterIdentifier(parameterIdentifier)
                        .build()
        );
        final UUID binaryTransferUUID = UUID.fromString(binary.getBinaryTransferUUID());
        log.info(
                "Starting binary upload of {} bytes ({} chunks) with UUID {}",
                numChunks,
                numChunks,
                binaryTransferUUID
        );

        final Set<Integer> chunksToUpload = ConcurrentHashMap.newKeySet();
        IntStream.range(0, numChunks - 1).forEach(chunksToUpload::add);
        final CompletableFuture<Void> voidCompletableFuture = new CompletableFuture<>();

        class UploadStreamObserver implements StreamObserver<SiLABinaryTransfer.UploadChunkResponse> {
            OffsetDateTime expiration = OffsetDateTime.now();

            @Override
            public void onNext(final SiLABinaryTransfer.UploadChunkResponse value) {
                final int chunkIndex = value.getChunkIndex();
                expiration = OffsetDateTime.now().plus(Duration.ofNanos(
                        value.getLifetimeOfBinary().getSeconds() * (long) Math.pow(10, 9) +
                                value.getLifetimeOfBinary().getNanos()));
                chunksToUpload.remove(chunkIndex);
                log.debug("Uploaded chunk {}", chunkIndex);
            }

            @Override
            public void onError(Throwable t) {
                log.error("UploadChunk failed", t);
                voidCompletableFuture.completeExceptionally(t);
            }

            @Override
            public void onCompleted() {
                if (!chunksToUpload.isEmpty()) {
                    throw new RuntimeException("Chunk upload ended before all chunks were uploaded");
                }
                log.debug("UploadChunk RPC completed");
                voidCompletableFuture.complete(null);
            }

        }

        final UploadStreamObserver uploadStreamObserver = new UploadStreamObserver();
        final StreamObserver<SiLABinaryTransfer.UploadChunkRequest> requestStreamObserver
                = uploadStreamStub.uploadChunk(
                uploadStreamObserver);

        final byte[] buffer = new byte[MAX_CHUNK_LENGTH];
        try (BufferedInputStream bufferedStream = new BufferedInputStream(byteStream)) {
            for (int chunkIndex = 0; chunkIndex < numChunks; ++chunkIndex) {
                final int offset = chunkIndex * MAX_CHUNK_LENGTH;
                final int chunkLength = (int) Math.min(length - offset, MAX_CHUNK_LENGTH);

                int bytesRead = bufferedStream.read(buffer);
                if (bytesRead < chunkLength) {
                    throw new RuntimeException(String.format(
                            "Failed to read from buffer (offset: %s, chunk length: %s)", offset, chunkLength));
                }

                log.debug("Uploading chunk {}", chunkIndex);
                requestStreamObserver.onNext(
                        SiLABinaryTransfer.UploadChunkRequest
                                .newBuilder()
                                .setChunkIndex(chunkIndex)
                                .setBinaryTransferUUID(binaryTransferUUID.toString())
                                .setPayload(ByteString.copyFrom(buffer, 0, chunkLength))
                                .build()
                );
            }
        }

        voidCompletableFuture.join();
        requestStreamObserver.onCompleted();

        log.info("Binary upload {} completed", binaryTransferUUID);
        return new BinaryInfo(binaryTransferUUID, uploadStreamObserver.expiration, length);
    }

    /**
     * Upload a binary
     *
     * @param bytes Bytes to upload
     * @param parameterIdentifier Fully qualified identifier of the command parameter for which the uploaded binary is intended
     * @return Information about the uploaded binary (UUID, size, lifetime)
     *
     * @implNote If the stream contains more that {@code length} bytes they will be ignored
     */
    public BinaryInfo upload(final byte[] bytes, @NonNull final String parameterIdentifier) {
        try (ByteArrayInputStream byteStream = new ByteArrayInputStream(bytes)) {
            return upload(byteStream, bytes.length, parameterIdentifier);
        } catch (IOException e) {
            // should never happen, ByteArrayInputStream does not perform I/O
            throw new RuntimeException(e);
        }
    }

    /**
     * Request that the server deletes the binary associated with the given {@link UUID}
     * @param binaryTransferUUID {@link UUID} of the binary to delete
     */
    public void deleteBinary(UUID binaryTransferUUID) {
        //noinspection ResultOfMethodCallIgnored
        uploadBlockingStub.deleteBinary(SiLABinaryTransfer.DeleteBinaryRequest.newBuilder()
                                                .setBinaryTransferUUID(binaryTransferUUID.toString()).build());
    }
}
