package sila_java.library.core.sila.types;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import sila2.org.silastandard.SiLAFramework;

/**
 * SiLA Integer utility class
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class SiLAInteger {
    /**
     * Create a {@link SiLAFramework.Integer} from a {@link Integer}
     * @param integer the integer
     * @return a {@link SiLAFramework.Integer}
     */
    public static SiLAFramework.Integer from(final long integer) {
        return SiLAFramework.Integer.newBuilder().setValue(integer).build();
    }
}
