package sila_java.library.core.sila.mapping.grpc;

/**
 * Collection of SiLA gRPC Name Mapping
 */
public class GrpcNameMapper {
    private final static String parameterSuffix = "_Parameters";
    private final static String responseSuffix = "_Responses";

    private final static String stateCommandSuffix = "_Info";
    private final static String intermediateCommandSuffix = "_Intermediate";
    private final static String intermediateResponseSuffix = "_IntermediateResponses";
    private final static String resultSuffix = "_Result";

    private final static String unobservablePropertyPrefix = "Get_";
    private final static String observablePropertyPrefix = "Subscribe_";

    private final static String metadataPrefix = "Metadata_";
    private final static String metadataRPCPrefix = "Get_FCPAffectedByMetadata_";

    private final static String dataTypePrefix = "DataType_";
    private final static String structSuffix = "_Struct";

    /**
     * Get parameter gRPC field name
     * @param parameter the parameter
     * @return the parameter gRPC field name
     */
    public static String getParameter(final String parameter) {
        return (parameter + GrpcNameMapper.parameterSuffix);
    }

    /**
     * Get response gRPC field name
     * @param response the response
     * @return the response gRPC field name
     */
    public static String getResponse(final String response) {
        return (response + GrpcNameMapper.responseSuffix);
    }

    /**
     * Get the Observable Command Info gRPC field name
     * @param stateCommand the Observable Command Info
     * @return the Observable Command Info gRPC field name
     */
    public static String getStateCommand(final String stateCommand) {
        return (stateCommand + GrpcNameMapper.stateCommandSuffix);
    }

    /**
     * Get intermediate command gRPC field name
     * @param intermediateCommand the intermediate command
     * @return the intermediate command gRPC field name
     */
    public static String getIntermediateCommand(final String intermediateCommand) {
        return (intermediateCommand + GrpcNameMapper.intermediateCommandSuffix);
    }

    /**
     * Get intermediate response gRPC field name
     * @param intermediateResponse the intermediate response
     * @return the parameter gRPC field name
     */
    public static String getIntermediateResponse(final String intermediateResponse) {
        return (intermediateResponse + GrpcNameMapper.intermediateResponseSuffix);
    }

    /**
     * Get observable command result gRPC field name
     * @param result the observable command result
     * @return the observable command result gRPC field name
     */
    public static String getResult(final String result) {
        return (result + GrpcNameMapper.resultSuffix);
    }

    /**
     * Get unobservable property gRPC field name
     * @param unobservableProperty the unobservable property
     * @return the unobservable property gRPC field name
     */
    public static String getUnobservableProperty(final String unobservableProperty) {
        return (GrpcNameMapper.unobservablePropertyPrefix + unobservableProperty);
    }

    /**
     * Get the observable property gRPC field name
     * @param observableProperty the observable property
     * @return the observable property gRPC field name
     */
    public static String getObservableProperty(final String observableProperty) {
        return (GrpcNameMapper.observablePropertyPrefix + observableProperty);
    }

    /**
     * Get metadata gRPC field name
     * @param metadata the parameter
     * @return the metadata gRPC field name
     */
    public static String getMetadata(final String metadata) {
        return (GrpcNameMapper.metadataPrefix + metadata);
    }

    /**
     * Get get affected by metadata gRPC field name
     * @param metadata the get affected by metadata
     * @return the get affected by metadata gRPC field name
     */
    public static String getMetadataRPC(final String metadata) {
        return (GrpcNameMapper.metadataRPCPrefix + metadata);
    }

    /**
     * Get dataType gRPC field name
     * @param dataType the dataType
     * @return the dataType gRPC field name
     */
    public static String getDataType(final String dataType) {
        return (GrpcNameMapper.dataTypePrefix + dataType);
    }

    /**
     * Get struct gRPC field name
     * @param struct the struct
     * @return the struct gRPC field name
     */
    public static String getStruct(final String struct) {
        return (struct + GrpcNameMapper.structSuffix);
    }
}
