package sila_java.library.core.encryption;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.apache.tools.ant.filters.StringInputStream;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMDecryptorProvider;
import org.bouncycastle.openssl.PEMEncryptedKeyPair;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.bouncycastle.openssl.jcajce.JcaPEMWriter;
import org.bouncycastle.openssl.jcajce.JcePEMDecryptorProviderBuilder;

import javax.annotation.Nullable;
import java.io.*;
import java.security.PrivateKey;
import java.security.Security;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Base64;

/**
 * Internal utility functions.
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class EncryptionUtils {
    private static final String CERTIFICATE_BEGIN = "-----BEGIN CERTIFICATE-----";
    private static final String CERTIFICATE_END = "-----END CERTIFICATE-----";
    private static final String RSA_PRIVATE_KEY_BEGIN = "-----BEGIN RSA PRIVATE KEY-----";
    private static final String RSA_PRIVATE_KEY_END = "-----END RSA PRIVATE KEY-----";

    /**
     * Provides full content of a crt like file.
     */
    @Deprecated
    public static InputStream certificateToStream(@NonNull final X509Certificate certificate)
            throws CertificateEncodingException {
        final String cert = new String(Base64.getEncoder().encode(certificate.getEncoded()));
        return new ByteArrayInputStream((CERTIFICATE_BEGIN + '\n' + cert + '\n' + CERTIFICATE_END).getBytes());
    }

    /**
     * Provides full content of a pem/key like file
     */
    public static InputStream keyToStream(@NonNull final PrivateKey privateKey) {
        final String key = new String(Base64.getEncoder().encode(privateKey.getEncoded()));
        return new ByteArrayInputStream((RSA_PRIVATE_KEY_BEGIN + '\n' + key + '\n' + RSA_PRIVATE_KEY_END).getBytes());
    }

    /**
     * Read and parse a private key from an optionally password protected file
     * @param file the file
     * @param password the password
     * @return the parsed private key
     * @throws IOException if failed to parse the private key
     */
    public static PrivateKey readPrivateKey(File file, @Nullable String password) throws IOException {
        try (FileInputStream keyFile = new FileInputStream(file)) {
            Security.addProvider(new BouncyCastleProvider());

            PEMParser pemParser = new PEMParser(new InputStreamReader(keyFile));
            Object keyPair = pemParser.readObject();
            if (keyPair == null) {
                throw new IOException("Malformed private key");
            }
            PEMKeyPair pemKeyPair = null ;
            if (keyPair instanceof PEMEncryptedKeyPair) {
                PEMDecryptorProvider decryptorProvider = new JcePEMDecryptorProviderBuilder().build((password == null ? "" : password).toCharArray());
                pemKeyPair = ((PEMEncryptedKeyPair)keyPair).decryptKeyPair(decryptorProvider);
            } else {
                pemKeyPair = (PEMKeyPair)keyPair;
            }

            JcaPEMKeyConverter converter = new JcaPEMKeyConverter().setProvider("BC");
            return converter.getPrivateKey(pemKeyPair.getPrivateKeyInfo());
        }
    }

    /**
     * Read and parse a certificate from a file
     * @param file the file
     * @return the parsed certificate
     * @throws IOException if failed to parse the certificate
     */
    public static X509Certificate readCertificate(File file) throws IOException {
        CertificateFactory cf = null;
        try (final FileInputStream fs = new FileInputStream(file)) {
            cf = CertificateFactory.getInstance("X.509");
            return (X509Certificate)cf.generateCertificate(fs);
        } catch (CertificateException e) {
            throw new IOException(e);
        }
    }

    /**
     * Parse a certificate from a string
     * @param certificateStr the string
     * @return the parsed certificate
     * @throws IOException if failed to parse the certificate
     */
    public static X509Certificate readCertificate(String certificateStr) throws IOException {
        CertificateFactory cf = null;
        try (final StringInputStream fs = new StringInputStream(certificateStr)) {
            cf = CertificateFactory.getInstance("X.509");
            return (X509Certificate)cf.generateCertificate(fs);
        } catch (CertificateException e) {
            throw new IOException(e);
        }
    }

    /**
     * Create and write certificate to file
     * @param file the file to create and write to certificate to
     * @param certificate the certificate to write
     * @throws IOException if the certificate could not be written
     */
    public static void writeCertificateToFile(File file, X509Certificate certificate) throws IOException {
        writePEMObjToFile(file, certificate);
    }

    /**
     * Create and write certificate to file
     * @param file the file to create and write to certificate to
     * @param privateKey the private key to write
     * @throws IOException if the private key could not be written
     */
    public static void writePrivateKeyToFile(File file, PrivateKey privateKey) throws IOException {
        writePEMObjToFile(file, privateKey);
    }

    /**
     * Create and write pem to file
     * @param file the file to create and write to certificate to
     * @param obj the pem
     * @throws IOException if the certificate could not be written
     */
    public static void writePEMObjToFile(File file, Object obj) throws IOException {
        try (final Writer writer = new PrintWriter(file)) {
            final JcaPEMWriter pemWriter = new JcaPEMWriter(writer);
            pemWriter.writeObject(obj);
            pemWriter.flush();
            pemWriter.close();
        }
    }

    /**
     * Write certificate to string
     * @param certificate the certificate
     * @return the certificate string representation
     * @throws IOException if the certificate could not be written into a certificate
     */
    public static String writeCertificateToString(X509Certificate certificate) throws IOException {
        try (final Writer writer = new StringWriter()) {
            final JcaPEMWriter pemWriter = new JcaPEMWriter(writer);
            pemWriter.writeObject(certificate);
            pemWriter.flush();
            pemWriter.close();
            return writer.toString();
        }
    }
}
