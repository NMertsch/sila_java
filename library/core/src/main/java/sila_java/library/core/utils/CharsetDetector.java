package sila_java.library.core.utils;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;
import java.nio.charset.StandardCharsets;

/**
 * Helper class to guess the characterset of a string.
 */
class CharsetDetector {
    private final static Charset[] SUPPORTED_CHARSETS = {
        StandardCharsets.ISO_8859_1,
        StandardCharsets.UTF_16BE,
        StandardCharsets.UTF_16,
        StandardCharsets.ISO_8859_1,
        StandardCharsets.US_ASCII,
        StandardCharsets.UTF_8,
        StandardCharsets.UTF_16LE
    };

    /**
     * Detect charset from string
     * @param value
     * @return
     */
    static String detectCharset(String value) {
        final String probe = StandardCharsets.UTF_8.name();

        for(Charset charset : SUPPORTED_CHARSETS) {
            if(charset != null) {
                try {
                    if(value.equals(convert(convert(value, charset.name(), probe), probe, charset.name()))) {
                        return charset.name();
                    }
                }catch (UnsupportedEncodingException e){
                    // should never happen but if yes
                    throw new IllegalCharsetNameException("Charset: " + charset + " : Error: " + e);
                }
            }
        }
        return StandardCharsets.UTF_8.name();
    }

    /**
     * Convert a string from an encoding to another
     * @param value the string
     * @param fromEncoding the source encoding
     * @param toEncoding the target encoding
     * @return the encoded string
     * @throws UnsupportedEncodingException if the string could not be encoded
     */
    private static String convert(String value, String fromEncoding, String toEncoding)
            throws UnsupportedEncodingException{
        return new String(value.getBytes(fromEncoding), toEncoding);
    }
}
