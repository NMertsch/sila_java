package sila_java.library.core.sila.errors;

import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import lombok.Getter;
import lombok.NonNull;
import sila2.org.silastandard.SiLABinaryTransfer;
import sila2.org.silastandard.SiLAFramework;

import javax.xml.bind.DatatypeConverter;

/**
 * SiLA Error Exception
 */
public class SiLAErrorException extends StatusRuntimeException {
    @Getter
    private final SiLAFramework.SiLAError siLAError;

    /**
     * Constructor
     *
     * @param siLAError the {@link SiLAFramework.SiLAError}
     */
    public SiLAErrorException(SiLAFramework.SiLAError siLAError) {
        super(Status.ABORTED.withDescription(getEncodedSiLAError(siLAError)));
        this.siLAError = siLAError;
    }

    /**
     * Constructor
     *
     * @param binaryTransferError the {@link SiLABinaryTransfer.BinaryTransferError}
     */
    public SiLAErrorException(SiLABinaryTransfer.BinaryTransferError binaryTransferError) {
        super(Status.ABORTED.withDescription(getEncodedSiLAError(getSiLAErrorFromBinaryTransferError(binaryTransferError))));
        this.siLAError = getSiLAErrorFromBinaryTransferError(binaryTransferError);
    }

    /**
     *
     * @implNote todo fixme (hack) implement real Binary Transfer Error
     *
     * @param binaryTransferError
     * @return
     */
    private static SiLAFramework.SiLAError getSiLAErrorFromBinaryTransferError(
            @NonNull final SiLABinaryTransfer.BinaryTransferError binaryTransferError
    ) {
        return SiLAFramework.SiLAError
                .newBuilder()
                .setUndefinedExecutionError(
                        SiLAFramework.UndefinedExecutionError
                                .newBuilder()
                                .setMessage(binaryTransferError.getErrorType().name() + ": " + binaryTransferError.getMessage())
                                .build()
                )
                .build();
    }

    /**
     * Constructor
     *
     * @param siLAError the {@link SiLAFramework.SiLAError}
     * @param status the {@link Status}
     */
    public SiLAErrorException(SiLAFramework.SiLAError siLAError, @NonNull final Status status) {
        super(status);
        this.siLAError = siLAError;
    }

    /**
     * Encode a {@link sila2.org.silastandard.SiLAFramework.SiLAError} into a string
     * @param siLAError the error to encode
     * @return the string encoded error
     */
    private static String getEncodedSiLAError(SiLAFramework.SiLAError siLAError) {
        return DatatypeConverter.printBase64Binary(siLAError.toByteArray());
    }
}
