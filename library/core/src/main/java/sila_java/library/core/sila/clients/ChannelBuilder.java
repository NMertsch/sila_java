package sila_java.library.core.sila.clients;

import io.grpc.ChannelCredentials;
import io.grpc.Grpc;
import io.grpc.InsecureChannelCredentials;
import io.grpc.ManagedChannelBuilder;
import io.grpc.TlsChannelCredentials;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.tools.ant.filters.StringInputStream;

import java.io.IOException;
import java.io.InputStream;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.concurrent.TimeUnit;

import static sila_java.library.core.encryption.EncryptionUtils.certificateToStream;

/**
 * Helper for creating {@link io.grpc.ManagedChannelBuilder}s
 *
 * @apiNote Use {@link ChannelFactory} instead if no further channel configuration is required
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class ChannelBuilder {
    private static final int CHANNEL_IDLE_TIMEOUT_SEC = 5;

    /**
     * Create a gRPC channel builder for encrypted communication with a SiLA Server
     *
     * @param host SiLA Server host
     * @param port SiLA Server port
     *
     * @return gRPC channel builder
     *
     * @apiNote If the SiLA Server does not use a trusted certificate, use one of the overloaded methods to
     *         provide the CA certificate, e.g.
     *         {@link ChannelBuilder#withTLSEncryption(String, int, X509Certificate)}.
     */
    public static ManagedChannelBuilder<?> withTLSEncryption(@NonNull String host, int port) {
        return getBuilder(host, port, TlsChannelCredentials.create());
    }

    /**
     * Create a gRPC channel builder for encrypted communication with a SiLA Server using a custom CA certificate
     *
     * @param host SiLA Server host
     * @param port SiLA Server port
     * @param customCertificateAuthority PEM-encoded custom CA certificate
     *
     * @return gRPC channel builder
     *
     * @throws IOException If the provided certificate could not be parsed
     * @apiNote Use {@link ChannelBuilder#withTLSEncryption(String, int)} if the SiLA Server uses a trusted
     *         certificate
     */
    public static ManagedChannelBuilder<?> withTLSEncryption(
            @NonNull String host, int port, @NonNull InputStream customCertificateAuthority) throws IOException {
        return getBuilder(
                host, port, TlsChannelCredentials.newBuilder().trustManager(customCertificateAuthority).build());
    }

    /**
     * Create a gRPC channel builder for encrypted communication with a SiLA Server using a custom CA certificate
     *
     * @param host SiLA Server host
     * @param port SiLA Server port
     * @param customCertificateAuthority PEM-encoded custom CA certificate
     *
     * @return gRPC channel builder
     *
     * @throws IOException If the provided certificate could not be parsed
     * @apiNote Use {@link ChannelBuilder#withTLSEncryption(String, int)} if the SiLA Server uses a trusted
     *         certificate
     */
    public static ManagedChannelBuilder<?> withTLSEncryption(
            @NonNull String host, int port, @NonNull String customCertificateAuthority) throws IOException {
        try (InputStream certAuthorityStream = new StringInputStream(customCertificateAuthority)) {
            return withTLSEncryption(host, port, certAuthorityStream);
        }
    }

    /**
     * Create a gRPC channel builder for encrypted communication with a SiLA Server using a custom CA certificate
     *
     * @param host SiLA Server host
     * @param port SiLA Server port
     * @param customCertificateAuthority PEM-encoded custom CA certificate
     *
     * @return gRPC channel builder
     *
     * @apiNote Use {@link ChannelBuilder#withTLSEncryption(String, int)} if the SiLA Server uses a trusted
     *         certificate
     */
    @SneakyThrows({CertificateEncodingException.class, IOException.class})
    public static ManagedChannelBuilder<?> withTLSEncryption(
            @NonNull String host, int port, @NonNull X509Certificate customCertificateAuthority) {
        return withTLSEncryption(host, port, certificateToStream(customCertificateAuthority));
    }

    /**
     * Create a gRPC channel builder for unencrypted communication with a SiLA Server
     *
     * @param host SiLA Server host
     * @param port SiLA Server port
     *
     * @return gRPC channel builder
     *
     * @deprecated Unencrypted communication violates the SiLA 2 specification. Use
     *         {@link ChannelBuilder#withTLSEncryption(String, int)} or
     *         {@link ChannelBuilder#withTLSEncryption(String, int, InputStream)} instead.
     */
    @Deprecated
    public static ManagedChannelBuilder<?> withoutEncryption(@NonNull String host, int port) {
        return getBuilder(host, port, InsecureChannelCredentials.create());
    }

    /**
     * Get {@link ManagedChannelBuilder}
     * @param host server host
     * @param port the port
     * @param credentials server credentials
     * @return {@link ManagedChannelBuilder}
     */
    private static ManagedChannelBuilder<?> getBuilder(
            @NonNull String host, int port, @NonNull ChannelCredentials credentials) {
        return configureChannelBuilder(Grpc.newChannelBuilderForAddress(host, port, credentials));
    }

    /**
     * Configure a {@link ManagedChannelBuilder}
     *
     * @param channelBuilder the channel builder to configure
     *
     * @return the configured {@link ManagedChannelBuilder}
     */
    private static ManagedChannelBuilder<?> configureChannelBuilder(
            @NonNull final ManagedChannelBuilder<?> channelBuilder) {
        return channelBuilder.idleTimeout(CHANNEL_IDLE_TIMEOUT_SEC, TimeUnit.SECONDS);
    }
}
