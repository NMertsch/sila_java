package sila_java.library.core.sila.binary_transfer;

import com.google.protobuf.InvalidProtocolBufferException;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import sila2.org.silastandard.SiLABinaryTransfer;

import javax.annotation.Nonnull;
import javax.xml.bind.DatatypeConverter;
import java.util.Optional;

/**
 * Utility class for Binary Transfer Error
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class BinaryTransferErrorHandler {

    /**
     * Generate a Binary Transfer Error
     * @param errorIdentifier The error identifier
     * @param message The error message
     * @return StatusRuntime according to SiLA
     */
    public static StatusRuntimeException generateBinaryTransferError(
            @NonNull final SiLABinaryTransfer.BinaryTransferError.ErrorType errorIdentifier,
            @NonNull final String message
    ) {
        return generateGRPCError(SiLABinaryTransfer.BinaryTransferError.newBuilder()
                .setErrorType(errorIdentifier)
                .setMessage(message)
                .build()
        );
    }

    /**
     * Retrieve BinaryTransferError from gRPC Exception
     *
     * @param statusRuntimeException gRPC Exception occurring during gRPC call
     * @return Optional BinaryTransferError if well formed
     */
    public static Optional<SiLABinaryTransfer.BinaryTransferError> retrieveBinaryTransferError(
            @Nonnull final StatusRuntimeException statusRuntimeException
    ) {
        final Status status = statusRuntimeException.getStatus();

        if (!status.getCode().equals(Status.Code.ABORTED)) {
            return Optional.empty();
        }

        try {
            final String description = (status.getDescription() == null) ? ("No description") : (status.getDescription());
            return Optional.of(SiLABinaryTransfer.BinaryTransferError.parseFrom(DatatypeConverter.parseBase64Binary(description)));
        } catch (InvalidProtocolBufferException e) {
            return Optional.empty();
        }
    }

    /**
     * Generating gRPC Error from built Binary Transfer Error
     *
     * @param binaryTransferError BinaryTransferError proto message
     * @return StatusRuntime according to SiLA
     */
    private static StatusRuntimeException generateGRPCError(@Nonnull final SiLABinaryTransfer.BinaryTransferError binaryTransferError) {
        return new StatusRuntimeException(
                Status.ABORTED.withDescription(DatatypeConverter.printBase64Binary(binaryTransferError.toByteArray()))
        );
    }
}
