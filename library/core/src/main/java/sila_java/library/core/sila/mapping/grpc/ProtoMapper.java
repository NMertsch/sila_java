package sila_java.library.core.sila.mapping.grpc;

import com.google.protobuf.DescriptorProtos;
import com.google.protobuf.Descriptors;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageOrBuilder;
import com.google.protobuf.util.JsonFormat;
import lombok.*;
import org.apache.commons.lang3.RandomStringUtils;
import sila2.org.silastandard.SiLAFramework;
import sila_java.library.core.models.*;
import sila_java.library.core.sila.mapping.feature.FeatureGenerator;
import sila_java.library.core.sila.mapping.feature.MalformedSiLAFeature;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static sila_java.library.core.sila.mapping.feature.ConstraintValidation.validateConstraints;
import static sila_java.library.core.sila.mapping.feature.FeatureGenerator.generateFeature;
import static sila_java.library.core.sila.mapping.feature.FeatureGenerator.getMajorVersion;
import static sila_java.library.core.sila.mapping.grpc.DynamicMessageBuilder.*;

/**
 * Mapping of the Feature Framework to gRPC Proto Files
 */
public class ProtoMapper {
    private static final List<String> SUPPORTED_SILA_VERSIONS = Arrays.asList(
            "1.0", // Original version
            "1.1" // No change for client initiated connection
    );

    private static final String OBSERVABLE_TRUE = "Yes";
    private static final String OBSERVABLE_FALSE = "No";
    /* SiLA Basic Types Protobuf Type Mapping */
    private static final Map<BasicType, String> TYPE_MAP = Collections.unmodifiableMap(new HashMap<BasicType, String>() {{
        put(BasicType.STRING, SiLAFramework.String.getDescriptor().getFullName());
        put(BasicType.INTEGER, SiLAFramework.Integer.getDescriptor().getFullName());
        put(BasicType.REAL, SiLAFramework.Real.getDescriptor().getFullName());
        put(BasicType.BOOLEAN, SiLAFramework.Boolean.getDescriptor().getFullName());
        put(BasicType.BINARY, SiLAFramework.Binary.getDescriptor().getFullName());
        put(BasicType.DATE, SiLAFramework.Date.getDescriptor().getFullName());
        put(BasicType.TIME, SiLAFramework.Time.getDescriptor().getFullName());
        put(BasicType.TIMESTAMP, SiLAFramework.Timestamp.getDescriptor().getFullName());
        put(BasicType.ANY, SiLAFramework.Any.getDescriptor().getFullName());
    }});
    private final Map<String, SiLAElement> dataTypeIdentifierMap = new HashMap<>();
    private final Feature feature;

    /**
     * Factory Method
     *
     * @param xmlContent feature in XML format
     */
    @SneakyThrows(IOException.class)
    public static ProtoMapper usingFeature(@NonNull final String xmlContent) {
        return usingFeature(generateFeature(xmlContent));
    }

    /**
     * Factory Method
     *
     * @param feature Feature POJO e.g. generated from XML
     *      {@link FeatureGenerator#generateFeature}
     */
    public static ProtoMapper usingFeature(@NonNull Feature feature) {
        return new ProtoMapper(feature);
    }

    /**
     * Converts a proto message into a serialized string
     * @param message the proto Message
     * @return The serialized string
     */
    public static String serializeToJson(@NonNull final MessageOrBuilder message)
            throws InvalidProtocolBufferException {
        return JsonFormat.printer().includingDefaultValueFields().print(message);
    }

    /**
     * Constructore
     * @param feature the feature
     */
    private ProtoMapper(@NonNull Feature feature) {
        this.feature = feature;
    }

    /**
     * Maps from Feature definitions to Protofields
     *
     * @return FileDescriptorProto for programmatic proto definition
     */
    public Descriptors.FileDescriptor generateProto() throws MalformedSiLAFeature {
        final DynamicProtoBuilder protoBuilder = new DynamicProtoBuilder(
                this.feature.getIdentifier(),
                this.getPackageName()
        );

        addDataTypeDefinitions(protoBuilder);
        addCommands(protoBuilder);
        addProperties(protoBuilder);
        addMetadata(protoBuilder);

        // Build protobuf with dependency on SiLA Framework
        try {
            return protoBuilder.generateDescriptor(
                    SiLAFramework.getDescriptor()
            );
        } catch (Descriptors.DescriptorValidationException e) {
            throw new MalformedSiLAFeature(e.getMessage());
        }
    }

    /**
     * Get the fully qualified package name
     * @return the fully qualified package name
     * @throws MalformedSiLAFeature if the feature is malformed
     */
    private String getPackageName() throws MalformedSiLAFeature {
        final String siLA2Version = this.feature.getSiLA2Version();
        if (!SUPPORTED_SILA_VERSIONS.contains(siLA2Version)) {
            throw new MalformedSiLAFeature(
                    "Only these SiLA 2 versions are supported: " + SUPPORTED_SILA_VERSIONS.toString() +
                            " the found version is: " + siLA2Version
            );
        }

        return Stream.of(
                "sila2",
                feature.getOriginator(),
                feature.getCategory(),
                feature.getIdentifier().toLowerCase(),
                "v" + getMajorVersion(feature))
                .filter(s -> s != null && !s.isEmpty())
                .collect(Collectors.joining("."));
    }

    /**
     * Add a Data type definition to {@link ProtoMapper#dataTypeIdentifierMap} and the specified {@link DynamicProtoBuilder}
     * @param protoBuilder the {@link DynamicProtoBuilder}
     * @throws MalformedSiLAFeature if the feature is malformed
     */
    private void addDataTypeDefinitions(@NonNull DynamicProtoBuilder protoBuilder) throws MalformedSiLAFeature {
        dataTypeIdentifierMap.clear();
        for (final SiLAElement dataTypeDefinition : feature.getDataTypeDefinition()) {
            final String dataTypeId = dataTypeDefinition.getIdentifier();

            if (dataTypeIdentifierMap.containsKey(dataTypeId)) {
                throw new MalformedSiLAFeature(dataTypeId + " data type is defined twice!");
            }
            dataTypeIdentifierMap.put(dataTypeId, dataTypeDefinition);
        }
        for (String dataTypeId : dataTypeIdentifierMap.keySet()) {
            final String msdId = GrpcNameMapper.getDataType(dataTypeId);
            protoBuilder.addMessage(
                    generateMessageDescription(
                            msdId,
                            Collections.singletonList(dataTypeIdentifierMap.get(dataTypeId)),
                            dataTypeIdentifierMap
                    )
            );
        }
    }

    /**
     * Add a command definition to {@link ProtoMapper#dataTypeIdentifierMap} and the specified {@link DynamicProtoBuilder}
     * @param protoBuilder the {@link DynamicProtoBuilder}
     * @throws MalformedSiLAFeature if the feature is malformed
     */
    private void addCommands(@NonNull DynamicProtoBuilder protoBuilder) throws MalformedSiLAFeature {
        final List<String> commandIdentifierList = new ArrayList<>();
        for (Feature.Command command : feature.getCommand()) {
            final String cmdId = command.getIdentifier();

            if (commandIdentifierList.contains(cmdId)) {
                throw new MalformedSiLAFeature(cmdId + " command is defined twice!");
            }
            commandIdentifierList.add(cmdId);

            final String parId = GrpcNameMapper.getParameter(cmdId);
            final String resId = GrpcNameMapper.getResponse(cmdId);

            protoBuilder.addMessage(generateMessageDescription(parId, command.getParameter(), dataTypeIdentifierMap));
            protoBuilder.addMessage(generateMessageDescription(resId, command.getResponse(), dataTypeIdentifierMap));

            if (command.getObservable().matches(ProtoMapper.OBSERVABLE_FALSE)) {
                if (!command.getIntermediateResponse().isEmpty()) {
                    throw new MalformedSiLAFeature(
                            "Command " + command.getIdentifier() + " cannot have intermediate responses because it is unobservable!"
                    );
                }
                // Unobservable command
                protoBuilder.addCall(cmdId, parId, false, resId, false);
            } else if (command.getObservable().matches(ProtoMapper.OBSERVABLE_TRUE)) {
                // Observable command
                final String cmdExecutionIdType = SiLAFramework.CommandExecutionUUID
                        .getDescriptor().getFullName();

                // Initial Proto Call
                protoBuilder.addCall(cmdId, parId, false,
                        SiLAFramework.CommandConfirmation.getDescriptor().getFullName(), false);

                // Status Proto Call
                protoBuilder.addCall(GrpcNameMapper.getStateCommand(cmdId), cmdExecutionIdType, false,
                        SiLAFramework.ExecutionInfo.getDescriptor().getFullName(), true);

                // Intermediate Responses
                final List<SiLAElement> intermediateResponses = command.getIntermediateResponse();
                if (!intermediateResponses.isEmpty()) {
                    final String interResId = GrpcNameMapper.getIntermediateResponse(cmdId);

                    protoBuilder.addMessage(
                            generateMessageDescription(interResId, intermediateResponses, dataTypeIdentifierMap)
                    );

                    protoBuilder.addCall(GrpcNameMapper.getIntermediateCommand(cmdId),
                            cmdExecutionIdType, false,
                            interResId, true);
                }

                // Final Result
                protoBuilder.addCall(GrpcNameMapper.getResult(cmdId),
                        cmdExecutionIdType, false,
                        resId, false);
            } else {
                throw new MalformedSiLAFeature(
                        "(should never happen) Unsupported SiLAType found");
            }
        }
    }

    /**
     * Add a propety definition to {@link ProtoMapper#dataTypeIdentifierMap} and the specified {@link DynamicProtoBuilder}
     * @param protoBuilder the {@link DynamicProtoBuilder}
     * @throws MalformedSiLAFeature if the feature is malformed
     */
    private void addProperties(@NonNull DynamicProtoBuilder protoBuilder) throws MalformedSiLAFeature {
        final List<String> propertyIdentifierList = new ArrayList<>();
        for (Feature.Property property : feature.getProperty()) {
            final String propId = property.getIdentifier();

            if (propertyIdentifierList.contains(propId)) {
                throw new MalformedSiLAFeature(propId + " property is defined twice!");
            }
            propertyIdentifierList.add(propId);

            if (property.getObservable().matches(ProtoMapper.OBSERVABLE_FALSE)) {
                final String callId = GrpcNameMapper.getUnobservableProperty(propId);
                final String parId = GrpcNameMapper.getParameter(callId);
                final String resId = GrpcNameMapper.getResponse(callId);

                // @Note: property field id = feature id
                protoBuilder.addMessage(
                        generateMessageDescription(parId, Collections.emptyList(), dataTypeIdentifierMap) // Empty parameters
                );
                protoBuilder.addMessage(
                        generateMessageDescription(resId, Collections.singletonList(property), dataTypeIdentifierMap)
                );

                protoBuilder.addCall(callId, parId, false, resId, false);
            } else if (property.getObservable().matches(ProtoMapper.OBSERVABLE_TRUE)) {
                final String callId = GrpcNameMapper.getObservableProperty(propId);
                final String parId = GrpcNameMapper.getParameter(callId);
                final String resId = GrpcNameMapper.getResponse(callId);

                // @implNote we only generate a one layered message so we don't use the generic getters here
                MessageDescription.MessageDescriptionBuilder messageDescriptionBuilder =
                        new MessageDescription.MessageDescriptionBuilder();

                messageDescriptionBuilder.identifier(parId);
                List<FieldDescription> fieldDescriptions = new ArrayList<>();

                messageDescriptionBuilder.fields(fieldDescriptions);
                // @implNote The description builder needs to have a list of nested messages
                messageDescriptionBuilder.nestedMessages(Collections.emptyList());

                protoBuilder.addMessage(messageDescriptionBuilder.build());

                protoBuilder.addMessage(
                        generateMessageDescription(resId, Collections.singletonList(property), dataTypeIdentifierMap)
                );

                protoBuilder.addCall(callId, parId, false, resId, true);
            }
        }
    }

    /**
     * Add a metadata definition to {@link ProtoMapper#dataTypeIdentifierMap} and the specified {@link DynamicProtoBuilder}
     * @param protoBuilder the {@link DynamicProtoBuilder}
     * @throws MalformedSiLAFeature if the feature is malformed
     */
    private void addMetadata(@NonNull DynamicProtoBuilder protoBuilder) throws MalformedSiLAFeature {
        final List<String> metadataIdentifierList = new ArrayList<>();
        for (Feature.Metadata metadata : feature.getMetadata()) {
            final String metadataIdentifier = metadata.getIdentifier();

            if (metadataIdentifierList.contains(metadataIdentifier)) {
                throw new MalformedSiLAFeature(metadataIdentifier + " metadata is defined twice!");
            }
            metadataIdentifierList.add(metadataIdentifier);

            final String callId = GrpcNameMapper.getMetadataRPC(metadataIdentifier);
            final String parId = GrpcNameMapper.getParameter(callId);
            final String resId = GrpcNameMapper.getResponse(callId);

            // Create parameter and response messages
            protoBuilder.addMessage(
                    generateMessageDescription(parId, Collections.emptyList(), dataTypeIdentifierMap) // Empty parameters
            );
            protoBuilder.addMessage(new MessageDescription(
                    resId,
                    Collections.singletonList(
                            new FieldDescription(
                                    "AffectedCalls",
                                    TYPE_MAP.get(BasicType.STRING),
                                    true
                            )
                    ),
                    Collections.emptyList()
            ));

            // Create metadata message
            protoBuilder.addMessage(generateMessageDescription(
                    GrpcNameMapper.getMetadata(metadataIdentifier),
                    Collections.singletonList(metadata),
                    dataTypeIdentifierMap
            ));

            protoBuilder.addCall(callId, parId, false, resId, false);
        }
    }

    private static <T> MessageDescription generateMessageDescription(
            @NonNull final String identifier,
            @NonNull final List<T> SiLAField,
            @NonNull final Map<String, SiLAElement> dataTypeIdentifierMap
    ) throws MalformedSiLAFeature {
        val messageDescriptionBuilder = new MessageDescription.MessageDescriptionBuilder();

        messageDescriptionBuilder.identifier(identifier);

        List<FieldDescription> fieldDescriptions = new ArrayList<>();
        List<MessageDescription> messageDescriptions = new ArrayList<>();

        for (T siLAField : SiLAField) {
            fieldDescriptions.add(generateFieldDescription(siLAField, messageDescriptions, dataTypeIdentifierMap));
        }

        messageDescriptionBuilder.fields(fieldDescriptions);
        messageDescriptionBuilder.nestedMessages(messageDescriptions);

        return messageDescriptionBuilder.build();
    }

    @Data
    private static class SiLAField {
        private final String identifier;
        private final DataTypeType dataType;
    }

    public static Descriptors.Descriptor dataTypeToDescriptor(@NonNull final DataTypeType dataTypeType)
            throws MalformedSiLAFeature {
        final String randomMessageIdentifier = RandomStringUtils.randomAlphabetic(12);
        final List<MessageDescription> messageDescriptionsOut = new ArrayList<>();
        final FieldDescription fieldDescription = ProtoMapper.generateFieldDescription(
                new SiLAField("value", dataTypeType), messageDescriptionsOut, Collections.emptyMap()
        );
        final MessageDescription messageDescription =
                new MessageDescription.MessageDescriptionBuilder()
                        .identifier(randomMessageIdentifier)
                        .fields(Collections.singletonList(fieldDescription))
                        .nestedMessages(messageDescriptionsOut)
                        .build();
        final DescriptorProtos.DescriptorProto messageType = createMessageType(messageDescription);
        final DescriptorProtos.FileDescriptorProto messageFileDescriptor = DescriptorProtos.FileDescriptorProto
                .newBuilder()
                .addMessageType(messageType)
                .build();
        try {
            final Descriptors.FileDescriptor dynamicDescriptor = Descriptors.FileDescriptor.buildFrom(
                    messageFileDescriptor,
                    new Descriptors.FileDescriptor[]{SiLAFramework.getDescriptor()}
            );
            val fields = dynamicDescriptor.findMessageTypeByName(randomMessageIdentifier).getFields();
            if (fields.size() != 1) {
                throw new RuntimeException("Unexpected number of descriptor fields, expected 1 but found " + fields.size());
            }
            if (dataTypeType.getList() != null) { // keep the type that contains the repeated inner type
                return fields.get(0).getContainingType();
            }
            // or else retrieve the inner type
            return fields.get(0).getMessageType();
        } catch (Descriptors.DescriptorValidationException e) {
            throw new MalformedSiLAFeature(e);
        }
    }

    private static FieldDescription generateFieldDescription(
            @NonNull final Object field,
            @NonNull final List<MessageDescription> messageDescriptions,
            @NonNull final Map<String, SiLAElement> dataTypeIdentifierMap
    ) throws MalformedSiLAFeature {
        // Get fields via Reflection
        String identifier;
        DataTypeType dataTypeType;
        try {
            final Method identifierMethod = field.getClass().getMethod("getIdentifier");
            identifier = (String) identifierMethod.invoke(field);

            final Method typeMethod = field.getClass().getMethod("getDataType");
            dataTypeType = (DataTypeType) typeMethod.invoke(field);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            throw new MalformedSiLAFeature(e.getMessage());
        }
        return generateFieldDescription(identifier, dataTypeType, messageDescriptions, dataTypeIdentifierMap);
    }

    private static FieldDescription generateFieldDescription(
            @NonNull final String identifier,
            @NonNull final DataTypeType dataTypeType,
            @NonNull final List<MessageDescription> messageDescriptions,
            @NonNull final Map<String, SiLAElement> dataTypeIdentifierMap
    ) throws MalformedSiLAFeature {
        // Then invoke creation dependent on type
        if (dataTypeType.getBasic() != null) {
            return new FieldDescription(
                    identifier,
                    TYPE_MAP.get(dataTypeType.getBasic()),
                    false
            );
        } else if (dataTypeType.getDataTypeIdentifier() != null) {
            final String dataTypeIdentifier = dataTypeType.getDataTypeIdentifier();
            final String typeName = GrpcNameMapper.getDataType(dataTypeIdentifier);

            if (!dataTypeIdentifierMap.containsKey(dataTypeIdentifier)) {
                throw new MalformedSiLAFeature(dataTypeIdentifier + " not defined in Feature!");
            }

            return new FieldDescription(
                    identifier,
                    typeName,
                    false
            );
        } else if (dataTypeType.getConstrained() != null) {
            final ConstrainedType constrainedType = dataTypeType.getConstrained();
            checkConstrainedValidity(constrainedType, dataTypeIdentifierMap);

            final SiLAField elementField = new SiLAField(identifier, constrainedType.getDataType());

            return generateFieldDescription(elementField, messageDescriptions, dataTypeIdentifierMap);
        } else if (dataTypeType.getList() != null) {
            final ListType listType = dataTypeType.getList();

            final DataTypeType elementType = listType.getDataType();

            // @Note: There shall be no Lists of Lists (this includes Lists of Constrained Lists)
            if (elementType.getList() != null) {
                throw new MalformedSiLAFeature(
                        "There MUST be no Lists of Lists (this includes Lists of Constrained Lists)");
            }

            final SiLAField elementField = new SiLAField(identifier, elementType);

            final FieldDescription baseField = generateFieldDescription(
                    elementField,
                    messageDescriptions,
                    dataTypeIdentifierMap
            );
            baseField.setRepeated(true);

            return baseField;
        } else if (dataTypeType.getStructure() != null) {
            StructureType structureType = dataTypeType.getStructure();
            final String nestedMessageIdentifier = GrpcNameMapper.getStruct(identifier);

            messageDescriptions.add(
                    generateMessageDescription(
                            nestedMessageIdentifier,
                            structureType.getElement(),
                            dataTypeIdentifierMap
                    )
            );

            return new FieldDescription(
                    identifier,
                    nestedMessageIdentifier,
                    false
            );
        } else {
            throw new MalformedSiLAFeature(
                    "(should never happen) Unsupported SiLAType found");
        }
    }

    private static void checkConstrainedValidity(
            @NonNull final ConstrainedType constrainedType,
            @NonNull final Map<String, SiLAElement> dataTypeIdentifierMap
    ) throws MalformedSiLAFeature {
        final Constraints constraints = constrainedType.getConstraints();

        // Get constrained type nested
        final DataTypeType dataTypeType = retrieveNestedDefinition(constrainedType.getDataType(), dataTypeIdentifierMap);

        // Validate constraints
        validateConstraints(constraints, dataTypeType);
    }

    private static DataTypeType retrieveNestedDefinition(
            @NonNull final DataTypeType superType,
            @NonNull final Map<String, SiLAElement> dataTypeIdentifierMap
    ) throws MalformedSiLAFeature {
        if (superType.getDataTypeIdentifier() != null) {
            final String dataTypeIdentifier = superType.getDataTypeIdentifier();
            if (!dataTypeIdentifierMap.containsKey(dataTypeIdentifier)) {
                throw new MalformedSiLAFeature(dataTypeIdentifier + " not defined in Feature!");
            }
            DataTypeType dataType = dataTypeIdentifierMap.get(dataTypeIdentifier).getDataType();
            return retrieveNestedDefinition(dataType, dataTypeIdentifierMap);
        }

        /*
         * If the type is a constrained type of a constrained type (sensible with DataTypeDefinitions), then
         * the nested type should still be retrieved, but the constraint check will be done a later iteration
         */
        if (superType.getConstrained() != null) {
            return retrieveNestedDefinition(superType.getConstrained().getDataType(), dataTypeIdentifierMap);
        }

        if (superType.getStructure() != null) {
            throw new MalformedSiLAFeature("The 'SiLA Constrained Type' can not wrap a 'SiLA Struct Type'.");
        }

        return superType;
    }
}
