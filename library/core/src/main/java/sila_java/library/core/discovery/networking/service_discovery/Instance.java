package sila_java.library.core.discovery.networking.service_discovery;

import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.xbill.DNS.AAAARecord;
import org.xbill.DNS.ARecord;
import org.xbill.DNS.Record;
import org.xbill.DNS.SRVRecord;
import org.xbill.DNS.TXTRecord;

import java.net.*;
import java.util.*;
import java.util.stream.Collectors;

import static sila_java.library.core.discovery.SiLADiscovery.SILA_MDNS_TARGET;

/**
 * Service Instance
 */
@Slf4j
@Getter
public class Instance {
    private final String name;
    private final String hostAddress;
    private final int port;
    private final Map<String, String> attributes;
    private final long ttl;

    /**
     * Constructor
     *
     * @param name the name
     * @param address the address
     * @param port the port
     * @param attributes the attributes
     * @param ttl the time to live
     */
    private Instance(
            @NonNull final String name,
            @NonNull final InetAddress address,
            final int port,
            @NonNull final Map<String, String> attributes,
            final long ttl) {
        this.name = name;
        this.ttl = ttl;
        this.hostAddress = address.getHostAddress();
        this.port = port;
        this.attributes = attributes;
    }

    /**
     * Create Service Instance from a Set of Records
     * @param records Set of All Records
     * @return an Instance Object
     */
    public static Optional<Instance> createFromRecords(@NonNull final Set<Record> records) {
        final List<InetAddress> addresses = new ArrayList<>();
        final Optional<SRVRecord> srv = records.stream()
                .filter(r -> r instanceof SRVRecord && r.getName().toString().contains(SILA_MDNS_TARGET))
                .map(r -> (SRVRecord) r).findFirst();

        if (!srv.isPresent()) {
            log.debug("Cannot create Instance when no SRV record is available");
            return Optional.empty();
        }

        final Optional<TXTRecord> txt = records.stream()
                .filter(r -> r instanceof TXTRecord && r.getName().equals(srv.get().getName()))
                .map(r -> (TXTRecord) r).findFirst();
        final int port = srv.get().getPort();
        final long ttl = srv.get().getTTL();
        final String name = srv.get().getName().toString().replace( "." + SILA_MDNS_TARGET, "");
        final Map<String, String> attributes = txt.map(txtRecord -> parseDataStrings(txtRecord.getStrings())).orElse(Collections.emptyMap());

        final Optional<ARecord> aRecord = records.stream()
                .filter(r -> r instanceof ARecord && r.getName().equals(srv.get().getTarget()))
                .map(r -> (ARecord) r).findFirst();
        final Optional<AAAARecord> aaaRecord = records.stream()
                .filter(r -> r instanceof AAAARecord && r.getName().equals(srv.get().getTarget()))
                .map(r -> (AAAARecord) r).findFirst();

        if (aRecord.isPresent()) {
            addresses.addAll(records.stream().filter(r -> r instanceof ARecord)
                    .filter(r -> r.getName().equals(srv.get().getTarget())).map(r -> ((ARecord) r).getAddress())
                    .collect(Collectors.toList()));
        } else if (aaaRecord.isPresent()) {
            addresses.addAll(records.stream().filter(r -> r instanceof AAAARecord)
                    .filter(r -> r.getName().equals(srv.get().getTarget())).map(r -> ((AAAARecord) r).getAddress())
                    .collect(Collectors.toList()));
        } else {
            return Optional.empty();
        }
        return Optional.of(new Instance(name, addresses.get(0), port, attributes, ttl));
    }

    /**
     * Convert string entries into a map
     * @param strings the string entries
     * @return the entries as a map
     */
    private static Map<String, String> parseDataStrings(@NonNull final List<String> strings) {
        final Map<String, String> pairs = new HashMap<>();
        strings.forEach(s -> {
            final String[] parts = s.split("=", 2);
            if (parts.length > 1) {
                pairs.put(parts[0], parts[1]);
            } else {
                pairs.put(parts[0], "");
            }
        });
        return pairs;
    }
}
