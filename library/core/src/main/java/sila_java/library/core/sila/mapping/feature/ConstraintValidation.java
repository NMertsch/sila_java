package sila_java.library.core.sila.mapping.feature;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import sila_java.library.core.models.BasicType;
import sila_java.library.core.models.Constraints;
import sila_java.library.core.models.DataTypeType;
import sila_java.library.core.models.ListType;

import java.util.Arrays;

/**
 * Helper Class to validate constraints
 *
 * TODO: Needs to include all constraints of the Schema, will be done for 1.0 Release
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class ConstraintValidation {
    /**
     * Validate {@link Constraints} against {@link DataTypeType}
     * @param constraints the constraints
     * @param dataTypeType the data type
     * @throws MalformedSiLAFeature if the feature is malformed
     */
    public static void validateConstraints(
            @NonNull Constraints constraints,
            @NonNull DataTypeType dataTypeType) throws MalformedSiLAFeature {
        final BasicType basicType = dataTypeType.getBasic();
        final ListType listType = dataTypeType.getList();

        if (basicType == null && listType == null) {
            throw new MalformedSiLAFeature("Only Basic Type and List Type constraints can be validated.");
        }

        if (basicType != null) {
            if (constraints.getLength() != null) {
                validateBasicTypeConstraint(
                        "Length",
                        dataTypeType,
                        BasicType.STRING
                );
            }

            if (constraints.getMinimalLength() != null) {
                validateBasicTypeConstraint(
                        "Minimal Length",
                        dataTypeType,
                        BasicType.STRING,
                        BasicType.BINARY
                );
            }

            if (constraints.getMaximalLength() != null) {
                validateBasicTypeConstraint(
                        "Maximal Length",
                        dataTypeType,
                        BasicType.STRING,
                        BasicType.BINARY
                );
            }

            if (constraints.getSet() != null) {
                validateBasicTypeConstraint(
                        "Set",
                        dataTypeType,
                        BasicType.STRING,
                        BasicType.INTEGER,
                        BasicType.REAL,
                        BasicType.DATE,
                        BasicType.TIME,
                        BasicType.TIMESTAMP
                );
            }

            if (constraints.getPattern() != null) {
                validateBasicTypeConstraint(
                        "Pattern",
                        dataTypeType,
                        BasicType.STRING
                );
            }
        }
    }

    /**
     * Validate {@link Constraints} against {@link DataTypeType} type
     * @param constraintName the constraints
     * @param dataTypeType the data type
     * @param validTypes the accepted types
     * @throws MalformedSiLAFeature if the feature is malformed
     */
    private static void validateBasicTypeConstraint(
            String constraintName,
            DataTypeType dataTypeType,
            BasicType... validTypes) throws MalformedSiLAFeature {
        if (dataTypeType.getList() != null) {
            throw new MalformedSiLAFeature(constraintName + " can not be applied to List type.");
        }

        final BasicType basicType = dataTypeType.getBasic();
        if (!Arrays.asList(validTypes).contains(basicType)) {
            throw new MalformedSiLAFeature(constraintName + " Constraint only applies to basic types: " + Arrays.toString(validTypes));
        }
    }
}
