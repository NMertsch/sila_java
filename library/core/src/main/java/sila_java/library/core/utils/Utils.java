package sila_java.library.core.utils;

import io.grpc.Server;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.xml.sax.SAXException;
import sila_java.library.sila_base.EmptyClass;

import javax.annotation.Nullable;
import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.*;
import java.net.*;
import java.util.Scanner;
import java.util.concurrent.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Container for general simple utilities
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class Utils {
    /**
     * Blocking call until 'stop' is typed on terminal
     *
     * @deprecated alternatives:
     * - {@link Utils#blockUntilShutdown()}
     * - {@link Utils#blockUntilShutdown(Runnable hook)}
     * - {@link Server#awaitTermination(long, TimeUnit)}
     */
    @Deprecated
    public static void blockUntilStop() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Type stop to stop the application");
        boolean run = true;
        while (run) {
            if (sc.hasNextLine()) {
                final String line = sc.nextLine();
                if (line.equals("stop")) {
                    run = false;
                } else {
                    System.out.println("Unknown command:" + line);
                }
            }
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }

    /**
     * Blocking call until the JVM is being to shut down
     */
    public static void blockUntilShutdown() {
        blockUntilShutdown(null);
    }

    /**
     * Blocking call until the JVM is being to shut down.
     * A hook can be provided to close/save/dispose resources gracefully.
     *
     * @param hook function that will be executed on JVM shutdown
     */
    public static void blockUntilShutdown(@Nullable final Runnable hook) {
        final CompletableFuture<Void> completableFuture = new CompletableFuture<>();
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            if (hook != null) {
                try {
                    hook.run();
                } catch (Throwable e) {
                    log.warn("Error occurred during shutdown hook", e);
                }
            }
            completableFuture.complete(null);
        }));
        try {
            completableFuture.join();
        } catch (CancellationException | CompletionException ignored) {}
    }

    /**
     * Validate Feature XML
     *
     * @param xmlSource XML File Source
     *
     * @throws IOException Thrown if not correctly validated
     */
    public static void validateFeatureXML(@NonNull final Source xmlSource) throws IOException {
        try {
            final URL schemaLocation = EmptyClass.class.getResource("/sila_base/schema/FeatureDefinition.xsd");
            final SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            final Schema schema = schemaFactory.newSchema(schemaLocation);
            schema.newValidator().validate(xmlSource);
        } catch (SAXException e) {
            throw new IOException(e.getMessage());
        }
    }

    /**
     * Clean a XML string by trimming it and replacing any character before the first field
     * @param xmlString the XML String to clean
     * @return A cleaned XML String
     */
    public static String cleanupXMLString(@NonNull final String xmlString) {
        return xmlString.trim().replaceFirst("^(.*?)<","<");
    }

    /**
     * @implNote Currently not used, but only directly validated XML used
     *
     * @param inXMLStr to check if XML or not
     * @return true of the string is XML, false otherwise
     */
    public static boolean isXMLLike(String inXMLStr) {
        Pattern pattern;
        Matcher matcher;

        // Check if it has XML Elements
        final String XML_PATTERN_STR = "<([^\\?;]+)>[\\S\\s]+</\\1>";

        // check if valid string at all
        if (inXMLStr != null
                && inXMLStr.trim().startsWith("<")
        ) {
            // Check XML tags
            pattern = Pattern.compile(XML_PATTERN_STR,
                    Pattern.CASE_INSENSITIVE | Pattern.DOTALL | Pattern.MULTILINE);

            matcher = pattern.matcher(inXMLStr);

            return matcher.find();
        }

        return false;
    }
}
