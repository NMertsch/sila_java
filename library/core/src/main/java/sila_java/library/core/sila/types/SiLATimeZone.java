package sila_java.library.core.sila.types;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import sila2.org.silastandard.SiLAFramework;

import java.time.ZoneOffset;

/**
 * SiLA Time zone utility class
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class SiLATimeZone {
    /**
     * Create a {@link SiLAFramework.Timezone} from a {@link ZoneOffset}
     * @param zoneOffset the zone offset
     * @return a {@link SiLAFramework.Timezone}
     */
    public static SiLAFramework.Timezone from(final ZoneOffset zoneOffset) {
        final int totalOffsetSeconds = zoneOffset.getTotalSeconds();
        final int offsetMinutes = (totalOffsetSeconds / 60) % 60;
        final int offsetHours = (totalOffsetSeconds / 3600);
        return SiLAFramework.Timezone.newBuilder().setHours(offsetHours).setMinutes(offsetMinutes).build();
    }
}
