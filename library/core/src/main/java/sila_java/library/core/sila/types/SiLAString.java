package sila_java.library.core.sila.types;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import sila2.org.silastandard.SiLAFramework;

import java.time.OffsetTime;

/**
 * SiLA String utility class
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class SiLAString {
    /**
     * Create a {@link SiLAFramework.String} from a {@link String}
     * @param str the String
     * @return a {@link SiLAFramework.String}
     */
    public static SiLAFramework.String from(final String str) {
        return SiLAFramework.String.newBuilder().setValue(str).build();
    }
}
