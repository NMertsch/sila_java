package sila_java.library.core.utils;

import lombok.Getter;

import java.io.IOException;
import java.util.Properties;

/**
 * Git repository state model
 */
@Getter
public class GitRepositoryState {
    private final String commitId;
    private final String buildVersion;

    /**
     * Constructor
     * @throws IOException if failed to load state from class loader
     */
    public GitRepositoryState() throws IOException {
        final Properties properties = new Properties();
        properties.load(getClass().getClassLoader().getResourceAsStream("git.properties"));
        this.commitId = String.valueOf(properties.get("git.commit.id"));
        this.buildVersion = String.valueOf(properties.get("git.build.version"));
    }

    /**
     * Generate version from build version with commit id
     * @return version
     */
    public String generateVersion() {
        return this.buildVersion + "-" + this.commitId;
    }
}
