package sila_java.library.core.sila.types;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import sila2.org.silastandard.SiLAFramework;

import java.time.Duration;
import java.time.OffsetDateTime;

/**
 * SiLA Date utility class
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class SiLADate {
    /**
     * Create a {@link SiLAFramework.Date} from a {@link OffsetDateTime}
     * @param offsetDateTime the offset date time
     * @return a {@link SiLAFramework.Date}
     */
    public static SiLAFramework.Date from(final OffsetDateTime offsetDateTime) {
        return SiLAFramework.Date
                .newBuilder()
                .setTimezone(SiLATimeZone.from(offsetDateTime.getOffset()))
                .setYear(offsetDateTime.getYear())
                .setMonth(offsetDateTime.getMonthValue())
                .setDay(offsetDateTime.getDayOfMonth())
                .build();
    }
}
