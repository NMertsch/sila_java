package sila_java.library.core.discovery.networking.service_discovery;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.xbill.DNS.Message;
import org.xbill.DNS.Record;
import org.xbill.DNS.Section;

import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Listens to DNS Responses and Updates instance Cache
 */
@Slf4j
public class ResponseListener implements Consumer<Message> {
    private final InstanceListener instanceListener;

    /**
     * Instance listener
     */
    public interface InstanceListener {
        /**
         * Callback when an instance is added
         * @param instance the added instance
         */
        void instanceAdded(Instance instance);
    }

    /**
     * Constructor
     * @param instanceListener the instance listener
     */
    public ResponseListener(@NonNull final InstanceListener instanceListener) {
        this.instanceListener = instanceListener;
    }

    /**
     * Listen for responses and notify listeners when an instance is added
     * @inheritDoc
     */
    @Override
    public void accept(@NonNull final Message response) {
        log.debug("Listener response: {}", response);

        // Add Instance if SVR Record exists and Service Type is matched
        final Set<Record> records = Stream
                .concat(response.getSection(Section.ANSWER).stream(), response.getSection(Section.ADDITIONAL).stream())
                .collect(Collectors.toSet());
        if (!records.isEmpty()) {
            try {
                final Optional<Instance> optionalInstance = Instance.createFromRecords(records);
                if (optionalInstance.isPresent()) {
                    if (optionalInstance.get().getTtl() > 0) {
                        instanceListener.instanceAdded(optionalInstance.get());
                    } else {
                        log.debug("Instance {} TTL is 0", optionalInstance.get().getName());
                    }
                }
            } catch (final Exception e) {
                log.debug(e.getMessage());
            }
        }
    }
}
