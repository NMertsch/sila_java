package sila_java.library.core.sila.binary_transfer;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.OffsetDateTime;
import java.util.UUID;

/**
 * Binary info model
 */
@Getter
@AllArgsConstructor
public class BinaryInfo {
    private final UUID id;
    private final OffsetDateTime expiration;
    /**
     * Length in byte
     */
    private final long length;
}
