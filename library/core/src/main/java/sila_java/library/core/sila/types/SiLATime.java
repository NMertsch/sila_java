package sila_java.library.core.sila.types;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import sila2.org.silastandard.SiLAFramework;

import java.time.OffsetDateTime;
import java.time.OffsetTime;

/**
 * SiLA Time utility class
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public abstract class SiLATime {
    /**
     * Create a {@link SiLAFramework.Time} from a {@link OffsetTime}
     * @param offsetTime the offset time
     * @return a {@link SiLAFramework.Time}
     */
    public static SiLAFramework.Time from(final OffsetTime offsetTime) {
        return SiLAFramework.Time
                .newBuilder()
                .setTimezone(SiLATimeZone.from(offsetTime.getOffset()))
                .setHour(offsetTime.getHour())
                .setMinute(offsetTime.getMinute())
                .setSecond(offsetTime.getSecond())
                .build();
    }
}
