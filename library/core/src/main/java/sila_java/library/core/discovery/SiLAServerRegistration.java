package sila_java.library.core.discovery;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.Nullable;
import javax.jmdns.JmDNS;
import javax.jmdns.ServiceInfo;
import javax.jmdns.impl.JmDNSImpl;
import java.io.IOException;
import java.net.*;
import java.security.cert.X509Certificate;
import java.util.*;
import java.util.stream.Collectors;

import static sila_java.library.core.encryption.EncryptionUtils.readCertificate;
import static sila_java.library.core.encryption.EncryptionUtils.writeCertificateToString;

/**
 * Register SiLAServer on a local network, it constantly checks if the network
 * interface is "healthy" and otherwise tries to reconnect and advertises its
 * services again.
 *
 * @implNote This class can only hold one server registration for now.
 */
@Slf4j
public class SiLAServerRegistration implements AutoCloseable {
    private static final String LOCAL_INTERFACE = "local";
    private static final Class<? extends InetAddress> FAVORITE_ADDRESS_TYPE = Inet4Address.class;
    private static final Class<? extends InetAddress> FALLBACK_ADDRESS_TYPE = Inet6Address.class;
    private final HeartbeatAgent agent;
    private final String interfaceName;

    private final InetAddress host;
    private final int port;
    private final X509Certificate pemCertificate;
    private final UUID uuid;

    /**
     * Heartbeat to check if the network interface has been disrupted.
     */
    private class HeartbeatAgent extends TimerTask implements AutoCloseable {
        private static final int SAMPLING_PERIOD = 1000; // [ms]
        private final Timer timer = new Timer("SiLAServerRegistration_Heartbeat", true);
        private boolean rebootMode = true;
        private int heartBeat = 0;
        private JmDNS jmdns = null;
        private InetAddress lastInetAddress = null;

        /**
         * Constructor and starts the HeartbeatAgent asynchronously
         */
        private HeartbeatAgent() {
            this.timer.scheduleAtFixedRate(this, 0, SAMPLING_PERIOD);
        }

        /**
         * Check for net address changes and reboot request
         */
        @Override
        public void run() {
            log.debug("heartBeat: {}", heartBeat);
            try {
                final InetAddress currentInetAddress = (SiLAServerRegistration.this.host != null) ?
                        SiLAServerRegistration.this.host :
                        SiLAServerRegistration.this.findInetAddress();

                if (!currentInetAddress.equals(this.lastInetAddress)) {
                    this.lastInetAddress = currentInetAddress;
                    rebootMode = true;
                }
            } catch (final ConnectException e) {
                // If the address can not be resolved correctly anymore, try re-registering
                log.warn("Connection to {} has been disrupted, reconnecting...",
                        (SiLAServerRegistration.this.host != null) ?
                                SiLAServerRegistration.this.host :
                                SiLAServerRegistration.this.interfaceName
                );
                rebootMode = true;
            }
            if (rebootMode) {
                rebootJmDNS();
            }
            ++heartBeat;
        }

        /**
         * Cancel timer and close mDNS
         * @inheritDoc
         */
        @Override
        public void close() {
            log.info("Closing heartbeat agent");
            timer.cancel();
            timer.purge();
            closeMDNS();
            log.info("The heartbeat agent was successfully closed");
        }

        /**
         * Reboot JmDNS by closing the existing instance and creatign a new one
         */
        private void rebootJmDNS() {
            try {
                log.info("Rebooting network");
                this.closeMDNS();
                this.createMDNS();
                log.info("JmDNS re-constructed");
                rebootMode = false;
            } catch (final IOException e) {
                log.debug(e.getMessage(), e);
                rebootMode = true;
            }
        }

        /**
         * Create mDNS instance
         * @throws IOException if the provided certificate is invalid
         */
        private synchronized void createMDNS() throws IOException {
            if (SiLAServerRegistration.this.host != null) {
                jmdns = JmDNS.create(SiLAServerRegistration.this.host);
            } else {
                final InetAddress inetAddress = findInetAddress();
                jmdns = JmDNS.create(inetAddress);
            }

            Map<String, String> textFields = new HashMap<>();
            textFields.put("description", "SiLA server discovery service");
            if (pemCertificate != null) {
                final String certificateStr = writeCertificateToString(pemCertificate);
                int index = 0;
                for (final String s : certificateStr.split("\r?\n")) {
                    textFields.put("ca" + index, s);
                    ++index;
                }
            }

            final ServiceInfo serviceInfo = ServiceInfo.create(
                    SiLADiscovery.SILA_MDNS_TYPE + SiLADiscovery.SILA_MDNS_DOMAIN,
                    SiLAServerRegistration.this.uuid.toString(),
                    SiLAServerRegistration.this.port,
                    0, 0, false,
                    textFields
            );
            jmdns.registerService(
                    serviceInfo
            );
        }

        /**
         * Close and cleanup mDNS instance
         */
        private synchronized void closeMDNS() {
            if (jmdns != null) {
                // Unregister all services
                log.info("Unregistering service...");
                jmdns.unregisterAllServices();
                try {
                    jmdns.close();

                    // Hack fix: we need to cancel timers by ourselves correctly
                    final JmDNSImpl jmdns = (JmDNSImpl) this.jmdns;
                    if (!jmdns.isClosed()) {
                        jmdns.cancelStateTimer();
                        jmdns.cancelTimer();
                    }
                } catch (IOException e) {
                    log.warn(e.getMessage(), e);
                } finally {
                    jmdns = null;
                }
                log.info("Done unregistering service!");
            }
        }
    }

    /**
     * Register SiLAServer on certain network connected through the network
     * interface and on certain port where SiLAService should be served.
     *
     * @param uuid Unique identifier of the SiLA Server
     * @param interfaceName Name of network interface
     * @param port where SiLAService is exposed/served
     */
    public static SiLAServerRegistration newInstanceFromInterface(
            @NonNull final UUID uuid,
            @NonNull final String interfaceName,
            final int port,
            @Nullable final String encodedPEMCertificate
    ) throws IOException {
        return new SiLAServerRegistration(uuid, interfaceName, null, port, encodedPEMCertificate);
    }

    /**
     * Register SiLAServer on certain network connected through the network
     * interface and on certain port where SiLAService should be served.
     *
     * @param uuid Unique identifier of the SiLA Server
     * @param host host
     * @param port where SiLAService is exposed/served
     */
    public static SiLAServerRegistration newInstanceFromHost(
            @NonNull final UUID uuid,
            @NonNull final InetAddress host,
            final int port,
            @Nullable final String encodedPEMCertificate
    ) throws IOException {
        return new SiLAServerRegistration(uuid, null, host, port, encodedPEMCertificate);
    }

    /**
     * Register SiLAServer on certain network connected through the network
     * interface and on certain port where SiLAService should be served.
     *
     * @param uuid Unique identifier of the SiLA Server
     * @param interfaceName Name of network interface
     * @param port where SiLAService is exposed/served
     * @param encodedPEMCertificate optional pem certificate to be provided in discovery
     *
     * @throws IOException if provided certificate string in invalid
     */
    private SiLAServerRegistration(
            @NonNull final UUID uuid,
            @Nullable final String interfaceName,
            @Nullable final InetAddress host,
            final int port,
            @Nullable final String encodedPEMCertificate
    ) throws IOException {
        this.uuid = uuid;
        this.interfaceName = interfaceName;
        this.host = host;
        this.port = port;
        this.agent = new HeartbeatAgent();
        this.pemCertificate = encodedPEMCertificate != null ? readCertificate(encodedPEMCertificate) : null;
    }

    /**
     * Unregister the SiLA server
     */
    @Override
    public void close() {
        this.agent.close();
    }

    /**
     * Attempt to retrieve a valid address from an interface
     * First it tries to get an address matching the preferred address type, in case this operation fails,
     * it will attempt to retrieve an address matching the fallback type
     * @return The address
     * @throws ConnectException When not able to find an address matching favorite and fallback address type
     */
    public InetAddress findInetAddress() throws ConnectException {
        return getInetAddressWithType(FAVORITE_ADDRESS_TYPE)
                .orElse(
                        getInetAddressWithType(FALLBACK_ADDRESS_TYPE)
                                .orElseThrow(() -> new ConnectException("No valid inet address available"))
                );
    }

    /**
     * Attempt to retrieve an IP Address of a specific type
     *
     * @param clazz InetAddress Type (either IPv4 or IPv6)
     * @return The address if retrievable
     */
    private Optional<InetAddress> getInetAddressWithType(@NonNull final Class<? extends InetAddress> clazz) {
        try {
            return getInetAddressForNetworkInterface(getNetworkInterface(), clazz);
        } catch (IOException e) {
            log.debug(e.getMessage(), e);
            return Optional.empty();
        }
    }

    /**
     * Get network interface associated with network interface name
     */
    private NetworkInterface getNetworkInterface() throws IOException {
        final boolean isLocal = isLocalInterface();
        final NetworkInterface networkInterface = isLocal ?
                NetworkInterface.getByInetAddress(InetAddress.getLoopbackAddress()) :
                NetworkInterface.getByName(this.interfaceName);

        if (networkInterface == null) {
            final List<String> availableInterfaces = (NetworkInterface.getNetworkInterfaces() == null) ?
                    Collections.emptyList() :
                    Collections.list(NetworkInterface.getNetworkInterfaces())
                            .stream()
                            .map(NetworkInterface::getDisplayName)
                            .collect(Collectors.toList());

            throw new IOException(this.interfaceName + " doesn't exist on this machine" + System.lineSeparator() +
                    "Use one of these instead: " + String.join(", ", availableInterfaces)
            );
        }

        if (!isLocal) {
            if (!networkInterface.isUp()) {
                throw new IOException(this.interfaceName + " is not up");
            }
            if (networkInterface.isPointToPoint()) {
                throw new IOException(this.interfaceName + " can not be PtP");
            }
        }

        return networkInterface;
    }

    /**
     * Retrieve an optional inet address with the specified address type
     * @param networkInterface The network interface to search from
     * @param clazz The class of the address type
     * @return An optional InetAddress
     */
    private static Optional<InetAddress> getInetAddressForNetworkInterface(
            @NonNull final NetworkInterface networkInterface,
            @NonNull final Class<? extends InetAddress> clazz
    ) {
        return Collections.list(networkInterface.getInetAddresses())
                .stream()
                .filter(clazz::isInstance)
                .findAny();
    }

    /**
     * Check if the interface is local
     * @return true of the interface is local, false otherwise
     */
    private boolean isLocalInterface() {
        return this.interfaceName.equals(LOCAL_INTERFACE);
    }
}
