package sila_java.examples.metadata;

import sila_java.library.core.utils.Utils;
import sila_java.library.server_base.SiLAServer;
import sila_java.library.server_base.identification.ServerInformation;

import java.io.IOException;
import java.util.Collections;

import static sila_java.library.core.utils.FileUtils.getResourceContent;

/**
 * Basic server application implementing the features Greeting Provider and Authorization Service
 *
 * This is not intended to be a best practice example for server development. It just provides the necessary server
 * scaffold to showcase metadata handling.
 */
public class Server implements AutoCloseable {
    private final SiLAServer silaServer;

    public Server(int port) throws IOException {
        ServerInformation serverInformation =
                new ServerInformation(
                        "MetadataExampleServer",
                        "Server to demonstrate SiLA Client Metadata using the Authorization Service feature",
                        "https://gitlab.com/SiLA2/sila_java",
                        "0.1"
                );

        final AuthorizationServiceImpl authorizationService =
                new AuthorizationServiceImpl(
                        "secret-access-token",
                        Collections.singletonList("org.silastandard/examples/GreetingProvider/v1")
                );

        this.silaServer = SiLAServer.Builder
                .newBuilder(serverInformation)
                .addFeature(
                        getResourceContent("AuthorizationService-v1_0.sila.xml"), authorizationService
                )
                .addInterceptor(authorizationService.accessTokenInterceptor)
                .addFeature(getResourceContent("GreetingProvider-v1_0.sila.xml"), new GreetingProviderImpl())
                .withUnsafeCommunication(true)
                .withPort(port)
                .start();
    }

    @Override
    public void close() {
        this.silaServer.close();
    }

    public static void main(String[] args) throws IOException {
        final Server server = new Server(50052);
        server.silaServer.blockUntilShutdown();
    }
}
