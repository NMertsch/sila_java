package sila_java.examples.metadata;

import io.grpc.Channel;
import io.grpc.StatusRuntimeException;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.core.authorizationservice.v1.AuthorizationServiceOuterClass;
import sila2.org.silastandard.examples.greetingprovider.v1.GreetingProviderGrpc;
import sila2.org.silastandard.examples.greetingprovider.v1.GreetingProviderOuterClass;
import sila_java.library.core.sila.clients.ChannelFactory;
import sila_java.library.core.sila.errors.SiLAErrors;
import sila_java.library.core.sila.types.SiLAString;
import sila_java.library.core.sila.utils.MetadataUtils;

/**
 * SiLA Client example for using metadata
 */
@Slf4j
public class Client {
    private final GreetingProviderGrpc.GreetingProviderBlockingStub greetingProviderStub;

    public Client(final int port) {
        Channel channel = ChannelFactory.getUnencryptedChannel("127.0.0.1", port);
        this.greetingProviderStub = GreetingProviderGrpc.newBlockingStub(channel);
    }

    public String sayHello(@NonNull final String name) {
        return sayHello(this.greetingProviderStub, name);
    }

    public String sayHelloWithAccessToken(@NonNull final String name, @NonNull final String accessToken) {
        final AuthorizationServiceOuterClass.Metadata_AccessToken accessTokenMessage =
                AuthorizationServiceOuterClass.Metadata_AccessToken
                        .newBuilder()
                        .setAccessToken(SiLAString.from(accessToken))
                        .build();
        final GreetingProviderGrpc.GreetingProviderBlockingStub stubWithToken =
                this.greetingProviderStub.withInterceptors(
                        MetadataUtils.newMetadataInjector(accessTokenMessage)
                );
        return sayHello(stubWithToken, name);
    }

    private String sayHello(
            @NonNull final GreetingProviderGrpc.GreetingProviderBlockingStub stub,
            @NonNull final String name
    ) {
        final GreetingProviderOuterClass.SayHello_Parameters request =
                GreetingProviderOuterClass.SayHello_Parameters.newBuilder()
                        .setName(SiLAString.from(name))
                        .build();
        final GreetingProviderOuterClass.SayHello_Responses response = stub.sayHello(request);

        return response.getGreeting().getValue();
    }

    public static void main(final String[] args) {
        final Client client = new Client(50052);

        // try without access token (should throw Invalid Metadata)
        try {
            final String response = client.sayHello("World");
            log.info("Got response: " + response);
        } catch (StatusRuntimeException e) {
            log.info("Caught error: " + SiLAErrors.retrieveSiLAError(e));
        }

        // try with wrong access token (should throw Invalid Access Token)
        try {
            final String response = client.sayHelloWithAccessToken("World", "invalid-token");
            log.info("Got response: " + response);
        } catch (StatusRuntimeException e) {
            log.info("Caught error: " + SiLAErrors.retrieveSiLAError(e));
        }

        // try with correct access token (should work)
        try {
            final String response = client.sayHelloWithAccessToken("World", "secret-access-token");
            log.info("Got response: " + response);
        } catch (StatusRuntimeException e) {
            log.info("Caught error: " + SiLAErrors.retrieveSiLAError(e));
        }
    }
}
