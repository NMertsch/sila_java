package sila_java.examples.binary_transfer;

import io.grpc.Channel;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.test.binarytransfertest.v1.BinaryTransferTestGrpc;
import sila2.org.silastandard.test.binarytransfertest.v1.BinaryTransferTestOuterClass;
import sila_java.library.core.sila.binary_transfer.BinaryDownloadHelper;
import sila_java.library.core.sila.binary_transfer.BinaryInfo;
import sila_java.library.core.sila.binary_transfer.BinaryUploadHelper;
import sila_java.library.core.sila.types.SiLABinary;

import java.util.UUID;

/**
 * Client demonstrating usage of SiLA Binary Transfer
 */
public class Client {
    private final BinaryTransferTestGrpc.BinaryTransferTestBlockingStub binaryTransferTestStub;
    private final BinaryUploadHelper uploadHelper;
    private final BinaryDownloadHelper downloadHelper;

    public Client(Channel channel) {
        this.binaryTransferTestStub = BinaryTransferTestGrpc.newBlockingStub(channel);
        this.uploadHelper = new BinaryUploadHelper(channel);
        this.downloadHelper = new BinaryDownloadHelper(channel);
    }

    /**
     * Read a small binary property value
     */
    public byte[] getBinaryValueDirectly() {
        final BinaryTransferTestOuterClass.Get_BinaryValueDirectly_Parameters request = BinaryTransferTestOuterClass.Get_BinaryValueDirectly_Parameters.newBuilder().build();
        final BinaryTransferTestOuterClass.Get_BinaryValueDirectly_Responses
                response = this.binaryTransferTestStub.getBinaryValueDirectly(request);
        return response.getBinaryValueDirectly().getValue().toByteArray();
    }

    /**
     * Read a large binary property value
     */
    public byte[] getBinaryValueDownload() {
        final BinaryTransferTestOuterClass.Get_BinaryValueDownload_Parameters request = BinaryTransferTestOuterClass.Get_BinaryValueDownload_Parameters.newBuilder().build();
        final BinaryTransferTestOuterClass.Get_BinaryValueDownload_Responses
                response = this.binaryTransferTestStub.getBinaryValueDownload(request);
        final UUID binaryDownloadUuid = UUID.fromString(response.getBinaryValueDownload().getBinaryTransferUUID());
        return this.downloadHelper.download(binaryDownloadUuid);
    }

    /**
     * Request the server to echo a given binary.
     */
    public byte[] echoBinaryValue(byte[] bytes) {
        final SiLAFramework.Binary binaryRequest;

        if (bytes.length < 2 * 1024 * 1024) {
            // direct request
            binaryRequest = SiLABinary.fromBytes(bytes);
        } else {
            // binary upload
            final BinaryInfo uploadInfo = this.uploadHelper.upload(
                    bytes,
                    "org.silastandard/test/BinaryTransferTest/v1/Command/EchoBinaryValue/Parameter/BinaryValue"
            );
            binaryRequest = SiLABinary.fromBinaryTransferUUID(uploadInfo.getId());
        }

        // execute command
        final BinaryTransferTestOuterClass.EchoBinaryValue_Parameters request = BinaryTransferTestOuterClass.EchoBinaryValue_Parameters.newBuilder()
                .setBinaryValue(binaryRequest).build();
        final BinaryTransferTestOuterClass.EchoBinaryValue_Responses
                response = this.binaryTransferTestStub.echoBinaryValue(request);

        // direct response
        if (response.getReceivedValue().hasValue()) {
            return response.getReceivedValue().getValue().toByteArray();
        }

        // binary download
        return this.downloadHelper.download(UUID.fromString(response.getReceivedValue().getBinaryTransferUUID()));
    }
}
