package sila_java.examples.binary_transfer;

import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.test.binarytransfertest.v1.BinaryTransferTestGrpc;
import sila2.org.silastandard.test.binarytransfertest.v1.BinaryTransferTestOuterClass;
import sila_java.library.core.sila.errors.SiLAErrors;
import sila_java.library.core.sila.types.SiLABinary;
import sila_java.library.server_base.binary_transfer.Binary;
import sila_java.library.server_base.binary_transfer.database.BinaryDatabase;
import sila_java.library.server_base.binary_transfer.database.BinaryDatabaseException;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.Collections;
import java.util.UUID;

/**
 * Provides commands and properties to set or respectively get the SiLA Basic Data Type Binary via command parameters or
 * property responses respectively.
 */
@Slf4j
public class BinaryTransferTestImpl extends BinaryTransferTestGrpc.BinaryTransferTestImplBase {
    /**
     * Receives a Binary value (transmitted either directly or via binary transfer) and returns the received value.
     */
    @Override
    public void echoBinaryValue(
            final BinaryTransferTestOuterClass.EchoBinaryValue_Parameters request,
            final StreamObserver<BinaryTransferTestOuterClass.EchoBinaryValue_Responses> responseObserver) {
        if (!request.hasBinaryValue()) {
            responseObserver.onError(SiLAErrors.generateValidationError(
                    "org.silastandard/test/BinaryTransferTest/v1/Command/EchoBinaryValue/Parameter/BinaryValue",
                    "Missing parameter 'Binary Value'"
            ));
            return;
        }

        SiLAFramework.Binary requestBinary = request.getBinaryValue();
        SiLAFramework.Binary responseBinary;
        if (requestBinary.hasValue()) {
            responseBinary = SiLABinary.fromBytes(requestBinary.getValue().toByteArray());
        } else {
            try {
                final BinaryDatabase database = BinaryDatabase.current();
                Binary uploadedBinary = database.getBinary(UUID.fromString(requestBinary.getBinaryTransferUUID()));

                UUID binaryDownloadUuid = UUID.randomUUID();
                database.addBinary(binaryDownloadUuid, uploadedBinary.getData().getBinaryStream());
                responseBinary = SiLABinary.fromBinaryTransferUUID(binaryDownloadUuid);
            } catch (BinaryDatabaseException | SQLException e) {
                log.error("Error during binary transfer", e);
                responseObserver.onError(SiLAErrors.generateUndefinedExecutionError("Error during binary transfer"));
                return;
            }
        }
        responseObserver.onNext(BinaryTransferTestOuterClass.EchoBinaryValue_Responses.newBuilder()
                                        .setReceivedValue(responseBinary).build());
        responseObserver.onCompleted();
    }

    /**
     * Returns the UTF-8 encoded string 'SiLA2_Test_String_Value' directly transmitted as Binary value.
     */
    @Override
    public void getBinaryValueDirectly(
            final BinaryTransferTestOuterClass.Get_BinaryValueDirectly_Parameters request,
            final StreamObserver<BinaryTransferTestOuterClass.Get_BinaryValueDirectly_Responses> responseObserver) {
        SiLAFramework.Binary responseBinary
                = SiLABinary.fromBytes("SiLA2_Test_String_Value".getBytes(StandardCharsets.UTF_8));
        responseObserver.onNext(BinaryTransferTestOuterClass.Get_BinaryValueDirectly_Responses.newBuilder()
                                        .setBinaryValueDirectly(responseBinary).build());
        responseObserver.onCompleted();
    }

    /**
     * Returns the Binary Transfer UUID to be used to download the binary data which is the UTF-8 encoded string
     * 'A_slightly_longer_SiLA2_Test_String_Value_used_to_demonstrate_the_binary_download', repeated 100,000 times.
     */
    @Override
    public void getBinaryValueDownload(
            final BinaryTransferTestOuterClass.Get_BinaryValueDownload_Parameters request,
            final StreamObserver<BinaryTransferTestOuterClass.Get_BinaryValueDownload_Responses> responseObserver) {
        UUID binaryDownloadUuid = UUID.randomUUID();
        final String longString = String.join(
                "",
                Collections.nCopies(
                        100000,
                        "A_slightly_longer_SiLA2_Test_String_Value_used_to_demonstrate_the_binary_download"
                )
        );

        try {
            BinaryDatabase.current().addBinary(binaryDownloadUuid, new ByteArrayInputStream(longString.getBytes(
                    StandardCharsets.UTF_8)));
        } catch (BinaryDatabaseException e) {
            log.error("Error during writing binary to database", e);
            responseObserver.onError(SiLAErrors.generateUndefinedExecutionError(
                    "Error during writing binary to database"));
            return;
        }

        responseObserver.onNext(BinaryTransferTestOuterClass.Get_BinaryValueDownload_Responses.newBuilder()
                                        .setBinaryValueDownload(SiLABinary.fromBinaryTransferUUID(binaryDownloadUuid))
                                        .build());
        responseObserver.onCompleted();
    }
}
