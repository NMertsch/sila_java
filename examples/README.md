## Examples
Example implementations of SiLA Servers made either for demonstration purposes or testing.

| Name                                     | Description                                                 |
| -----------------------------------------| ----------------------------------------------------------- |
| [hello_sila](hello_sila)        | Simple Hello World like server                              |
| [test_server](test_server)      | Server to show-case and test complex features               |
| [thermostat](thermostat)        | Control a fake thermostat                                   |

## Arguments
The SiLA Java servers supports the following arguments by default.
There might also be server specific arguments.

| Short flag | Long flag         | Default value | Description
| ---------- | ----------------- | --------------| ---------------------------------------------------------------------- |
| -p         | --port            | 50052         | Specify the port to use
| -n         | --networkInterface| None          | Specify the network interface to use for SiLA Discovery
| -c         | --config          | None          | Specify the file name to use to read/store server information
| -l         | --listNetworks    | N/A           | List names of available network interfaces
| -e         | --encryption      | yes           | Specify if the server must encrypt communication or not
| -s         | --simulation      | no            | Specify (if supported) to start the server with simulation mode enabled

## Building and Running
SiLA Java Building instructions are [here](../README.md).

To install the package, simply use maven:
```bash
mvn install
```
    
Once the server is compiled, the server jar can be found in the target directory usually name as follow `<SERVER_NAME>-0.5.0.jar` or `<SERVER_NAME>-exec.jar`
The server can then be executed using Java 11 with the following command:
```bash
java -jar <SERVER_NAME>-exec.jar
```
    
Display help & usage:
```bash
java -jar <SERVER_NAME>-exec.jar -h
```
You can run the server and client on any host on your local network with discovery enabled:
```bash
java -jar <SERVER_NAME>-exec.jar -n local
```
The jar is usually packaged using the spring boot packager but this can be done differently in your own code.

### Running Multiple SiLA Servers

If you want to run multiple instance on the same machine, the port will be assigned automatically. 
You will have to define a different configuration file though by providing the path with the `-c` flag,
as you want to have different UUID and SiLA Server Names for different instances.

### SiLA Server persistence (UUID and Name)
To persist both `name` and `uuid` pertaining to SiLAServer Name and UUID, we use an optional configuration 
argument `-c [FILE.json]`.This config file will allow for persistence through reboots of the SiLA 2 Server, 
if the flag correctly points to the same `config.json` file. This will be a JSON file, ending with file extension `.json`.

To use this option you specify argument `-c [./PATH/config.json]`. Remember a file will be created in a relative path 
to your current execution path. In our example a `config.json` file will be created in `./config.json`, rather 
then `./target/config.json` as the execution path is not in the directory `target/`.
 
Example:

```bash
java -jar <SERVER_NAME>-exec.jar -n config.json
```

### Encryption
SiLA Servers are using encrypted connections by default. You can use plain text channels if necessary, although this
option is not recommended.

Running without encryption:

```bash
java -jar <SERVER_NAME>-exec.jar -e no
```

### List Network interfaces and use them

To display all network interfaces use `-l yes` as an argument:

```bash
java -jar <SERVER_NAME>-exec.jar -l yes
```

To use a network interface use `-n [NETWORK_INTERFACE]` as an argument:

```bash
java -jar <SERVER_NAME>-exec.jar -n wlan0
```

## Logs
Each SiLA server should have a persistent log. It can be found at the following path: 
```
<USER_HOME_DIRECTORY>/logs/.sila/<NAME_OF_THE_SERVER>.log
```