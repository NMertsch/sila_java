package sila_java.examples.test_server;

import io.grpc.ConnectivityState;
import io.grpc.ManagedChannel;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;
import sila2.ch.unitelabs.test.unobservablecommandtest.v1.UnobservableCommandTestGrpc;
import sila2.org.silastandard.BinaryDownloadGrpc;
import sila2.org.silastandard.BinaryUploadGrpc;
import sila_java.library.core.asynchronous.MethodPoller;
import sila_java.library.core.sila.clients.ChannelFactory;
import sila_java.library.manager.ServerFinder;
import sila_java.library.manager.models.Server;
import sila_java.library.server_base.utils.ArgumentHelper;

import java.io.IOException;
import java.time.Duration;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@Slf4j
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestBase {
    private static final int SHUTDOWN_TIMEOUT = 5; // Timeout to wait for Test Server shut down in seconds
    private TestServer testServer;

    private ManagedChannel channel;

    BinaryUploadGrpc.BinaryUploadBlockingStub uploadBlockingStub;
    BinaryUploadGrpc.BinaryUploadStub uploadStreamStub;
    BinaryDownloadGrpc.BinaryDownloadStub downloadStreamStub;
    UnobservableCommandTestGrpc.UnobservableCommandTestBlockingStub unobservableCommandStub;

    @BeforeAll
    void setupTestServer() throws TimeoutException, ExecutionException, IOException {
        log.info("Setting up Test server...");

        final String[] args = {"-n", "local"};

        testServer = new TestServer(new ArgumentHelper(args, TestServer.SERVER_TYPE));
        testServer.startClientInitiatedServer();

        final Server server = ServerFinder
                .filterBy(ServerFinder.Filter.type(TestServer.SERVER_TYPE))
                .scanAndFindOne(Duration.ofMinutes(1))
                .orElseThrow(RuntimeException::new);
        this.channel = ChannelFactory.getTLSEncryptedChannel(
                server.getHost(), server.getPort(), server.getCertificateAuthority());

        uploadBlockingStub = BinaryUploadGrpc.newBlockingStub(channel);
        uploadStreamStub = BinaryUploadGrpc.newStub(channel);
        downloadStreamStub = BinaryDownloadGrpc.newStub(channel);
        unobservableCommandStub = UnobservableCommandTestGrpc.newBlockingStub(channel);

        // Make sure the server is up
        waitUntilReady(channel, Duration.ofMinutes(1));
    }

    @AfterAll
    void cleanup() throws InterruptedException {
        channel.shutdown().awaitTermination(SHUTDOWN_TIMEOUT, TimeUnit.SECONDS);
        testServer.close();
    }

    /**
     * Waits until gRPC channel is in a ready state, waits until the connection is up
     * @param managedChannel Channel to check
     * @param timeout Maximum time to wait for, if not met throws exception
     */
    private static void waitUntilReady(
            @NonNull ManagedChannel managedChannel,
            @NonNull Duration timeout) throws TimeoutException, ExecutionException {
        MethodPoller.await()
                    .withInterval(Duration.ofSeconds(2))
                    .atMost(timeout)
                    .until(()-> managedChannel.getState(true).equals(ConnectivityState.READY));
    }
}
