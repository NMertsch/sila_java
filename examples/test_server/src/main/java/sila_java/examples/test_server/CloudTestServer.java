package sila_java.examples.test_server;

import com.google.common.collect.ImmutableMap;
import io.grpc.ManagedChannel;
import lombok.extern.slf4j.Slf4j;
import sila2.ch.unitelabs.test.observablecommandtest.v1.ObservableCommandTestOuterClass;
import sila2.ch.unitelabs.test.observablepropertytest.v1.ObservablePropertyTestOuterClass;
import sila2.ch.unitelabs.test.parameterconstraintsprovidertest.v1.ParameterConstraintsProviderTestOuterClass;
import sila2.ch.unitelabs.test.unobservablecommandtest.v1.UnobservableCommandTestOuterClass;
import sila2.ch.unitelabs.test.unobservablepropertytest.v1.UnobservablePropertyTestOuterClass;
import sila2.org.silastandard.CloudClientEndpointGrpc;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.core.commands.parameterconstraintsprovider.v1.ParameterConstraintsProviderGrpc;
import sila2.org.silastandard.core.commands.parameterconstraintsprovider.v1.ParameterConstraintsProviderOuterClass;
import sila_java.examples.test_server.impl.*;
import sila_java.library.cloudier.server.CloudierConnectionConfigurationService;
import sila_java.library.cloudier.server.CloudierServerEndpoint;
import sila_java.library.cloudier.server.CloudierSiLAService;
import sila_java.library.cloudier.server.MessageCaseHandler;
import sila_java.library.core.sila.clients.ChannelFactory;
import sila_java.library.server_base.binary_transfer.database.BinaryDatabase;
import sila_java.library.server_base.binary_transfer.database.impl.H2BinaryDatabase;
import sila_java.library.server_base.binary_transfer.download.DownloadService;
import sila_java.library.server_base.binary_transfer.upload.UploadService;
import sila_java.library.sila_base.EmptyClass;

import java.io.IOException;
import java.util.HashMap;
import java.util.Objects;
import java.util.UUID;

import static sila_java.library.core.utils.FileUtils.getFileContent;
import static sila_java.library.core.utils.Utils.blockUntilShutdown;

@Slf4j
public class CloudTestServer implements AutoCloseable {
    private final CloudierSiLAService cloudierSiLAService;
    private final CloudierConnectionConfigurationService cloudierConnectionConfigurationService;
    private final ObservableCommand observableCommand = new ObservableCommand();
    private final UnobservableProperty unobservableProperty = new UnobservableProperty();
    private final ObservableProperty observableProperty = new ObservableProperty();
    private final UnobservableCommand unobservableCommand;
    private final sila_java.library.server_base.standard_features.ParameterConstraintsProvider ParameterConstraintsProvider =
            new sila_java.library.server_base.standard_features.ParameterConstraintsProvider();
    private final ParameterConstraintsProvider parameterConstraintsProvider = new ParameterConstraintsProvider(ParameterConstraintsProvider);
    private final ParameterConstraints parameterConstraints = new ParameterConstraints();
    private final BinaryDatabase binaryDatabase;
    private CloudClientEndpointGrpc.CloudClientEndpointStub clientEndpoint;
    private CloudierServerEndpoint cloudServerEndpointService;
    private ManagedChannel channel;


    public static void main(String[] args) throws Exception {
        final UUID serverUUID = UUID.randomUUID();
        final BinaryDatabase binaryDatabase = new H2BinaryDatabase(serverUUID);
        final CloudTestServer cloudTestServer = new CloudTestServer(serverUUID, binaryDatabase);
        log.info("To stop the server press CTRL + C.");
        blockUntilShutdown(() -> {
            try {
                binaryDatabase.close();
            } catch (Exception ignored) {}
            cloudTestServer.close();
        });
    }

    public CloudTestServer(UUID serverUUID, BinaryDatabase binaryDatabase) throws IOException {
        this.binaryDatabase = binaryDatabase;
        this.unobservableCommand = new UnobservableCommand(binaryDatabase);
        ImmutableMap<String, String> immutableMap = ImmutableMap.<String, String>builder().put(
                "SiLAService", getFileContent(
                        Objects.requireNonNull(
                                EmptyClass.class.getResourceAsStream("/sila_base/feature_definitions/org/silastandard/core/SiLAService.sila.xml")
                        )
                )
        ).put(
                "ConnectionConfigurationService", getFileContent(
                        Objects.requireNonNull(
                                EmptyClass.class.getResourceAsStream("/sila_base/feature_definitions/org/silastandard/core/ConnectionConfigurationService.sila.xml")
                        )
                )
        ).put(
                "ParameterConstraintsProvider", getFileContent(
                        Objects.requireNonNull(
                                EmptyClass.class.getResourceAsStream("/sila_base/feature_definitions/org/silastandard/core/commands/ParameterConstraintsProvider.sila.xml")
                        )
                )
        ).put(
                "ObservablePropertyTest", getFileContent(
                        Objects.requireNonNull(
                                TestServer.class.getResourceAsStream("/features/ObservablePropertyTest.sila.xml")
                        )
                )
        ).put(
                "UnobservableCommandTest", getFileContent(
                        Objects.requireNonNull(
                                TestServer.class.getResourceAsStream("/features/UnobservableCommandTest.sila.xml")
                        )
                )
        ).put(
                "ObservableCommandTest", getFileContent(
                        Objects.requireNonNull(
                                TestServer.class.getResourceAsStream("/features/ObservableCommandTest.sila.xml")
                        )
                )
        ).put(
                "ParameterConstraintsProviderTest", getFileContent(
                        Objects.requireNonNull(
                                TestServer.class.getResourceAsStream("/features/ParameterConstraintsProviderTest.sila.xml")
                        )
                )
        )/*.put(
                "ParameterConstraintsTest", getFileContent(
                        Objects.requireNonNull(
                                TestServer.class.getResourceAsStream("/features/ParameterConstraintsTest.sila.xml")
                        )
                )
        )*/.put(
                "UnobservablePropertyTest", getFileContent(
                        Objects.requireNonNull(
                                TestServer.class.getResourceAsStream("/features/UnobservablePropertyTest.sila.xml")
                        )
                )
        ).build();
        this.cloudierSiLAService = new CloudierSiLAService(
                "Cloud Test Server",
                TestServer.serverInfo.getType(),
                serverUUID.toString(),
                TestServer.serverInfo.getVersion(),
                TestServer.serverInfo.getDescription(),
                TestServer.serverInfo.getVendorURL(),
                immutableMap
        );
        this.cloudierConnectionConfigurationService = new CloudierConnectionConfigurationService(
                false, (connectionMode) -> {}
        );
        this.startServerInitiatedConnection();
    }

    private void startServerInitiatedConnection() {
        this.channel = ChannelFactory.getTLSEncryptedChannel("localhost", 50051);
        this.clientEndpoint = CloudClientEndpointGrpc.newStub(this.channel);
        HashMap<String, MessageCaseHandler> callForwarderMap = new HashMap<String, MessageCaseHandler>() {{
            put("ch.unitelabs/test/ObservableCommandTest/v1/Command/RestartDevice", new MessageCaseHandler()
                    .withObservableCommand(ObservableCommandTestOuterClass.RestartDevice_Parameters.parser(), observableCommand::restartDevice)
                    .withExecInfo(SiLAFramework.CommandExecutionUUID.parser(), observableCommand::restartDeviceInfo)
                    .withIntermediate(SiLAFramework.CommandExecutionUUID.parser(), observableCommand::restartDeviceIntermediate)
                    .withResult(SiLAFramework.CommandExecutionUUID.parser(), observableCommand::restartDeviceResult)
            );
            put("ch.unitelabs/test/ObservablePropertyTest/v1/Property/ListString", new MessageCaseHandler()
                    .withObservableProperty(ObservablePropertyTestOuterClass.Subscribe_ListString_Parameters.parser(), observableProperty::subscribeListString)
            );
            put("ch.unitelabs/test/ObservablePropertyTest/v1/Property/SingletonListString", new MessageCaseHandler()
                    .withObservableProperty(ObservablePropertyTestOuterClass.Subscribe_SingletonListString_Parameters.parser(), observableProperty::subscribeSingletonListString)
            );
            put("ch.unitelabs/test/ParameterConstraintsProviderTest/v1/Command/GoToPos", new MessageCaseHandler()
                    .withUnobservableCommand(ParameterConstraintsProviderTestOuterClass.GoToPos_Parameters.parser(), parameterConstraintsProvider::goToPos)
            );
            put("ch.unitelabs/test/ParameterConstraintsProviderTest/v1/Command/AddPosition", new MessageCaseHandler()
                    .withUnobservableCommand(ParameterConstraintsProviderTestOuterClass.AddPosition_Parameters.parser(), parameterConstraintsProvider::addPosition)
            );
            put("ch.unitelabs/test/ParameterConstraintsProviderTest/v1/Command/RemovePosition", new MessageCaseHandler()
                    .withUnobservableCommand(ParameterConstraintsProviderTestOuterClass.RemovePosition_Parameters.parser(), parameterConstraintsProvider::removePosition)
            );
            put("ch.unitelabs/test/ParameterConstraintsProviderTest/v1/Property/Positions", new MessageCaseHandler()
                    .withUnobservableProperty(ParameterConstraintsProviderTestOuterClass.Get_Positions_Parameters.parser(), parameterConstraintsProvider::getPositions)
            );
            put("ch.unitelabs/test/UnobservableCommandTest/v1/Command/MakeCoffee", new MessageCaseHandler()
                    .withUnobservableCommand(UnobservableCommandTestOuterClass.MakeCoffee_Parameters.parser(), unobservableCommand::makeCoffee)
            );
            put("ch.unitelabs/test/UnobservableCommandTest/v1/Command/Sleep", new MessageCaseHandler()
                    .withUnobservableCommand(UnobservableCommandTestOuterClass.Sleep_Parameters.parser(), unobservableCommand::sleep)
            );
            put("ch.unitelabs/test/UnobservableCommandTest/v1/Command/ListProvider", new MessageCaseHandler()
                    .withUnobservableCommand(UnobservableCommandTestOuterClass.ListProvider_Parameters.parser(), unobservableCommand::listProvider)
            );
            put("ch.unitelabs/test/UnobservableCommandTest/v1/Command/ThreeDimensionalStruct", new MessageCaseHandler()
                    .withUnobservableCommand(UnobservableCommandTestOuterClass.ThreeDimensionalStruct_Parameters.parser(), unobservableCommand::threeDimensionalStruct)
            );
            put("ch.unitelabs/test/UnobservableCommandTest/v1/Command/ThreeDimensionalList", new MessageCaseHandler()
                    .withUnobservableCommand(UnobservableCommandTestOuterClass.ThreeDimensionalList_Parameters.parser(), unobservableCommand::threeDimensionalList)
            );
            put("ch.unitelabs/test/UnobservableCommandTest/v1/Command/WhyMakeItSimpleWhenYouCanMakeItComplicated", new MessageCaseHandler()
                    .withUnobservableCommand(UnobservableCommandTestOuterClass.WhyMakeItSimpleWhenYouCanMakeItComplicated_Parameters.parser(), unobservableCommand::whyMakeItSimpleWhenYouCanMakeItComplicated)
            );
            put("ch.unitelabs/test/UnobservableCommandTest/v1/Command/XOREncipher", new MessageCaseHandler()
                    .withUnobservableCommand(UnobservableCommandTestOuterClass.XOREncipher_Parameters.parser(), unobservableCommand::xOREncipher)
            );
            put("ch.unitelabs/test/UnobservableCommandTest/v1/Command/ValueForTypeProvider", new MessageCaseHandler()
                    .withUnobservableCommand(UnobservableCommandTestOuterClass.ValueForTypeProvider_Parameters.parser(), unobservableCommand::valueForTypeProvider)
            );
            put("ch.unitelabs/test/UnobservableCommandTest/v1/Metadata/User", new MessageCaseHandler()
                    .withMetadata(UnobservableCommandTestOuterClass.Get_FCPAffectedByMetadata_User_Parameters.parser(), unobservableCommand::getFCPAffectedByMetadataUser)
            );
            put("ch.unitelabs/test/UnobservablePropertyTest/v1/Property/HugeList", new MessageCaseHandler()
                    .withUnobservableProperty(UnobservablePropertyTestOuterClass.Get_HugeList_Parameters.parser(), unobservableProperty::getHugeList)
            );
            put("ch.unitelabs/test/UnobservablePropertyTest/v1/Property/Boolean", new MessageCaseHandler()
                    .withUnobservableProperty(UnobservablePropertyTestOuterClass.Get_Boolean_Parameters.parser(), unobservableProperty::getBoolean)
            );
            put("ch.unitelabs/test/UnobservablePropertyTest/v1/Property/ListString", new MessageCaseHandler()
                    .withUnobservableProperty(UnobservablePropertyTestOuterClass.Get_ListString_Parameters.parser(), unobservableProperty::getListString)
            );
            put("ch.unitelabs/test/UnobservablePropertyTest/v1/Property/RandomChangingListString", new MessageCaseHandler()
                    .withUnobservableProperty(UnobservablePropertyTestOuterClass.Get_RandomChangingListString_Parameters.parser(), unobservableProperty::getRandomChangingListString)
            );
            put("ch.unitelabs/test/UnobservablePropertyTest/v1/Property/ListStructPairString", new MessageCaseHandler()
                    .withUnobservableProperty(UnobservablePropertyTestOuterClass.Get_ListStructPairString_Parameters.parser(), unobservableProperty::getListStructPairString)
            );
            put("ch.unitelabs/test/UnobservablePropertyTest/v1/Property/StructPairString", new MessageCaseHandler()
                    .withUnobservableProperty(UnobservablePropertyTestOuterClass.Get_StructPairString_Parameters.parser(), unobservableProperty::getStructPairString)
            );
            put("ch.unitelabs/test/UnobservablePropertyTest/v1/Property/DataType", new MessageCaseHandler()
                    .withUnobservableProperty(UnobservablePropertyTestOuterClass.Get_DataType_Parameters.parser(), unobservableProperty::getDataType)
            );
            put("ch.unitelabs/test/UnobservablePropertyTest/v1/Property/ThrowException", new MessageCaseHandler()
                    .withUnobservableProperty(UnobservablePropertyTestOuterClass.Get_ThrowException_Parameters.parser(), unobservableProperty::getThrowException)
            );
            final ParameterConstraintsProviderGrpc.ParameterConstraintsProviderImplBase s = (ParameterConstraintsProviderGrpc.ParameterConstraintsProviderImplBase) ParameterConstraintsProvider.getService();
            put("org.silastandard/core.commands/ParameterConstraintsProvider/v1/Property/ParametersConstraints", new MessageCaseHandler()
                    .withObservableProperty(ParameterConstraintsProviderOuterClass.Subscribe_ParametersConstraints_Parameters.parser(), s::subscribeParametersConstraints)
            );
        }};
        UploadService uploadService = new UploadService(binaryDatabase);
        DownloadService downloadService = new DownloadService(binaryDatabase);
        this.cloudServerEndpointService = new CloudierServerEndpoint(
                this.cloudierSiLAService,
                this.cloudierConnectionConfigurationService,
                this.clientEndpoint,
                callForwarderMap,
                uploadService,
                downloadService
        );
    }

    @Override
    public void close() {
        this.observableCommand.close();
        this.channel.shutdown();
    }
}
