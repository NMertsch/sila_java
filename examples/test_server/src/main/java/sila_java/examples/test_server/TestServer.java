package sila_java.examples.test_server;

import com.google.common.base.Verify;
import io.grpc.ManagedChannel;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import sila2.ch.unitelabs.test.observablecommandtest.v1.ObservableCommandTestOuterClass;
import sila2.ch.unitelabs.test.observablepropertytest.v1.ObservablePropertyTestOuterClass;
import sila2.ch.unitelabs.test.parameterconstraintsprovidertest.v1.ParameterConstraintsProviderTestOuterClass;
import sila2.ch.unitelabs.test.unobservablecommandtest.v1.UnobservableCommandTestOuterClass;
import sila2.ch.unitelabs.test.unobservablepropertytest.v1.UnobservablePropertyTestOuterClass;
import sila2.org.silastandard.CloudClientEndpointGrpc;
import sila2.org.silastandard.SiLAFramework;
import sila2.org.silastandard.core.commands.parameterconstraintsprovider.v1.ParameterConstraintsProviderGrpc;
import sila2.org.silastandard.core.commands.parameterconstraintsprovider.v1.ParameterConstraintsProviderOuterClass;
import sila_java.examples.test_server.impl.*;
import sila_java.library.cloudier.server.CloudierConnectionConfigurationService;
import sila_java.library.cloudier.server.CloudierServerEndpoint;
import sila_java.library.cloudier.server.CloudierSiLAService;
import sila_java.library.cloudier.server.MessageCaseHandler;
import sila_java.library.core.sila.clients.ChannelFactory;
import sila_java.library.core.utils.Utils;
import sila_java.library.server_base.SiLAServer;
import sila_java.library.server_base.binary_transfer.database.BinaryDatabase;
import sila_java.library.server_base.binary_transfer.database.impl.H2BinaryDatabase;
import sila_java.library.server_base.config.IServerConfigWrapper;
import sila_java.library.server_base.identification.ServerInformation;
import sila_java.library.server_base.utils.ArgumentHelper;
import sila_java.library.sila_base.EmptyClass;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static sila_java.library.core.utils.FileUtils.getFileContent;
import static sila_java.library.core.utils.FileUtils.getResourceContent;

/**
 * SiLA Server for test purposes
 */
@Slf4j
public class TestServer implements AutoCloseable {
    static final String SERVER_TYPE = "TestServer";
    public static final ServerInformation serverInfo = new ServerInformation(
            SERVER_TYPE,
            "Server for test purposes",
            "https://www.sila-standard.org",
            "0.0"
    );
    private final ObservableCommand observableCommandService = new ObservableCommand();
    private final UnobservableProperty unobservablePropertyService = new UnobservableProperty();
    private final ObservableProperty observablePropertyService = new ObservableProperty();
    private final UnobservableCommand unobservableCommandService;
    private final sila_java.examples.test_server.impl.ParameterConstraintsProvider parameterConstraintsProviderService;
    private final sila_java.library.server_base.standard_features.ParameterConstraintsProvider parameterConstraintsProvider;
    private final ParameterConstraints parameterConstraintsTestService = new ParameterConstraints();
    private final CloudierConnectionConfigurationService connectionConfigurationService =
            new CloudierConnectionConfigurationService(
                false, this::onServerConnectionModeChange
            );
    private final SiLAServer.Builder builder;
    private final BinaryDatabase binaryDatabase;
    private final IServerConfigWrapper configuration;
    private SiLAServer clientInitiatedServer;
    // server initiated variables
    private CloudierSiLAService cloudierSiLAService;
    private CloudClientEndpointGrpc.CloudClientEndpointStub clientEndpoint;
    private CloudierServerEndpoint cloudServerEndpointService;
    private ManagedChannel channel;
    private Thread serverInitiatedServerThread;

    private HashMap<String, MessageCaseHandler> getCallForwarderMap() {
        return new HashMap<String, MessageCaseHandler>() {{
            put("ch.unitelabs/test/ObservableCommandTest/v1/Command/RestartDevice", new MessageCaseHandler()
                    .withObservableCommand(ObservableCommandTestOuterClass.RestartDevice_Parameters.parser(), observableCommandService::restartDevice)
                    .withExecInfo(SiLAFramework.CommandExecutionUUID.parser(), observableCommandService::restartDeviceInfo)
                    .withIntermediate(SiLAFramework.CommandExecutionUUID.parser(), observableCommandService::restartDeviceIntermediate)
                    .withResult(SiLAFramework.CommandExecutionUUID.parser(), observableCommandService::restartDeviceResult)
            );
            put("ch.unitelabs/test/ObservablePropertyTest/v1/Property/ListString", new MessageCaseHandler()
                    .withObservableProperty(ObservablePropertyTestOuterClass.Subscribe_ListString_Parameters.parser(), observablePropertyService::subscribeListString)
            );
            put("ch.unitelabs/test/ObservablePropertyTest/v1/Property/SingletonListString", new MessageCaseHandler()
                    .withObservableProperty(ObservablePropertyTestOuterClass.Subscribe_SingletonListString_Parameters.parser(), observablePropertyService::subscribeSingletonListString)
            );
            put("ch.unitelabs/test/ParameterConstraintsProviderTest/v1/Command/GoToPos", new MessageCaseHandler()
                    .withUnobservableCommand(ParameterConstraintsProviderTestOuterClass.GoToPos_Parameters.parser(), parameterConstraintsProviderService::goToPos)
            );
            put("ch.unitelabs/test/ParameterConstraintsProviderTest/v1/Command/AddPosition", new MessageCaseHandler()
                    .withUnobservableCommand(ParameterConstraintsProviderTestOuterClass.AddPosition_Parameters.parser(), parameterConstraintsProviderService::addPosition)
            );
            put("ch.unitelabs/test/ParameterConstraintsProviderTest/v1/Command/RemovePosition", new MessageCaseHandler()
                    .withUnobservableCommand(ParameterConstraintsProviderTestOuterClass.RemovePosition_Parameters.parser(), parameterConstraintsProviderService::removePosition)
            );
            put("ch.unitelabs/test/ParameterConstraintsProviderTest/v1/Property/Positions", new MessageCaseHandler()
                    .withUnobservableProperty(ParameterConstraintsProviderTestOuterClass.Get_Positions_Parameters.parser(), parameterConstraintsProviderService::getPositions)
            );
            put("ch.unitelabs/test/UnobservableCommandTest/v1/Command/MakeCoffee", new MessageCaseHandler()
                    .withUnobservableCommand(UnobservableCommandTestOuterClass.MakeCoffee_Parameters.parser(), unobservableCommandService::makeCoffee)
            );
            put("ch.unitelabs/test/UnobservableCommandTest/v1/Command/Sleep", new MessageCaseHandler()
                    .withUnobservableCommand(UnobservableCommandTestOuterClass.Sleep_Parameters.parser(), unobservableCommandService::sleep)
            );
            put("ch.unitelabs/test/UnobservableCommandTest/v1/Command/ListProvider", new MessageCaseHandler()
                    .withUnobservableCommand(UnobservableCommandTestOuterClass.ListProvider_Parameters.parser(), unobservableCommandService::listProvider)
            );
            put("ch.unitelabs/test/UnobservableCommandTest/v1/Command/ThreeDimensionalStruct", new MessageCaseHandler()
                    .withUnobservableCommand(UnobservableCommandTestOuterClass.ThreeDimensionalStruct_Parameters.parser(), unobservableCommandService::threeDimensionalStruct)
            );
            put("ch.unitelabs/test/UnobservableCommandTest/v1/Command/ThreeDimensionalList", new MessageCaseHandler()
                    .withUnobservableCommand(UnobservableCommandTestOuterClass.ThreeDimensionalList_Parameters.parser(), unobservableCommandService::threeDimensionalList)
            );
            put("ch.unitelabs/test/UnobservableCommandTest/v1/Command/WhyMakeItSimpleWhenYouCanMakeItComplicated", new MessageCaseHandler()
                    .withUnobservableCommand(UnobservableCommandTestOuterClass.WhyMakeItSimpleWhenYouCanMakeItComplicated_Parameters.parser(), unobservableCommandService::whyMakeItSimpleWhenYouCanMakeItComplicated)
            );
            put("ch.unitelabs/test/UnobservableCommandTest/v1/Command/XOREncipher", new MessageCaseHandler()
                    .withUnobservableCommand(UnobservableCommandTestOuterClass.XOREncipher_Parameters.parser(), unobservableCommandService::xOREncipher)
            );
            put("ch.unitelabs/test/UnobservableCommandTest/v1/Command/ValueForTypeProvider", new MessageCaseHandler()
                    .withUnobservableCommand(UnobservableCommandTestOuterClass.ValueForTypeProvider_Parameters.parser(), unobservableCommandService::valueForTypeProvider)
            );
            put("ch.unitelabs/test/UnobservableCommandTest/v1/Metadata/User", new MessageCaseHandler()
                    .withMetadata(UnobservableCommandTestOuterClass.Get_FCPAffectedByMetadata_User_Parameters.parser(), unobservableCommandService::getFCPAffectedByMetadataUser)
            );
            put("ch.unitelabs/test/UnobservablePropertyTest/v1/Property/HugeList", new MessageCaseHandler()
                    .withUnobservableProperty(UnobservablePropertyTestOuterClass.Get_HugeList_Parameters.parser(), unobservablePropertyService::getHugeList)
            );
            put("ch.unitelabs/test/UnobservablePropertyTest/v1/Property/Boolean", new MessageCaseHandler()
                    .withUnobservableProperty(UnobservablePropertyTestOuterClass.Get_Boolean_Parameters.parser(), unobservablePropertyService::getBoolean)
            );
            put("ch.unitelabs/test/UnobservablePropertyTest/v1/Property/ListString", new MessageCaseHandler()
                    .withUnobservableProperty(UnobservablePropertyTestOuterClass.Get_ListString_Parameters.parser(), unobservablePropertyService::getListString)
            );
            put("ch.unitelabs/test/UnobservablePropertyTest/v1/Property/RandomChangingListString", new MessageCaseHandler()
                    .withUnobservableProperty(UnobservablePropertyTestOuterClass.Get_RandomChangingListString_Parameters.parser(), unobservablePropertyService::getRandomChangingListString)
            );
            put("ch.unitelabs/test/UnobservablePropertyTest/v1/Property/ListStructPairString", new MessageCaseHandler()
                    .withUnobservableProperty(UnobservablePropertyTestOuterClass.Get_ListStructPairString_Parameters.parser(), unobservablePropertyService::getListStructPairString)
            );
            put("ch.unitelabs/test/UnobservablePropertyTest/v1/Property/StructPairString", new MessageCaseHandler()
                    .withUnobservableProperty(UnobservablePropertyTestOuterClass.Get_StructPairString_Parameters.parser(), unobservablePropertyService::getStructPairString)
            );
            put("ch.unitelabs/test/UnobservablePropertyTest/v1/Property/DataType", new MessageCaseHandler()
                    .withUnobservableProperty(UnobservablePropertyTestOuterClass.Get_DataType_Parameters.parser(), unobservablePropertyService::getDataType)
            );
            put("ch.unitelabs/test/UnobservablePropertyTest/v1/Property/ThrowException", new MessageCaseHandler()
                    .withUnobservableProperty(UnobservablePropertyTestOuterClass.Get_ThrowException_Parameters.parser(), unobservablePropertyService::getThrowException)
            );
            final ParameterConstraintsProviderGrpc.ParameterConstraintsProviderImplBase s = (ParameterConstraintsProviderGrpc.ParameterConstraintsProviderImplBase) parameterConstraintsProvider.getService();
            put("org.silastandard/core.commands/ParameterConstraintsProvider/v1/Property/ParametersConstraints", new MessageCaseHandler()
                    .withObservableProperty(ParameterConstraintsProviderOuterClass.Subscribe_ParametersConstraints_Parameters.parser(), s::subscribeParametersConstraints)
            );
        }};
    }

    @SneakyThrows
    private static String getCoreFeatureContent(@NonNull final String coreFeaturePath) {
        return getFileContent(Objects.requireNonNull(EmptyClass.class.getResourceAsStream(
                "/sila_base/feature_definitions/org/silastandard/core/" + coreFeaturePath
        )));
    }

    public TestServer(@NonNull final ArgumentHelper argumentHelper) {
        try {
            this.builder = SiLAServer.Builder.newBuilder(this.serverInfo);

            this.builder.withPersistentConfig(argumentHelper.getConfigFile().isPresent());

            argumentHelper.getConfigFile().ifPresent(this.builder::withPersistentConfigFile);

            this.builder.withPersistentTLS(
                    argumentHelper.getPrivateKeyFile(),
                    argumentHelper.getCertificateFile(),
                    argumentHelper.getCertificatePassword()
            );

            argumentHelper.getPort().ifPresent(this.builder::withPort);
            argumentHelper.getInterface().ifPresent(this.builder::withNetworkInterface);

            if (argumentHelper.useUnsafeCommunication()) {
                this.builder.withUnsafeCommunication(true);
            }

            this.builder.withBinaryTransferSupport(true);
            this.configuration = builder.getNewServerConfigurationWrapper();
            this.builder.withCustomConfigWrapperProvider((configPath, serverConfiguration) -> this.configuration);
            this.binaryDatabase = new H2BinaryDatabase(this.configuration.getCacheConfig().getUuid());
            this.builder.withCustomBinaryDatabaseProvider((uuid) -> this.binaryDatabase);
            this.unobservableCommandService = new UnobservableCommand(binaryDatabase);

            this.parameterConstraintsProvider = new sila_java.library.server_base.standard_features.ParameterConstraintsProvider();
            this.parameterConstraintsProviderService = new ParameterConstraintsProvider(parameterConstraintsProvider);

            this.builder.addFeature(
                    getResourceContent("features/UnobservablePropertyTest.sila.xml"),
                    this.unobservablePropertyService
            ).addFeature(
                    getResourceContent("features/ObservablePropertyTest.sila.xml"),
                    this.observablePropertyService
            ).addFeature(
                    getResourceContent("features/UnobservableCommandTest.sila.xml"),
                    this.unobservableCommandService
            ).addFeature(
                    getResourceContent("features/ObservableCommandTest.sila.xml"),
                    this.observableCommandService
            ).addFeature(
                    getResourceContent("features/ParameterConstraintsProviderTest.sila.xml"),
                    this.parameterConstraintsProviderService
            ).addFeature(
                    getResourceContent("features/ParameterConstraintsTest.sila.xml"),
                    this.parameterConstraintsTestService
            ).addFeature(getCoreFeatureContent("ConnectionConfigurationService.sila.xml"),
                    this.connectionConfigurationService
            ).addFeature(
                    this.parameterConstraintsProvider
            );

        } catch (IOException | SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void startClientInitiatedServer() throws IOException {
        log.info("Starting server in client initiated mode");
        this.clientInitiatedServer = this.builder.start();
    }

    private void onServerConnectionModeChange(boolean serverInitiatedConnectionEnabled) {
        log.info("Server connection mode switching to {}", (serverInitiatedConnectionEnabled) ? "Server Initiated" : "Client Initiated");
        // cleanup
        if (serverInitiatedConnectionEnabled) {
            try {
                this.clientInitiatedServer.close();
            } catch (Exception e) {
                log.warn("Exception occurred while closing client initiated server");
            } finally {
                this.clientInitiatedServer = null;
            }
        } else {
            try {
                this.channel.shutdown();
                try {
                    this.channel.awaitTermination(1, TimeUnit.SECONDS);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    // Allow thread to exit.
                } finally {
                    this.channel.shutdownNow();
                }
                Verify.verify(this.channel.isShutdown());
                this.serverInitiatedServerThread.interrupt();
                int i = 0;
                while (this.serverInitiatedServerThread.isAlive() && i < 50) {
                    Thread.sleep(100);
                    ++i;
                }
                if (this.serverInitiatedServerThread.isAlive()) {
                    this.serverInitiatedServerThread.stop();
                }
            } catch (Exception e) {
                log.warn("Exception occurred while closing server initiated server!");
            } finally {
                this.channel = null;
                this.cloudServerEndpointService = null; // todo check that garbage collected once null
                this.clientEndpoint = null;
                this.cloudierSiLAService = null;
                this.serverInitiatedServerThread = null;
            }
        }
        // start (any exception occurring here should be fatal for the server execution
        if (serverInitiatedConnectionEnabled) {
            this.startServerInitiatedConnection(this.configuration.getCacheConfig().getUuid());
        } else {
            try {
                this.startClientInitiatedServer();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        log.info("Server connection mode switched to {}", (serverInitiatedConnectionEnabled) ? "Server Initiated" : "Client Initiated");
    }

    private void startServerInitiatedConnection(@NonNull final UUID serverUUID) {
        this.serverInitiatedServerThread = new Thread(() -> {
            this.cloudierSiLAService = new CloudierSiLAService(
                    this.configuration.getCacheConfig().getName(),
                    TestServer.serverInfo.getType(),
                    serverUUID.toString(),
                    TestServer.serverInfo.getVersion(),
                    TestServer.serverInfo.getDescription(),
                    TestServer.serverInfo.getVendorURL(),
                    this.builder.getFeatureDefinitions()
            );
            // todo iterate on clients and create a channel for each
            this.channel = ChannelFactory.getTLSEncryptedChannel("localhost", 50051);
            this.clientEndpoint = CloudClientEndpointGrpc.newStub(this.channel);
            this.cloudServerEndpointService = new CloudierServerEndpoint(
                    this.cloudierSiLAService,
                    this.connectionConfigurationService,
                    this.clientEndpoint,
                    this.getCallForwarderMap(),
                    null,
                    null
                    //this.clientInitiatedServer.getUploadService().orElse(null), // todo fix nullptr exception since clientInitiatedServer is null
                    //this.clientInitiatedServer.getDownloadService().orElse(null)
            );
        });
        this.serverInitiatedServerThread.start();
    }

    @SneakyThrows
    @Override
    public void close() {
        if (this.clientInitiatedServer != null) {
            this.clientInitiatedServer.close();
        }
        if (this.channel != null) {
            this.channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
        }
        this.observableCommandService.close();
    }

    @SneakyThrows
    public static void main(final String[] args) {
        final ArgumentHelper argumentHelper = new ArgumentHelper(args, SERVER_TYPE);
        final TestServer server = new TestServer(argumentHelper);
        server.startClientInitiatedServer();
        log.info("To stop the server press CTRL + C.");
        Utils.blockUntilShutdown(server::close);
        log.info("Server closed.");
    }
}
