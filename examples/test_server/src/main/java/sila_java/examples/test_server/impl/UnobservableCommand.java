package sila_java.examples.test_server.impl;

import com.google.protobuf.Descriptors;
import com.google.protobuf.DynamicMessage;
import com.google.protobuf.InvalidProtocolBufferException;
import io.grpc.stub.StreamObserver;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import sila2.ch.unitelabs.test.unobservablecommandtest.v1.UnobservableCommandTestGrpc;
import sila2.ch.unitelabs.test.unobservablecommandtest.v1.UnobservableCommandTestOuterClass;
import sila_java.examples.test_server.utils.Bytes;
import sila_java.library.core.sila.types.SiLABinary;
import sila2.org.silastandard.SiLAFramework;
import sila_java.library.core.sila.types.SiLAAny;
import sila_java.library.core.sila.errors.SiLAErrors;
import sila_java.library.core.sila.types.SiLAString;
import sila_java.examples.test_server.utils.ByteArrayProcessedInputStream;
import sila_java.library.server_base.Constants;
import sila_java.library.server_base.FullyQualifiedMetadataContextKey;
import sila_java.library.server_base.binary_transfer.Binary;
import sila_java.library.server_base.binary_transfer.database.BinaryDatabase;
import sila_java.library.server_base.binary_transfer.database.BinaryDatabaseException;
import sila_java.library.core.sila.types.SiLAInteger;

import java.io.IOException;
import java.sql.SQLException;
import java.util.*;

import sila_java.library.core.models.BasicType;
import sila_java.library.core.models.DataTypeType;
import sila_java.library.core.models.SiLAElement;
import sila_java.library.core.models.StructureType;
import sila_java.library.core.sila.mapping.feature.MalformedSiLAFeature;
import sila_java.library.core.sila.mapping.grpc.ProtoMapper;
import sila_java.library.core.sila.types.*;

@Slf4j
public class UnobservableCommand extends UnobservableCommandTestGrpc.UnobservableCommandTestImplBase {
    public static final UnobservableCommandTestOuterClass.MakeCoffee_Responses MAKE_COFFEE_RESPONSE = UnobservableCommandTestOuterClass.MakeCoffee_Responses
            .newBuilder()
            .setResult(SiLAString.from("Your coffee is ready!"))
            .build();
    public static final UnobservableCommandTestOuterClass.WhyMakeItSimpleWhenYouCanMakeItComplicated_Responses COMPLICATED_RESPONSES =
            UnobservableCommandTestOuterClass.WhyMakeItSimpleWhenYouCanMakeItComplicated_Responses
                    .newBuilder()
                    .setFirstResponse(SiLAString.from("Dead"))
                    .setSecondResponse(SiLAString.from("Alive"))
                    .build();
    public static final UnobservableCommandTestOuterClass.ThreeDimensionalStruct_Responses THREE_DIMENSIONAL_STRUCT_RESPONSES =
            UnobservableCommandTestOuterClass.ThreeDimensionalStruct_Responses
                    .newBuilder()
                    .setThreeDimensionalListResult(UnobservableCommandTestOuterClass.DataType_ThreeDimensionalString
                            .newBuilder()
                            .addThreeDimensionalString(
                                    UnobservableCommandTestOuterClass.DataType_TwoDimensionalString
                                            .newBuilder()
                                            .addTwoDimensionalString(UnobservableCommandTestOuterClass.DataType_OneDimensionalString
                                                    .newBuilder()
                                                    .addOneDimensionalString(SiLAString.from("000"))
                                                    .build()
                                            ).build()
                            )
                    ).build();
    public static final UnobservableCommandTestOuterClass.ThreeDimensionalList_Responses THREE_DIMENSIONAL_LIST_RESPONSES =
            UnobservableCommandTestOuterClass.ThreeDimensionalList_Responses
                    .newBuilder()
                    .setThreeDimensionalStructResult(UnobservableCommandTestOuterClass.ThreeDimensionalList_Responses.ThreeDimensionalStructResult_Struct
                            .newBuilder()
                            .setFirstDimension(UnobservableCommandTestOuterClass.ThreeDimensionalList_Responses.ThreeDimensionalStructResult_Struct.FirstDimension_Struct
                                    .newBuilder()
                                    .setSecondDimension(UnobservableCommandTestOuterClass.ThreeDimensionalList_Responses.ThreeDimensionalStructResult_Struct.FirstDimension_Struct.SecondDimension_Struct
                                            .newBuilder()
                                            .setThirdDimension(SiLAString.from("111"))
                                            .build())
                                    .build())
                            .build())
                    .build();

    private final BinaryDatabase binaryDatabase;

    public UnobservableCommand(@NonNull final BinaryDatabase binaryDatabase) {
        this.binaryDatabase = binaryDatabase;
    }

    @Override
    public void makeCoffee(UnobservableCommandTestOuterClass.MakeCoffee_Parameters request, StreamObserver<UnobservableCommandTestOuterClass.MakeCoffee_Responses> responseObserver) {
        final Set<FullyQualifiedMetadataContextKey<byte[]>> fullyQualifiedMetadataContextKeys
                = Constants.METADATA_IDENTIFIERS_CTX_KEY.get();
        final Optional<FullyQualifiedMetadataContextKey<byte[]>> optionalUser = fullyQualifiedMetadataContextKeys
                .stream()
                .filter(k -> k.getFullyQualifiedIdentifier().endsWith("User"))
                .findAny();
        if (optionalUser.isPresent()) {
            try {
                final UnobservableCommandTestOuterClass.Metadata_User user = UnobservableCommandTestOuterClass.Metadata_User.parseFrom(optionalUser.get().getContextKey().get());
                log.info("received metadata user: " + user.getUser().getIdentifier());
            } catch (InvalidProtocolBufferException e) {
                log.info("failed to parse metadata user", e);
            }
        } else {
            log.info("user metadata was not provided");
        }
        if (request.getSugar().getValue()) {
            responseObserver.onError(SiLAErrors.generateDefinedExecutionError(
                    "SugarException",
                    "Sorry, there is no more sugar! Buy some sugar or take your coffee without sugar"
            ));
            return;
        }
        responseObserver.onNext(UnobservableCommand.MAKE_COFFEE_RESPONSE);
        responseObserver.onCompleted();
    }

    @Override
    public void whyMakeItSimpleWhenYouCanMakeItComplicated(
            UnobservableCommandTestOuterClass.WhyMakeItSimpleWhenYouCanMakeItComplicated_Parameters request,
            StreamObserver<UnobservableCommandTestOuterClass.WhyMakeItSimpleWhenYouCanMakeItComplicated_Responses> responseObserver) {
        responseObserver.onNext(UnobservableCommand.COMPLICATED_RESPONSES);
        responseObserver.onCompleted();
    }

    @Override
    public void threeDimensionalStruct(
            UnobservableCommandTestOuterClass.ThreeDimensionalStruct_Parameters request,
            StreamObserver<UnobservableCommandTestOuterClass.ThreeDimensionalStruct_Responses> responseObserver) {
        responseObserver.onNext(UnobservableCommand.THREE_DIMENSIONAL_STRUCT_RESPONSES);
        responseObserver.onCompleted();
    }

    @Override
    public void threeDimensionalList(
            UnobservableCommandTestOuterClass.ThreeDimensionalList_Parameters request,
            StreamObserver<UnobservableCommandTestOuterClass.ThreeDimensionalList_Responses> responseObserver) {
        responseObserver.onNext(UnobservableCommand.THREE_DIMENSIONAL_LIST_RESPONSES);
        responseObserver.onCompleted();
    }

    @Override
    public void xOREncipher(
            UnobservableCommandTestOuterClass.XOREncipher_Parameters request,
            StreamObserver<UnobservableCommandTestOuterClass.XOREncipher_Responses> responseObserver
    ) {
        final byte key = (byte)request.getCipherKey().getValue();
        if (!request.getData().getBinaryTransferUUID().isEmpty()) {
            try {
                final UUID uuid = XOREncipherSiLABinary(UUID.fromString(request.getData().getBinaryTransferUUID()), key);
                responseObserver.onNext(
                        UnobservableCommandTestOuterClass.XOREncipher_Responses
                                .newBuilder()
                                .setData(SiLABinary.fromBinaryTransferUUID(uuid))
                                .build()
                );
            } catch (BinaryDatabaseException | SQLException e) {
                log.error(
                        "Error happened while applying XOR Cipher on SiLA Binary {}",
                        request.getData().getBinaryTransferUUID(),
                        e
                );
                responseObserver.onError(e);
                return;
            }
        } else {
            final byte[] data = request.getData().getValue().toByteArray();
            Bytes.XOREncipherBytes(data, key, 0, data.length);
            responseObserver.onNext(
                    UnobservableCommandTestOuterClass.XOREncipher_Responses
                            .newBuilder()
                            .setData(SiLABinary.fromBytes(data))
                            .build()
            );
        }
        responseObserver.onCompleted();
    }

    /**
     * XOR encipher a SiLA Binary and returns a Binary Transfer UUID to the new enciphered SiLA Binary
     * @param binaryIdentifier The Binary Transfer UUID of the SiLA Binary to encipher
     * @param key The key to encipher the data with
     * @return a Binary Transfer UUID to the new enciphered SiLA Binary
     * @throws BinaryDatabaseException If the SiLA Binary does not exist or if the enciphered SiLA Binary cannot be created
     * @throws SQLException If an error occurs while reading the SiLA Binary blob from the database
     */
    private UUID XOREncipherSiLABinary(@NonNull final UUID binaryIdentifier, final byte key)
            throws BinaryDatabaseException, SQLException {
        final Binary binary = this.binaryDatabase.getBinary(binaryIdentifier);
        final UUID xorBinaryId = UUID.randomUUID();
        try(ByteArrayProcessedInputStream byteArrayProcessedInputStream = new ByteArrayProcessedInputStream(
                binary.getData().getBinaryStream(),
                (bytes, off, len) -> Bytes.XOREncipherBytes(bytes, key, off, len)
        )) {
            this.binaryDatabase.addBinary(xorBinaryId, byteArrayProcessedInputStream);
        } catch (IOException e) {
            // should never happen
            throw new RuntimeException(e);
        }
        return xorBinaryId;
    }

    @Override
    public void valueForTypeProvider(
            UnobservableCommandTestOuterClass.ValueForTypeProvider_Parameters request,
            StreamObserver<UnobservableCommandTestOuterClass.ValueForTypeProvider_Responses> responseObserver
    ) {
        final DataTypeType integerType = new DataTypeType();
        integerType.setBasic(BasicType.INTEGER);

        final DataTypeType anonymousStructureType = new DataTypeType();
        final StructureType structureType = new StructureType();
        final SiLAElement element = new SiLAElement();
        element.setDataType(integerType);
        element.setDescription("X");
        element.setDisplayName("X");
        element.setIdentifier("X");
        structureType.getElement().add(element);
        anonymousStructureType.setStructure(structureType);

        final Descriptors.Descriptor anonymousStructureDescriptor;
        final DynamicMessage anonymousStructureMessage;
        try {
            anonymousStructureDescriptor = ProtoMapper.dataTypeToDescriptor(anonymousStructureType);
            anonymousStructureMessage = DynamicMessage
                    .newBuilder(anonymousStructureDescriptor)
                    .setField(anonymousStructureDescriptor.findFieldByName("X"), SiLAInteger.from(101))
                    .build();
        } catch (MalformedSiLAFeature e) {
            throw new RuntimeException(e);
        }

        final SiLAFramework.Any any;
        switch (request.getType().getValue()) {
            case "Integer":
                any = SiLAAny.from(integerType, SiLAInteger.from(1337));
                break;
            case "String":
                any = SiLAAny.from("<DataType><Basic>String</Basic></DataType>", SiLAString.from("test"));
                break;
            case "ConstrainedReal":
                any = SiLAAny.from(
                        "<DataType><Constrained><DataType><Basic>Real</Basic></DataType><Constraints><MinimalInclusive>4.2</MinimalInclusive><MaximalInclusive>133.7</MaximalInclusive></Constraints></Constrained></DataType>",
                        SiLAReal.from(7.7)
                );
                break;
            case "AnonymousStructure":
                any = SiLAAny.from(anonymousStructureType, anonymousStructureMessage);
                break;
            case "AnonymousList":
                any = SiLAAny.from(
                        "<DataType><List><DataType><Basic>Any</Basic></DataType></List></DataType>",
                        UnobservableCommandTestOuterClass.DataType_AnonymousList
                                .newBuilder()
                                .addAnonymousList(SiLAAny.from(integerType, SiLAInteger.from(777)))
                                .addAnonymousList(SiLAAny.from(
                                        "<DataType><Basic>String</Basic></DataType>",
                                        SiLAString.from("SiLA"))
                                )
                                .build()
                );
                break;
            default:
                responseObserver.onError(
                        SiLAErrors.generateUndefinedExecutionError("Unsupported type: " + request.getType().getValue())
                );
                return;
        }
        responseObserver.onNext(
                UnobservableCommandTestOuterClass.ValueForTypeProvider_Responses
                        .newBuilder()
                        .setAny(any)
                        .build()
        );
        responseObserver.onCompleted();
    }

    @Override
    public void getFCPAffectedByMetadataUser(
            UnobservableCommandTestOuterClass.Get_FCPAffectedByMetadata_User_Parameters request,
            StreamObserver<UnobservableCommandTestOuterClass.Get_FCPAffectedByMetadata_User_Responses> responseObserver
    ) {
        responseObserver.onNext(
                UnobservableCommandTestOuterClass.Get_FCPAffectedByMetadata_User_Responses
                        .newBuilder()
                        .addAffectedCalls(SiLAString.from("ch.unitelabs/test/UnobservableCommandTest/v1/Command/XOREncipher"))
                        .build()
        );
        responseObserver.onCompleted();
    }

    @Override
    public void sleep(
            final UnobservableCommandTestOuterClass.Sleep_Parameters request,
            final StreamObserver<UnobservableCommandTestOuterClass.Sleep_Responses> responseObserver
    ) {
        try {
            Thread.sleep(request.getSecondsToSleep().getValue() * 1000);
        } catch (InterruptedException e) {
            log.warn("sleep interrupted", e);
            Thread.currentThread().interrupt();
        }
        responseObserver.onNext(UnobservableCommandTestOuterClass.Sleep_Responses.newBuilder().build());
        responseObserver.onCompleted();
    }

    @Override
    public void listProvider(
            final UnobservableCommandTestOuterClass.ListProvider_Parameters request,
            final StreamObserver<UnobservableCommandTestOuterClass.ListProvider_Responses> responseObserver
    ) {
        final long listLength = request.getListLength().getValue();
        final long elementLength = request.getElementLength().getValue();
        final List<SiLAFramework.String> list = new ArrayList<>((int) listLength);

        for (int i = 0; i < listLength; ++i) {
            list.add(i, SiLAString.from(RandomStringUtils.randomAlphanumeric((int) elementLength)));
        }
        responseObserver.onNext(UnobservableCommandTestOuterClass.ListProvider_Responses
                .newBuilder()
                .addAllList(list)
                .build());
        responseObserver.onCompleted();
    }
}
