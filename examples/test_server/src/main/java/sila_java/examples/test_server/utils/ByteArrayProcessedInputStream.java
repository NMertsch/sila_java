package sila_java.examples.test_server.utils;

import lombok.AllArgsConstructor;
import lombok.NonNull;

import java.io.IOException;
import java.io.InputStream;

/**
 * InputStream wrapper that allows binary data to be processed and modified while the stream is being read
 */
@AllArgsConstructor
public class ByteArrayProcessedInputStream extends InputStream {
    private final InputStream in;
    private final ApplyOnBytes applyOnBytes;

    @FunctionalInterface
    public interface ApplyOnBytes {
        /**
         * The function to apply on binary data being read
         * @param bytes the data
         * @param off the data offset
         * @param len the data length
         */
        void apply(byte[] bytes, int off, int len);
    }

    /**
     * Read and process one byte
     * {@inheritDoc}
     */
    @Override
    public int read() throws IOException {
        final int singleByte = this.in.read();
        if (singleByte == -1) { // end of the stream
            return -1;
        }
        final byte[] singleByteArray = {(byte) singleByte};
        applyOnBytes.apply(singleByteArray,0, 1);
        return singleByteArray[0];
    }

    /**
     * Read and process a byte array
     * {@inheritDoc}
     */
    @Override
    public int read(byte[] b) throws IOException {
        final int read = this.in.read(b);
        if (read == -1) { // end of the stream
            return -1;
        }
        applyOnBytes.apply(b, 0, read);
        return read;
    }

    /**
     * Read and process a byte array with a specific offset and length
     * {@inheritDoc}
     */
    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        final int read = this.in.read(b, off, len);
        if (read == -1) { // end of the stream
            return -1;
        }
        applyOnBytes.apply(b, off, read);
        return read;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long skip(long n) throws IOException {
        return this.in.skip(n);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int available() throws IOException {
        return this.in.available();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void close() throws IOException {
        this.in.close();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized void mark(int readlimit) {
        this.in.mark(readlimit);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized void reset() throws IOException {
        this.in.reset();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean markSupported() {
        return this.in.markSupported();
    }
}
