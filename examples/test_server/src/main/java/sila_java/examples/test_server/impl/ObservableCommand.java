package sila_java.examples.test_server.impl;

import io.grpc.stub.StreamObserver;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import sila2.ch.unitelabs.test.observablecommandtest.v1.ObservableCommandTestGrpc;
import sila2.ch.unitelabs.test.observablecommandtest.v1.ObservableCommandTestOuterClass;
import sila2.org.silastandard.SiLAFramework;
import sila_java.library.core.sila.types.SiLAString;
import sila_java.library.server_base.command.observable.ObservableCommandManager;
import sila_java.library.server_base.command.observable.ObservableCommandTaskRunner;

import java.time.Duration;

@Slf4j
public class ObservableCommand extends ObservableCommandTestGrpc.ObservableCommandTestImplBase
        implements AutoCloseable {
    private static final int RESTART_DELAY = 5; // [sec]
    private static final int UPDATE_DELAY = 10; // [sec]

    private ObservableCommandManager<
            ObservableCommandTestOuterClass.RestartDevice_Parameters,
            ObservableCommandTestOuterClass.RestartDevice_Responses
            > controlTemperatureManager = new ObservableCommandManager<>(
            new ObservableCommandTaskRunner(1, 1),
            command -> {
                final int delay = (command.getParameter().getUpdateDevice().getValue()) ?
                        (RESTART_DELAY + UPDATE_DELAY) :
                        (RESTART_DELAY);
                try {
                    for (int i = 0; i < delay; ++i) {
                        Thread.sleep(1000);
                        if(command.getParameter().getUpdateDevice().getValue()) {
                            command.notifyIntermediateResponse(ObservableCommandTestOuterClass.RestartDevice_IntermediateResponses
                                    .newBuilder()
                                    .setFileBeingUpdated(SiLAString.from("Matrix.exe"))
                                    .build());
                        }
                    }
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
                return ObservableCommandTestOuterClass.RestartDevice_Responses.newBuilder().build();
            },
            Duration.ofSeconds(25)
    );

    @Override
    public void close() {
        this.controlTemperatureManager.close();
    }

    @Override
    public void restartDevice(
            @NonNull final ObservableCommandTestOuterClass.RestartDevice_Parameters request,
            @NonNull final StreamObserver<SiLAFramework.CommandConfirmation> responseObserver
    ) {
        this.controlTemperatureManager.addCommand(request, responseObserver);
    }

    @Override
    public void restartDeviceInfo(
            @NonNull final SiLAFramework.CommandExecutionUUID request,
            @NonNull final StreamObserver<SiLAFramework.ExecutionInfo> responseObserver
    ) {
       this.controlTemperatureManager.get(request).addStateObserver(responseObserver);
    }

    @Override
    public void restartDeviceResult(
            @NonNull final SiLAFramework.CommandExecutionUUID request,
            @NonNull final StreamObserver<ObservableCommandTestOuterClass.RestartDevice_Responses> responseObserver
    ) {
        this.controlTemperatureManager.get(request).sendResult(responseObserver);
    }

    @Override
    public void restartDeviceIntermediate(
            @NonNull final SiLAFramework.CommandExecutionUUID request,
            @NonNull final StreamObserver<ObservableCommandTestOuterClass.RestartDevice_IntermediateResponses> responseObserver
    ) {
        this.controlTemperatureManager.get(request).addIntermediateResponseObserver(responseObserver);
    }
}
