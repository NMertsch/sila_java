package sila_java.examples.hello_sila;

import java.io.IOException;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import sila_java.library.server_base.SiLAServer;
import sila_java.library.server_base.identification.ServerInformation;
import sila_java.library.server_base.utils.ArgumentHelper;

import static sila_java.library.core.utils.FileUtils.getResourceContent;

/**
 * Example minimal SiLA Server
 */
@Slf4j
public class HelloSiLAServer implements AutoCloseable {
    // Every SiLA Server needs to define a type
    public static final String SERVER_TYPE = "HelloSiLAServer";

    // SiLA Server constructed in constructor
    private final SiLAServer server;
    public static final ServerInformation serverInfo = new ServerInformation(
            SERVER_TYPE,
            "Simple Example of a SiLA Server",
            "https://www.sila-standard.org",
            "0.0"
    );

    /**
     * Application Class using command line arguments
     *
     * @param argumentHelper Custom Argument Helper
     */
    HelloSiLAServer(@NonNull final ArgumentHelper argumentHelper) {
        /*
        Start the minimum SiLA Server with the Meta Information
        and the gRPC Server Implementations (found below)
          */


        try {
            /*
            A configuration file has to be given if the developer wants to persist server configurations
            (such as the generated UUID)
             */
            final SiLAServer.Builder builder = SiLAServer.Builder.newBuilder(serverInfo);

            builder.withPersistentConfig(argumentHelper.getConfigFile().isPresent());

            argumentHelper.getConfigFile().ifPresent(builder::withPersistentConfigFile);

            builder.withPersistentTLS(
                    argumentHelper.getPrivateKeyFile(),
                    argumentHelper.getCertificateFile(),
                    argumentHelper.getCertificatePassword()
            );

            if (argumentHelper.useUnsafeCommunication()) {
                builder.withUnsafeCommunication(true);
            }

            argumentHelper.getHost().ifPresent(builder::withHost);
            argumentHelper.getPort().ifPresent(builder::withPort);
            argumentHelper.getInterface().ifPresent(builder::withNetworkInterface);
            builder.withDiscovery(argumentHelper.hasNetworkDiscovery());

            builder.addFeature(
                    getResourceContent("GreetingProvider.sila.xml"),
                    new SayHelloCommand()
            );

            this.server = builder.start();
        } catch (IOException e) {
            log.error("Something went wrong when building / starting server", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() {
        this.server.close();
    }

    /**
     * Simple main function that starts the server and keeps it alive
     */
    public static void main(final String[] args) {
        final ArgumentHelper argumentHelper = new ArgumentHelper(args, SERVER_TYPE);
        final HelloSiLAServer helloSiLAServer = new HelloSiLAServer(argumentHelper);
        log.info("To stop the server press CTRL + C.");
        helloSiLAServer.server.blockUntilShutdown();
        log.info("termination complete.");
    }

}
