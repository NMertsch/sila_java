package sila_java.library.tools.code_generator;

import com.google.protobuf.Descriptors;
import sila_java.library.core.sila.mapping.feature.MalformedSiLAFeature;
import sila_java.library.core.sila.mapping.grpc.ProtoMapper;
import sila_java.library.core.utils.FileUtils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import static sila_java.library.core.sila.mapping.grpc.DynamicProtoBuilder.generateProtoFile;

/**
 * Code Generator
 *
 * Currently only responsible for generating proto definitions from Feature Definitions
 */
public class SiLACodeGenerator {
    /**
     * Main function takes absolute path to XML document and absolute path to
     * proto file it should create. the XML document is assumed to be SiLA
     * conforming and the .proto file will be created accordingly.
     */
    public static void main(String[] args) {
        if (args.length != 2) {
            throw new IllegalArgumentException("[path_to_xml] [path_to_proto]");
        }
        final String xmlPath = args[0];
        final String protoPath = args[1];

        Descriptors.FileDescriptor protoFile;
        try {
            protoFile = ProtoMapper
                    .usingFeature(FileUtils.getFileContent(xmlPath))
                    .generateProto();
        } catch (IOException e) {
            throw new RuntimeException("XML Document couldn't be parsed: " + e.getMessage());
        } catch (MalformedSiLAFeature e2) {
            System.out.println("Generated proto is not valid!");
            throw new RuntimeException(e2);
        }

        try (BufferedWriter writer = new BufferedWriter(
                new FileWriter(  protoPath ))) {
            writer.write( generateProtoFile(protoFile));
        } catch (IOException e) {
            throw new RuntimeException("Error while creating proto!", e);
        }
    }
}
