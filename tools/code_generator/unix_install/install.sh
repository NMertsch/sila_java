#!/usr/bin/env bash
find ../target/* -name code_generator.jar | xargs cat stub.sh > sila_code_gen && chmod +x sila_code_gen
mv sila_code_gen /usr/local/bin/
