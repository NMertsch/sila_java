# SiLA Java
[![pipeline status](https://gitlab.com/SiLA2/sila_java/badges/master/pipeline.svg)](https://gitlab.com/SiLA2/sila_java/commits/master)
[![license](https://img.shields.io/badge/license-MIT-blue.svg)](%3Chttps://gitlab.com/sila2/sila_python/blob/master/LICENSE)

SiLA Java reference implementation. This repository consists of reference implementations, Software Drivers, and libraries to drive the adoption of the SiLA 2 Standard.

|||
| ---------------| ----------------------------------------------------------- |
| SiLA Homepage  | [https://sila-standard.com](https://sila-standard.com)      |
| Chat group     | [Join the group on Slack](https://join.slack.com/t/sila-standard/shared_invite/enQtNDI0ODcxMDg5NzkzLTBhOTU3N2I0NTc4NDcyMjg2ZDIwZDc1Yjg4N2FmYjZkMzljZDAyZjAwNTc5OTVjYjIwZWJjYjA0YTY0NTFiNDA)|
| Maintainer     | Timothy Diguiet ([sila.timothy@diguiet.com](mailto:sila.timothy@diguiet.com)) of [Diguiet Consulting](https://consulting.diguiet.com)|

If you are new to the tooling with Git Repositories, and the Java build systems, we provided a step by step guide in [Quick Start](https://gitlab.com/SiLA2/sila_java/wikis/Quick-Start).

## Documentation
The [Wiki](https://gitlab.com/SiLA2/sila_java/wikis/home) includes a quick start guide and tutorials.
The API documentation can be found [here](https://sila2.gitlab.io/sila_java).

## Status
**Important**: This code, in its current form, is mainly to give active SiLA 2 WG Members and other interested parties a reference point. 
It might not comply with the latest version of the Standard, and its contents may change in the future.

For more general information about the standard, we refer to the [sila_base](https://gitlab.com/SiLA2/sila_base) repository.

The reference code is currently mainly usable from source, proper versioned binaries are in progress and will be available beginning of 2020.

## Cloning
Clone the repository either with HTTPs or SSH (choose the appropriate link in the clone button)
```bash
git clone https://gitlab.com/SiLA2/sila_java.git
```
In the new folder: Initialize the yor local configuration file:
```bash
git submodule update --init --recursive
```
Fetch all the data from the project and check out the appropriate commit listed.
```bash
git submodule update --recursive
```

## How to build
This reference implementation is being tested with Adoptium 11 builds provided by 
[Adoptium](https://adoptium.net/).
This implementation does not support other JDK. If you encounter any issue, please make sure that you are using Java 11 from Adoptium.

You can simply install all modules by invoking `mvn clean install` in the root directory. You can then test the entry package in `servers/hello_sila`.

If you want to install without running the tests every time, simply use `mvn clean install "-Dskip.sila_java.tests=true"`.

## Components
Refer to the components inside for eventual additional READMEs. Note that the servers may be dependent on any components in library but not vice versa.

### library
The SiLA Library provides base classes for implementing SiLA Servers and SiLA Clients according to  Part A + B of the SiLA 2 Specification (including SiLA Discovery).

### Examples
Contains example servers.
For more information, you can check [examples](examples/README.md) 

### integration_test
Test library to test SiLA Library code against servers.

## License
This code is licensed under the [MIT License](https://en.wikipedia.org/wiki/MIT_License)

## Contributing
To get involved, read our [contributing docs](https://gitlab.com/SiLA2/sila_base/blob/master/CONTRIBUTING.md) in [sila_base](https://gitlab.com/SiLA2/sila_base).

Core members can check the release process in [Releasing](RELEASING.md).

### Style Guide
This reference implementation is maintained by UniteLabs, please refer to our guidelines for development,
[gitlab link](https://gitlab.com/unitelabs/unitelabs_guidelines/blob/master/languages/java/guideline.md).

### XML Schema
The schema is currently hosted on the [sila_base](https://gitlab.com/SiLA2/sila_base) repository. To have feature validation for example in [IntelliJ IDEA](https://www.jetbrains.com/idea/), you can assign the appropriate namespace in `IntelliJ IDEA -> Preferences... -> Schemas and DTD`.

### Maven Project Structure
In case you change the directory structure of the Maven projects, 
you also need to change the `pom.xml`, maven does not allow absolute
parent paths [link](https://stackoverflow.com/questions/36134651/maven-how-set-absolute-path-for-parent-pom).

## Acknowledgements
* [UniteLabs](https://unitelabs.ch/) who initially created and maintained sila_java
* [GitLab](https://gitlab.com/) for hosting this open source project
* [Spring](https://spring.io/) for providing an awesome open source framework
* [JetBrains](https://www.jetbrains.com/) who provides the best cross-platform IDE's
* [Adoptium](https://adoptium.net/) for providing pre build open jdk binaries
